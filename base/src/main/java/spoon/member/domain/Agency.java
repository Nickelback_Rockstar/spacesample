package spoon.member.domain;

import lombok.Data;

@Data
public class Agency {

    private String agency;

    private String agencyDepth;

    private String agency1;

    private String agency2;

    private String agency3;

    private String agency4;

    private String agency5;

    private String agency6;

    private String agency7;

}
