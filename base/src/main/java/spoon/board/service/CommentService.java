package spoon.board.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.board.domain.BoardDto;
import spoon.board.domain.CommentDto;
import spoon.board.entity.Comment;
import spoon.support.web.AjaxResult;

public interface CommentService {

    boolean adminAdd(CommentDto.AdminAdd add);

    boolean add(CommentDto.Add add);

    AjaxResult deleteAdmin(Long id, Long boardId);

    AjaxResult delete(Long id);

    AjaxResult hidden(CommentDto.Hidden hidden);

    Page<Comment> page(BoardDto.Command command, Pageable pageable);
}
