package spoon.game.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.service.BetClosingService;
import spoon.common.utils.ErrorUtils;
import spoon.event.entity.Logger;
import spoon.event.service.LoggerService;
import spoon.game.domain.GameDto;
import spoon.game.entity.Game;
import spoon.game.repository.GameRepository;
import spoon.mapper.GameMapper;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class GameServiceImpl implements GameService {

    private GameRepository gameRepository;

    private BetClosingService betClosingService;

    private GameMapper gameMapper;

    private LoggerService loggerService;

    @Transactional
    @Override
    public AjaxResult gameScore(GameDto.Result result) {
        Game game = gameRepository.findOne(result.getId());

        if (game == null) {
            return new AjaxResult(false, "게임이 존재하지 않습니다.");
        }

        try {
            // 이미 처리된 경기인지 확인
            if (game.isClosing()) {
                boolean notChange = game.getScoreHome().equals(result.getScoreHome())
                        && game.getScoreAway().equals(result.getScoreAway())
                        && game.isCancel() == result.isCancel();
                if (notChange) {
                    return new AjaxResult(false, "결과가 변경되지 않았습니다.");
                }

                betClosingService.rollbackBetting(game.getId());
            }

            // 취소경기일 경우 스코어를 넣지 않아도 되도록
            if (result.isCancel()) {
                result.setScoreHome(0);
                result.setScoreAway(0);
            }

            game.updateScore(result.getScoreHome(), result.getScoreAway(), result.isCancel());
            game.setClosing(true);
            gameRepository.saveAndFlush(game);
            betClosingService.closingGameBetting(result.getId(), result.getScoreHome(), result.getScoreAway(), result.isCancel());
        } catch (RuntimeException e) {
            log.error("현재 경기의 스코어를 기록하지 못하였습니다. ({}) - {}", e.getMessage(), game.toString());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, "현재 경기의 스코어를 기록하지 못하였습니다.");
        }
        return new AjaxResult(true, game.getTeamHome() + " VS " + game.getTeamAway() + " 결과처리를 완료 하였습니다.");
    }

    @Transactional
    @Override
    public AjaxResult gameReset(Long id) {
        Game game = gameRepository.findOne(id);

        if (game == null) {
            return new AjaxResult(false, "게임이 존재하지 않습니다.");
        }

        if (!game.isClosing()) {
            return new AjaxResult(false, "이미 정산 취소된 경기 입니다.");
        }

        try {
            betClosingService.rollbackBetting(game.getId());
            game.setScoreHome(null);
            game.setScoreAway(null);
            game.setClosing(false);
            game.setCancel(false);
            gameRepository.saveAndFlush(game);
        } catch (RuntimeException e) {
            log.error("현재 경기의 리셋하지 못하였습니다. ({}) - {}", e.getMessage(), game.toString());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, "현재 경기의 리셋하지 못하였습니다.");
        }

        return new AjaxResult(true, game.getTeamHome() + " VS " + game.getTeamAway() + " 결과처리를 리셋 하였습니다.");
    }

    @Transactional(readOnly = true)
    @Override
    public Game getGame(Long gameId) {
        return gameRepository.findOne(gameId);
    }

    @Transactional
    @Override
    public void addGame(Game game) {
        gameRepository.saveAndFlush(game);
    }

    @Override
    public void godBalanceSetting() {
        gameMapper.godBalanceSetting();
        Logger logger = new Logger();
        logger.setCode("GOD_SETTING");
        logger.setData("");
        logger.setRegDate(new Date());
        loggerService.addLog(logger);
    }
}
