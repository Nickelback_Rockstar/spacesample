package spoon.bet.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import spoon.bet.domain.BetReport;
import spoon.bet.entity.Bet;

public interface BetRepository extends JpaRepository<Bet, Long>, QueryDslPredicateExecutor<Bet> {

}
