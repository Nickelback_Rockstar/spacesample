package spoon.bot.zone.binanceCoin1;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.DateUtils;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.binanceCoin1.BinanceCoin1;
import spoon.gameZone.binanceCoin1.service.BinanceCoin1BotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BinanceCoin1Parsing implements GameBotParsing {

    private BinanceCoin1BotService binanceCoin1BotService;

    private static boolean isClosing = false;

    private static Date sdate = new Date();

    @Async
    @Override
    public void parsingGame() {
//        //System.out.println("바이낸스코인1 파싱");
        isClosing = false;

        int count = 0;
        int round = ZoneConfig.getBinanceCoin1().getZoneMaker().getRound();
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getBinanceCoin1().getZoneMaker().getGameDate());

        for (int i = 0; i < 6; i++) {
            if (cal.getTime().before(sdate)) continue;

            if (binanceCoin1BotService.notExist(cal.getTime())) {
                BinanceCoin1 binanceCoin1 = new BinanceCoin1(round > 1440 ? round % 1440 : round, cal.getTime());
                binanceCoin1.setOdds(ZoneConfig.getBinanceCoin1().getOdds());
                binanceCoin1BotService.addGame(binanceCoin1);
                count++;
            }
            round++;
            cal.add(Calendar.MINUTE, 1);
        }
        sdate = cal.getTime();
        log.debug("바이낸스코인1 경기등록 : {}건", count);
    }

    @Async
    @Override
    public void closingGame() {

        ////System.out.println("바이낸스 코인1 = " + isClosing);
        ////System.out.println("바이낸스 코인1 = " + DateUtils.format(new Date(),"yyyy.MM.dd HH:mm:ss"));

        if (isClosing) return;
        isClosing = true;

        Date gameDate = new Date(ZoneConfig.getBinanceCoin1().getZoneMaker().getGameDate().getTime() - 1 * 60 * 1000);
        //System.out.println("바이낸스 gameDate = " + gameDate);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getBinanceCoin1Url());
        //System.out.println("바이낸스 json = " + json);
        if (json == null) {
            isClosing = false;
            return;
        }

        BinanceCoin1 result = JsonUtils.toModel(json, BinanceCoin1.class);
        if (result == null) {
            isClosing = false;
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            isClosing = false;
            return;
        }

        isClosing = binanceCoin1BotService.closingGame(result);

        log.debug("바이낸스코인1 경기 종료 : {}회차", result.getRound());
    }

    @Async
    @Override
    public void checkResult() {
        binanceCoin1BotService.checkResult();
    }

    @Async
    @Override
    public void deleteGame() {
        binanceCoin1BotService.deleteGame(3);
    }
}
