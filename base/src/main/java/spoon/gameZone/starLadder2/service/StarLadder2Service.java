package spoon.gameZone.starLadder2.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.starLadder1.StarLadder1;
import spoon.gameZone.starLadder1.StarLadder1Config;
import spoon.gameZone.starLadder1.StarLadder1Dto;
import spoon.gameZone.starLadder2.StarLadder2;
import spoon.gameZone.starLadder2.StarLadder2Config;
import spoon.gameZone.starLadder2.StarLadder2Dto;
import spoon.support.web.AjaxResult;

public interface StarLadder2Service {

    /**
     * 별다리2분 설정을 변경한다.
     */
    boolean updateConfig(StarLadder2Config starLadder2Config);

    /**
     * 별다리2분 등록된 게임을 가져온다.
     */
    Iterable<StarLadder2> getComplete();

    /**
     * 별다리2분 종료된 게임을 가져온다.
     */
    Page<StarLadder2> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * 별다리2분 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    StarLadder2Dto.Score findScore(Long id);

    /**
     * 별다리2분 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(StarLadder2Dto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    StarLadder2Dto.Config gameConfig();

}
