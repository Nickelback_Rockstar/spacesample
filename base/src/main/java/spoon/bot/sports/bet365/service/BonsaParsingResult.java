package spoon.bot.sports.bet365.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.sports.bet365.domain.BotBet365;
import spoon.bot.sports.service.ParsingResult;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.WebUtils;
import spoon.config.domain.Config;
import spoon.game.domain.GameDto;
import spoon.game.entity.Game;
import spoon.game.service.GameBotService;
import spoon.game.service.GameService;
import spoon.monitor.service.MonitorService;

import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class BonsaParsingResult implements ParsingResult {

    private GameBotService gameBotService;

    private MonitorService monitorService;

    private GameService gameService;

    private static String ut;

    private static boolean action = false;

    @Override
    @Async
    public void closingGame() {
        if (action) return;

        action = true;
        int closed = 0;

        String json = HttpParsing.getJson(getClosingUrl());
        if (json == null) {
            action = false;
            return;
        }

        List<BotBet365> list = JsonUtils.toBet365List(json);
        if (list == null) {
            action = false;
            return;
        }

        for (BotBet365 bot : list) {
            if (ut == null || ut.compareTo(bot.getUt()) < 0) ut = bot.getUt();
            if (bot.getScoreHome() == null || bot.getScoreAway() == null) continue;

            Game game = gameBotService.getGame(bot.getSiteCode(), bot.getSiteId());
            //if (game == null || game.isClosing() || game.isCancel()) continue;
            if (game == null) continue;

            if (game.isClosing() || game.isCancel()){
                GameDto.Result result = new GameDto.Result();
                result.setCancel(game.isCancel());
                result.setId(game.getId());
                result.setScoreHome(game.getScoreHome());
                result.setScoreAway(game.getScoreAway());
                gameService.gameScore(result);
            }

            boolean success = gameBotService.gameScore(game.getId(), bot.getScoreHome(), bot.getScoreAway(), bot.isCancel());
            if (success) {
                closed++;
            }
        }

        monitorService.checkSports();

        log.debug("Bet365 게임 클로징 - 전체 : {}, 클로징 : {}", list.size(), closed);
        action = false;
    }

    private String getClosingUrl() {
        if (ut == null) ut = gameBotService.getMaxUt("bonsa", true);
        switch (Config.getSysConfig().getSports().getBet365()) {
            case "all":
                return Config.getSysConfig().getSports().getBet365Api() + "/api/bonsa/result" + (ut == null ? "" : "?ut=" + WebUtils.encoding(ut));
            default:
                throw new IllegalArgumentException("SysConfig > Sports > bet365Api 의 정보가 잘못되었습니다. (" + Config.getSysConfig().getSports().getBet365() + ")");
        }
    }
}
