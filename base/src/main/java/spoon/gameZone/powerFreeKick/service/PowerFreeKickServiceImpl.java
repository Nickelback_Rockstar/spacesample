package spoon.gameZone.powerFreeKick.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.powerFreeKick.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class PowerFreeKickServiceImpl implements PowerFreeKickService {

    private ConfigService configService;

    private MemberService memberService;

    private PowerFreeKickGameService powerFreeKickGameService;

    private PowerFreeKickBotService powerFreeKickBotService;

    private PowerFreeKickRepository powerFreeKickRepository;

    private BetItemRepository betItemRepository;

    private static QPowerFreeKick q = QPowerFreeKick.powerFreeKick;

    @Transactional
    @Override
    public boolean updateConfig(PowerFreeKickConfig powerFreeKickConfig) {
        try {
            configService.updateZoneConfig("powerFreeKick", JsonUtils.toString(powerFreeKickConfig));
            ZoneConfig.setPowerFreeKick(powerFreeKickConfig);
            // 이미 등록된 게임의 배당을 변경한다.
            powerFreeKickRepository.updateOdds(ZoneConfig.getPowerFreeKick().getOdds());
        } catch (RuntimeException e) {
            log.error("파워프리킥 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<PowerFreeKick> getComplete() {
        return powerFreeKickRepository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<PowerFreeKick> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return powerFreeKickRepository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public PowerFreeKickDto.Score findScore(Long id) {
        PowerFreeKick powerFreeKick = powerFreeKickRepository.findOne(id);

        PowerFreeKickDto.Score score = new PowerFreeKickDto.Score();
        score.setId(powerFreeKick.getId());
        score.setRound(powerFreeKick.getRound());
        score.setTimes(powerFreeKick.getTimes());
        score.setGameDate(powerFreeKick.getGameDate());
        score.setLastBall(powerFreeKick.getLastBall());
        score.setCancel(powerFreeKick.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getPowerFreeKickUrl() + "?sdate=" + powerFreeKick.getSdate());
        if (json == null) return score;

        powerFreeKick = JsonUtils.toModel(json, PowerFreeKick.class);
        if (powerFreeKick == null) return score;

        // 봇에 결과가 있다면
        if (powerFreeKick.isClosing()) {
            score.setLastBall(powerFreeKick.getLastBall());
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(PowerFreeKickDto.Score score) {
        PowerFreeKick powerFreeKick = powerFreeKickRepository.findOne(score.getId());

        try {
            if (powerFreeKick.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                powerFreeKickGameService.rollbackPayment(powerFreeKick);
            }

            // 스코어 입력
            powerFreeKick.updateScore(score);
            powerFreeKickRepository.saveAndFlush(powerFreeKick);
            powerFreeKickGameService.closingBetting(powerFreeKick);
            powerFreeKickBotService.checkResult();
        } catch (RuntimeException e) {
            log.error("파워프리킥 {} - {}회차 수동처리를 하지 못하였습니다. - {}", powerFreeKick.getGameDate(), powerFreeKick.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<PowerFreeKick> iterable = powerFreeKickRepository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(5))));
        for (PowerFreeKick powerFreeKick : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.POWERFREEKICK).and(qi.groupId.eq(powerFreeKick.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getPowerFreeKickUrl() + "?sdate=" + powerFreeKick.getSdate());
            if (json == null) continue;

            PowerFreeKick result = JsonUtils.toModel(json, PowerFreeKick.class);
            if (result == null) continue;

            if (result.isClosing()) {
                powerFreeKick.setLastBall(result.getLastBall());
                powerFreeKick.setPlayer(result.getPlayer());
                powerFreeKick.setDirection(result.getDirection());
                powerFreeKick.setGoal(result.getGoal());
                powerFreeKick.setClosing(true);
                powerFreeKickRepository.saveAndFlush(powerFreeKick);
                closing++;
            }
        }
        powerFreeKickBotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public PowerFreeKickDto.Config gameConfig() {
        PowerFreeKickDto.Config gameConfig = new PowerFreeKickDto.Config();
        PowerFreeKickConfig config = ZoneConfig.getPowerFreeKick();

        // 동행 현재 회차에서 1을 더해준다.
        int times = ZoneConfig.getPowerFreeKick().getPowerFreeKickMaker().getTimes() + 1;
        Date gameDate = ZoneConfig.getPowerFreeKick().getPowerFreeKickMaker().getGameDate(times);
        PowerFreeKick powerFreeKick = powerFreeKickRepository.findOne(q.gameDate.eq(gameDate));

        if (powerFreeKick == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setTimes(config.getPowerFreeKickMaker().getTimes());
            gameConfig.setRound(config.getPowerFreeKickMaker().getRound(times));
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isPowerFreeKick());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(powerFreeKick.getGameDate());
        gameConfig.setSdate(powerFreeKick.getSdate());
        gameConfig.setRound(powerFreeKick.getRound());
        gameConfig.setTimes(powerFreeKick.getTimes());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(powerFreeKick.getOdds());

        int betTime = (int) (powerFreeKick.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setPlayer(config.isPlayer());
        gameConfig.setDirection(config.isDirection());
        gameConfig.setGoal(config.isGoal());

        return gameConfig;
    }
}
