package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.starLadder3.StarLadder3Config;
import spoon.gameZone.starLadder3.StarLadder3Dto;
import spoon.gameZone.starLadder3.service.StarLadder3Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.starLadder3Controller")
@RequestMapping(value = "#{config.pathAdmin}")
public class StarLadder3Controller {

    private StarLadder3Service starLadder3Service;

    /**
     * 별다리3분 설정
     */
    @RequestMapping(value = "zone/starLadder3/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getStarLadder3());
        return "admin/zone/starLadder3/config";
    }

    /**
     * 별다리3분 설정 변경
     */
    @RequestMapping(value = "zone/starLadder3/config", method = RequestMethod.POST)
    public String config(StarLadder3Config starLadder3Config, RedirectAttributes ra) {
        boolean success = starLadder3Service.updateConfig(starLadder3Config);
        if (success) {
            ra.addFlashAttribute("message", "별다리3분게임 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "별다리3분게임 설정변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/starLadder3/config";
    }

    /**
     * 별다리3분 진행
     */
    @RequestMapping(value = "zone/starLadder3/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", starLadder3Service.getComplete());
        map.addAttribute("config", ZoneConfig.getLadder());
        return "admin/zone/starLadder3/complete";
    }

    /**
     * 별다리3분 완료
     */
    @RequestMapping(value = "zone/starLadder3/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", starLadder3Service.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getStarLadder3());
        return "admin/zone/starLadder3/closing";
    }

    /**
     * 별다리3분 스코어 입력 폼
     */
    @RequestMapping(value = "zone/starLadder3/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", starLadder3Service.findScore(id));
        return "admin/zone/starLadder3/score";
    }

    /**
     * 별다리3분 스코어 결과처리
     */
    @RequestMapping(value = "zone/starLadder3/score", method = RequestMethod.POST)
    public String score(StarLadder3Dto.Score score, RedirectAttributes ra) {
        boolean success = starLadder3Service.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "별다리3분 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "별다리3분 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/starLadder3/score?id=" + score.getId();
    }

    /**
     * 별다리3분 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/starLadder3/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return starLadder3Service.closingAllGame();
    }

}
