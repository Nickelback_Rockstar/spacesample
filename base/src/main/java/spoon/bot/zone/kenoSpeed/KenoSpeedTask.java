package spoon.bot.zone.kenoSpeed;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class KenoSpeedTask {

    private GameBotParsing kenoSpeedParsing;

    @Scheduled(cron = "2 0/5 * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        kenoSpeedParsing.parsingGame();
    }

    @Scheduled(cron = "47,52 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        kenoSpeedParsing.closingGame();
    }

    @Scheduled(cron = "3/9 1/5 * * * *")
    public void checkResult() {
        if (notParsing()) return;
        kenoSpeedParsing.checkResult();
    }

    @Scheduled(cron = "1 1 4 * * * ")
    public void deleteGame() {
        kenoSpeedParsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isKenoSpeed() || !ZoneConfig.getKenoSpeed().isEnabled();
    }

}
