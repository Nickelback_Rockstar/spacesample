package spoon.bot.zone.eosPower2;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class EosPower2Task {

    private GameBotParsing eosPower2Parsing;

    @Scheduled(cron = "0 0/2 * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        eosPower2Parsing.parsingGame();
    }

    @Scheduled(cron = "10 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        eosPower2Parsing.closingGame();
    }

    @Scheduled(cron = "5,8 2/2 * * * *")
    public void checkResult() {
        if (notParsing()) return;
        eosPower2Parsing.checkResult();
    }

    @Scheduled(cron = "7 1 4 * * * ")
    public void deleteGame() {
        eosPower2Parsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isEosPower2() || !ZoneConfig.getEosPower2().isEnabled();
    }

}
