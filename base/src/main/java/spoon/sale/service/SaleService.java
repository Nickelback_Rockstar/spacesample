package spoon.sale.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.sale.domain.SaleDto;
import spoon.sale.entity.PowerLottoSale;
import spoon.sale.entity.PowerLottoSaleItem;
import spoon.sale.entity.Sale;
import spoon.sale.entity.SaleItem;
import spoon.support.web.AjaxResult;

import java.util.List;

public interface SaleService {

    List<SaleItem> currentSale(SaleDto.Command command);

    AjaxResult balanceSale(String userid);

    Page<Sale> getPage(SaleDto.Command command, Pageable pageable);
    Page<PowerLottoSale> getPagePbLotto(SaleDto.Command command, Pageable pageable);

    Page<SaleItem> getPageItem(SaleDto.Command command, Pageable pageable);

    AjaxResult payment(SaleDto.Payment payment);

    List<SaleItem> currentSaleNew(SaleDto.Command command);
    List<SaleItem> currentSaleNew2(SaleDto.Command command);
    List<SaleItem> currentSaleReNew(SaleDto.Command command);

    List<SaleItem> currentListSale(SaleDto.Command command);
    List<SaleItem> currentListSale2(SaleDto.Command command);

    AjaxResult balanceSaleNew(String userid);

    List<PowerLottoSaleItem> currentSaleNewPbLotto(SaleDto.Command command);

    AjaxResult balanceSalePbLotto(String userid);

    AjaxResult paymentPbLotto(SaleDto.Payment payment);

    AjaxResult balanceSaleNewPbLotto(String userid);


}
