package spoon.gameZone.starLadder1.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.starLadder1.StarLadder1;
import spoon.support.web.AjaxResult;

public interface StarLadder1GameService {

    /**
     * 별다리1분 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 별다리1분 게임 베팅을 클로징 한다.
     */
    void closingBetting(StarLadder1 starLadder1);

    /**
     * 별다리1분 게임을 롤백 한다.
     */
    void rollbackPayment(StarLadder1 starLadder1);
}
