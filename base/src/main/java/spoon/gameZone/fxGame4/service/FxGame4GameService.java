package spoon.gameZone.fxGame4.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame4.FxGame4;
import spoon.support.web.AjaxResult;

public interface FxGame4GameService {

    /**
     * fxGame4 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * fxGame4 게임 베팅을 클로징 한다.
     */
    void closingBetting(FxGame4 ladder);

    /**
     * fxGame4 게임을 롤백 한다.
     */
    void rollbackPayment(FxGame4 ladder);
}
