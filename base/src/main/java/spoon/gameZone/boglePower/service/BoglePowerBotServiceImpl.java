package spoon.gameZone.boglePower.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.boglePower.BoglePower;
import spoon.gameZone.boglePower.BoglePowerRepository;
import spoon.gameZone.boglePower.QBoglePower;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BoglePowerBotServiceImpl implements BoglePowerBotService {

    private BoglePowerGameService boglePowerGameService;

    private BoglePowerRepository boglePowerRepository;

    private static QBoglePower q = QBoglePower.boglePower;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return boglePowerRepository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(BoglePower boglePower) {
        boglePowerRepository.saveAndFlush(boglePower);
    }

    @Transactional
    @Override
    public boolean closingGame(BoglePower result) {
        BoglePower boglePower = boglePowerRepository.findOne(q.sdate.eq(result.getSdate()));
        if (boglePower == null) {
            return true;
        }

        try {
            boglePower.setOddeven(result.getOddeven());
            boglePower.setOverunder(result.getOverunder());
            boglePower.setPb_oddeven(result.getPb_oddeven());
            boglePower.setPb_overunder(result.getPb_overunder());

            boglePower.setPb(result.getPb());
            boglePower.setBall(result.getBall());
            boglePower.setSize(result.getSize());
            boglePower.setSum(result.getSum());

            boglePower.setClosing(true);

            boglePowerRepository.saveAndFlush(boglePower);
            boglePowerGameService.closingBetting(boglePower);
        } catch (RuntimeException e) {
            log.error("보글파워볼 {}회차 결과 업데이트에 실패하였습니다. - {}", boglePower.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = boglePowerRepository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getBoglePower().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        boglePowerRepository.deleteGame(DateUtils.beforeDays(days));
    }
}
