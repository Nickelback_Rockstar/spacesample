package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.dragonTiger.DragonTigerConfig;
import spoon.gameZone.dragonTiger.DragonTigerDto;
import spoon.gameZone.dragonTiger.service.DragonTigerService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.dragonTigerController")
@RequestMapping(value = "#{config.pathAdmin}")
public class DragonTigerController {

    private DragonTigerService dragonTigerService;


    /**
     * 드래곤타이거 설정
     */
    @RequestMapping(value = "zone/dragonTiger/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getDragonTiger());
        return "admin/zone/dragonTiger/config";
    }

    /**
     * 드래곤타이거 설정 변경
     */
    @RequestMapping(value = "zone/dragonTiger/config", method = RequestMethod.POST)
    public String config(DragonTigerConfig dragonTigerConfig, RedirectAttributes ra) {
        boolean success = dragonTigerService.updateConfig(dragonTigerConfig);
        if (success) {
            ra.addFlashAttribute("message", "드래곤타이거 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "드래곤타이거 설정변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/dragonTiger/config";
    }

    /**
     * 드래곤타이거 진행
     */
    @RequestMapping(value = "zone/dragonTiger/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", dragonTigerService.getComplete());
        map.addAttribute("config", ZoneConfig.getDragonTiger());
        return "admin/zone/dragonTiger/complete";
    }

    /**
     * 드래곤타이거 완료
     */
    @RequestMapping(value = "zone/dragonTiger/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "gameDate") Pageable pageable) {
        map.addAttribute("page", dragonTigerService.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getDragonTiger());
        return "admin/zone/dragonTiger/closing";
    }

    /**
     * 드래곤타이거 스코어 입력 폼
     */
    @RequestMapping(value = "zone/dragonTiger/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", dragonTigerService.findScore(id));
        return "admin/zone/dragonTiger/score";
    }

    /**
     * 드래곤타이거 결과처리
     */
    @RequestMapping(value = "zone/dragonTiger/score", method = RequestMethod.POST)
    public String score(DragonTigerDto.Score score, RedirectAttributes ra) {
        //수동 결과처리
        int dragon = Integer.parseInt(score.getDragon());
        int tiger = Integer.parseInt(score.getTiger());
        score.setSum(dragon + tiger);

        if(dragon > tiger){
            score.setResult("DRAGON");
        }else if(dragon < tiger) {
            score.setResult("TIGER");
        }else{
            score.setResult("TIE");
        }

        boolean success = dragonTigerService.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "드래곤타이거 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "드래곤타이거 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/dragonTiger/score?id=" + score.getId();
    }

    /**
     * 드래곤타이거 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/dragonTiger/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return dragonTigerService.closingAllGame();
    }

}
