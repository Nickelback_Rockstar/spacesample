package spoon.gameZone.binanceCoin3.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.binanceCoin3.BinanceCoin3;
import spoon.gameZone.binanceCoin3.BinanceCoin3Repository;
import spoon.gameZone.binanceCoin3.QBinanceCoin3;
import spoon.gameZone.binanceCoin3.BinanceCoin3Repository;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BinanceCoin3BotServiceImpl implements BinanceCoin3BotService {

    private BinanceCoin3GameService binanceCoin3GameService;

    private BinanceCoin3Repository binanceCoin3Repository;

    private static QBinanceCoin3 q = QBinanceCoin3.binanceCoin3;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return binanceCoin3Repository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(BinanceCoin3 binanceCoin3) {
        binanceCoin3Repository.saveAndFlush(binanceCoin3);
    }

    @Transactional
    @Override
    public boolean closingGame(BinanceCoin3 result) {
        BinanceCoin3 binanceCoin3 = binanceCoin3Repository.findOne(q.sdate.eq(result.getSdate()));
        if (binanceCoin3 == null) {
            return true;
        }

        try {
//            binanceCoin3.setLowHigh(result.getLowHigh());
            binanceCoin3.setOverUnder(result.getOverUnder());
            binanceCoin3.setOddEven(result.getOddEven());
            binanceCoin3.setClosing(true);

            binanceCoin3Repository.saveAndFlush(binanceCoin3);
            binanceCoin3GameService.closingBetting(binanceCoin3);
        } catch (RuntimeException e) {
            log.error("바이낸스코인3분 {}회차 결과 업데이트에 실패하였습니다. - {}", binanceCoin3.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = binanceCoin3Repository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getBinanceCoin3().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        binanceCoin3Repository.deleteGame(DateUtils.beforeDays(days));
    }
}
