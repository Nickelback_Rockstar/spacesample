package spoon.sale.domain;

import lombok.Data;

public class SaleDto {

    @Data
    public static class Command {
        private int agencyDepth;
        private String agency;
        private String agency1;
        private String agency2;
        private String agency3;
        private String agency4;
        private String agency5;
        private String agency6;
        private String agency7;
        private double rateCasino;
    }

    @Data
    public static class Payment {
        private String userid;
        private Long saleId;
        private long amount;
    }

}
