package spoon.gameZone.dragonTiger.service;

import spoon.gameZone.dragonTiger.DragonTiger;

import java.util.Date;

public interface DragonTigerBotService {

    boolean notExist(Date gameDate);

    void addGame(DragonTiger dari);

    boolean closingGame(DragonTiger result);

    void checkResult();

    void deleteGame(int days);

}
