package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.common.utils.JsonUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.powerFreeKick.PowerFreeKickDto;
import spoon.gameZone.powerFreeKick.service.PowerFreeKickGameService;
import spoon.gameZone.powerFreeKick.service.PowerFreeKickService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class PowerFreeKickController {

    private PowerFreeKickService powerFreeKickService;

    private PowerFreeKickGameService powerFreeKickGameService;

    @RequestMapping(value = "zone/powerFreeKick", method = RequestMethod.GET)
    public String zone(ModelMap map) {
        map.addAttribute("config", JsonUtils.toString(powerFreeKickService.gameConfig()));
        return "site/zone/powerFreeKick";
    }

    @RequestMapping(value = "zone/powerFreeKick/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", powerFreeKickService.getClosing(command, pageable));
        return "site/score/powerFreeKick";
    }

    @ResponseBody
    @RequestMapping(value = "zone/powerFreeKick/config", method = RequestMethod.POST)
    public PowerFreeKickDto.Config config() {
        return powerFreeKickService.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/powerFreeKick/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return powerFreeKickGameService.betting(bet);
    }
}
