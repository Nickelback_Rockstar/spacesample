package spoon.gameZone.newSnail;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import spoon.bot.support.ZoneHelper;
import spoon.common.utils.DateUtils;
import spoon.game.domain.MenuCode;
import spoon.gameZone.GameZoneException;
import spoon.gameZone.Zone;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneScore;
import spoon.support.convert.DoubleArrayConvert;
import spoon.support.convert.LongArrayConvert;

import javax.persistence.*;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "ZONE_NEWSNAIL", indexes = {
        @Index(name = "IDX_sdate", columnList = "sdate")
})
public class NewSnail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date gameDate;

    private int round;

    @Column(length = 16)
    private String sdate;

    @Column(length = 16)
    private String rank1;

    @Column(length = 16)
    private String rank2;

    @Column(length = 16)
    private String rank3;

    @Column(length = 16)
    private String rank4;

    @Column(length = 16)
    private String hurdles1;

    @Column(length = 16)
    private String hurdles2;

    @Column(length = 16)
    private String hurdles3;

    @Column(length = 16)
    private String hurdles4;

    @Column(length = 16)
    @JsonProperty("oddEven")
    private String oddeven;

    @Column(length = 16)
    @JsonProperty("overUnder")
    private String overunder;

    @Convert(converter = LongArrayConvert.class)
    private long[] amount = new long[27];

    @Convert(converter = DoubleArrayConvert.class)
    private double[] odds = new double[27];

    private boolean closing;

    private boolean cancel;

    //---------------------------------------------------------

    public NewSnail(int round, Date gameDate) {
        this.round = round;
        this.gameDate = gameDate;
        this.sdate = DateUtils.format(gameDate, "yyyyMMddHHmm");
    }

    public boolean isBeforeGameDate() {
        return this.gameDate.getTime() - System.currentTimeMillis() > 0;
    }

    public Zone getZone(String gameCode) {
        switch (gameCode) {

            case "oddeven":
                return getZone("oddeven", "홀/짝", "홀", "짝", 1, 2);
            case "overunder":
                return getZone("overunder", "오버언더", "오버", "언더", 3, 4);

            case "snail1":
                return getZone("snail1", "", "달팽이1", "X", 5, 0);
            case "snail2":
                return getZone("snail2", "", "달팽이2", "X", 6, 0);
            case "snail3":
                return getZone("snail3", "", "달팽이3", "X", 7, 0);
            case "snail4":
                return getZone("snail4", "", "달팽이4", "X", 8, 0);

            case "quinella12":
                return getZone("quinella12", "복승식", "달팽이1,2", "X", 9, 0);
            case "quinella13":
                return getZone("quinella13", "복승식", "달팽이1,3", "X", 10, 0);
            case "quinella14":
                return getZone("quinella14", "복승식", "달팽이1,4", "X", 11, 0);
            case "quinella23":
                return getZone("quinella23", "복승식", "달팽이2,3", "X", 12, 0);
            case "quinella24":
                return getZone("quinella24", "복승식", "달팽이2,4", "X", 13, 0);
            case "quinella34":
                return getZone("quinella34", "복승식", "달팽이3,4", "X", 14, 0);

            case "exacta12":
                return getZone("exacta12", "쌍승식", "달팽이1,2", "X", 15, 0);
            case "exacta13":
                return getZone("exacta13", "쌍승식", "달팽이1,3", "X", 16, 0);
            case "exacta14":
                return getZone("exacta14", "쌍승식", "달팽이1,4", "X", 17, 0);
            case "exacta21":
                return getZone("exacta21", "쌍승식", "달팽이2,1", "X", 18, 0);
            case "exacta23":
                return getZone("exacta23", "쌍승식", "달팽이2,3", "X", 19, 0);
            case "exacta24":
                return getZone("exacta24", "쌍승식", "달팽이2,4", "X", 20, 0);
            case "exacta31":
                return getZone("exacta31", "쌍승식", "달팽이3,1", "X", 21, 0);
            case "exacta32":
                return getZone("exacta32", "쌍승식", "달팽이3,2", "X", 22, 0);
            case "exacta34":
                return getZone("exacta34", "쌍승식", "달팽이3,4", "X", 23, 0);
            case "exacta41":
                return getZone("exacta41", "쌍승식", "달팽이4,1", "X", 24, 0);
            case "exacta42":
                return getZone("exacta42", "쌍승식", "달팽이4,2", "X", 25, 0);
            case "exacta43":
                return getZone("exacta43", "쌍승식", "달팽이4,3", "X", 26, 0);

            default:
                throw new GameZoneException("NEW달팽이 코드 " + gameCode + " 를 확인 할 수 없습니다.");

        }
    }

    private Zone getZone(String gameCode, String type, String teamHome, String teamAway, int idxHome, int idxAway) {
        Zone zone = new Zone();
        zone.setId(this.getId());
        zone.setSdate(this.getSdate());
        zone.setMenuCode(MenuCode.NEWSNAIL);
        zone.setGameCode(gameCode);
        zone.setLeague(String.format("%03d회차 N달팽이", this.round));
        zone.setTeamHome(String.format("%03d회차 N달팽이 %s [%s]", this.round, type, teamHome));
        zone.setTeamAway(String.format("%03d회차 N달팽이 %s [%s]", this.round, type, teamAway));
        zone.setHandicap(0);
        zone.setOddsHome(ZoneConfig.getNewSnail().getOdds()[idxHome]);
        zone.setOddsDraw(0);
        zone.setOddsAway(ZoneConfig.getNewSnail().getOdds()[idxAway]);
        zone.setGameDate(this.gameDate);
        return zone;
    }

    public ZoneScore getGameResult(String gameCode) {
        if(gameCode.startsWith("oddeven")) {
            return ZoneHelper.zoneResult(this.oddeven ,this.cancel);
        }else if(gameCode.startsWith("overunder")){
            return ZoneHelper.zoneResult(this.overunder ,this.cancel);
        }else if(gameCode.startsWith("snail")){
            return ZoneHelper.zoneNewSnailRank(gameCode, this.rank1, this.cancel);
        }else if(gameCode.startsWith("quinella")){
            return ZoneHelper.zoneNewSnailQuinella(gameCode, this.rank1, this.rank2, this.cancel);
        }else if(gameCode.startsWith("exacta")){
            return ZoneHelper.zoneNewSnailExacta(gameCode, this.rank1, this.rank2, this.cancel);
        }else{
            throw new GameZoneException("NEW달팽이 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }


    public boolean isChangeResult(NewSnailDto.Score score) {
        return !this.closing || this.cancel != score.isCancel() || !score.getRank1().equals(this.rank1);
    }

    public void updateScore(NewSnailDto.Score score) {
        if (score.isCancel()) {
            this.rank1 = "";
            this.rank2 = "";
            this.rank3 = "";
            this.rank4 = "";
            this.hurdles1 = "";
            this.hurdles2 = "";
            this.hurdles3 = "";
            this.hurdles4 = "";
            this.oddeven = "";
            this.overunder = "";
            this.cancel = true;
        } else {
            this.rank1 = score.getRank1();
            this.rank2 = score.getRank2();
            this.rank3 = score.getRank3();
            this.rank4 = score.getRank4();

            //허들 일단 안씀.
            this.hurdles1 = "";
            this.hurdles2 = "";
            this.hurdles3 = "";
            this.hurdles4 = "";

            if("snail1".equals(this.rank1) || "snail3".equals(this.rank1)){
                this.oddeven = "odd";
            }else if("snail2".equals(this.rank1) || "snail4".equals(this.rank1)){
                this.oddeven = "even";
            }

            if("snail1".equals(this.rank1) || "snail2".equals(this.rank1)){
                this.overunder = "under";
            }else if("snail3".equals(this.rank1) || "snail4".equals(this.rank1)){
                this.overunder = "over";
            }

            this.cancel = false;
        }
        this.closing = true;
    }
}
