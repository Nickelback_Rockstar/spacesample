package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.common.utils.JsonUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.binanceCoin1.BinanceCoin1Dto;
import spoon.gameZone.binanceCoin1.service.BinanceCoin1GameService;
import spoon.gameZone.binanceCoin1.service.BinanceCoin1Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class BinanceCoin1Controller {

    private BinanceCoin1Service binanceCoin1Service;

    private BinanceCoin1GameService binanceCoin1GameService;

    @RequestMapping(value = "zone/binanceCoin1", method = RequestMethod.GET)
    public String zone(ModelMap map) {
        map.addAttribute("config", JsonUtils.toString(binanceCoin1Service.gameConfig()));
        return "site/zone/binanceCoin1";
    }

    @RequestMapping(value = "zone/binanceCoin1/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", binanceCoin1Service.getClosing(command, pageable));
        return "site/score/binanceCoin1";
    }

    @ResponseBody
    @RequestMapping(value = "zone/binanceCoin1/config", method = RequestMethod.POST)
    public BinanceCoin1Dto.Config config() {
        return binanceCoin1Service.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/binanceCoin1/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return binanceCoin1GameService.betting(bet);
    }
}
