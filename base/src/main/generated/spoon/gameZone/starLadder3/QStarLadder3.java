package spoon.gameZone.starLadder3;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QStarLadder3 is a Querydsl query type for StarLadder3
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QStarLadder3 extends EntityPathBase<StarLadder3> {

    private static final long serialVersionUID = 1358806973L;

    public static final QStarLadder3 starLadder3 = new QStarLadder3("starLadder3");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath line = createString("line");

    public final StringPath oddeven = createString("oddeven");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public final StringPath start = createString("start");

    public QStarLadder3(String variable) {
        super(StarLadder3.class, forVariable(variable));
    }

    public QStarLadder3(Path<? extends StarLadder3> path) {
        super(path.getType(), path.getMetadata());
    }

    public QStarLadder3(PathMetadata metadata) {
        super(StarLadder3.class, metadata);
    }

}

