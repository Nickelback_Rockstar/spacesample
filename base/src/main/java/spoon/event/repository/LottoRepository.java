package spoon.event.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import spoon.event.entity.Lotto;

public interface LottoRepository extends JpaRepository<Lotto, Long>, QueryDslPredicateExecutor<Lotto> {
}
