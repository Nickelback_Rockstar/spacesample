package spoon.mapper;

import org.apache.ibatis.annotations.Param;
import spoon.banking.domain.Rolling;

import java.util.Date;

public interface BankingMapper {

    Rolling getRolling(String userid);

    String getOutBanking(String userid);

    void deleteBeforeRegDate(@Param("regDate") Date regDate);

    Long getTodayAmount(@Param("userid") String userid, @Param("start") String start, @Param("end") String end);

    Long getTodayLotto(@Param("userid") String userid, @Param("start") String start, @Param("end") String end);

    Long getTodayWithdraw(@Param("userid") String userid, @Param("start") String start, @Param("end") String end);
}
