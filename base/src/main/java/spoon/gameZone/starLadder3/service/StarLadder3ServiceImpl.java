package spoon.gameZone.starLadder3.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.starLadder3.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class StarLadder3ServiceImpl implements StarLadder3Service {

    private ConfigService configService;

    private MemberService memberService;

    private StarLadder3GameService starLadder3GameService;

    private StarLadder3BotService starLadder3BotService;

    private StarLadder3Repository starLadder3Repository;

    private BetItemRepository betItemRepository;

    private static QStarLadder3 q = QStarLadder3.starLadder3;

    @Transactional
    @Override
    public boolean updateConfig(StarLadder3Config starLadder3Config) {
        try {
            configService.updateZoneConfig("starladder3", JsonUtils.toString(starLadder3Config));
            ZoneConfig.setStarLadder3(starLadder3Config);
            // 이미 등록된 게임의 배당을 변경한다.
            starLadder3Repository.updateOdds(ZoneConfig.getStarLadder3().getOdds());
        } catch (RuntimeException e) {
            log.error("별다리3분 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<StarLadder3> getComplete() {
        return starLadder3Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<StarLadder3> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return starLadder3Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public StarLadder3Dto.Score findScore(Long id) {
        StarLadder3 starLadder3 = starLadder3Repository.findOne(id);

        StarLadder3Dto.Score score = new StarLadder3Dto.Score();
        score.setId(starLadder3.getId());
        score.setRound(starLadder3.getRound());
        score.setGameDate(starLadder3.getGameDate());
        score.setStart(starLadder3.getStart());
        score.setLine(starLadder3.getLine());
        score.setOddeven(starLadder3.getOddeven());
        score.setCancel(starLadder3.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getStarLadder3Url() + "?sdate=" + starLadder3.getSdate());
        if (json == null) return score;

        starLadder3 = JsonUtils.toModel(json, StarLadder3.class);
        if (starLadder3 == null) return score;

        // 봇에 결과가 있다면
        if (starLadder3.isClosing()) {
            score.setOddeven(starLadder3.getOddeven());
            score.setLine(starLadder3.getLine());
            score.setStart(starLadder3.getStart());

            if (!"ODD".equals(score.getOddeven()) && !"EVEN".equals(score.getOddeven())) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(StarLadder3Dto.Score score) {
        StarLadder3 starLadder3 = starLadder3Repository.findOne(score.getId());

        try {
            if (starLadder3.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                starLadder3GameService.rollbackPayment(starLadder3);
            }

            // 스코어 입력
            starLadder3.updateScore(score);
            starLadder3Repository.saveAndFlush(starLadder3);
            starLadder3GameService.closingBetting(starLadder3);
            starLadder3BotService.checkResult();
        } catch (RuntimeException e) {
            log.error("별다리3분 {} - {}회차 수동처리를 하지 못하였습니다. - {}", starLadder3.getGameDate(), starLadder3.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<StarLadder3> iterable = starLadder3Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(5))));
        for (StarLadder3 starLadder3 : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.STARLADDER3).and(qi.groupId.eq(starLadder3.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getStarLadder3Url() + "?sdate=" + starLadder3.getSdate());
            if (json == null) continue;

            StarLadder3 result = JsonUtils.toModel(json, StarLadder3.class);
            if (result == null) continue;

            if (result.isClosing()) {
                starLadder3.setStart(result.getStart());
                starLadder3.setLine(result.getLine());
                starLadder3.setOddeven(result.getOddeven());
                starLadder3.setClosing(true);
                starLadder3Repository.saveAndFlush(starLadder3);
                closing++;
            }
        }
        starLadder3BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public StarLadder3Dto.Config gameConfig() {
        StarLadder3Dto.Config gameConfig = new StarLadder3Dto.Config();
        StarLadder3Config config = ZoneConfig.getStarLadder3();

        Date gameDate = config.getZoneMaker().getGameDate();
        StarLadder3 starLadder3 = starLadder3Repository.findOne(q.gameDate.eq(gameDate));

        if (starLadder3 == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isStarLadder3());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(30000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(starLadder3.getGameDate());
        gameConfig.setSdate(starLadder3.getSdate());
        gameConfig.setRound(starLadder3.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(starLadder3.getOdds());

        int betTime = (int) (starLadder3.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 3000) / 3000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setStart(config.isStart());
        gameConfig.setLine(config.isLine());
        gameConfig.setLineStart(config.isLineStart());

        return gameConfig;
    }
}
