package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.StringUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame1.FxGame1Dto;
import spoon.gameZone.fxGame1.service.FxGame1GameService;
import spoon.gameZone.fxGame1.service.FxGame1Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class FxGame1Controller {

    private FxGame1Service fxGame1Service;

    private FxGame1GameService fxGame1GameService;

    @RequestMapping(value = "zone/fxGame1", method = RequestMethod.GET)
    public String zone(ModelMap map, @RequestParam(value = "round", required = false) String round) {
        FxGame1Dto.Config config = fxGame1Service.gameConfig(getNextRound(round));
        map.addAttribute("config", JsonUtils.toString(config));
        return "site/zone/fxGame1";
    }

    @ResponseBody
    @RequestMapping(value = "zone/fxGame1/config", method = RequestMethod.POST)
    public FxGame1Dto.Config config(@RequestParam(value = "round", required = false) String round) {
        return fxGame1Service.gameConfig(getNextRound(round));
    }

    public int getNextRound(String round){
        int nextRound = 2;
        try {
            if (StringUtils.notEmpty(round)) {
                nextRound = Integer.parseInt(round);
                if (nextRound != 2 && nextRound != 3) {
                    nextRound = 2;
                }
            }
        }catch (Exception e){
        }finally {
            return nextRound;
        }
    }

    @RequestMapping(value = "zone/fxGame1/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", fxGame1Service.getClosing(command, pageable));
        return "site/score/fxGame1";
    }

    @ResponseBody
    @RequestMapping(value = "zone/fxGame1/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return fxGame1GameService.betting(bet);
    }

}
