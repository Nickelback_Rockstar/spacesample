package spoon.gameZone.roulette.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.roulette.Roulette;
import spoon.support.web.AjaxResult;

public interface RouletteGameService {

    /**
     * 룰렛 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 룰렛 게임 베팅을 클로징 한다.
     */
    void closingBetting(Roulette roulette);

    /**
     * 룰렛 게임을 롤백 한다.
     */
    void rollbackPayment(Roulette roulette);
}
