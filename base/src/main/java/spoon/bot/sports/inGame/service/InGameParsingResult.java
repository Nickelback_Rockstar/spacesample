package spoon.bot.sports.inGame.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bet.entity.BetItem;
import spoon.bet.service.BetListService;
import spoon.bet.service.BetService;
import spoon.bot.sports.service.ParsingResult;
import spoon.common.net.HttpParsing;
import spoon.config.domain.Config;
import spoon.game.domain.StatusCode;
import spoon.game.service.GameBotService;
import spoon.game.service.InGameService;
import spoon.monitor.service.MonitorService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class InGameParsingResult implements ParsingResult {

    private GameBotService gameBotService;

    private InGameService inGameService;

    private MonitorService monitorService;

    private BetListService betListService;

    private BetService betService;

    private static String ut;

    private static boolean action = false;

    @Override
    @Async
    public void closingGame() {
        //http://inplay.rerezone.com/api/gameOdds?idx=1076058

        Iterable<BetItem> list = betService.getInGameBetList();
        int closed = closeInGame(list);
        monitorService.checkSports();
        log.debug("인게임 클로징 - 전체 : {}, 클로징 : {}", closed, closed);
    }

    private int closeInGame(Iterable<BetItem> list) {
        int closed = 0;
        for (BetItem bot : list) {
            Date d1 = bot.getBet().getBetDate();
            Date d2 = new Date();
            //인게임 베팅 후 60초가 지나야 체크 시작
            if (d1.getTime() + 60000 > d2.getTime()) {
                continue;
            }

            int result = 0;
            String json = HttpParsing.getJson(getClosingUrl(bot.getGameId()));
//            //System.out.println(json);
            JSONParser parser = new JSONParser();
            JSONObject gameJson = new JSONObject();
            JSONObject game = new JSONObject();
            JSONObject gameDetail = new JSONObject();
            String inGameBetScore = "";

//            //System.out.println(getClosingUrl(bot.getGameId()));

            /*
            //데이터가 없으면 무조건 취소였으나 주석처리
            if (json == null) {
                AjaxResult ar = betService.cancelBetInGame(bot.getBet().getId());
                return 1;
            }*/

            try {
                gameJson = (JSONObject) parser.parse(json);
                game = (JSONObject) gameJson.get("game");
                gameDetail = (JSONObject) gameJson.get("detail");

                if (bot.getBet().getStatus() == StatusCode.STAY) {
                    JSONObject liveScore = (JSONObject) parser.parse(game.get("liveScore").toString());
                    JSONObject scoreboard = (JSONObject) liveScore.get("Scoreboard");
                    JSONArray scoreArr = (JSONArray) scoreboard.get("Results");
                    String home = "";
                    String away = "";
                    for (int i = 0; i < scoreArr.size(); i++) {
                        JSONObject j = (JSONObject) scoreArr.get(i);
                        if ("1".equals(j.get("Position").toString())) {
                            home = j.get("Value").toString();
                        } else if ("2".equals(j.get("Position").toString())) {
                            away = j.get("Value").toString();
                        }
                    }
                    inGameBetScore = home + " : " + away;

                    String gameStatus = "";// 게임상태 2 정상
                    if(game.containsKey("sts")){
                        gameStatus = game.get("sts").toString();
                    }

                    String oddsStatus = "";// 배당상태 1 정상
                    if(gameDetail.containsKey("status")){
                        oddsStatus = gameDetail.get("status").toString();
                    }

                    double odds = 999;
                    if(gameDetail.containsKey("odds")){
                        odds = Double.parseDouble(gameDetail.get("odds").toString());
                        //odds = Math.abs(odds - bot.getOddsHome());
//                        //System.out.println("odds.toString()="+gameDetail.get("odds").toString());
//                        //System.out.println("bot.getOddsHome()="+bot.getOddsHome());
                    }

//                    //System.out.println("gameStatus="+gameStatus);
//                    //System.out.println("oddsStatus="+oddsStatus);
//                    //System.out.println("odds="+odds);
//                    //System.out.println("inGameBetScore="+inGameBetScore);
//                    //System.out.println("bot.getBet().getBetScore()="+bot.getBet().getBetScore());

                    //스코어가 다르면 취소처리. - 게임상태, 배당상태 체크 추가함
                    //배당차이가 1.0 이상 하락 이면 취소처리
                    if (!inGameBetScore.equals(bot.getBet().getBetScore())){
                        AjaxResult ar = betService.cancelBetInGame(bot.getBet().getId(), StatusCode.CANCEL_IG1);
                    }else if( odds == 999 || bot.getOddsHome() <= odds - 1 ){
                        AjaxResult ar = betService.cancelBetInGame(bot.getBet().getId(), StatusCode.CANCEL_IG2);
                    }else if(!"2".equals(gameStatus) || !"1".equals(oddsStatus)){
                        AjaxResult ar = betService.cancelBetInGame(bot.getBet().getId(), StatusCode.CANCEL_IG3);
                    } else {
                        //스코어가 같다면 정상처리
                        betService.betSuccessInGame(bot.getBet());
                    }
                }

            } catch (ParseException e) {
                //파싱 에러시 취소처리
                AjaxResult ar = betService.cancelBetInGame(bot.getBet().getId(), StatusCode.CANCEL_IG3);
                e.printStackTrace();
            }
//            //System.out.println("inGameDetail = "+gameDetail.toString());

            if (bot.getBet().getStatus() == StatusCode.SUCCESS) {
                if (gameDetail.containsKey("onameKor") && gameDetail.containsKey("name")) {
                    String[] g = bot.getSpecial().split(" #### ");
//                //System.out.println("g[0]="+g[0]);
//                //System.out.println("g[1]="+g[1]);
                    if (g.length > 1) {
                        String oname = gameDetail.get("onameKor").toString();
                        if (Double.parseDouble(gameDetail.get("baseLine").toString()) != 0) {
                            oname += "(" + gameDetail.get("baseLine").toString() + ")";
                        }

                    /*//System.out.println("oname============================="+oname);
                    //System.out.println("g[0]============================="+g[0]);
                    //System.out.println("g[1]============================="+g[1]);
                    //System.out.println("name============================="+gameDetail.get("name").toString());*/

                        if (g[0].equals(oname) && g[1].equals(gameDetail.get("name").toString())) {
                            result = Integer.parseInt(gameDetail.get("result").toString()); //결과넣어줌
                        /*0 : 진행중(생성시 기본 DB 값)
                        -1 : 취소
                        1 : 미당첨
                        2 : 당첨
                        3 : 적특
                        4 : 50% 미당첨
                        5 : 50% 당첨*/
                        }
                    }
                }

                //결과처리 경우 처리해줌
                if (result != 0) {
//                //System.out.println("결과 들어옴 = " +result);
                    boolean success = gameBotService.inGameScore(bot, result);
                    if (success) {
                        closed++;
                    }
                }
            }
        }
        return closed;
    }

    private String getClosingUrl(Long idx) {
        return Config.getSysConfig().getSports().getInGameApi() + "/api/gameOdds?idx=" + idx;
    }
}
