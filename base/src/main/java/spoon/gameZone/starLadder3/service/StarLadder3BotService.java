package spoon.gameZone.starLadder3.service;

import spoon.gameZone.starLadder3.StarLadder3;

import java.util.Date;

public interface StarLadder3BotService {

    boolean notExist(Date gameDate);

    void addGame(StarLadder3 starLadder3);

    boolean closingGame(StarLadder3 result);

    void checkResult();

    void deleteGame(int days);

}
