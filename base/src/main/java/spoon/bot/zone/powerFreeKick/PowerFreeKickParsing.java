package spoon.bot.zone.powerFreeKick;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.powerFreeKick.PowerFreeKick;
import spoon.gameZone.powerFreeKick.service.PowerFreeKickBotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class PowerFreeKickParsing implements GameBotParsing {

    private PowerFreeKickBotService powerFreeKickBotService;

    private static boolean isClosing = false;

    private static Date sdate = new Date();

    @Async
    @Override
    public void parsingGame() {
        isClosing = false;

        int count = 0;
        int times = ZoneConfig.getPowerFreeKick().getPowerFreeKickMaker().getTimes();
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getPowerFreeKick().getPowerFreeKickMaker().getGameDate(times));

        for (int i = 0; i < 6; i++) {
            times++;
            cal.add(Calendar.MINUTE, 5);

            if (cal.getTime().before(sdate)) continue;

            if (powerFreeKickBotService.notExist(cal.getTime())) {
                PowerFreeKick powerFreeKick = new PowerFreeKick(times, cal.getTime());
                String hh = powerFreeKick.getSdate().substring(8,10);//시간

                if (powerFreeKick.getRound() == 0) {
                    continue;
                }

                if(Integer.parseInt(hh) < 6){
                    //경기 생성중 6시 미만 경기는 생성 안한다.
                    continue;
                }
                powerFreeKick.setOdds(ZoneConfig.getPowerFreeKick().getOdds());
                powerFreeKickBotService.addGame(powerFreeKick);
                count++;
            }
        }
        log.debug("파워프리킥 경기등록 : {}건", count);
    }

    @Async
    @Override
    public void closingGame() {
        //if (isClosing) return;
        isClosing = true;

        int times = ZoneConfig.getPowerFreeKick().getPowerFreeKickMaker().getTimes();
        Date gameDate = ZoneConfig.getPowerFreeKick().getPowerFreeKickMaker().getGameDate(times);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getPowerFreeKickUrl());
        if (json == null) {
            isClosing = false;
            return;
        }
        try {
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(json);

            //System.out.println("json = "+obj.toString());

            PowerFreeKick result = new PowerFreeKick();
            long gDate = Long.parseLong(obj.get("gameDate").toString());
            result.setGameDate(new Date(gDate));
            result.setRound(Integer.parseInt(obj.get("round").toString()));
            result.setTimes(Integer.parseInt(obj.get("times").toString()));
            result.setSdate(obj.get("sdate").toString());
            result.setClosing(Boolean.parseBoolean(obj.get("closing").toString()));
            result.setPlayer(obj.get("player").toString());
            result.setDirection(obj.get("direction").toString());
            result.setGoal(obj.get("goal").toString());
            result.setLastBall(obj.get("lastBall").toString());

            //결과 처리 score //api에서 다 가져오기 때문에 주석
//            PowerFreeKickDto.Score score = new PowerFreeKickDto.Score();
//            score.setLastBall(result.getLastBall());
//            result.updateScore(score);

            if (result == null) {
                isClosing = false;
                return;
            }

            if (!gameDate.equals(result.getGameDate())) {
                isClosing = false;
                return;
            }

            //System.out.println("파워프리킥 클로징 게임 result =" + result);

            isClosing = powerFreeKickBotService.closingGame(result);

            log.debug("파워프리킥 경기 종료 : {}회차", result.getRound());

        } catch (ParseException e) {
            e.printStackTrace();
            isClosing = false;
            return;
        }

    }

    @Async
    @Override
    public void checkResult() {
        powerFreeKickBotService.checkResult();
    }

    @Async
    @Override
    public void deleteGame() {
        powerFreeKickBotService.deleteGame(3);
    }
}
