package spoon.member.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.mapper.MemberMapper;
import spoon.member.domain.Agency;
import spoon.member.domain.MemberDto;
import spoon.member.domain.User;
import spoon.member.entity.Member;
import spoon.member.entity.QMember;
import spoon.member.repository.MemberRepository;

import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class MemberServiceImpl implements MemberService {

    private MemberRepository memberRepository;

    private MemberMapper memberMapper;

    private static QMember q = QMember.member;

    @Transactional(readOnly = true)
    @Override
    public Member getMember(String userid) {
        return memberRepository.findByUserid(userid);
    }

    @Override
    public List<MemberDto.Agency> getAgencyMemberList(MemberDto.Seller seller) {
        return memberMapper.getAgencyMemberList(seller);
    }
    @Override
    public List<MemberDto.Agency> getAgencyMemberList2(MemberDto.Seller seller) {
        return memberMapper.getAgencyMemberList2(seller);
    }

    @Transactional(readOnly = true)
    @Override
    public boolean getBalanceLeak(String userid) {
        return memberRepository.getBalanceLeak(userid);
    }

    @Transactional(readOnly = true)
    @Override
    public boolean getBalanceDariLeak(String userid) {
        return memberRepository.getBalanceDariLeak(userid);
    }

    @Transactional(readOnly = true)
    @Override
    public boolean getBalanceLadderLeak(String userid) {
        return memberRepository.getBalanceLadderLeak(userid);
    }

    @Transactional(readOnly = true)
    @Override
    public boolean getBalancePowerLeak(String userid) {
        return memberRepository.getBalancePowerLeak(userid);
    }

    @Transactional(readOnly = true)
    @Override
    public User getUser(String userid) {
        return memberMapper.getUser(userid);
    }

    @Transactional(readOnly = true)
    @Override
    public MemberDto.Balance getUserBalance(String userid) {
        return memberMapper.getUserBalance(userid);
    }

    @Transactional(readOnly = true)
    @Override
    public User getRandomUser() {
        return memberMapper.getRandomUser();
    }

    @Transactional(readOnly = true)
    @Override
    public User getUserAndMemo(String userid) {
        return memberMapper.getUserAndMemo(userid);
    }

    @Override
    public List<Agency> getAgencyList() {
        return memberMapper.getAgencyList();
    }

    @Override
    public List<Agency> getAgencyListNew() {
        return memberMapper.getAgencyListNew();
    }

    @Transactional(readOnly = true)
    @Override
    public List<String> getAgency1List(String agency2) {
        return memberMapper.getAgency1List(agency2);
    }

    @Transactional(readOnly = true)
    @Override
    public List<String> getAgency7List(MemberDto.Seller command) {
        return memberMapper.getAgency7List(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<String> getAgency7List2(MemberDto.Seller command) {
        return memberMapper.getAgency7List2(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<String> getAgency2List() {
        return memberMapper.getAgency2List();
    }

    @Override
    public void deleteMember(String userid) {
        memberMapper.deleteMember(userid);
    }

    @Transactional(readOnly = true)
    @Override
    public List<MemberDto.Exchange> getExchangeList(MemberDto.Seller command) {
        return memberMapper.getExchangeListNew(command);
    }

    @Transactional(readOnly = true)
    @Override
    public User getRecomm(String userid) {
        return memberMapper.getRecomm(userid);
    }

    @Transactional
    @Override
    public void update(Member member) {
        memberRepository.saveAndFlush(member);
    }

    @Transactional(readOnly = true)
    @Override
    public boolean isExist(String userid) {
        return memberRepository.count(q.userid.eq(userid)) > 0;
    }

}
