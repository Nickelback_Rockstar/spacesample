package spoon.bot.balance;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.balance.service.gate.GodPowerLadderService;
import spoon.bot.balance.service.gate.GodPowerService;
import spoon.common.utils.DateUtils;
import spoon.config.domain.Config;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Component
public class GodTask {

    private GodPowerService godPowerService;
    private GodPowerLadderService godPowerLadderService;

    /*//마감 35초? 전
    @Scheduled(cron = "20 2/5 * * * ?")
    public void b1() {
        //System.out.println("20 2/5");
    }

    //마감 15초? 전
    @Scheduled(cron = "40 2/5 * * * ?")
    public void b2() {
        //System.out.println("40 2/5");
    }*/

    /**
     * 파워볼
     */
    @Scheduled(cron = "6 2/5 * * * ?")
    public void powerBalance() {
        if (
                Config.getSysConfig().getZone().isGodPowerBalance()
                        || Config.getSysConfig().getZone().isGodPowerBalanceHp()
                        || Config.getSysConfig().getZone().isGodPowerBalance1()
                        || Config.getSysConfig().getZone().isGodPowerBalance2()
        ) {
            godPowerService.balance();
        }
    }

    /**
     * 파워 사다리
     */
    @Scheduled(cron = "2 2/5 * * * ?")
    public void powerLadderBalance() {
        if (
                Config.getSysConfig().getZone().isGodPowerLadderBalance()
                        || Config.getSysConfig().getZone().isGodPowerLadderBalanceHp()
                        || Config.getSysConfig().getZone().isGodPowerLadderBalance1()
                        || Config.getSysConfig().getZone().isGodPowerLadderBalance2()
        ) {
            godPowerLadderService.balance();
        }
    }

    @Scheduled(cron = "30 2/5 * * * ?")
    public void powerBalanceSetting() {
        if(Config.getSysConfig().getZone().isGodBalanceAutoReset()) {
            godPowerService.setting();
        }
    }

}
