package spoon.member.domain;

import lombok.Data;

import java.util.Date;

@Data
public class User {

    private String userid;

    private String nickname;

    private int agencyDepth;

    private String agency1;

    private String agency2;

    private String agency3;

    private String agency4;

    private String agency5;

    private String agency6;

    private String agency7;

    private Role role;

    private int level;

    private int memo;

    private int qna;

    private long money;

    private long point;

    private boolean enabled;

    private boolean secession;

    private boolean block;

    private boolean black;

    private boolean folderBlock;

    private String loginIp;

    private long inMoney;

    private long outMoney;

    private Date loginDate;

    private long lotto;

    public long getMilliseconds() {
        return System.currentTimeMillis();
    }

}
