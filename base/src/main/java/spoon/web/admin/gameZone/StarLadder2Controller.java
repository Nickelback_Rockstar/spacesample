package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.starLadder2.StarLadder2Config;
import spoon.gameZone.starLadder2.StarLadder2Dto;
import spoon.gameZone.starLadder2.service.StarLadder2Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.starLadder2Controller")
@RequestMapping(value = "#{config.pathAdmin}")
public class StarLadder2Controller {

    private StarLadder2Service starLadder2Service;

    /**
     * 별다리2분 설정
     */
    @RequestMapping(value = "zone/starLadder2/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getStarLadder2());
        return "admin/zone/starLadder2/config";
    }

    /**
     * 별다리2분 설정 변경
     */
    @RequestMapping(value = "zone/starLadder2/config", method = RequestMethod.POST)
    public String config(StarLadder2Config starLadder2Config, RedirectAttributes ra) {
        boolean success = starLadder2Service.updateConfig(starLadder2Config);
        if (success) {
            ra.addFlashAttribute("message", "별다리2분게임 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "별다리2분게임 설정변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/starLadder2/config";
    }

    /**
     * 별다리2분 진행
     */
    @RequestMapping(value = "zone/starLadder2/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", starLadder2Service.getComplete());
        map.addAttribute("config", ZoneConfig.getLadder());
        return "admin/zone/starLadder2/complete";
    }

    /**
     * 별다리2분 완료
     */
    @RequestMapping(value = "zone/starLadder2/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", starLadder2Service.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getStarLadder2());
        return "admin/zone/starLadder2/closing";
    }

    /**
     * 별다리2분 스코어 입력 폼
     */
    @RequestMapping(value = "zone/starLadder2/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", starLadder2Service.findScore(id));
        return "admin/zone/starLadder2/score";
    }

    /**
     * 별다리2분 스코어 결과처리
     */
    @RequestMapping(value = "zone/starLadder2/score", method = RequestMethod.POST)
    public String score(StarLadder2Dto.Score score, RedirectAttributes ra) {
        boolean success = starLadder2Service.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "별다리2분 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "별다리2분 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/starLadder2/score?id=" + score.getId();
    }

    /**
     * 별다리2분 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/starLadder2/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return starLadder2Service.closingAllGame();
    }

}
