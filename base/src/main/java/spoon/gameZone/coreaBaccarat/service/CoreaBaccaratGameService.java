package spoon.gameZone.coreaBaccarat.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.baccarat.Baccarat;
import spoon.gameZone.coreaBaccarat.CoreaBaccarat;
import spoon.support.web.AjaxResult;

public interface CoreaBaccaratGameService {

    /**
     * 바카라 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 바카라 게임 베팅을 클로징 한다.
     */
    void closingBetting(CoreaBaccarat coreaBaccarat);

    /**
     * 바카라 게임을 롤백 한다.
     */
    void rollbackPayment(CoreaBaccarat coreaBaccarat);
}
