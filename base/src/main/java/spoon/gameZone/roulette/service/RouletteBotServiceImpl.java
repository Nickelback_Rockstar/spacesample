package spoon.gameZone.roulette.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.common.utils.StringUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.roulette.QRoulette;
import spoon.gameZone.roulette.Roulette;
import spoon.gameZone.roulette.RouletteRepository;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class RouletteBotServiceImpl implements RouletteBotService {

    private RouletteGameService rouletteGameService;

    private RouletteRepository rouletteRepository;

    private static QRoulette q = QRoulette.roulette;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return rouletteRepository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmmss"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(Roulette roulette) {
        rouletteRepository.saveAndFlush(roulette);
    }

    @Transactional
    @Override
    public boolean closingGame(Roulette result) {
        Roulette roulette = rouletteRepository.findOne(q.sdate.eq(result.getSdate()));
        if (roulette == null) {
            return true;
        }

        try {
            roulette.setResult(result.getResult());

            if (StringUtils.empty(result.getResult())) {
                roulette.setCancel(true);
            } else {
                roulette.setCancel(false);
            }
            roulette.setClosing(true);

            rouletteRepository.saveAndFlush(roulette);
            rouletteGameService.closingBetting(roulette);
        } catch (RuntimeException e) {
            log.error("룰렛 {}회차 결과 업데이트에 실패하였습니다. - {}", roulette.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = rouletteRepository.count(q.gameDate.before(DateUtils.beforeSeconds(100)).and(q.closing.isFalse()));
        ZoneConfig.getRoulette().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        rouletteRepository.deleteGame(DateUtils.beforeDays(days));
    }

}
