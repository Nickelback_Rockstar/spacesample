package spoon.bot.zone.coreaBaccarat;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class CoreaBaccaratTask {

    private GameBotParsing coreaBaccaratParsing;

    @Scheduled(cron = "0 * * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        coreaBaccaratParsing.parsingGame();
    }

    @Scheduled(cron = "9/15 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        coreaBaccaratParsing.closingGame();
    }

    @Scheduled(cron = "8/13 * * * * *")
    public void checkResult() {
        if (notParsing()) return;
        coreaBaccaratParsing.checkResult();
    }

    @Scheduled(cron = "15 1 4 * * * ")
    public void deleteGame() {
        coreaBaccaratParsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isCoreaBaccarat() || !ZoneConfig.getCoreaBaccarat().isEnabled();
    }
}
