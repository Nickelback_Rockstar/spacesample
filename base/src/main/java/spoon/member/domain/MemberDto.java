package spoon.member.domain;

import lombok.Data;
import spoon.common.utils.DateUtils;
import spoon.config.domain.Config;

public class MemberDto {

    @Data
    public static class Command {
        private String mode = "";
        private String searchType = "";
        private String searchValue = "";
        private String username = "";
        private boolean match;
        private String sort = "";
        private String level = "";
        private int page = 1;
        private int size = 20;
        private String agency2;
    }

    @Data
    public static class Join {
        private String code;
        private String recommender;
        private String userid;
        private String nickname;
        private String password;
        private String phone;
        private String bank;
        private String depositor;
        private String account;
        private String bankPassword;
        private String defenseCode;

    }

    @Data
    public static class Add {
        private String userid;
        private String password;
        private String nickname;
        private String joinKey;
        private Role role;
        private int level = Config.getSiteConfig().getJoin().getJoinLevel();
        private String phone = "";
        private String recommender = "";
        private long money;
        private long point;
        private String agency1 = "";
        private String agency2 = "";
        private String agency3 = "";
        private String agency4 = "";
        private String agency5 = "";
        private String agency6 = "";
        private String agency7 = "";
        private String bank = "";
        private String depositor = "";
        private String account = "";
        private String bankPassword = "";
        private String memo = "";
    }

    @Data
    public static class Agency {
        private String userid;
        private String password;
        private String nickname;
        private String phone = "";
        private String bank = "";
        private String depositor = "";
        private String account = "";
        private String bankPassword = "";
        private String rateCode;
        private String agency1 = "";
        private String agency2 = "";
        private String agency3 = "";
        private String agency4 = "";
        private String agency5 = "";
        private String agency6 = "";
        private String agency7 = "";
        private double rateShare;
        private double rateSports;
        private double rateSports2;
        private double rateZone;
        private double rateZone1;
        private double rateZone2;
        private double rateCasino;

    }

    @Data
    public static class Dummy {
        private int start = 1;
        private int end = 10;
        private String text;
    }

    @Data
    public static class Update {
        private String userid;
        private String password;
        private String nickname;
        private int level;
        private boolean enabled;
        private boolean black;
        private boolean block;
        private boolean secession;
        private boolean balanceLadder;
        private boolean balanceDari;
        private boolean balanceLowhi;
        private boolean balanceAladdin;
        private boolean balancePower;
        private boolean balancePowerLadder;
        private boolean balancePowerRe;
        private boolean balancePowerLadderRe;
        private boolean balanceLeak;
        private boolean balanceDariLeak;
        private boolean balanceLadderLeak;
        private boolean balancePowerLeak;
        private boolean balanceDariReverse;
        private boolean balanceLadderReverse;
        private boolean balancePowerReverse;
        private boolean folderBlock;
        private String phone;
        private String recommender;
        private String agency1;
        private String agency2;
        private String agency3;
        private String agency4;
        private String agency5;
        private String agency6;
        private String agency7;
        private int agencyDepth;
        private String memo;
        private String bank;
        private String depositor;
        private String account;
        private String bankPassword;
        private String virtualId;
        private String agency;


    }

    @Data
    public static class Seller {
        private String agency;
        private String agency1;
        private String agency2;
        private String agency3;
        private String agency4;
        private String agency5;
        private String agency6;
        private String agency7;
        private int agencyDepth;
        private String mode = "";
        private String sort = "";
    }

    @Data
    public static class Exchange {
        private String userid;
        private String nickname;
    }

    @Data
    public static class Balance {
        private boolean balanceDari;
        private boolean balanceDariLeak;
        private boolean balanceDariReverse;
        private boolean balanceLadder;
        private boolean balanceLadderLeak;
        private boolean balanceLadderReverse;
        private boolean balancePower;
        private boolean balancePowerLeak;
        private boolean balancePowerReverse;
        private boolean balancePowerLadder;
        private boolean balancePowerRe;
        private boolean balancePowerLadderRe;
    }

    @Data
    public static class schCommon {
        private String sdate;
        private String edate;
        private String orderBy = "";
        private String username;
        private boolean match;
        private int page = 1;
        private int size = 20;

        public schCommon() {
            this.sdate = DateUtils.todayString();
            this.edate = this.sdate;
        }

        public String getStart() {
            return this.sdate.replaceAll("\\.", "-");
        }

        public String getEnd() {
            return this.edate.replaceAll("\\.", "-");
        }
    }

}
