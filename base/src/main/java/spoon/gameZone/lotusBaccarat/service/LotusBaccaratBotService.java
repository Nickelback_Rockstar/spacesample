package spoon.gameZone.lotusBaccarat.service;

import spoon.gameZone.lotusBaccarat.LotusBaccarat;

import java.util.Date;

public interface LotusBaccaratBotService {

    boolean notExist(Date gameDate);

    void addGame(LotusBaccarat baccarat);

    boolean closingGame(LotusBaccarat result);

    void checkResult();

    void deleteGame();

}
