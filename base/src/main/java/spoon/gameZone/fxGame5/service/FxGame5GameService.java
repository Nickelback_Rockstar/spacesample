package spoon.gameZone.fxGame5.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame5.FxGame5;
import spoon.support.web.AjaxResult;

public interface FxGame5GameService {

    /**
     * fxGame5 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * fxGame5 게임 베팅을 클로징 한다.
     */
    void closingBetting(FxGame5 ladder);

    /**
     * fxGame5 게임을 롤백 한다.
     */
    void rollbackPayment(FxGame5 ladder);
}
