package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.StringUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame2.FxGame2Dto;
import spoon.gameZone.fxGame2.service.FxGame2GameService;
import spoon.gameZone.fxGame2.service.FxGame2Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class FxGame2Controller {

    private FxGame2Service fxGame2Service;

    private FxGame2GameService fxGame2GameService;

    @RequestMapping(value = "zone/fxGame2", method = RequestMethod.GET)
    public String zone(ModelMap map, @RequestParam(value = "round", required = false) String round) {
        FxGame2Dto.Config config = fxGame2Service.gameConfig(getNextRound(round));
        map.addAttribute("config", JsonUtils.toString(config));
        return "site/zone/fxGame2";
    }

    @ResponseBody
    @RequestMapping(value = "zone/fxGame2/config", method = RequestMethod.POST)
    public FxGame2Dto.Config config(@RequestParam(value = "round", required = false) String round) {
        return fxGame2Service.gameConfig(getNextRound(round));
    }

    public int getNextRound(String round){
        int nextRound = 2;
        try {
            if (StringUtils.notEmpty(round)) {
                nextRound = Integer.parseInt(round);
                if (nextRound != 2 && nextRound != 3) {
                    nextRound = 2;
                }
            }
        }catch (Exception e){
        }finally {
            return nextRound;
        }
    }

    @RequestMapping(value = "zone/fxGame2/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", fxGame2Service.getClosing(command, pageable));
        return "site/score/fxGame2";
    }

    @ResponseBody
    @RequestMapping(value = "zone/fxGame2/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return fxGame2GameService.betting(bet);
    }

}
