package spoon.bet.domain;

import lombok.Data;
import spoon.common.utils.DateUtils;
import spoon.game.domain.GameResult;
import spoon.game.domain.MenuCode;

import java.util.Date;

public class BetDto {

    @Data
    public static class BetGame {
        private long betMoney;
        private MenuCode menuCode;
        private Long[] ids;
        private String[] bets;
        private Double[] odds;
        private String[] gids;
        private String[] games;
        private String[] mc;
    }

    @Data
    public static class BetBalGame {
        private long betMoney;
        private String special;
        private String betTeam;
        private String userid;
        private String nickname;

        private long total;
        private long hit;
        private long nohit;
        private int oneCnt;
        private int betCnt;
        private long result1;
        private long result2;
        private long result3;
        private Date regDate;
        private Date date1;
        private Date date2;
        private Date date3;

        private int hitTotCnt;
        private int nohitTotCnt;
        private int hit3Cnt;
        private int nohit3Cnt;
        private String b1;
        private String b2;
        private String b3;
        private String b4;
        private String b5;
        private String b6;
        private String b7;
        private String b8;
        private String b9;
        private String b10;
        private String b11;
        private String b12;
        private String b13;
        private String b14;
        private String b15;
        private String b16;
        private String b17;
        private String b18;
        private String b19;
        private String b20;

    }

    @Data
    public static class Command {
        private Long gameId;
        private String betDate = "";
        private String role = "";
        private MenuCode menuCode;
        private String result = "";
        private String username = "";
        private boolean match;
        private String orderBy = "";
        private String userid = "";
        private String ip = "";
        private String teamName = "";
        private String sports = "";
        private String special = "";
        private String black = "";
    }

    @Data
    public static class UserCommand {
        private String userid;
        private String sDate = DateUtils.format(DateUtils.beforeDays(7));
        private String eDate = DateUtils.format(new Date());
    }

    @Data
    public static class SellerCommand {
        private String username;
        private boolean match;
        private String agency;
        private String agency1;
        private String agency2;
        private String agency3;
        private String agency4;
        private String agency5;
        private String agency6;
        private String agency7;
        String mode = "";
    }

    @Data
    public static class Delete {
        private String userid;
        private Long[] betIds;
    }

    @Data
    public static class Score {
        private int scoreHome;
        private int scoreAway;
        private GameResult gameResult;
    }

    @Data
    public static class ZoneAmount {
        private String gameCode;
        private int betZone;
        private long amount;
    }

    @Data
    public static class UserBet {
        private MenuCode menuCode;
        private long cnt;
        private long betMoney;
        private long hitMoney;
        private Date betDate;
    }

    @Data
    public static class BetRate {
        private String sdate;
        private String edate;
        private String orderBy = "";
        private String username;
        private boolean match;
        private int page = 1;
        private int size = 20;

        public BetRate() {
            this.sdate = DateUtils.todayString();
            this.edate = this.sdate;
        }

        public String getStart() {
            return this.sdate.replaceAll("\\.", "-");
        }

        public String getEnd() {
            return this.edate.replaceAll("\\.", "-");
        }
    }

    @Data
    public static class ReportCommand {
        private String sdate = DateUtils.format(new Date());
        private String edate = DateUtils.format(new Date());
        private int betCount = 2;
        private String orderBy = "totalCount";
        private int page = 1;
        private int size = 20;
    }

}
