package spoon.seller.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.common.utils.StringUtils;
import spoon.member.domain.User;
import spoon.member.service.MemberService;
import spoon.seller.domain.JoinCodeDto;
import spoon.seller.entity.JoinCode;
import spoon.seller.entity.QJoinCode;
import spoon.seller.repository.JoinCodeRepository;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Service
public class JoinCodeServiceImpl implements JoinCodeService {

    private MemberService memberService;

    private JoinCodeRepository joinCodeRepository;

    private static QJoinCode q = QJoinCode.joinCode;

    @Transactional(readOnly = true)
    @Override
    public Iterable<JoinCode> getList(JoinCodeDto.Command command) {
        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.notEmpty(command.getJoinCode())) {
            builder.and(q.code.like("%" + command.getJoinCode() + "%"));
        }

        if (!command.getAgency().equals("-")) {
            builder.and(q.agency2.eq(command.getAgency()));
        }

        return joinCodeRepository.findAll(builder, new Sort(Sort.Direction.ASC, "agency2", "agency1"));
    }

    @Transactional(readOnly = true)
    @Override
    public JoinCode getSellerCode(JoinCodeDto.Partner command) {

        //System.out.println("command2 = " + command.toString());

        return joinCodeRepository.findOne(
                q.agency1.eq(command.getAgency1())
                .and(q.agency2.eq(command.getAgency2()))
                .and(q.agency3.eq(command.getAgency3()))
                .and(q.agency4.eq(command.getAgency4()))
                .and(q.agency5.eq(command.getAgency5()))
                .and(q.agency6.eq(command.getAgency6()))
                .and(q.agency7.eq(command.getAgency7()))
        );
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<JoinCode> getPartner(JoinCodeDto.Partner command) {
        BooleanBuilder builder = new BooleanBuilder();
        builder.and(q.agency7.eq(command.getAgency7()));
        return joinCodeRepository.findAll(builder, new Sort(Sort.Direction.ASC, "agency7"));
    }

    @Transactional
    @Override
    public AjaxResult add(JoinCodeDto.Add add) {
        // 코드 중복 체크
        String code = add.getCode().trim().toUpperCase();
        long count = joinCodeRepository.count(q.code.eq(code));

        if (count > 0) {
            return new AjaxResult(false, "가입코드가 중복 되었습니다.");
        }

        User user = memberService.getUser(add.getAgency2());

        if(user == null){
            return new AjaxResult(false, "총판 정보가 없습니다.");
        }

        JoinCode joinCode = new JoinCode();
        joinCode.setCode(code);
        joinCode.setAgency1(user.getAgency1());
        joinCode.setAgency2(user.getAgency2());
        joinCode.setMemo(add.getMemo());
        joinCode.setEnabled(add.isEnabled());

        joinCodeRepository.saveAndFlush(joinCode);

        return new AjaxResult(true, "가입코드를 등록하였습니다.");
    }

    @Transactional
    @Override
    public AjaxResult update(JoinCodeDto.Update update) {
        JoinCode joinCode = joinCodeRepository.findOne(update.getId());
        if(StringUtils.notEmpty(update.getCode())){
            joinCode.setCode(update.getCode());
        }
        joinCode.setEnabled(update.isEnabled());
        joinCode.setMemo(update.getMemo());
        joinCodeRepository.saveAndFlush(joinCode);

        return new AjaxResult(true, joinCode.getCode() + " 수정을 완료하였습니다.");
    }

    @Transactional
    @Override
    public AjaxResult delete(Long id) {
        joinCodeRepository.delete(id);

        return new AjaxResult(true, "가입코드를 삭제하였습니다.");
    }

    @Transactional(readOnly = true)
    @Override
    public boolean existJoinCode(String code) {
        return joinCodeRepository.count(q.code.eq(code.trim().toUpperCase()).and(q.enabled.isTrue())) > 0;
    }

    @Transactional(readOnly = true)
    @Override
    public JoinCode get(String code) {
        return joinCodeRepository.findOne(q.code.eq(code).and(q.enabled.isTrue()));
    }

}
