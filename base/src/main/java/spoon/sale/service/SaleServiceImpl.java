package spoon.sale.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.ErrorUtils;
import spoon.common.utils.StringUtils;
import spoon.mapper.ShareMapper;
import spoon.member.domain.Role;
import spoon.member.domain.User;
import spoon.member.service.MemberService;
import spoon.payment.domain.PointCode;
import spoon.payment.service.PaymentService;
import spoon.sale.domain.SaleDto;
import spoon.sale.entity.*;
import spoon.sale.repository.PowerLottoSaleItemRepository;
import spoon.sale.repository.PowerLottoSaleRepository;
import spoon.sale.repository.SaleItemRepository;
import spoon.sale.repository.SaleRepository;
import spoon.support.web.AjaxResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class SaleServiceImpl implements SaleService {

    private MemberService memberService;

    private PaymentService paymentService;

    private ShareMapper shareMapper;

    private SaleRepository saleRepository;

    private SaleItemRepository saleItemRepository;

    private PowerLottoSaleRepository powerLottoSaleRepository;

    private PowerLottoSaleItemRepository powerLottoSaleItemRepository;

    private static QSale q = QSale.sale;

    @Transactional(readOnly = true)
    @Override
    public List<SaleItem> currentSale(SaleDto.Command command) {
        return shareMapper.currentSale(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<SaleItem> currentListSale(SaleDto.Command command) {
        return shareMapper.currentListSale(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<SaleItem> currentListSale2(SaleDto.Command command) {
        return shareMapper.currentListSale2(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<SaleItem> currentSaleNew(SaleDto.Command command) {
        return shareMapper.currentSaleNew(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<SaleItem> currentSaleNew2(SaleDto.Command command) {
        return shareMapper.currentSaleNew2(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<SaleItem> currentSaleReNew(SaleDto.Command command) {
        return shareMapper.currentSaleReNew(command);
    }

    @Transactional
    @Override
    public AjaxResult balanceSale(String userid) {
        User user = memberService.getUser(userid);
        SaleDto.Command command = new SaleDto.Command();
        command.setAgency(userid);
        command.setAgencyDepth(user.getAgencyDepth());
        List<SaleItem> saleItems = shareMapper.currentSaleDepth(command);
        Sale sale = new Sale();

        try {
            for (SaleItem item : saleItems) {
                item.setLastMoney(item.getCalcMoney());
                item.setRegDate(new Date());
            }

            sale.setUserid(userid);
            sale.setAgency1(user.getAgency1());
            sale.setAgency2(user.getAgency2());
            sale.setAgency3(user.getAgency3());
            sale.setAgency4(user.getAgency4());
            sale.setAgency5(user.getAgency5());
            sale.setAgency6(user.getAgency6());
            sale.setAgency7(user.getAgency7());
            sale.setRole(user.getRole());
            sale.setRegDate(new Date());
            sale.setSaleItems(saleItems);


            saleRepository.saveAndFlush(sale);

        } catch (RuntimeException e) {
            log.error("{} - 총판 정산에 실패하였습니다.: {}", e.getMessage(), userid);
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, userid + " 총판 정산에 실패 하였습니다.");
        }

        return new AjaxResult(true, userid + " 총판 정산을 완료하였습니다.");
    }

    @Transactional
    @Override
    public AjaxResult balanceSaleNew(String userid) {
        User user = memberService.getUser(userid);
        SaleDto.Command command = new SaleDto.Command();
        command.setAgency(userid);
        command.setAgencyDepth(user.getAgencyDepth());
        List<SaleItem> saleItems = shareMapper.currentSaleDepthNew2(command);

        try {
            for(SaleItem item : saleItems){
                // '####' 구분자로 합쳐진 문자열 금액들을 배열로 쪼개서 셋해줌
                String[] money = item.getBetZoneMoney().split("####");
                if(money != null && money.length == 3){
                    item.setBetSports(Long.parseLong(money[0]));
                    item.setBetSports2(Long.parseLong(money[1]));
                    item.setBetZone(Long.parseLong(money[2]));
                }
            }

            for(SaleItem si1 : saleItems){
                long tmpMoney1 = 0;
                long tmpMoney2 = 0;

                for(SaleItem si2 : saleItems){
                    if(si1.getAgencyDepth() == 1 && si2.getAgencyDepth() == 2 && si1.getAgency1().equals(si2.getAgency1())){
                        tmpMoney1 += si2.getCalcMoney();
                    }else if(si1.getAgencyDepth() == 2 && si2.getAgencyDepth() == 3 && si1.getAgency2().equals(si2.getAgency2())){
                        tmpMoney2 += si2.getCalcMoney();
                    }
                }
                si1.setTmpMoney1(tmpMoney1);
                si1.setTmpMoney2(tmpMoney2);
            }

            for (SaleItem item : saleItems) {
                List<SaleItem> Items = new ArrayList<>();
                if(item.getAgencyDepth() == 1){
                    item.setLastMoney(item.getCalcMoney() - item.getTmpMoney1());
                }else if(item.getAgencyDepth() == 2){
                    item.setLastMoney(item.getCalcMoney() - item.getTmpMoney2());
                }else{
                    item.setLastMoney(item.getCalcMoney());
                }

                item.setRegDate(new Date());

                Items.add(item);

                Sale sale = new Sale();
                sale.setUserid(userid);
                sale.setAgency1(user.getAgency1());
                sale.setAgency2(user.getAgency2());
                sale.setAgency3(user.getAgency3());
                sale.setAgency4(user.getAgency4());
                sale.setAgency5(user.getAgency5());
                sale.setAgency6(user.getAgency6());
                sale.setAgency7(user.getAgency7());
                sale.setRole(user.getRole());
                sale.setRegDate(new Date());
                sale.setSaleItems(Items);

                saleRepository.saveAndFlush(sale);
            }



        } catch (RuntimeException e) {
            log.error("{} - 총판 정산에 실패하였습니다.: {}", e.getMessage(), userid);
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, userid + " 총판 정산에 실패 하였습니다.");
        }

        return new AjaxResult(true, userid + " 총판 정산을 완료하였습니다.");
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Sale> getPage(SaleDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();
        if (StringUtils.notEmpty(command.getAgency1())) {
            builder.and(q.agency1.eq(command.getAgency1()));
        }
        if (StringUtils.notEmpty(command.getAgency2())) {
            builder.and(q.agency2.eq(command.getAgency2()));
        }
        if (StringUtils.notEmpty(command.getAgency3())) {
            builder.and(q.agency3.eq(command.getAgency3()));
        }
        if (StringUtils.notEmpty(command.getAgency4())) {
            builder.and(q.agency4.eq(command.getAgency4()));
        }
        if (StringUtils.notEmpty(command.getAgency5())) {
            builder.and(q.agency5.eq(command.getAgency5()));
        }
        if (StringUtils.notEmpty(command.getAgency6())) {
            builder.and(q.agency6.eq(command.getAgency6()));
        }
        if (StringUtils.notEmpty(command.getAgency7())) {
            builder.and(q.agency7.eq(command.getAgency7()));
        }
        return saleRepository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<SaleItem> getPageItem(SaleDto.Command command, Pageable pageable) {
        QSaleItem q = QSaleItem.saleItem;
        BooleanBuilder builder = new BooleanBuilder();
        if (StringUtils.notEmpty(command.getAgency1())) {
            builder.and(q.agency1.eq(command.getAgency1()));
        }
        if (StringUtils.notEmpty(command.getAgency2())) {
            builder.and(q.agency2.eq(command.getAgency2()));
        }
        if (StringUtils.notEmpty(command.getAgency3())) {
            builder.and(q.agency3.eq(command.getAgency3()));
        }
        if (StringUtils.notEmpty(command.getAgency4())) {
            builder.and(q.agency4.eq(command.getAgency4()));
        }
        if (StringUtils.notEmpty(command.getAgency5())) {
            builder.and(q.agency5.eq(command.getAgency5()));
        }
        if (StringUtils.notEmpty(command.getAgency6())) {
            builder.and(q.agency6.eq(command.getAgency6()));
        }
        if (StringUtils.notEmpty(command.getAgency7())) {
            builder.and(q.agency7.eq(command.getAgency7()));
        }

        return saleItemRepository.findAll(builder, pageable);
    }

    @Transactional
    @Override
    public AjaxResult payment(SaleDto.Payment payment) {
        Sale sale = saleRepository.findOne(payment.getSaleId());
        if (sale.isClosing()) return new AjaxResult(false, "이미 지급된 정산입니다.");

        try {
            sale.setClosing(true);
            for (SaleItem item : sale.getSaleItems()) {
                if (item.getRole() == Role.AGENCY) {
                    item.setTotalMoney(payment.getAmount());
                }
                item.setClosing(true);
            }
            paymentService.addPoint(PointCode.SALE, payment.getSaleId(), payment.getUserid(), payment.getAmount(), "");
            saleRepository.saveAndFlush(sale);

        } catch (RuntimeException e) {
            log.error("{} - 총판 정산금 지급에 실패하였습니다.: {}", e.getMessage(), payment.toString());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, payment.getUserid() + " 총판 정산금 지급에 실패 하였습니다.");
        }

        return new AjaxResult(true, "총판 정산금 지급을 완료 하였습니다.");
    }

    @Transactional(readOnly = true)
    @Override
    public List<PowerLottoSaleItem> currentSaleNewPbLotto(SaleDto.Command command) {
        return shareMapper.currentSaleNewPbLotto(command);
    }

    @Override
    public AjaxResult balanceSalePbLotto(String userid) {
        User user = memberService.getUser(userid);
        SaleDto.Command command = new SaleDto.Command();
        command.setAgency(userid);
        command.setAgencyDepth(user.getAgencyDepth());
        List<PowerLottoSaleItem> saleItems = shareMapper.currentSaleDepthPbLotto(command);
        PowerLottoSale sale = new PowerLottoSale();

        try {
            for (PowerLottoSaleItem item : saleItems) {
                item.setLastMoney(item.getCalcMoney());
                item.setRegDate(new Date());
            }

            sale.setUserid(userid);
            sale.setAgency1(user.getAgency1());
            sale.setAgency2(user.getAgency2());
            sale.setAgency3(user.getAgency3());
            sale.setAgency4(user.getAgency4());
            sale.setAgency5(user.getAgency5());
            sale.setAgency6(user.getAgency6());
            sale.setAgency7(user.getAgency7());
            sale.setRole(user.getRole());
            sale.setRegDate(new Date());
            sale.setPowerLottoSaleItems(saleItems);

            powerLottoSaleRepository.saveAndFlush(sale);

        } catch (RuntimeException e) {
            log.error("{} - 파워로또  총판 정산에 실패하였습니다.: {}", e.getMessage(), userid);
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, userid + " 파워로또 총판 정산에 실패 하였습니다.");
        }

        return new AjaxResult(true, userid + " 파워로또 총판 정산을 완료하였습니다.");
    }

    @Override
    public AjaxResult paymentPbLotto(SaleDto.Payment payment) {
        PowerLottoSale sale = powerLottoSaleRepository.findOne(payment.getSaleId());
        if (sale.isClosing()) return new AjaxResult(false, "이미 지급된 정산입니다.");

        try {
            sale.setClosing(true);
            for (PowerLottoSaleItem item : sale.getPowerLottoSaleItems()) {
                if (item.getRole() == Role.AGENCY) {
                    item.setTotalMoney(payment.getAmount());
                }
                item.setClosing(true);
            }
            paymentService.addPoint(PointCode.POWERLOTTOSALE, payment.getSaleId(), payment.getUserid(), payment.getAmount(), "");
            powerLottoSaleRepository.saveAndFlush(sale);

        } catch (RuntimeException e) {
            log.error("{} - 파워로또 총판 정산금 지급에 실패하였습니다.: {}", e.getMessage(), payment.toString());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, payment.getUserid() + " 파워로또 총판 정산금 지급에 실패 하였습니다.");
        }

        return new AjaxResult(true, "총판 정산금 지급을 완료 하였습니다.");
    }

    @Override
    public AjaxResult balanceSaleNewPbLotto(String userid) {
        User user = memberService.getUser(userid);
        SaleDto.Command command = new SaleDto.Command();
        command.setAgency(userid);
        command.setAgencyDepth(user.getAgencyDepth());
        List<PowerLottoSaleItem> saleItems = shareMapper.currentSaleDepthNewPbLotto(command);

        try {

            for(PowerLottoSaleItem si1 : saleItems){
                long tmpMoney1 = 0;
                long tmpMoney2 = 0;
                long tmpMoney3 = 0;
                long tmpMoney4 = 0;
                long tmpMoney5 = 0;
                long tmpMoney6 = 0;
                long tmpMoney7 = 0;
                for(PowerLottoSaleItem si2 : saleItems){
                    if(si1.getAgencyDepth() == 1 && si2.getAgencyDepth() == 2 && si1.getAgency1().equals(si2.getAgency1())){
                        if(si2.getCalcMoney() > 0) tmpMoney1 += si2.getCalcMoney();
                    }else if(si1.getAgencyDepth() == 2 && si2.getAgencyDepth() == 3 && si1.getAgency2().equals(si2.getAgency2())){
                        if(si2.getCalcMoney() > 0) tmpMoney2 += si2.getCalcMoney();
                    }else if(si1.getAgencyDepth() == 3 && si2.getAgencyDepth() == 4 && si1.getAgency3().equals(si2.getAgency3())){
                        if(si2.getCalcMoney() > 0) tmpMoney3 += si2.getCalcMoney();
                    }else if(si1.getAgencyDepth() == 4 && si2.getAgencyDepth() == 5 && si1.getAgency4().equals(si2.getAgency4())){
                        if(si2.getCalcMoney() > 0) tmpMoney4 += si2.getCalcMoney();
                    }else if(si1.getAgencyDepth() == 5 && si2.getAgencyDepth() == 6 && si1.getAgency5().equals(si2.getAgency5())){
                        if(si2.getCalcMoney() > 0) tmpMoney5 += si2.getCalcMoney();
                    }else if(si1.getAgencyDepth() == 6 && si2.getAgencyDepth() == 7 && si1.getAgency6().equals(si2.getAgency6())){
                        if(si2.getCalcMoney() > 0) tmpMoney6 += si2.getCalcMoney();
                    }
                }
                si1.setTmpMoney1(tmpMoney1);
                si1.setTmpMoney2(tmpMoney2);
                si1.setTmpMoney3(tmpMoney3);
                si1.setTmpMoney4(tmpMoney4);
                si1.setTmpMoney5(tmpMoney5);
                si1.setTmpMoney6(tmpMoney6);
                si1.setTmpMoney7(tmpMoney7);
            }

            for (PowerLottoSaleItem item : saleItems) {
                List<PowerLottoSaleItem> Items = new ArrayList<>();
                if(item.getAgencyDepth() == 1){
                    item.setLastMoney(item.getCalcMoney() - item.getTmpMoney1());
                }else if(item.getAgencyDepth() == 2){
                    item.setLastMoney(item.getCalcMoney() - item.getTmpMoney2());
                }else if(item.getAgencyDepth() == 3){
                    item.setLastMoney(item.getCalcMoney() - item.getTmpMoney3());
                }else if(item.getAgencyDepth() == 4){
                    item.setLastMoney(item.getCalcMoney() - item.getTmpMoney4());
                }else if(item.getAgencyDepth() == 5){
                    item.setLastMoney(item.getCalcMoney() - item.getTmpMoney5());
                }else if(item.getAgencyDepth() == 6){
                    item.setLastMoney(item.getCalcMoney() - item.getTmpMoney6());
                }else{
                    item.setLastMoney(item.getCalcMoney());
                }

                item.setRegDate(new Date());

                Items.add(item);

                PowerLottoSale sale = new PowerLottoSale();
                sale.setUserid(userid);
                sale.setAgency1(user.getAgency1());
                sale.setAgency2(user.getAgency2());
                sale.setAgency3(user.getAgency3());
                sale.setAgency4(user.getAgency4());
                sale.setAgency5(user.getAgency5());
                sale.setAgency6(user.getAgency6());
                sale.setAgency7(user.getAgency7());
                sale.setRole(user.getRole());
                sale.setRegDate(new Date());
                sale.setPowerLottoSaleItems(Items);

                powerLottoSaleRepository.saveAndFlush(sale);
            }



        } catch (RuntimeException e) {
            log.error("{} - 총판 정산에 실패하였습니다.: {}", e.getMessage(), userid);
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, userid + " 총판 정산에 실패 하였습니다.");
        }

        return new AjaxResult(true, userid + " 총판 정산을 완료하였습니다.");
    }

    @Transactional(readOnly = true)
    @Override
    public Page<PowerLottoSale> getPagePbLotto(SaleDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();
        return powerLottoSaleRepository.findAll(builder, pageable);
    }
}
