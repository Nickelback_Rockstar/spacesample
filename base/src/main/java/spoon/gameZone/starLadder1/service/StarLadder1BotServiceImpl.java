package spoon.gameZone.starLadder1.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.starLadder1.QStarLadder1;
import spoon.gameZone.starLadder1.StarLadder1;
import spoon.gameZone.starLadder1.StarLadder1Repository;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class StarLadder1BotServiceImpl implements StarLadder1BotService {

    private StarLadder1GameService starLadder1GameService;

    private StarLadder1Repository starLadder1Repository;

    private static QStarLadder1 q = QStarLadder1.starLadder1;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return starLadder1Repository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(StarLadder1 starLadder1) {
        starLadder1Repository.saveAndFlush(starLadder1);
    }

    @Transactional
    @Override
    public boolean closingGame(StarLadder1 result) {
        StarLadder1 starLadder1 = starLadder1Repository.findOne(q.sdate.eq(result.getSdate()));
        if (starLadder1 == null) {
            return true;
        }

        try {
            starLadder1.setOddeven(result.getOddeven());
            starLadder1.setLine(result.getLine());
            starLadder1.setStart(result.getStart());
            starLadder1.setClosing(true);

            starLadder1Repository.saveAndFlush(starLadder1);
            starLadder1GameService.closingBetting(starLadder1);
        } catch (RuntimeException e) {
            log.error("별다리1분 {}회차 결과 업데이트에 실패하였습니다. - {}", starLadder1.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = starLadder1Repository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getStarLadder1().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        starLadder1Repository.deleteGame(DateUtils.beforeDays(days));
    }
}
