package spoon.gameZone.soccer.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.soccer.QSoccer;
import spoon.gameZone.soccer.Soccer;
import spoon.gameZone.soccer.SoccerDto;
import spoon.gameZone.soccer.SoccerRepository;

import java.text.DecimalFormat;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class SoccerBotServiceImpl implements SoccerBotService {

    private SoccerGameService soccerGameService;

    private SoccerRepository soccerRepository;

    private static QSoccer q = QSoccer.soccer;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return soccerRepository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(Soccer soccer) {

        soccer.setMaHome(getOdds(soccer.getMaHome()));
        soccer.setMaDraw(getOdds(soccer.getMaDraw()));
        soccer.setMaAway(getOdds(soccer.getMaAway()));

        double ouH = soccer.getOuHome();
        double ouA = soccer.getOuAway();
        double sum = ouH + ouA;
        if(sum > 3.6){
            if(ouH > ouA) {
                ouH = ouH - (sum - 3.6);
            }else{
                ouA = ouA - (sum - 3.6);
            }

        }
        soccer.setOuHome(ouH);
        soccer.setOuAway(ouA);

        soccerRepository.saveAndFlush(soccer);
    }

    public double getOdds(double odds){
        odds = odds * 0.85;
        DecimalFormat form = new DecimalFormat("#.##");
        odds = Double.parseDouble(form.format( odds ));
        return odds;
    }

    public double getOddsOU(double odds){
        double val = 12.2345324;
        DecimalFormat form = new DecimalFormat("#.#");
        String dVal = form.format( val );
        //System.out.println( dVal );
        return odds;
    }

    @Transactional
    @Override
    public boolean closingGame(Soccer result) {
        Soccer soccer = soccerRepository.findOne(q.sdate.eq(result.getSdate()));
        if (soccer == null) {
            return true;
        }

        try {
            SoccerDto.Score score = new SoccerDto.Score();
            score.setScoreHome(result.getScoreHome());
            score.setScoreAway(result.getScoreAway());
            soccer.updateScore(score);

            soccerRepository.saveAndFlush(soccer);
            soccerGameService.closingBetting(soccer);
        } catch (RuntimeException e) {
            log.error("가상축구 {}회차 결과 업데이트에 실패하였습니다. - {}", soccer.getSdate(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = soccerRepository.count(q.gameDate.before(DateUtils.beforeMinutes(3)).and(q.closing.isFalse()));
        ZoneConfig.getSoccer().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        soccerRepository.deleteGame(DateUtils.beforeDays(days));
    }
}
