package spoon.member.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.ErrorUtils;
import spoon.common.utils.StringUtils;
import spoon.common.utils.WebUtils;
import spoon.config.domain.Config;
import spoon.member.domain.MemberDto;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.monitor.service.MonitorService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Service
public class MemberUpdateServiceImpl implements MemberUpdateService {

    private MemberService memberService;

    private BCryptPasswordEncoder passwordEncoder;

    private MonitorService monitorService;

    @Transactional
    @Override
    public AjaxResult enabled(String userid) {
        Member member = memberService.getMember(userid);

/*        if (StringUtils.empty(member.getVirtualId()) && !"".equals(Config.getSiteConfig().getVirtualAccountId())) {
            return new AjaxResult(false, "가상계좌 미등록 회원입니다. 수동 등록이 필요합니다.");
        }*/

        boolean enabled = !member.isEnabled();
        member.setEnabled(enabled);

        memberService.update(member);

        AjaxResult result = new AjaxResult(true, member.getUserid() + "님을 " + (enabled ? "승인완료" : "승인대기") + "로 변경하였습니다.");
        result.setValue(enabled ? "Y" : "N");

        monitorService.checkMember();
        return result;
    }

    @Transactional
    @Override
    public AjaxResult black(String userid) {
        Member member = memberService.getMember(userid);
        boolean black = !member.isBlack();
        member.setBlack(black);

        AjaxResult result = new AjaxResult(true, member.getUserid() + "님을 " + (black ? "블랙처리" : "블랙해제") + " 하였습니다.");
        result.setValue(black ? "Y" : "N");
        return result;
    }

    @Transactional
    @Override
    public AjaxResult adminUpdate(MemberDto.Update update) {
        Member member = memberService.getMember(update.getUserid());
        if (member == null) return new AjaxResult(false, "업데이트를 수행 할 수 없습니다.");

        if (member.getRole().getValue() > WebUtils.role().getValue()) return new AjaxResult(false, "회원수정 권한이 없습니다.");

        try {
            // 비밀번호 변경
            if (StringUtils.notEmpty(update.getPassword())) {
                member.setPassword(passwordEncoder.encode(update.getPassword()));
            }

            member.setNickname(update.getNickname());
            member.setLevel(update.getLevel());
            member.setEnabled(update.isEnabled());
            member.setBlack(update.isBlack());
            member.setBlock(update.isBlock());
            member.setSecession(update.isSecession());
            member.setBalanceLadder(update.isBalanceLadder());
            member.setBalanceDari(update.isBalanceDari());
            member.setBalanceLowhi(update.isBalanceLowhi());
            member.setBalanceAladdin(update.isBalanceAladdin());
            //갓만 밸런스 수정가능
            if(WebUtils.role() == Role.GOD) {
                member.setBalancePower(update.isBalancePower());
                member.setBalancePowerLadder(update.isBalancePowerLadder());
                member.setBalancePowerRe(update.isBalancePowerRe());
                member.setBalancePowerLadderRe(update.isBalancePowerLadderRe());
            }
            member.setBalanceLeak(update.isBalanceLeak());
            member.setBalanceDariLeak(update.isBalanceDariLeak());
            member.setBalanceLadderLeak(update.isBalanceLadderLeak());
            member.setBalancePowerLeak(update.isBalancePowerLeak());
            member.setBalanceDariReverse(update.isBalanceDariReverse());
            member.setBalanceLadderReverse(update.isBalanceLadderReverse());
            member.setBalancePowerReverse(update.isBalancePowerReverse());
            member.setPhone(update.getPhone().replaceAll("[^0-9]", ""));
            member.setRecommender(update.getRecommender());
            member.setMemo(update.getMemo());
            member.setBank(update.getBank());
            member.setBankPassword(update.getBankPassword());
            member.setDepositor(update.getDepositor());
            member.setAccount(update.getAccount());
            member.setVirtualId(update.getVirtualId());
            member.setFolderBlock(update.isFolderBlock());

            // 유저, 더미일 경우만 총판정보를 업데이트 한다.
//           if (member.getRole() == Role.USER || member.getRole() == Role.DUMMY) {
//                member.setAgency1(update.getAgency1());
//                member.setAgency2(update.getAgency2());
//            }

            if (StringUtils.empty(member.getPassKey())) {
                member.setPassKey(member.getPhone());
            }
            memberService.update(member);
            monitorService.checkMember();
        } catch (RuntimeException e) {
            log.error("{} - 관리자 회원수정에 실패하였습니다.: {}", e.getMessage(), update.toString());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, update.getUserid() + "님 회원정보 수정에 실패 하였습니다.");
        }

        return new AjaxResult(true, update.getUserid() + "님의 회원정보를 수정 하였습니다.");
    }

    @Transactional
    @Override
    public AjaxResult agencyUpdate(MemberDto.Update update) {
        Member member = memberService.getMember(update.getUserid());
        Member agencyMember = null;
        if (StringUtils.notEmpty(update.getAgency())) {
            agencyMember = memberService.getMember(update.getAgency());
        }

        if (member == null) return new AjaxResult(false, "업데이트를 수행 할 수 없습니다.");

        if (member.getRole().getValue() > WebUtils.role().getValue()) return new AjaxResult(false, "회원수정 권한이 없습니다.");

        try {
            if (agencyMember != null) {
                member.setAgency1(agencyMember.getAgency1());
                member.setAgency2(agencyMember.getAgency2());
            } else {
                member.setAgency1("");
                member.setAgency2("");
            }

            memberService.update(member);
            monitorService.checkMember();

        } catch (RuntimeException e) {
            log.error("{} - 관리자 회원수정에 실패하였습니다.: {}", e.getMessage(), update.toString());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, update.getUserid() + "님 회원정보 수정에 실패 하였습니다.");
        }

        return new AjaxResult(true, update.getUserid() + "님의 총판정보를 수정 하였습니다.");
    }

    @Override
    public AjaxResult adminDelete(MemberDto.Update delete) {
        Member member = memberService.getMember(delete.getUserid());
        if (member == null) return new AjaxResult(false, "회원삭제를 수행 할 수 없습니다.");

        if (member.getRole().getValue() > WebUtils.role().getValue()) return new AjaxResult(false, "회원삭제 권한이 없습니다.");

        memberService.deleteMember(delete.getUserid());

        return new AjaxResult(true, delete.getUserid() + "아이디가 완전삭제 되었습니다.");
    }

    @Override
    public AjaxResult alarm(String userid) {
        Member member = memberService.getMember(userid);
        boolean alarm = !member.isAlarm();
        member.setAlarm(alarm);
        memberService.update(member);

        AjaxResult result = new AjaxResult(true, member.getUserid() + "님을 " + (alarm ? "알람처리" : "알람해제") + " 하였습니다.");
        result.setValue(alarm ? "Y" : "N");

        monitorService.checkMember();
        return result;
    }

    @Transactional
    @Override
    public AjaxResult vAccount(String userid) {
        /*
        //회원 승인 처리 로직으로 변경. 이건 사용안함
        AjaxResult result = new AjaxResult(true, "none");
        Member member = memberService.getMember(userid);
        String virtualId = member.getVirtualId();
        if(StringUtils.empty(virtualId)) {
            String returnId = send(member);
            if(!StringUtils.empty(returnId)) {
                member.setVirtualId(returnId);
                memberService.update(member);
                result = new AjaxResult(true, member.getUserid() + "님을 가상계좌 회원으로 등록 하였습니다.");
                result.setValue("Y");
            }
        }
        return result;
        */
        return null;
    }

    @Override
    public AjaxResult powerLottoUpdate(MemberDto.Update update) {
        Member member = memberService.getMember(update.getUserid());
        if (member == null) return new AjaxResult(false, "업데이트를 수행 할 수 없습니다.");
        member.setPowerLotto(true);
        try {
            memberService.update(member);
        } catch (RuntimeException e) {
            log.error("{} - 가입에 실패하였습니다.: {}", e.getMessage(), update.toString());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, update.getUserid() + "님 가입에 실패 하였습니다.");
        }

        return new AjaxResult(true, update.getUserid() + "님이 가입되었습니다.");
    }

}
