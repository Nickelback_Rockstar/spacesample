package spoon.seller.domain;

import lombok.Data;

public class SellerDto {

    @Data
    public static class Command {
        private String agency1;
        private String agency2;
        private String agency3;
        private String agency4;
        private String agency5;
        private String agency6;
        private String agency7;
        private int agencyDepth;
    }

    @Data
    public static class Update {
        private String userid;
        private String agencyDepth;
        private String rateCode;
        private double rateShare;
        private double rateSports;
        private double rateSports2;
        private double rateZone;
        private double rateZone1;
        private double rateZone2;
        private double rateCasino;
        private double ratePowerLotto;
    }
}
