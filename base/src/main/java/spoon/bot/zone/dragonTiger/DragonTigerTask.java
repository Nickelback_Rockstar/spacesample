package spoon.bot.zone.dragonTiger;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class DragonTigerTask {

    private GameBotParsing dragonTigerParsing;

    @Scheduled(cron = "1 * * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        dragonTigerParsing.parsingGame();
    }

    @Scheduled(cron = "3,5,7 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        dragonTigerParsing.closingGame();
    }

    @Scheduled(cron = "1 * * * * *")
    public void checkResult() {
        if (notParsing()) return;
        dragonTigerParsing.checkResult();
    }

    @Scheduled(cron = "5 1 4 * * * ")
    public void deleteGame() {
        dragonTigerParsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isDragonTiger() || !ZoneConfig.getDragonTiger().isEnabled();
    }

}
