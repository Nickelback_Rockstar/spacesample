package spoon.game.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import spoon.game.entity.Game;

public interface GameRepository extends JpaRepository<Game, Long>, QueryDslPredicateExecutor<Game> {

    @Query(value = "SELECT MAX(g.ut) FROM Game g WHERE g.siteCode = :siteCode AND g.closing = :closing")
    String findMaxUt(@Param("siteCode") String siteCode, @Param("closing") boolean closing);

    @Query(value = "SELECT MIN(g.ut) FROM Game g WHERE g.siteCode = :siteCode")
    String minUt(@Param("siteCode") String siteCode);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE Game g SET ut = :ut WHERE siteCode = :siteCode AND g.gameDate > CURRENT_TIMESTAMP")
    void reload(@Param("siteCode") String siteCode, @Param("ut") String ut);
}
