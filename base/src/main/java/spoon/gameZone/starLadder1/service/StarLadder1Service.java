package spoon.gameZone.starLadder1.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.starLadder1.StarLadder1;
import spoon.gameZone.starLadder1.StarLadder1Config;
import spoon.gameZone.starLadder1.StarLadder1Dto;
import spoon.support.web.AjaxResult;

public interface StarLadder1Service {

    /**
     * 별다리1분 설정을 변경한다.
     */
    boolean updateConfig(StarLadder1Config starLadder1Config);

    /**
     * 별다리1분 등록된 게임을 가져온다.
     */
    Iterable<StarLadder1> getComplete();

    /**
     * 별다리1분 종료된 게임을 가져온다.
     */
    Page<StarLadder1> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * 별다리1분 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    StarLadder1Dto.Score findScore(Long id);

    /**
     * 별다리1분 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(StarLadder1Dto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    StarLadder1Dto.Config gameConfig();

}
