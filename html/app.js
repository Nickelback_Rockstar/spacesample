'use strict';

// Load express
var express = require('express');
var app = express();

// Load task
app.use(express.static('./public'));
app.listen(8888);
console.log('Server running at 8888 port');
