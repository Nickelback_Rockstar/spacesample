package spoon.bot.balance.service.gate;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bet.domain.BetDto;
import spoon.bet.service.BetService;
import spoon.bot.balance.entity.GodBalance;
import spoon.bot.balance.repository.GodBalanceRepository;
import spoon.bot.support.PowerMaker;
import spoon.common.net.HttpParsing;
import spoon.common.utils.DateUtils;
import spoon.common.utils.StringUtils;
import spoon.config.domain.Config;
import spoon.event.entity.Logger;
import spoon.event.service.LoggerService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.powerLadder.PowerLadder;
import spoon.gameZone.powerLadder.PowerLadderRepository;
import spoon.gameZone.powerLadder.QPowerLadder;

import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class GodPowerLadderService {

    private BetService betService;

    private GodBalanceRepository godBalanceRepository;

    private LoggerService loggerService;

    private PowerLadderRepository powerLadderRepository;

    @Async
    public void balance() {

        double rate = Config.getSysConfig().getZone().getGodPowerLadderBalanceRate();
        double rateHp = Config.getSysConfig().getZone().getGodPowerLadderBalanceHpRate();
        double rate1 = Config.getSysConfig().getZone().getGodPowerLadderBalance1Rate();
        double rate2 = Config.getSysConfig().getZone().getGodPowerLadderBalance2Rate();

        if ( rate == 0 && rateHp == 0 && rate1 == 0 && rate2 == 0 ) return;

        PowerMaker powerMaker = ZoneConfig.getPower().getPowerMaker();
        long times = powerMaker.getTimes() + 1;

        Date gameDate = powerMaker.getGameDate(times);
        String sdate = DateUtils.format(gameDate, "yyyyMMddHHmm");

        long calc;
        long odd = 0, even = 0, left = 0, right = 0, line3 = 0, line4 = 0;
        long totalOe = 0, totalSt = 0, totalLi = 0;
        int posOe = 0, posSt = 0, posLi = 0;
        boolean canOe = false, canSt = false, canLi = false;
        String round = String.format("%03d", times);

        List<BetDto.BetBalGame> items = betService.getGodBalanceBet(MenuCode.POWER_LADDER, sdate);

        long money = 500000;
        long donwMoney = 250000;

        for (BetDto.BetBalGame item : items) {
            if(item.getBetMoney() >= money){
                item.setBetMoney(donwMoney);
            }
            switch (item.getSpecial()) {
                case "oddeven":
                    if ("home".equals(item.getBetTeam())) {
                        odd += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        even += item.getBetMoney();
                    }
                    break;
                case "start":
                    if ("home".equals(item.getBetTeam())) {
                        left += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        right += item.getBetMoney();
                    }
                    break;
                case "line":
                    if ("home".equals(item.getBetTeam())) {
                        line3 += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        line4 += item.getBetMoney();
                    }
                    break;
            }
        }

        List<BetDto.BetBalGame> itemsRe = betService.getGodBalanceReBet(MenuCode.POWER_LADDER, sdate);

        for (BetDto.BetBalGame item : itemsRe) {
            if(item.getBetMoney() >= money){
                item.setBetMoney(donwMoney);
            }
            switch (item.getSpecial()) {
                case "oddeven":
                    if ("home".equals(item.getBetTeam())) {
                        even += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        odd += item.getBetMoney();
                    }
                    break;
                case "start":
                    if ("home".equals(item.getBetTeam())) {
                        right += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        left += item.getBetMoney();
                    }
                    break;
                case "line":
                    if ("home".equals(item.getBetTeam())) {
                        line4 += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        line3 += item.getBetMoney();
                    }
                    break;
            }
        }

//        //System.out.println("sdate=" + sdate);
//
//        //System.out.println("left=" + left);
//        //System.out.println("right=" + right);
//
//        //System.out.println("line3=" + line3);
//        //System.out.println("line4=" + line4);
//
//        //System.out.println("odd=" + odd);
//        //System.out.println("even=" + even);



        if(Config.getSysConfig().getZone().isGodPowerLadderBalance()) {
            //System.out.println("스타 사다리 시작");
            // 홀짝
            calc = (long) ((odd - even) * rate / 100);
            if (calc >= 10) {
                totalOe = calc;
                posOe = 2;
                canOe = true;
            }
            calc = (long) ((even - odd) * rate / 100);
            if (calc >= 10) {
                totalOe = calc;
                posOe = 1;
                canOe = true;
            }

            // 좌우
            calc = (long) ((left - right) * rate / 100);
            if (calc >= 10) {
                totalSt = calc;
                posSt = 2;
                canSt = true;
            }
            calc = (long) ((right - left) * rate / 100);
            if (calc >= 10) {
                totalSt = calc;
                posSt = 1;
                canSt = true;
            }

            // 줄수
            calc = (long) ((line3 - line4) * rate / 100);
            if (calc >= 10) {
                totalLi = calc;
                posLi = 2;
                canLi = true;
            }
            calc = (long) ((line4 - line3) * rate / 100);
            if (calc >= 10) {
                totalLi = calc;
                posLi = 1;
                canLi = true;
            }

            if (canOe) {
                String url = Config.getSysConfig().getZone().getGodPowerLadderBalanceUrl()
                        + "?UserId=" + Config.getSysConfig().getZone().getGodPowerLadderBalanceId()
                        + "&AuthCode=" + Config.getSysConfig().getZone().getGodPowerLadderBalanceKey()
                        + "&SiteName=STAR"
                        + "&GameType=F"
                        + "&BetMoney=" + totalOe
                        + "&PickNum=" + posOe
                        + "&GameTypeSub=A";
                sendQuery(url, sdate, round, "홀짝", posOe == 1 ? "홀" : "짝", totalOe, totalOe);
            }

            if (canSt) {
                String url = Config.getSysConfig().getZone().getGodPowerLadderBalanceUrl()
                        + "?UserId=" + Config.getSysConfig().getZone().getGodPowerLadderBalanceId()
                        + "&AuthCode=" + Config.getSysConfig().getZone().getGodPowerLadderBalanceKey()
                        + "&SiteName=STAR"
                        + "&GameType=F"
                        + "&BetMoney=" + totalSt
                        + "&PickNum=" + posSt
                        + "&GameTypeSub=B";
                sendQuery(url, sdate, round, "좌우", posSt == 1 ? "좌" : "우", totalSt, totalSt);
            }

            if (canLi) {
                String url = Config.getSysConfig().getZone().getGodPowerLadderBalanceUrl()
                        + "?UserId=" + Config.getSysConfig().getZone().getGodPowerLadderBalanceId()
                        + "&AuthCode=" + Config.getSysConfig().getZone().getGodPowerLadderBalanceKey()
                        + "&SiteName=STAR"
                        + "&GameType=F"
                        + "&BetMoney=" + totalLi
                        + "&PickNum=" + posLi
                        + "&GameTypeSub=C";
                sendQuery(url, sdate, round, "라인", posLi == 1 ? "3줄" : "4줄", totalLi, totalLi);
            }
        }


        QPowerLadder q = QPowerLadder.powerLadder;
        PowerLadder powerLadder = powerLadderRepository.findOne(q.sdate.eq(sdate));

        if(Config.getSysConfig().getZone().isGodPowerLadderBalanceHp()) {
            sendBalance(powerLadder, sdate, round, rateHp, odd, even, left, right, line3, line4, Config.getSysConfig().getZone().getGodPowerLadderBalanceHpUrl(), Config.getSysConfig().getZone().getGodPowerLadderBalanceHpId(), Config.getSysConfig().getZone().getGodPowerLadderBalanceHpKey());
        }

        if(Config.getSysConfig().getZone().isGodPowerLadderBalance1()) {
            sendBalance(powerLadder, sdate, round, rateHp, odd, even, left, right, line3, line4, Config.getSysConfig().getZone().getGodPowerLadderBalance1Url(), Config.getSysConfig().getZone().getGodPowerLadderBalance1Id(), Config.getSysConfig().getZone().getGodPowerLadderBalance1Key());
        }

        if(Config.getSysConfig().getZone().isGodPowerLadderBalance2()) {
            sendBalance(powerLadder, sdate, round, rateHp, odd, even, left, right, line3, line4, Config.getSysConfig().getZone().getGodPowerLadderBalance2Url(), Config.getSysConfig().getZone().getGodPowerLadderBalance2Id(), Config.getSysConfig().getZone().getGodPowerLadderBalance2Key());
        }

    }

    public void sendBalance(PowerLadder powerLadder, String sdate, String round, double rate, long odd, long even, long left, long right, long line3, long line4, String url, String id, String key){
        long calc;
        long totalOe = 0, totalSt = 0, totalLi = 0;
        int posOe = 0, posSt = 0, posLi = 0;
        boolean canOe = false, canSt = false, canLi = false;

        // 홀짝
        calc = (long) ((odd - even) * rate / 100);
        if (calc >= 10) {
            totalOe = calc;
            posOe = 2;
            canOe = true;
        }
        calc = (long) ((even - odd) * rate / 100);
        if (calc >= 10) {
            totalOe = calc;
            posOe = 1;
            canOe = true;
        }

        // 좌우
        calc = (long) ((left - right) * rate / 100);
        if (calc >= 10) {
            totalSt = calc;
            posSt = 2;
            canSt = true;
        }
        calc = (long) ((right - left) * rate / 100);
        if (calc >= 10) {
            totalSt = calc;
            posSt = 1;
            canSt = true;
        }

        // 줄수
        calc = (long) ((line3 - line4) * rate / 100);
        if (calc >= 10) {
            totalLi = calc;
            posLi = 2;
            canLi = true;
        }
        calc = (long) ((line4 - line3) * rate / 100);
        if (calc >= 10) {
            totalLi = calc;
            posLi = 1;
            canLi = true;
        }

        long pp1 = 0;//좌
        long pp2 = 0;//우

        long pp3 = 0;//3줄
        long pp4 = 0;//4줄

        long pp5 = 0;//홀
        long pp6 = 0;//짝

        if (canOe) {
            if (posOe == 1) {
                pp5 = totalOe; //홀
            } else if (posOe == 2) {
                pp6 = totalOe; //짝
            }
        }

        if (canSt) {
            if (posSt == 1) {
                pp1 = totalSt; //좌
            } else if (posSt == 2) {
                pp2 = totalSt; //우
            }
        }

        if (canLi) {
            if (posLi == 1) {
                pp3 = totalLi;
            } else if (posLi == 2) {
                pp4 = totalLi;
            }
        }

        url += "?gm=PSA";
        url += "&userid=" + id;
        url += "&key=" + key;
        url += "&tdate=" + powerLadder.getSdate().substring(0, 8);
        url += "&rno=" + powerLadder.getRound();
        if (pp1 > 0) url += "&pp1=" + pp1;
        if (pp2 > 0) url += "&pp2=" + pp2;
        if (pp3 > 0) url += "&pp3=" + pp3;
        if (pp4 > 0) url += "&pp4=" + pp4;
        if (pp5 > 0) url += "&pp5=" + pp5;
        if (pp6 > 0) url += "&pp6=" + pp6;
        url += "&nonce=88888";

        String bpm = "";
        long p = pp1 + pp2 + pp3 + pp4 + pp5 + pp6;
        if (p > 0) {
            sendQuery(url, sdate, round, pp1, pp2, pp3, pp4, pp5, pp6, bpm, id);
        }
    }

    private void sendQuery(String param, String betDate, String round, String gameType, String betType, long price, long viewPrice) {

        Logger logger = new Logger();
        logger.setCode("GOD_PLADDER_BALANCE");
        logger.setData(param);
        logger.setRegDate(new Date());
        loggerService.addLog(logger);

        //System.out.println("param1 = "+param);

        String json = HttpParsing.getJson(param);
        if (json == null) return;

        JSONParser parser = new JSONParser();
        JSONObject jsonObj = new JSONObject();
        if (!StringUtils.empty(json)) {
            try {
                jsonObj = (JSONObject) parser.parse(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        GodBalance b = new GodBalance();
        b.setGame("갓파워사다리");
        b.setGameDate(betDate);
        b.setRound(round);
        b.setGameType(gameType);
        b.setBetType(betType);
        b.setPrice(viewPrice);
        b.setRealPrice(price);
        b.setRegDate(new Date());
        b.setMessage(jsonObj.get("resultcode").toString());
        godBalanceRepository.saveAndFlush(b);

    }

    private void sendQuery(String param, String betDate, String round, long pp1, long pp2, long pp3, long pp4, long pp5, long pp6, String bpm, String id) {

        Logger logger = new Logger();
        logger.setCode("GOD_PL_B_"+id);
        logger.setData(param);
        logger.setRegDate(new Date());
        loggerService.addLog(logger);

        //System.out.println("paramPLH1=" + param);

        String json = HttpParsing.getJson(param);

        if (json == null) return;

        String resultcode = "";
        JSONParser parser = new JSONParser();
        JSONObject jsonObj = new JSONObject();
        if (!StringUtils.empty(json)) {
            try {
                jsonObj = (JSONObject) parser.parse(json);
                resultcode = jsonObj.get("comment").toString()+" / "+jsonObj.get("more_info").toString();
            } catch (Exception e) {
                resultcode = "리턴값 에러남요";
            }
        }

        resultcode += bpm;

        saveBalance("파워사다리H 홀짝", betDate, round, "홀", pp5, resultcode, id);
        saveBalance("파워사다리H 홀짝", betDate, round, "짝", pp6, resultcode, id);
        saveBalance("파워사다리H 좌우", betDate, round, "좌", pp1, resultcode, id);
        saveBalance("파워사다리H 좌우", betDate, round, "우", pp2, resultcode, id);
        saveBalance("파워사다리H 3줄4줄", betDate, round, "3줄", pp3, resultcode, id);
        saveBalance("파워사다리H 3줄4줄", betDate, round, "4줄", pp4, resultcode, id);

    }

    private void saveBalance(String gameType, String betDate, String round, String betType, long price, String resultcode, String id) {

        if (price > 0) {
            GodBalance b = new GodBalance();
            b.setGame("갓파사다리"+id);
            b.setGameDate(betDate);
            b.setRound(round);
            b.setGameType(gameType);
            b.setBetType(betType);
            b.setPrice(price);
            b.setRealPrice(price);
            b.setRegDate(new Date());
            b.setMessage(resultcode);
            godBalanceRepository.saveAndFlush(b);
        }
    }

}
