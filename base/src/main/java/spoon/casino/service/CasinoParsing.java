package spoon.casino.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.casino.domain.BetCasinoDto;
import spoon.casino.entity.BetCasino;
import spoon.casino.repository.BetCasinoRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.DateUtils;
import spoon.common.utils.NumberUtils;
import spoon.common.utils.StringUtils;
import spoon.mapper.BetMapper;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.payment.domain.PointCode;
import spoon.payment.service.PaymentService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class CasinoParsing {

    private BetCasinoRepository betCasinoRepository;
    private BetMapper betMapper;
    private PaymentService paymentService;
    private MemberService memberService;

    @Async
    public void parsing(int page) {
        //System.out.println("카지노 베팅내역 배치" + page);

        Date sDate = DateUtils.parse(DateUtils.format(DateUtils.beforeHours(2), "yyyy-MM-dd HH") + ":00:00", "yyyy-MM-dd HH:mm:ss");
        Date eDate = DateUtils.parse(DateUtils.format(DateUtils.beforeHours(1), "yyyy-MM-dd HH") + ":00:00", "yyyy-MM-dd HH:mm:ss");
        String startDate = DateUtils.format(sDate, "yyyy-MM-dd HH") + ":00:00";
        String endDate = DateUtils.format(eDate, "yyyy-MM-dd HH") + ":00:00";

        String tableStartDate = betMapper.casinoMaxDate();
        if (StringUtils.notEmpty(tableStartDate)) {//마지막 시간 가져옴.
            startDate = DateUtils.format(DateUtils.parse(tableStartDate, "yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH") + ":00:00";
        }

        //System.out.println("startDate="+startDate);
        //System.out.println("endDate="+endDate);

        String url = "http://fafagogo.com/block/casinoApi?mode=getBetWinHistoryAll" +
                "&startDate=" + startDate +
                "&endDate=" + endDate +
                "&operatorID=mobimobi" +
                "&pageSize=2000" +
                "&pageStart=" + page +
                "&vendorID=0";

        JSONObject jsonObj;
        String json = HttpParsing.getJson2(url);

//        System.out.println("========================================================");
//        System.out.println(url);
//        System.out.println(json);
//        System.out.println("========================================================");

        //System.out.println("========================================================");
        JSONParser parser = new JSONParser();
        if (!StringUtils.empty(json)) {
            try {
                jsonObj = (JSONObject) parser.parse(json);
                if ("0".equals(jsonObj.get("returnCode").toString())) {
                    JSONArray arr = (JSONArray) jsonObj.get("history");
                    for (int i = 0; i < arr.size(); i++) {
                        JSONObject j = (JSONObject) arr.get(i);
                        if ("BET".equals(StringUtils.nvl(j.get("transType")))) {
                            try {
                                BetCasino b = new BetCasino();
                                b.setGameID(StringUtils.nvl(j.get("gameID")));
                                b.setThirdParty(Integer.parseInt(j.get("thirdParty").toString()));
                                b.setAmount((long) Double.parseDouble((j.get("amount").toString())));
                                b.setTransTime(DateUtils.parse(StringUtils.nvl(j.get("transTime")), "yyyy-MM-dd HH:mm:ss"));
                                b.setTransType(StringUtils.nvl(j.get("transType")));
                                String transID = StringUtils.nvl(j.get("transID")).replaceAll("_", "");
                                b.setTransID(transID);
                                b.setTransID2(StringUtils.nvl(j.get("transID")));
                                b.setHistory(StringUtils.nvl(j.get("history")));
                                b.setCategory(StringUtils.nvl(j.get("category")));
                                b.setUserID(StringUtils.nvl(j.get("userID")));
                                b.setRoundID(StringUtils.nvl(j.get("roundID")));
                                b.setRegDate(new Date());
                                b.setStartDate(DateUtils.parse(startDate, "yyyy-MM-dd HH:mm:ss"));
                                b.setEndDate(eDate);
                                b.setClosing(false);
                                //System.out.println(b.toString());
                                betCasinoRepository.saveAndFlush(b);
                            } catch (Exception e) {
                                e.getMessage();
                                e.printStackTrace();
                            }
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //System.out.println("========================================================");

    }

    @Transactional
    public void closing() {
        try {
            HashMap agencyMap = new HashMap();
            List<BetCasinoDto.BetRoll> list = betMapper.casinoBetRollList();
            boolean chk = false;
            for (BetCasinoDto.BetRoll dto : list) {
                if (dto.getBetMoney() > 0) {
                    chk = true;
                    Member agency1 = null;
                    Member agency2 = null;

                    long rollingPoint2 = 0;
                    long rollingPoint3 = 0;

                    if (StringUtils.notEmpty(dto.getAgency2())) {
                        //agency2
                        if (agencyMap.get(dto.getAgency2()) != null) {
                            agency2 = (Member) agencyMap.get(dto.getAgency2());
                        } else {
                            agency2 = memberService.getMember(dto.getAgency2());
                            agencyMap.put(dto.getAgency2(), agency2);
                        }
                        double rate2 = 0;
                        rate2 = agency2.getRateCasino();

                        rollingPoint2 = NumberUtils.lVal(dto.getBetMoney(), rate2) - rollingPoint3;
                        paymentService.addPoint(PointCode.ROLLING_CASINO_ADD, 0L, agency2.getUserid(), rollingPoint2, dto.getBetMoney() + " / " + rate2 + " - " + (rollingPoint3) + " 롤링 포인트");

                    }

                    if (StringUtils.notEmpty(dto.getAgency1())) {

                        //agency1
                        if (agencyMap.get(dto.getAgency1()) != null) {
                            agency1 = (Member) agencyMap.get(dto.getAgency1());
                        } else {
                            agency1 = memberService.getMember(dto.getAgency1());
                            agencyMap.put(dto.getAgency1(), agency1);
                        }
                        double rate1 = 0;
                        rate1 = agency1.getRateCasino();

                        long rollingPoint1 = NumberUtils.lVal(dto.getBetMoney(), rate1) - rollingPoint2 - rollingPoint3;
                        paymentService.addPoint(PointCode.ROLLING_CASINO_ADD, 0L, agency1.getUserid(), rollingPoint1, dto.getBetMoney() + " / " + rate1 + " - " + (rollingPoint2 + rollingPoint3) + " 롤링 포인트");
                    }
                }
            }

            if (chk) {
                betMapper.closingBetCasino();
            }
        } catch (RuntimeException e) {
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
        }
    }

    /*public void closing() {
        System.out.println("카지노 베팅 롤링");
        List<BetCasinoDto.BetRoll> list = betMapper.casinoBetRollList();
        BetCasinoDto.BetRoll tmp = null;
        for(BetCasinoDto.BetRoll dto : list){
            try {
                System.out.println("시작 dto = " + dto.toString());
                if (tmp != null) {
                    System.out.println("tmp = " + tmp.toString());
                }
                //배팅금이 있으면 시작
                if (dto.getBetMoney() > 0 && dto.getAgencyDepth() == 7) {
                    long rollingPoint = NumberUtils.lVal(dto.getBetMoney(), dto.getRateCasino());
                    if(tmp == null){
                        tmp = dto;
                    }
                    tmp.setRollingPoint(tmp.getRollingPoint() + rollingPoint);
                    System.out.println(dto.toString());
                    System.out.println("tmp = " + tmp.toString());
                    System.out.println("7 롤링 포인트-" + rollingPoint);
                    paymentService.addPoint(PointCode.ROLLING_CASINO_ADD, 0L, dto.getUserid(), rollingPoint, "롤링 포인트 7D : " + dto.getBetMoney() +" / "+ dto.getRateCasino());
                } else if (tmp != null && tmp.getRollingPoint() > 0 && dto.getAgencyDepth() == 6 && dto.getAgency6().equals(tmp.getAgency6())) {
                    System.out.println("6========= "+tmp.getBetMoney());
                    System.out.println("betMoney = "+tmp.getBetMoney());
                    System.out.println("rate = "+dto.getRateCasino());
                    System.out.println("remove = "+tmp.getRollingPoint());
                    long rollingPoint = NumberUtils.lVal(tmp.getBetMoney(), dto.getRateCasino()) - tmp.getRollingPoint();
                    System.out.println(dto.toString());
                    System.out.println("6 롤링 포인트-" + tmp.getRollingPoint());
                    paymentService.addPoint(PointCode.ROLLING_CASINO_ADD, 0L, dto.getUserid(), rollingPoint, "롤링 포인트 6D : " + tmp.getBetMoney() +" / "+ dto.getRateCasino() +" - "+tmp.getRollingPoint());
                    tmp.setRollingPoint(tmp.getRollingPoint() + rollingPoint);
                } else if (tmp != null && tmp.getRollingPoint() > 0 && dto.getAgencyDepth() == 5 && dto.getAgency5().equals(tmp.getAgency5())) {
                    System.out.println("5========= "+tmp.getBetMoney());
                    System.out.println("betMoney = "+tmp.getBetMoney());
                    System.out.println("rate = "+dto.getRateCasino());
                    System.out.println("remove = "+tmp.getRollingPoint());
                    long rollingPoint = NumberUtils.lVal(tmp.getBetMoney(), dto.getRateCasino()) - tmp.getRollingPoint();
                    System.out.println(dto.toString());
                    System.out.println("5 롤링 포인트-" + tmp.getRollingPoint());
                    paymentService.addPoint(PointCode.ROLLING_CASINO_ADD, 0L, dto.getUserid(), rollingPoint, "롤링 포인트 5D : " + tmp.getBetMoney() +" / "+ dto.getRateCasino() +" - "+tmp.getRollingPoint());
                    tmp.setRollingPoint(tmp.getRollingPoint() + rollingPoint);
                } else if (tmp != null && tmp.getRollingPoint() > 0 && dto.getAgencyDepth() == 4 && dto.getAgency4().equals(tmp.getAgency4())) {
                    System.out.println("4========= "+tmp.getBetMoney());
                    System.out.println("betMoney = "+tmp.getBetMoney());
                    System.out.println("rate = "+dto.getRateCasino());
                    System.out.println("remove = "+tmp.getRollingPoint());
                    long rollingPoint = NumberUtils.lVal(tmp.getBetMoney(), dto.getRateCasino()) - tmp.getRollingPoint();
                    System.out.println(dto.toString());
                    System.out.println("4 롤링 포인트-" + tmp.getRollingPoint());
                    paymentService.addPoint(PointCode.ROLLING_CASINO_ADD, 0L, dto.getUserid(), rollingPoint, "롤링 포인트 4D : " + tmp.getBetMoney() +" / "+ dto.getRateCasino() +" - "+tmp.getRollingPoint());
                    tmp.setRollingPoint(tmp.getRollingPoint() + rollingPoint);
                } else if (tmp != null && tmp.getRollingPoint() > 0 && dto.getAgencyDepth() == 3 && dto.getAgency3().equals(tmp.getAgency3())) {
                    System.out.println("3========= "+tmp.getBetMoney());
                    System.out.println("betMoney = "+tmp.getBetMoney());
                    System.out.println("rate = "+dto.getRateCasino());
                    System.out.println("remove = "+tmp.getRollingPoint());
                    long rollingPoint = NumberUtils.lVal(tmp.getBetMoney(), dto.getRateCasino()) - tmp.getRollingPoint();
                    System.out.println(dto.toString());
                    System.out.println("3 롤링 포인트-" + tmp.getRollingPoint());
                    paymentService.addPoint(PointCode.ROLLING_CASINO_ADD, 0L, dto.getUserid(), rollingPoint, "롤링 포인트 3D : " + tmp.getBetMoney() +" / "+ dto.getRateCasino() +" - "+tmp.getRollingPoint());
                    tmp.setRollingPoint(tmp.getRollingPoint() + rollingPoint);
                } else if (tmp != null && tmp.getRollingPoint() > 0 && dto.getAgencyDepth() == 2 && dto.getAgency2().equals(tmp.getAgency2())) {
                    System.out.println("2========= "+tmp.getBetMoney());
                    System.out.println("betMoney = "+tmp.getBetMoney());
                    System.out.println("rate = "+dto.getRateCasino());
                    System.out.println("remove = "+tmp.getRollingPoint());
                    long rollingPoint = NumberUtils.lVal(tmp.getBetMoney(), dto.getRateCasino()) - tmp.getRollingPoint();
                    System.out.println(dto.toString());
                    System.out.println("2 롤링 포인트-" + tmp.getRollingPoint());
                    paymentService.addPoint(PointCode.ROLLING_CASINO_ADD, 0L, dto.getUserid(), rollingPoint, "롤링 포인트 2D : " + tmp.getBetMoney() +" / "+ dto.getRateCasino() +" - "+tmp.getRollingPoint());
                    tmp.setRollingPoint(tmp.getRollingPoint() + rollingPoint);
                } else if (tmp != null && tmp.getRollingPoint() > 0 && dto.getAgencyDepth() == 1 && dto.getAgency1().equals(tmp.getAgency1())) {
                    System.out.println("1========= "+tmp.getBetMoney());
                    System.out.println("betMoney = "+tmp.getBetMoney());
                    System.out.println("rate = "+dto.getRateCasino());
                    System.out.println("remove = "+tmp.getRollingPoint());
                    long rollingPoint = NumberUtils.lVal(tmp.getBetMoney(), dto.getRateCasino()) - tmp.getRollingPoint();
                    System.out.println(dto.toString());
                    System.out.println("1 롤링 포인트-" + tmp.getRollingPoint());
                    paymentService.addPoint(PointCode.ROLLING_CASINO_ADD, 0L, dto.getUserid(), rollingPoint, "롤링 포인트 1D : " + tmp.getBetMoney() +" / "+ dto.getRateCasino() +" - "+tmp.getRollingPoint());
                    tmp.setRollingPoint(tmp.getRollingPoint() + rollingPoint);
                } else {
                    tmp = null;
                }
            }catch(Exception e){
                e.getMessage();
                e.printStackTrace();
            }
        }

        if(list != null && list.size() > 0){
            betMapper.closingBetCasino();
        }
    }*/
}
