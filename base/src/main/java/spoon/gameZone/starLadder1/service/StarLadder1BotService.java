package spoon.gameZone.starLadder1.service;

import spoon.gameZone.starLadder1.StarLadder1;

import java.util.Date;

public interface StarLadder1BotService {

    boolean notExist(Date gameDate);

    void addGame(StarLadder1 starLadder1);

    boolean closingGame(StarLadder1 result);

    void checkResult();

    void deleteGame(int days);

}
