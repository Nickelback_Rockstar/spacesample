package spoon.web.seller.agency;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.WebUtils;
import spoon.config.domain.Config;
import spoon.member.domain.CurrentUser;
import spoon.member.domain.MemberDto;
import spoon.member.entity.Member;
import spoon.member.service.MemberAddService;
import spoon.member.service.MemberListService;
import spoon.member.service.MemberService;
import spoon.sale.entity.SaleItem;
import spoon.seller.domain.Seller;
import spoon.support.web.AjaxResult;

import java.util.List;

@AllArgsConstructor
@Controller("seller.sellerController")
@RequestMapping("#{config.pathSeller}")
public class MemberController {

    private MemberListService memberListService;

    private MemberAddService memberAddService;

    private MemberService memberService;

    @RequestMapping(value = "agency/member", method = RequestMethod.GET)
    public String list(ModelMap map, MemberDto.Seller command,
                       @PageableDefault(size = 50, sort = {"joinDate"}, direction = Sort.Direction.DESC) Pageable pageable) {

        CurrentUser user = WebUtils.user();
        command.setAgency1(user.getAgency1());
        command.setAgency2(user.getAgency2());
        command.setAgency3(user.getAgency3());
        command.setAgency4(user.getAgency4());
        command.setAgency5(user.getAgency5());
        command.setAgency6(user.getAgency6());
        command.setAgency7(user.getAgency7());
        command.setAgencyDepth(user.getAgencyDepth());

        map.addAttribute("agencyList", memberService.getAgency7List(command));
        map.addAttribute("command", command);
        map.addAttribute("page", memberListService.sellerList(command, pageable));

        return "seller/agency/member";
    }

    @RequestMapping(value = "agency/agencyMember", method = RequestMethod.GET)
    public String agencyList(ModelMap map, MemberDto.Seller command) {

        CurrentUser user = WebUtils.user();
        command.setAgency1(user.getAgency1());
        command.setAgency2(user.getAgency2());
        command.setAgency3(user.getAgency3());
        command.setAgency4(user.getAgency4());
        command.setAgency5(user.getAgency5());
        command.setAgency6(user.getAgency6());
        command.setAgency7(user.getAgency7());
        command.setAgencyDepth(user.getAgencyDepth());

        List<Seller> list = memberListService.sellerListNew2(command); //추천인 리스트

        for(Seller item : list){
            // '####' 구분자로 합쳐진 문자열 금액들을 배열로 쪼개서 셋해줌
            String[] money = item.getBetZoneMoney().split("####");
            if(money != null && money.length == 3){
                item.setBetSports(Long.parseLong(money[0]));
                item.setBetSports2(Long.parseLong(money[1]));
                item.setBetZone(Long.parseLong(money[2]));
            }
        }

        map.addAttribute("command", command);
        map.addAttribute("list", list);

        return "seller/agency/agencyMember";
    }

    @RequestMapping(value = "agency/add", method = RequestMethod.GET)
    public String add(ModelMap map) {
        Member member = memberService.getMember(WebUtils.userid());
        map.addAttribute("mem", member);
        map.addAttribute("member", new MemberDto.Agency());
        map.addAttribute("banks", Config.getBanks());
        map.addAttribute("pathJoin", Config.getPathJoin());

        return "seller/agency/add";
    }

    @RequestMapping(value = "agency/memberAdd", method = RequestMethod.GET)
    public String memberAdd(ModelMap map) {
        map.addAttribute("member", new MemberDto.Add());
        map.addAttribute("agencies", JsonUtils.toString(memberService.getAgencyList()));
        map.addAttribute("banks", Config.getBanks());
        map.addAttribute("pathJoin", Config.getPathJoin());
        return "seller/agency/memberAdd";
    }

    @ResponseBody
    @RequestMapping(value = "member/add", method = RequestMethod.POST)
    public AjaxResult add(MemberDto.Agency add) {
        return memberAddService.sellerAdd(add);
    }

    @ResponseBody
    @RequestMapping(value = "member/memberAdd", method = RequestMethod.POST)
    public AjaxResult memberAdd(MemberDto.Add add) {
        return memberAddService.sellerMemberAdd(add);
    }

}
