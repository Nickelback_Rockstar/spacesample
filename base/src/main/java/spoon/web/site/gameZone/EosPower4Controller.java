package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.bet.domain.BetDto;
import spoon.bet.service.BetListService;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.WebUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.eosPower4.EosPower4Dto;
import spoon.gameZone.eosPower4.service.EosPower4GameService;
import spoon.gameZone.eosPower4.service.EosPower4Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class EosPower4Controller {

    private EosPower4Service eosPower4Service;

    private EosPower4GameService eosPower4GameService;

    private BetListService betListService;

    @RequestMapping(value = "zone/eosPower4", method = RequestMethod.GET)
    public String zone(ModelMap map, BetDto.UserCommand command, @PageableDefault(size = 10, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        String userid = WebUtils.userid();
        command.setUserid(userid);
        map.addAttribute("powerBetList", betListService.userEosPower4Bet(command, pageable));
        map.addAttribute("config", JsonUtils.toString(eosPower4Service.gameConfig()));
        return "site/zone/eosPower4";
    }

    @RequestMapping(value = "zone/eosPower4/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", eosPower4Service.getClosing(command, pageable));
        return "site/score/eosPower4";
    }

    @ResponseBody
    @RequestMapping(value = "zone/eosPower4/config", method = RequestMethod.POST)
    public EosPower4Dto.Config config() {
        return eosPower4Service.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/eosPower4/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return eosPower4GameService.betting(bet);
    }
}
