package spoon.gameZone.starLadder2.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.starLadder2.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class StarLadder2ServiceImpl implements StarLadder2Service {

    private ConfigService configService;

    private MemberService memberService;

    private StarLadder2GameService starLadder2GameService;

    private StarLadder2BotService starLadder2BotService;

    private StarLadder2Repository starLadder2Repository;

    private BetItemRepository betItemRepository;

    private static QStarLadder2 q = QStarLadder2.starLadder2;

    @Transactional
    @Override
    public boolean updateConfig(StarLadder2Config starLadder2Config) {
        try {
            configService.updateZoneConfig("starladder2", JsonUtils.toString(starLadder2Config));
            ZoneConfig.setStarLadder2(starLadder2Config);
            // 이미 등록된 게임의 배당을 변경한다.
            starLadder2Repository.updateOdds(ZoneConfig.getStarLadder2().getOdds());
        } catch (RuntimeException e) {
            log.error("별다리2분 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<StarLadder2> getComplete() {
        return starLadder2Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<StarLadder2> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return starLadder2Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public StarLadder2Dto.Score findScore(Long id) {
        StarLadder2 starLadder2 = starLadder2Repository.findOne(id);

        StarLadder2Dto.Score score = new StarLadder2Dto.Score();
        score.setId(starLadder2.getId());
        score.setRound(starLadder2.getRound());
        score.setGameDate(starLadder2.getGameDate());
        score.setStart(starLadder2.getStart());
        score.setLine(starLadder2.getLine());
        score.setOddeven(starLadder2.getOddeven());
        score.setCancel(starLadder2.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getStarLadder2Url() + "?sdate=" + starLadder2.getSdate());
        if (json == null) return score;

        starLadder2 = JsonUtils.toModel(json, StarLadder2.class);
        if (starLadder2 == null) return score;

        // 봇에 결과가 있다면
        if (starLadder2.isClosing()) {
            score.setOddeven(starLadder2.getOddeven());
            score.setLine(starLadder2.getLine());
            score.setStart(starLadder2.getStart());

            if (!"ODD".equals(score.getOddeven()) && !"EVEN".equals(score.getOddeven())) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(StarLadder2Dto.Score score) {
        StarLadder2 starLadder2 = starLadder2Repository.findOne(score.getId());

        try {
            if (starLadder2.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                starLadder2GameService.rollbackPayment(starLadder2);
            }

            // 스코어 입력
            starLadder2.updateScore(score);
            starLadder2Repository.saveAndFlush(starLadder2);
            starLadder2GameService.closingBetting(starLadder2);
            starLadder2BotService.checkResult();
        } catch (RuntimeException e) {
            log.error("별다리2분 {} - {}회차 수동처리를 하지 못하였습니다. - {}", starLadder2.getGameDate(), starLadder2.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<StarLadder2> iterable = starLadder2Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(5))));
        for (StarLadder2 starLadder2 : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.STARLADDER2).and(qi.groupId.eq(starLadder2.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getStarLadder2Url() + "?sdate=" + starLadder2.getSdate());
            if (json == null) continue;

            StarLadder2 result = JsonUtils.toModel(json, StarLadder2.class);
            if (result == null) continue;

            if (result.isClosing()) {
                starLadder2.setStart(result.getStart());
                starLadder2.setLine(result.getLine());
                starLadder2.setOddeven(result.getOddeven());
                starLadder2.setClosing(true);
                starLadder2Repository.saveAndFlush(starLadder2);
                closing++;
            }
        }
        starLadder2BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public StarLadder2Dto.Config gameConfig() {
        StarLadder2Dto.Config gameConfig = new StarLadder2Dto.Config();
        StarLadder2Config config = ZoneConfig.getStarLadder2();

        Date gameDate = config.getZoneMaker().getGameDate();
        StarLadder2 starLadder2 = starLadder2Repository.findOne(q.gameDate.eq(gameDate));

        if (starLadder2 == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isStarLadder2());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(starLadder2.getGameDate());
        gameConfig.setSdate(starLadder2.getSdate());
        gameConfig.setRound(starLadder2.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(starLadder2.getOdds());

        int betTime = (int) (starLadder2.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setStart(config.isStart());
        gameConfig.setLine(config.isLine());
        gameConfig.setLineStart(config.isLineStart());

        return gameConfig;
    }
}
