package spoon.gameZone.eosPower1.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.eosPower1.EosPower1;
import spoon.gameZone.eosPower1.EosPower1Repository;
import spoon.gameZone.eosPower1.QEosPower1;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class EosPower1BotServiceImpl implements EosPower1BotService {

    private EosPower1GameService eosPower1GameService;

    private EosPower1Repository eosPower1Repository;

    private static QEosPower1 q = QEosPower1.eosPower1;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return eosPower1Repository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(EosPower1 power) {
        eosPower1Repository.saveAndFlush(power);
    }

    @Transactional
    @Override
    public boolean closingGame(EosPower1 result) {
        EosPower1 power = eosPower1Repository.findOne(q.sdate.eq(result.getSdate()));
        if (power == null) {
            return true;
        }

        try {
            power.setOddeven(result.getOddeven());
            power.setOdd_overunder(result.getOdd_overunder());
            power.setEven_overunder(result.getEven_overunder());

            power.setPb_oddeven(result.getPb_oddeven());
            power.setPb_odd_overunder(result.getPb_odd_overunder());
            power.setPb_even_overunder(result.getPb_even_overunder());

            power.setOverunder(result.getOverunder());
            power.setPb_overunder(result.getPb_overunder());

            power.setSize(result.getSize());
            power.setOdd_size(result.getOdd_size());
            power.setEven_size(result.getEven_size());

            power.setPb(result.getPb());
            power.setBall(result.getBall());
            power.setSize(result.getSize());
            power.setSum(result.getSum());

            power.setClosing(true);

            eosPower1Repository.saveAndFlush(power);
            eosPower1GameService.closingBetting(power);
        } catch (RuntimeException e) {
            log.error("파워볼 1분 {}회차 결과 업데이트에 실패하였습니다. - {}", power.getTimes(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = eosPower1Repository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getEosPower1().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        eosPower1Repository.deleteGame(DateUtils.beforeDays(days));
    }
}
