package spoon.web.site;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.common.net.HttpParsing;
import spoon.common.utils.StringUtils;
import spoon.event.entity.Logger;
import spoon.event.service.LoggerService;
import spoon.member.service.MemberService;
import spoon.payment.service.PaymentService;

import java.security.MessageDigest;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Controller
public class CasinoApiController {

    private SessionRegistry sessionRegistry;

    private MemberService memberService;

    private PaymentService paymentService;

    private LoggerService loggerService;

    public String getMd5(String s) throws  Exception {
        String rtnMD5;
        try {
            //MessageDigest 인스턴스 생성
            MessageDigest md = MessageDigest.getInstance("MD5");
            //해쉬값 업데이트
            md.update(s.getBytes());
            //해쉬값(다이제스트) 얻기
            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for(byte byteTmp : byteData) {
                sb.append(Integer.toString((byteTmp&0xff) + 0x100, 16).substring(1));
            }
            rtnMD5 = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            rtnMD5 = null;
        }
        return rtnMD5;
    }

    @ResponseBody
    @RequestMapping(value = "/block/casinoApi")
    public JSONObject casinoApi(
            @RequestParam(value = "mode", required = false) String mode,
            @RequestParam(value = "operatorID", required = false) String operatorID,
            @RequestParam(value = "userID", required = false) String userID,
            @RequestParam(value = "userPassword", required = false) String userPassword,
            @RequestParam(value = "vendorID", required = false) String vendorID,
            @RequestParam(value = "walletID", required = false) String walletID,
            @RequestParam(value = "amount", required = false) String amount,
            @RequestParam(value = "transactionID", required = false) String transactionID,
            @RequestParam(value = "thirdPartyCode", required = false) String thirdPartyCode,
            @RequestParam(value = "platform", required = false) String platform,
            @RequestParam(value = "gameID", required = false) String gameID,
            @RequestParam(value = "session", required = false) String session,
            @RequestParam(value = "isRenew", required = false) String isRenew,
            @RequestParam(value = "timeout", required = false) String timeout,
            @RequestParam(value = "lang", required = false) String lang

    ) {

        JSONObject jsonObj = new JSONObject();

        try {
            String key = "24C1400790B91389FFBE5D529904BE45";
            String url = "http://api.krw.ximaxgames.com";
            String param = "";

            switch (mode) {

                /*회원 생성*/
                case "createAccount":
                    url += "/wallet/api/createAccount?";
                    param += "operatorID=" + operatorID;
                    param += "&time=" + new Date().getTime();
                    param += "&userID=" + userID;
                    param += "&userPassword=" + getMd5(userPassword);
                    param += "&vendorID=" + vendorID;
                    param += "&walletID=" + walletID;
                    break;

                /*각 게임사의 유저 정보 반환*/
                case "getAccountInfo":
                    url += "/wallet/api/getAccountInfo?";
                    param += "operatorID=" + operatorID;
                    param += "&thirdPartyCode=" + thirdPartyCode;
                    param += "&time=" + new Date().getTime();
                    param += "&userID=" + userID;
                    param += "&vendorID=" + vendorID;
                    break;

                /*
                    각 게임사의 유저 정보 반환
                    주기적으로 호출해서 유저에게 보여줄 경우, 유저별로 30초당 1번 호출 요망
                    너무 잦은 호출은 계정이 제한될 수 있음
                    thirdPartyBalance : 게임사에 입금된 포인트
                    memberBalance : 유저의 내부 포인트
                */
                case "getAccountBalance":
                    url += "/wallet/api/getAccountInfo?";
                    param += "isRenew=" + isRenew;
                    param += "&operatorID=" + operatorID;
                    param += "&thirdPartyCode=" + thirdPartyCode;
                    param += "&time=" + new Date().getTime();
                    param += "&userID=" + userID;
                    param += "&vendorID=" + vendorID;
                    break;

                /*
                    모든 게임사의 포인트 조회
                    모든 게임사측에 API 호출을 하기 때문에 응답이 다소 느림
                    주기적으로 호출해서 유저에게 보여줄 용도인 경우, 유저별로 최소 1분 이상의 호출 간격 요망
                    returnCode(0) 이 아니면 세부내용 확인필요 (일부 게임사 오류 발생가능)
                    thirdParty(0) 은 유저의 내부 포인트를 의미
                */
                case "getAccountBalanceAll":
                    url += "/wallet/api/getAccountBalanceAll?";
                    param += "isRenew=" + isRenew;
                    param += "&operatorID=" + operatorID;
                    param += "&time=" + new Date().getTime();
                    param += "&userID=" + userID;
                    param += "&vendorID=" + vendorID;
                    break;

                /*
                    모든 게임사의 포인트 조회
                    모든 게임사측에 API 호출을 하기 때문에 응답이 다소 느림
                    주기적으로 호출해서 유저에게 보여줄 용도인 경우, 유저별로 최소 1분 이상의 호출 간격 요망
                    returnCode(0) 이 아니면 세부내용 확인필요 (일부 게임사 오류 발생가능)
                    thirdParty(0) 은 유저의 내부 포인트를 의미
                */
                case "getAccountBalanceAllWithTimeout":
                    url += "/wallet/api/getAccountBalanceAllWithTimeout?";
                    param += "isRenew=" + isRenew;
                    param += "&operatorID=" + operatorID;
                    param += "&time=" + new Date().getTime();
                    param += "&timeout=" + timeout;
                    param += "&userID=" + userID;
                    param += "&vendorID=" + vendorID;
                    break;

                /*
                    유저가 가입한 게임사들의 정보 반환
                    실시간 수치가 아니라 서버에 캐시된 값을 리턴
                    다음 API 호출이 있을때만 수치 업데이트됨 :
                    - getAccountBalance
                    - transferPointM2G
                    - transferPointG2M
                    리턴 데이터에서 stateCode, stateMessage 는 게임사별 API 결과이며 유저 포인트 이동시 문제가 발생하면 해당 내역을 참조
                    유저의 내부 포인트는 포함되지 않음
                */
                case "getAccountGameState":
                    url += "/wallet/api/getAccountGameState?";
                    param += "operatorID=" + operatorID;
                    param += "&time=" + new Date().getTime();
                    param += "&userID=" + userID;
                    param += "&vendorID=" + vendorID;
                    break;

                /*
                    유저가 가입한 게임사들의 정보 반환
                    실시간 수치가 아니라 서버에 캐시된 값을 리턴
                    다음 API 호출이 있을때만 수치 업데이트됨 :
                    - getAccountBalance
                    - transferPointM2G
                    - transferPointG2M
                    리턴 데이터에서 stateCode, stateMessage 는 게임사별 API 결과이며 유저 포인트 이동시 문제가 발생하면 해당 내역을 참조
                    thirdParty(0) 은 유저의 내부 포인트를 의미
                */
                case "getAccountGamePointState":
                    url += "/wallet/api/getAccountGamePointState?";
                    param += "operatorID=" + operatorID;
                    param += "&time=" + new Date().getTime();
                    param += "&userID=" + userID;
                    param += "&vendorID=" + vendorID;
                    break;

                /*
                    모든 게임사의 포인트를 유저의 내부 포인트로 이동
                    유저가 출금 요청시 유저 포인트 확인용도로 사용 가능
                    모든 게임사측에 API 호출을 하기 때문에 10초이상 걸릴 수 있음
                    returnCode(0) 0이 아니면 세부내용 확인필요 (일부 게임사 오류 발생가능)
                */
                case "collectAccountGameBalanceAll":
                    url += "/wallet/api/collectAccountGameBalanceAll?";
                    param += "operatorID=" + operatorID;
                    param += "&time=" + new Date().getTime();
                    param += "&userID=" + userID;
                    param += "&vendorID=" + vendorID;
                    break;

                /*
                    포인트이동 : user -> 게임사
                */
                case "transferPointM2G":
                    url += "/wallet/api/transferPointM2G?";
                    param += "amount=" + amount;
                    param += "&operatorID=" + operatorID;
                    param += "&thirdPartyCode=" + thirdPartyCode;
                    param += "&time=" + new Date().getTime();
                    param += "&transactionID=" + transactionID;
                    param += "&userID=" + userID;
                    param += "&vendorID=" + vendorID;
                    break;

                /*
                    포인트이동 : 게임사 -> user
                */
                case "transferPointG2M":
                    url += "/wallet/api/transferPointG2M?";
                    param += "amount=" + amount;
                    param += "&operatorID=" + operatorID;
                    param += "&thirdPartyCode=" + thirdPartyCode;
                    param += "&time=" + new Date().getTime();
                    param += "&transactionID=" + transactionID;
                    param += "&userID=" + userID;
                    param += "&vendorID=" + vendorID;
                    break;

                /*회원 포인트 추가 transactionID : 유니크값*/
                case "addMemberPoint":
                    url += "/wallet/api/addMemberPoint?";
                    param += "amount=" + amount;
                    param += "&operatorID=" + operatorID;
                    param += "&time=" + new Date().getTime();
                    param += "&transactionID=" + transactionID;
                    param += "&userID=" + userID;
                    param += "&vendorID=" + vendorID;
                    break;

                /*회원 포인트 제거 transactionID : 유니크값*/
                case "subtractMemberPoint":
                    url += "/wallet/api/subtractMemberPoint?";
                    param += "amount=" + amount;
                    param += "&operatorID=" + operatorID;
                    param += "&time=" + new Date().getTime();
                    param += "&transactionID=" + transactionID;
                    param += "&userID=" + userID;
                    param += "&vendorID=" + vendorID;
                    break;

                /*로비 형태의 게임 리스트 리턴*/
                case "getLobbyList":
                    url += "/wallet/api/getLobbyList?";
                    param += "operatorID=" + operatorID;
                    param += "&thirdPartyCode=" + thirdPartyCode;
                    param += "&time=" + new Date().getTime();
                    param += "&vendorID=" + vendorID;
                    break;

                /*
                    로비 형태의 게임 리스트 리턴
                    [게임 실행]
                    실행시마다 다음중 하나의 방법으로 사용하세요
                    1. generateSession + openGame (바로 열기)
                    2. generateSession + getGameUrl (URL 리턴)
                */
                case "getGameList":
                    url += "/wallet/api/getGameList?";
                    param += "operatorID=" + operatorID;
                    param += "&thirdPartyCode=" + thirdPartyCode;
                    param += "&time=" + new Date().getTime();
                    param += "&vendorID=" + vendorID;
                    break;

                /*게임을 열기위한 일회용 세션토큰 발급.*/
                case "generateSession":
                    url += "/wallet/api/generateSession?";
                    param += "operatorID=" + operatorID;
                    param += "&time=" + new Date().getTime();
                    param += "&userID=" + userID;
                    param += "&vendorID=" + vendorID;
                    break;

                /*로비 타입 게임 열기*/
                case "getLobbyUrl":
                    url += "/wallet/api/getLobbyUrl?";
                    param += "lang=" + lang;
                    param += "&operatorID=" + operatorID;
                    param += "&platform=" + platform;
                    param += "&thirdPartyCode=" + thirdPartyCode;
                    param += "&time=" + new Date().getTime();
                    param += "&userID=" + userID;
                    param += "&vendorID=" + vendorID;
                    break;

                /*
                    로비 타입 게임 열기
                    개별 접속 가능한 게임 URL
                    seamlessAssist 기능을 사용하면서 이전 접속한 게임사 포인트 자동 출금에서 오류가 발생하는 경우(서버점검 혹은 HTTP 오류)
                    returnCode : 1 로 리턴됨
                */
                case "getGameUrl":
                    url += "/wallet/api/getGameUrl?";
                    param += "gameID=" + gameID;
                    param += "&lang=" + lang;
                    param += "&operatorID=" + operatorID;
                    param += "&session=" + session;
                    param += "&time=" + new Date().getTime();
                    param += "&vendorID=" + vendorID;
                    break;

            }

            //파라미터 해시값 추가 ?다음 파라미터 모두 md5 hash
            param += "&hash=" + getMd5(key + param);

            String json = HttpParsing.getJson2(url + param);
            JSONParser parser = new JSONParser();
            if (!StringUtils.empty(json)) {
                //System.out.println("json = "+json);
                try {
                    jsonObj = (JSONObject) parser.parse(json);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Logger logger = new Logger();
            logger.setCode("casinoApi");
            logger.setRegDate(new Date());
            logger.setEtc1(mode);
            logger.setEtc2(param);
            logger.setData(url + param + jsonObj.toJSONString());
            loggerService.addLog(logger);

        } catch (Exception e){
            e.printStackTrace();
        }

        return jsonObj;
    }


}
