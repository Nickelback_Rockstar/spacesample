package spoon.gameZone.starLadder3.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.starLadder3.QStarLadder3;
import spoon.gameZone.starLadder3.StarLadder3;
import spoon.gameZone.starLadder3.StarLadder3Repository;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class StarLadder3BotServiceImpl implements StarLadder3BotService {

    private StarLadder3GameService starLadder3GameService;

    private StarLadder3Repository starLadder3Repository;

    private static QStarLadder3 q = QStarLadder3.starLadder3;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return starLadder3Repository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(StarLadder3 starLadder3) {
        starLadder3Repository.saveAndFlush(starLadder3);
    }

    @Transactional
    @Override
    public boolean closingGame(StarLadder3 result) {
        StarLadder3 starLadder3 = starLadder3Repository.findOne(q.sdate.eq(result.getSdate()));
        if (starLadder3 == null) {
            return true;
        }

        try {
            starLadder3.setOddeven(result.getOddeven());
            starLadder3.setLine(result.getLine());
            starLadder3.setStart(result.getStart());
            starLadder3.setClosing(true);

            starLadder3Repository.saveAndFlush(starLadder3);
            starLadder3GameService.closingBetting(starLadder3);
        } catch (RuntimeException e) {
            log.error("별다리3분 {}회차 결과 업데이트에 실패하였습니다. - {}", starLadder3.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = starLadder3Repository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getStarLadder3().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        starLadder3Repository.deleteGame(DateUtils.beforeDays(days));
    }
}
