package spoon.web.admin.report;

import com.querydsl.jpa.impl.JPAQuery;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import spoon.bet.domain.BetDto;
import spoon.bet.domain.BetReport;
import spoon.bet.domain.BetUserRate;
import spoon.bet.entity.Bet;
import spoon.report.service.ReportListService;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@AllArgsConstructor
@Controller("admin.ReportListController")
@RequestMapping("#{config.pathAdmin}")
public class ReportListController {

    private ReportListService reportListService;

    /**
     * 베팅 리스트
     */
    @RequestMapping(value = "/report/list", method = RequestMethod.GET)
    public String list(ModelMap map, BetDto.ReportCommand command,
                       @PageableDefault(size = 20) Pageable pageable) {


        command.setSize(pageable.getPageSize());

        List<BetReport> list = reportListService.reportList(command);

        Page<BetReport> page = new PageImpl<>(list, pageable, reportListService.reportListTotal(command));

        map.addAttribute("page", page);
        map.addAttribute("command", command);

        return "admin/report/list";
    }

}
