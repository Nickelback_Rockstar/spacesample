package spoon.event.service;

import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.banking.entity.Banking;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.domain.LottoConfig;
import spoon.config.entity.JsonConfig;
import spoon.config.repository.JsonRepository;
import spoon.event.domain.DailyDto;
import spoon.event.domain.EventCode;
import spoon.event.entity.Lotto;
import spoon.event.entity.QLotto;
import spoon.event.repository.LottoRepository;
import spoon.mapper.BankingMapper;
import spoon.mapper.DailyEventMapper;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.entity.QMember;
import spoon.member.repository.MemberRepository;
import spoon.payment.domain.PointCode;
import spoon.payment.service.PaymentService;
import spoon.support.web.AjaxResult;

import java.util.Arrays;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class LottoServiceImpl implements LottoService {

    private JsonRepository jsonRepository;

    private BankingMapper bankingMapper;

    private DailyEventMapper dailyEventMapper;

    private LottoRepository lottoRepository;

    private MemberRepository memberRepository;

    private PaymentService paymentService;

    @Transactional
    @Override
    public boolean updateConfig(LottoConfig lottoConfig) {
        JsonConfig jsonConfig = new JsonConfig("lotto");
        jsonConfig.setJson(JsonUtils.toString(lottoConfig));
        jsonRepository.saveAndFlush(jsonConfig);
        Config.setLottoConfig(lottoConfig);

        return true;
    }

    @Transactional
    @Override
    public void checkLotto(Banking banking) {

        //System.out.println("=checkLotto=");

        //오늘 충전 금액 가져옴
        String today = DateUtils.format(new Date(), "yyyy-MM-dd");
        Long totalAmount = bankingMapper.getTodayAmount(banking.getUserid(), today + " 00:00:00.000", today + " 23:59:59.997");

        if(Config.getLottoConfig().getToDayLotto() > 0 && Config.getLottoConfig().getToDayAmount() > 0){
            Long todayLotto = bankingMapper.getTodayLotto(banking.getUserid(), today + " 00:00:00.000", today + " 23:59:59.997");

//            //System.out.println("todayLotto="+todayLotto);
//            //System.out.println("Config.getLottoConfig().getToDayAmount()="+Config.getLottoConfig().getToDayAmount());
//            //System.out.println("banking.getAmount()="+banking.getAmount());

            if(todayLotto == 0 && Config.getLottoConfig().getToDayAmount() <= banking.getAmount()) {
                for (int i = 0; i < Config.getLottoConfig().getToDayLotto(); i++) {//복권 저장
                    Lotto lt = new Lotto();
                    lt.setUserid(banking.getUserid());
                    lt.setPoint(0);
                    lt.setMemo("일충전 복권");
                    lt.setRegDate(new Date());
                    lt.setStartEnd(false);
                    lt.setEventCode(EventCode.TODAYDEPOSIT);
                    lt.setCancel(false);
                    lt.setClosing(false);
                    lt.setWorker("AUTO_BOT");
                    lottoRepository.saveAndFlush(lt);
                }
            }
        }


        if("출석".equals(Config.getLottoConfig().getOption())){

            ////System.out.println("=출석=");

            long[] amount1 = Config.getLottoConfig().getAmount();
            //출석은 금액이 동일하므로 하나만 조회해봄
            if(amount1 != null && amount1.length > 0){
                if(amount1[0] <= banking.getAmount()){

                    long yCnt = dailyEventMapper.getLottoYesterday(banking.getUserid(), amount1[0]);
                    //어제 받은게 없으면 startend 초기화
                    if(yCnt == 0){
                        dailyEventMapper.updateStartEnd(banking.getUserid());
                    }

                    DailyDto.Lotto lottoDto = dailyEventMapper.getLottoDaily(banking.getUserid(), amount1[0]);
                    int[] lotto = Config.getLottoConfig().getLotto();
                    int[] dailys = Config.getLottoConfig().getDaily();

                    int index = Ints.indexOf(dailys, lottoDto.getEventDay());//해당 출석일이 세팅된 인덱스 가져옴

                    //System.out.println("lottoDto.getEventDay()="+lottoDto.getEventDay());
                    //System.out.println("index="+index);

                    if(index >= 0) {
                        int min = Arrays.stream(dailys).min().getAsInt(); //30
                        int max = Arrays.stream(dailys).max().getAsInt(); //30

                        int minIndex = Ints.indexOf(dailys, min);//해당 출석일이 세팅된 인덱스 가져옴
                        int maxIndex = Ints.indexOf(dailys, max);//해당 출석일이 세팅된 인덱스 가져옴

                        ////System.out.println("dailys="+dailys.toString());
                        ////System.out.println("min="+minIndex);
                        ////System.out.println("max="+maxIndex);

                        int lottoCnt = lotto[index] - lottoDto.getLotto(); //금일 받은게 있으면 차감한다.
                        for (int i = 0; i < lottoCnt; i++) {//복권 저장
                            Lotto lt = new Lotto();
                            lt.setUserid(banking.getUserid());
                            lt.setPoint(0);
                            lt.setMemo(lottoDto.getEventDay() + "일 출석 복권");
                            lt.setRegDate(new Date());
                            if (minIndex == index) { //출석일이 min,max면 true;
                                ////System.out.println("startend true");
                                lt.setStartEnd(true);
                            } else {
                                ////System.out.println("startend false");
                                lt.setStartEnd(false);
                            }
                            lt.setEventCode(EventCode.DAILY);
                            lt.setCancel(false);
                            lt.setClosing(false);
                            lt.setWorker("AUTO_BOT");
                            lottoRepository.saveAndFlush(lt);
                        }

                        //마지막 일자 지급 후 startend 초기화
                        if(maxIndex == index){
                            dailyEventMapper.updateStartEnd(banking.getUserid());
                        }
                    }
                }
            }
        }else if("충전".equals(Config.getLottoConfig().getOption())){

            //System.out.println("=충전=");

            Long todayLotto = dailyEventMapper.getTodayLotto(banking.getUserid(), today + " 00:00:00.000", today + " 23:59:59.997", EventCode.DEPOSIT.name());

            //매충인지 한번인지
            if(!Config.getLottoConfig().isOnes() || (Config.getLottoConfig().isOnes() && todayLotto == 0)) {
                long[] amount1 = new long[5];
                amount1[0] = Config.getLottoConfig().getAmount()[0];
                amount1[1] = Config.getLottoConfig().getAmount()[1];
                amount1[2] = Config.getLottoConfig().getAmount()[2];
                amount1[3] = Config.getLottoConfig().getAmount()[3];
                amount1[4] = Config.getLottoConfig().getAmount()[4];

                Arrays.sort(amount1);

                long amount = 0;
                for (int i = amount1.length - 1; i >= 0; i--) {
                    if (totalAmount >= amount1[i]) {
                        amount = amount1[i];
                        break;
                    }
                }

                int index = Longs.indexOf(Config.getLottoConfig().getAmount(), amount);
                int lottoCnt = Config.getLottoConfig().getLotto()[index];
                for (int i = 0; i < lottoCnt; i++) {//복권 저장
                    Lotto lt = new Lotto();
                    lt.setUserid(banking.getUserid());
                    lt.setPoint(0);
                    lt.setMemo(amount+"원 이상 충전 복권, 총"+lottoCnt+"장");
                    lt.setRegDate(new Date());
                    lt.setStartEnd(true);
                    lt.setStartEnd(true);
                    lt.setCancel(false);
                    lt.setClosing(false);
                    lt.setEventCode(EventCode.DEPOSIT);
                    lt.setWorker("AUTO_BOT");
                    lottoRepository.saveAndFlush(lt);
                }
            }
        }
    }

    @Override
    public long getLottoCount(String userid) {
        QLotto q = QLotto.lotto;
        return lottoRepository.count(q.userid.eq(userid).and(q.cancel.isFalse()).and(q.closing.isFalse()));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Lotto> lottoPage(DailyDto.Command command, Pageable pageable) {
        QLotto q = QLotto.lotto;

        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid()));

        Sort sort = new Sort(Sort.Direction.DESC, "id");

        PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), sort);

        return lottoRepository.findAll(builder, pageRequest);
    }

    @Transactional
    @Override
    public AjaxResult addLotto(DailyDto.Add add) {

        if (WebUtils.role().getValue() < Role.ADMIN.getValue()) {
            return new AjaxResult(false, "권한이 부족합니다.");
        }

        try {
            Lotto lt = new Lotto();
            lt.setUserid(add.getUserid());
            lt.setPoint(0);
            lt.setMemo(add.getMemo());
            lt.setRegDate(new Date());
            lt.setStartEnd(false);
            lt.setCancel(false);
            lt.setClosing(false);
            lt.setWorker(WebUtils.userid());
            lt.setEventCode(EventCode.ADMIN);
            lottoRepository.saveAndFlush(lt);
        } catch (RuntimeException e) {
            log.error("복권 증정에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, "복권 증정에 실패하였습니다.");
        }
        return new AjaxResult(true);
    }

    @Transactional
    @Override
    public AjaxResult changeCancel(DailyDto.Add add) {

        try {
            Lotto lotto = lottoRepository.findOne(add.getId());
            if(lotto.isCancel()){
                lotto.setCancel(false);
            }else{
                lotto.setCancel(true);
            }
            lottoRepository.saveAndFlush(lotto);
        } catch (RuntimeException e) {
            log.error("복권 수정에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, "복권 수정에 실패하였습니다.");
        }
        return new AjaxResult(true);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Lotto> lottoListPage(DailyDto.Command command, Pageable pageable) {
        QLotto q = QLotto.lotto;
        BooleanBuilder builder = new BooleanBuilder();
        if(StringUtils.notEmpty(command.getUserid())){
            builder.and(q.userid.eq(command.getUserid()));
        }

        if(StringUtils.notEmpty(command.getSdate()) && StringUtils.notEmpty(command.getEdate())) {
            builder.and(q.regDate.between(DateUtils.start(command.getSdate()), DateUtils.end(command.getEdate())));
        }

        Sort sort = new Sort(Sort.Direction.DESC, "regDate");
        PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), sort);
        return lottoRepository.findAll(builder, pageRequest);
    }

    @Transactional
    @Override
    public long lottoRolling(String userid) {
        long id = dailyEventMapper.getLottoMinId(userid);

        if(id < 1){
            return 0;
        }

        //System.out.println("id===="+id);

        int rd = NumberUtils.random(1,10000);

        int[] lottoRate = new int[7];
        lottoRate[0] = Config.getLottoConfig().getLottoRate()[0];
        lottoRate[1] = Config.getLottoConfig().getLottoRate()[1];
        lottoRate[2] = Config.getLottoConfig().getLottoRate()[2];
        lottoRate[3] = Config.getLottoConfig().getLottoRate()[3];
        lottoRate[4] = Config.getLottoConfig().getLottoRate()[4];
        lottoRate[5] = Config.getLottoConfig().getLottoRate()[5];
        lottoRate[6] = Config.getLottoConfig().getLottoRate()[6];

        Arrays.sort(lottoRate);

        long result = 0;

        //System.out.println("rd===="+rd);

        for(int i=0; i<=lottoRate.length-1; i++){
            if(lottoRate[i] >= rd){
                //System.out.println("타써");
                int index = Ints.indexOf(Config.getLottoConfig().getLottoRate(), lottoRate[i]);//해당 출석일이 세팅된 인덱스 가져옴
                result =  Config.getLottoConfig().getLottoWon()[index];
                break;
            }
        }

        if(result == 0){
            result = Arrays.stream(Config.getLottoConfig().getLottoWon()).min().getAsInt();
            if(result == 0){
                return 0;
            }
        }

        Lotto lotto = lottoRepository.findOne(id);
        lotto.setPoint(result);
        lotto.setClosing(true);
        lotto.setClosingDate(new Date());
        lottoRepository.saveAndFlush(lotto);

        //포인트
        paymentService.addPoint(PointCode.LOTTO, lotto.getId(), lotto.getUserid(), lotto.getPoint(), "");

        return result;
    }

    @Override
    public AjaxResult addTotalLotto(DailyDto.Add add) {
        if (WebUtils.role().getValue() < Role.ADMIN.getValue()) {
            return new AjaxResult(false, "권한이 부족합니다.");
        }

        QMember q = QMember.member;
        Iterable<Member> members = memberRepository.findAll(q.role.eq(Role.USER).and(q.enabled.isTrue()).and(q.secession.isFalse()));

        try {
            for (Member m : members) {
                Lotto lt = new Lotto();
                lt.setUserid(m.getUserid());
                lt.setPoint(0);
                lt.setMemo(add.getMemo());
                lt.setRegDate(new Date());
                lt.setStartEnd(false);
                lt.setCancel(false);
                lt.setClosing(false);
                lt.setWorker(WebUtils.userid());
                lt.setEventCode(EventCode.ADMIN);
                lottoRepository.saveAndFlush(lt);
            }
        } catch (RuntimeException e) {
            log.error("복권 증정에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, "복권 증정에 실패하였습니다.");
        }
        return new AjaxResult(true);
    }


}
