package spoon.bot.zone.roulette;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class RouletteTask {

    private GameBotParsing rouletteParsing;

    @Scheduled(cron = "0 * * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        rouletteParsing.parsingGame();
    }

    @Scheduled(cron = "9,16,21 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        rouletteParsing.closingGame();
    }

    @Scheduled(cron = "11,15,20 * * * * *")
    public void checkResult() {
        if (notParsing()) return;
        rouletteParsing.checkResult();
    }

    @Scheduled(cron = "15 1 4 * * * ")
    public void deleteGame() {
        rouletteParsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isRoulette() || !ZoneConfig.getRoulette().isEnabled();
    }
}
