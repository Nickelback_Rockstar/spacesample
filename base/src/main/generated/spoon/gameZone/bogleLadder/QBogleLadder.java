package spoon.gameZone.bogleLadder;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBogleLadder is a Querydsl query type for BogleLadder
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBogleLadder extends EntityPathBase<BogleLadder> {

    private static final long serialVersionUID = -320917271L;

    public static final QBogleLadder bogleLadder = new QBogleLadder("bogleLadder");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath line = createString("line");

    public final StringPath oddeven = createString("oddeven");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public final StringPath start = createString("start");

    public QBogleLadder(String variable) {
        super(BogleLadder.class, forVariable(variable));
    }

    public QBogleLadder(Path<? extends BogleLadder> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBogleLadder(PathMetadata metadata) {
        super(BogleLadder.class, metadata);
    }

}

