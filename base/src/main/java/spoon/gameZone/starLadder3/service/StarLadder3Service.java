package spoon.gameZone.starLadder3.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.starLadder3.StarLadder3;
import spoon.gameZone.starLadder3.StarLadder3Config;
import spoon.gameZone.starLadder3.StarLadder3Dto;
import spoon.support.web.AjaxResult;

public interface StarLadder3Service {

    /**
     * 별다리3분 설정을 변경한다.
     */
    boolean updateConfig(StarLadder3Config starLadder3Config);

    /**
     * 별다리3분 등록된 게임을 가져온다.
     */
    Iterable<StarLadder3> getComplete();

    /**
     * 별다리3분 종료된 게임을 가져온다.
     */
    Page<StarLadder3> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * 별다리3분 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    StarLadder3Dto.Score findScore(Long id);

    /**
     * 별다리3분 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(StarLadder3Dto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    StarLadder3Dto.Config gameConfig();

}
