package spoon.gameZone.dragonTiger.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.dragonTiger.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class DragonTigerServiceImpl implements DragonTigerService {

    private ConfigService configService;

    private MemberService memberService;

    private DragonTigerGameService dragonTigerGameService;

    private DragonTigerBotService dragonTigerBotService;

    private DragonTigerRepository dragonTigerRepository;

    private BetItemRepository betItemRepository;

    private static QDragonTiger q = QDragonTiger.dragonTiger;

    @Transactional
    @Override
    public boolean updateConfig(DragonTigerConfig dragonTigerConfig) {
        try {
            configService.updateZoneConfig("dragonTiger", JsonUtils.toString(dragonTigerConfig));
            ZoneConfig.setDragonTiger(dragonTigerConfig);
            // 이미 등록된 게임의 배당을 변경한다.
            dragonTigerRepository.updateOdds(ZoneConfig.getDragonTiger().getOdds());
        } catch (RuntimeException e) {
            log.error("드래곤타이거 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<DragonTiger> getComplete() {
        return dragonTigerRepository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<DragonTiger> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return dragonTigerRepository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public DragonTigerDto.Score findScore(Long id) {
        DragonTiger dragonTiger = dragonTigerRepository.findOne(id);

        DragonTigerDto.Score score = new DragonTigerDto.Score();
        score.setId(dragonTiger.getId());
        score.setRound(dragonTiger.getRound());
        score.setGameDate(dragonTiger.getGameDate());
        score.setDragon(dragonTiger.getDragon());
        score.setTiger(dragonTiger.getTiger());
        score.setSum(dragonTiger.getSum());
        score.setResult(dragonTiger.getResult());
        score.setCancel(dragonTiger.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getDragonTigerUrl() + "?sdate=" + dragonTiger.getSdate());
        if (json == null) return score;

        dragonTiger = JsonUtils.toModel(json, DragonTiger.class);
        if (dragonTiger == null) return score;

        // 봇에 결과가 있다면
        if (dragonTiger.isClosing()) {
            score.setDragon(dragonTiger.getDragon());
            score.setTiger(dragonTiger.getTiger());
            score.setSum(dragonTiger.getSum());
            score.setResult(dragonTiger.getResult());

            if (!("DRAGON".equals(score.getResult()) || "TIE".equals(score.getResult()) || "TIGER".equals(score.getResult()))) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(DragonTigerDto.Score score) {
        DragonTiger dragonTiger = dragonTigerRepository.findOne(score.getId());

        try {
            if (dragonTiger.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                dragonTigerGameService.rollbackPayment(dragonTiger);
            }

            // 스코어 입력
            dragonTiger.updateScore(score);
            dragonTigerRepository.saveAndFlush(dragonTiger);
            dragonTigerGameService.closingBetting(dragonTiger);
            dragonTigerBotService.checkResult();
        } catch (RuntimeException e) {
            log.error("드래곤타이거 {} - {}회차 수동처리를 하지 못하였습니다. - {}", dragonTiger.getGameDate(), dragonTiger.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<DragonTiger> iterable = dragonTigerRepository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(3))));
        for (DragonTiger dragonTiger : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.DRAGONTIGER).and(qi.groupId.eq(dragonTiger.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getDragonTigerUrl() + "?sdate=" + dragonTiger.getSdate());
            if (json == null) continue;

            DragonTiger result = JsonUtils.toModel(json, DragonTiger.class);
            if (result == null) continue;

            if (result.isClosing()) {
                dragonTiger.setDragon(result.getDragon());
                dragonTiger.setTiger(result.getTiger());
                dragonTiger.setSum(result.getSum());
                dragonTiger.setResult(result.getResult());
                dragonTiger.setClosing(true);
                dragonTigerRepository.saveAndFlush(dragonTiger);
                closing++;
            }
        }
        dragonTigerBotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public DragonTigerDto.Config gameConfig() {
        DragonTigerDto.Config gameConfig = new DragonTigerDto.Config();
        DragonTigerConfig config = ZoneConfig.getDragonTiger();

        Date gameDate = config.getZoneMaker().getGameDate();
        DragonTiger dragonTiger = dragonTigerRepository.findOne(q.gameDate.eq(gameDate));

        if (dragonTiger == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isDragonTiger());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(dragonTiger.getGameDate());
        gameConfig.setSdate(dragonTiger.getSdate());
        gameConfig.setRound(dragonTiger.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(dragonTiger.getOdds());

        int betTime = (int) (dragonTiger.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setDragonTiger(config.isDragonTiger());
        gameConfig.setOddeven(config.isOddeven());

        return gameConfig;
    }
}
