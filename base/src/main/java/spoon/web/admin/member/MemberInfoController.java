package spoon.web.admin.member;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.bet.domain.BetUserInfo;
import spoon.bet.service.BetService;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.WebUtils;
import spoon.config.domain.Config;
import spoon.event.service.LottoService;
import spoon.game.domain.MenuCode;
import spoon.member.domain.MemberDto;
import spoon.member.domain.User;
import spoon.member.entity.Member;
import spoon.member.service.MemberListService;
import spoon.member.service.MemberService;
import spoon.member.service.MemberUpdateService;
import spoon.support.web.AjaxResult;

import java.util.List;

@AllArgsConstructor
@Controller("admin.memberInfoController")
@RequestMapping("#{config.pathAdmin}")
public class MemberInfoController {

    private MemberService memberService;

    private MemberUpdateService memberUpdateService;

    private MemberListService memberListService;

    private LottoService lottoService;

    private BetService betService;

    @RequestMapping(value = "/member/info/{userid}", method = RequestMethod.GET)
    public String memberInfo(ModelMap map, @PathVariable("userid") String userid) {
        Member member = memberService.getMember(userid);
        if (member == null) {
            map.addAttribute("message", "회원을 찾을 수 없습니다.");
            return "admin/popupError";
        } else if (WebUtils.role() != null && WebUtils.role().getValue() < member.getRole().getValue()) {
            map.addAttribute("message", "수정 권한이 없습니다.");
            return "admin/popupError";
        }

        List<User> recommList = memberListService.userRecommList(userid); //추천인 리스트

        map.addAttribute("member", member);
        map.addAttribute("recommender", recommList);
        map.addAttribute("agencyList", memberService.getAgency7List2(new MemberDto.Seller()));
        map.addAttribute("agencies", JsonUtils.toString(memberService.getAgencyList()));
        map.addAttribute("banks", Config.getBanks());
        map.addAttribute("pathJoin", Config.getPathJoin());
        map.addAttribute("betInfo", new BetUserInfo(betService.userBetList(userid)));
        map.addAttribute("betInfo2", new BetUserInfo(betService.userBetList2(userid)));
        map.addAttribute("zoneMenu", MenuCode.getZoneList());
        map.addAttribute("lotto", lottoService.getLottoCount(userid));

        return "admin/member/popup/info";
    }

    @ResponseBody
    @RequestMapping(value = "/member/update", method = RequestMethod.POST)
    public AjaxResult update(MemberDto.Update update) {
        return memberUpdateService.adminUpdate(update);
    }

    @ResponseBody
    @RequestMapping(value = "/member/delete", method = RequestMethod.POST)
    public AjaxResult delete(MemberDto.Update delete) {
        return memberUpdateService.adminDelete(delete);
    }

    @ResponseBody
    @RequestMapping(value = "/member/agencyUpdate", method = RequestMethod.POST)
    public AjaxResult agencyUpdate(MemberDto.Update update) {
        return memberUpdateService.agencyUpdate(update);
    }

    @ResponseBody
    @RequestMapping(value = "/member/powerLotto", method = RequestMethod.POST)
    public AjaxResult powerLotto(MemberDto.Update update) {
        return memberUpdateService.powerLottoUpdate(update);
    }
}
