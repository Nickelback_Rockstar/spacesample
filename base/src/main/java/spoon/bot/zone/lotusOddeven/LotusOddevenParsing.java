package spoon.bot.zone.lotusOddeven;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.DateUtils;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.StringUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.lotusOddeven.LotusOddeven;
import spoon.gameZone.lotusOddeven.service.LotusOddevenBotService;
import spoon.mapper.GameMapper;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class LotusOddevenParsing implements GameBotParsing {

    private LotusOddevenBotService lotusOddevenBotService;

    private static boolean isClosing = false;

    private GameMapper gameMapper;

    private static int lotusOddevenRound = 0;

    private static Date sdate = DateUtils.beforeMinutes(2);

    @Async
    @Override
    public void parsingGame() {
        isClosing = false;

        int count = 0;
        int round = ZoneConfig.getLotusOddeven().getZoneMaker().getRound();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(ZoneConfig.getLotusOddeven().getZoneMaker().getGameDate().getTime() - 60 * 1000));

        for (int i = 0; i < 30; i++) {
            if (cal.getTime().before(sdate)) continue;

            if (lotusOddevenBotService.notExist(cal.getTime())) {
                LotusOddeven lotusOddeven = new LotusOddeven(round > 1440 ? round % 1440 : round, cal.getTime());
                lotusOddeven.setOdds(ZoneConfig.getLotusOddeven().getOdds());
                lotusOddevenBotService.addGame(lotusOddeven);
                count++;
            }
            round++;
            cal.add(Calendar.MINUTE, 1);
        }
        sdate = cal.getTime();
        log.debug("로투스 홀짝 경기등록 : {}건", count);
    }

    @Async
    @Override
    public void closingGame() {
        //if (isClosing) return;
        //isClosing = true;

        // 한회차 땡겨 줘야 한다.
        //Date gameDate = new Date(ZoneConfig.getLotusOddeven().getZoneMaker().getGameDate().getTime() - 2 * 60 * 1000);

        String sdate = gameMapper.closingLotusOddevenSdate();

        String json = null;
        if(StringUtils.notEmpty(sdate)) {
            json = HttpParsing.getJson(Config.getSysConfig().getZone().getLotusOddevenUrl() + "/result?sdate=" + sdate);
        }
        ////System.out.println("json = "+json.toString());
        if (json == null) {
            isClosing = false;
            return;
        }else{
            JSONParser parser = new JSONParser();
            JSONArray jsonArr = null;
            try {
                jsonArr = (JSONArray) parser.parse(json);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(jsonArr != null && jsonArr.size() > 0){
                JSONObject obj = (JSONObject) jsonArr.get(0);
                if(obj.containsKey("result") && obj.get("result") != null && !"".equals(obj.get("result").toString())){
                    json = jsonArr.get(0).toString();
                }else{
                    return;
                }
            }
        }

        LotusOddeven result = JsonUtils.toModel(json, LotusOddeven.class);
        if (result == null) {
            isClosing = false;
            return;
        }else{
            if(StringUtils.empty(result.getCard1())) return;
        }

        ////System.out.println("result = "+result.toString());

        if(lotusOddevenRound > 0 && lotusOddevenRound == result.getRound()){ //처리된 경기는 패스
            return;
        }else{
            lotusOddevenRound = result.getRound();
        }

        lotusOddevenBotService.closingGame(result);

        log.debug("OZ 홀짝 경기 종료 : {}회차", result.getRound());
    }

    @Async
    @Override
    public void checkResult() {
        lotusOddevenBotService.checkResult();
    }

    @Async
    @Override
    public void deleteGame() {
        lotusOddevenBotService.deleteGame();
    }
}
