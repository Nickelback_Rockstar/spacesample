package spoon.bot.balance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import spoon.bot.balance.entity.GodBalance;

public interface GodBalanceRepository extends JpaRepository<GodBalance, Long>, QueryDslPredicateExecutor<GodBalance> {
    
}
