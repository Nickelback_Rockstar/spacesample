package spoon.bot.sports.bet365.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.sports.bet365.domain.BotBet365;
import spoon.bot.sports.service.ParsingGame;
import spoon.common.net.HttpParsing;
import spoon.common.utils.DateUtils;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.NumberUtils;
import spoon.common.utils.WebUtils;
import spoon.config.domain.Config;
import spoon.config.domain.LogInfo;
import spoon.game.domain.GameCode;
import spoon.game.domain.MenuCode;
import spoon.game.entity.Game;
import spoon.game.entity.League;
import spoon.game.entity.Sports;
import spoon.game.entity.Team;
import spoon.game.service.GameBotService;
import spoon.game.service.sports.LeagueService;
import spoon.game.service.sports.SportsService;
import spoon.game.service.sports.TeamService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class Bet365ParsingGame implements ParsingGame {

    private GameBotService gameBotService;

    private SportsService sportsService;

    private LeagueService leagueService;

    private TeamService teamService;

    private static String ut;

    private static boolean action = false;

    @Override
    @Async
    public void parsingGame() {
        if (action) return;

        action = true;
        int saved = 0;
        int updated = 0;

        String json = HttpParsing.getJson(getParsingUrl());
        ////System.out.println("파싱 url = "+getParsingUrl());
        if (json == null) {
            action = false;
            return;
        }

        List<BotBet365> list = JsonUtils.toBet365List(json);
        if (list == null) {
            action = false;
            return;
        }

        for (BotBet365 bot : list) {
            if (ut == null || ut.compareTo(bot.getUt()) < 0) ut = bot.getUt();

            if (gameBotService.isExist(bot.getSiteCode(), bot.getSiteId())) {
                if (updateGame(bot)) {
                    updated++;
                }
            } else {
                if (addGame(bot)) {
                    saved++;
                }
            }
        }

        LogInfo.setBet365ParsingDate(DateUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));

        log.debug("Bet365 게임 업데이트 - 전체 : {}, 신규: {}, 업데이트: {}", list.size(), saved, updated);

        action = false;
    }

    @Override
    public void resetUt(String update) {
        ut = update;
    }

    private boolean addGame(BotBet365 bot) {
        Game game = makeGame(bot);
        updateSports(game.getSportsBean());
        updateLeague(game.getLeagueBean(), bot.getLeagueFlag());
        updateTeam(game.getTeamHomeBean());
        updateTeam(game.getTeamAwayBean());

        /*if("야구".equals(bot.getSports())){
            if ("CPBL".equals(bot.getLeague())) {
                return false;
            }
        }

        if("야구".equals(bot.getSports())){
            if (bot.getMenuCode() == MenuCode.SPECIAL) {
                if (bot.getTeamHome().contains("첫볼넷"))
                    return false;
            }
        }

        if("야구".equals(bot.getSports())){
            if ("1이닝".equals(bot.getSpecial()) || "4이닝".equals(bot.getSpecial()) || "5이닝".equals(bot.getSpecial()))
                return false;
        }

        if("배구".equals(bot.getSports())){
            if( bot.getMenuCode() == MenuCode.MATCH || bot.getMenuCode() == MenuCode.HANDICAP || bot.getMenuCode() == MenuCode.SPECIAL ) {
                return false;
            }
        }

        if("배구".equals(bot.getSports())){
            if(bot.getTeamHome().contains("5득")) {
                return false;
            }
        }

        if("농구".equals(bot.getSports())){
            if ("KBL".equals(bot.getLeague()) || "WKBL".equals(bot.getLeague()) || "NBA".equals(bot.getLeague()))
                if( bot.getMenuCode() == MenuCode.SPECIAL || "3점".contains(bot.getTeamHome()) || "7점".contains(bot.getTeamHome()) || "자유투".contains(bot.getTeamHome())) {
                    return false;
                }
        }

        if("농구".equals(bot.getSports())){
            if(bot.getTeamHome().contains("7득")) {
                return false;
            }
        }

        if("축구".equals(bot.getSports())){
            if ("Premier".equals(bot.getLeague()) || "GRE FL".equals(bot.getLeague()) || "CHA D2".equals(bot.getLeague()))
            return false;
        }

        if("E스포츠".equals(bot.getSports()) || "e스포츠".equals(bot.getSports()) || "이스포츠".equals(bot.getSports())){
            return false;
        }*/

        return gameBotService.addGame(game);
    }

    private boolean updateGame(BotBet365 bot) {
        Game game = gameBotService.getGame(bot.getSiteCode(), bot.getSiteId());
        // 자동 업데이트가 아니라면 업데이트를 하지 않는다.
        if (!game.isAutoUpdate()) return false;
        // 업데이트가 바뀌지 않으면 업데이트를 하지 않는다.
        if (bot.getUt().equals(game.getUt())) return false;

        copyGame(bot, game);

        updateSports(game.getSportsBean());
        updateLeague(game.getLeagueBean(), bot.getLeagueFlag());
        updateTeam(game.getTeamHomeBean());
        updateTeam(game.getTeamAwayBean());

        return gameBotService.updateGame(game);
    }

    private Game makeGame(BotBet365 bot) {
        Game game = new Game();
        game.setSiteCode(bot.getSiteCode());
        game.setSiteId(bot.getSiteId());
        game.setGroupId(bot.getGroupId());
        game.setGameDate(bot.getGameDate());
        game.setSports(bot.getSports());
        game.setMenuCode(bot.getMenuCode());
        game.setGameCode(bot.getGameCode());
        game.setSpecial(bot.getSpecial());
        game.setLeague(bot.getLeague());
        game.setTeamHome(bot.getTeamHome());
        game.setTeamAway(bot.getTeamAway());
        game.setHandicap(bot.getHandicap());
        game.setBetHome(bot.isBetHome());
        game.setBetDraw(bot.isBetDraw());
        game.setBetAway(bot.isBetAway());
        game.setDeleted(bot.isDel());
        game.setClosing(bot.isClosing());
        game.setCancel(bot.isCancel());
        game.setSort(bot.getSort());
        game.setUt(bot.getUt());
        game.setAutoUpdate(autoUpdate());
        if (game.getOddsRate() >= 100) {
            game.setEnabled(false);
        } else {
            game.setEnabled(autoParsing());
        }
        game.setSType(bot.getSType());
        makeOdds(bot, game);

        if (game.getOddsHome() < 1 || game.getOddsAway() < 1) {
            game.setEnabled(false);
        }

        return game;
    }

    private void copyGame(BotBet365 bot, Game game) {
        game.setGameDate(bot.getGameDate());
        game.setSports(bot.getSports());
        game.setMenuCode(bot.getMenuCode());
        game.setGameCode(bot.getGameCode());
        game.setSpecial(bot.getSpecial());
        game.setLeague(bot.getLeague());
        game.setTeamHome(bot.getTeamHome());
        game.setTeamAway(bot.getTeamAway());
        game.setHandicap(bot.getHandicap());
        game.setBetHome(bot.isBetHome());
        game.setBetDraw(bot.isBetDraw());
        game.setBetAway(bot.isBetAway());
        game.setDeleted(bot.isDel());
        game.setClosing(bot.isClosing());
        game.setCancel(bot.isCancel());
        game.setSort(bot.getSort());
        game.setUt(bot.getUt());
        game.setSType(bot.getSType());
        makeOdds(bot, game);
    }

    private void makeOdds(BotBet365 bot, Game game) {
        double oddsUpDown = getOddsUpDown(bot.getGameCode(), bot.getMenuCode());
        double oddsPlus = getOddsPlus(bot.getGameCode(), bot.getMenuCode());
        double oddsSum = getOddsSum(bot.getOddsDraw(), bot.getGameCode(), bot.getMenuCode());

        if (oddsUpDown == 100D && oddsPlus == 0D && oddsSum == 0D) {
            game.updateOdds(bot.getOddsHome(), bot.getOddsDraw(), bot.getOddsAway());
        } else {
            double oddsHome = 0D;
            double oddsDraw = 0D;
            double oddsAway = 0D;

            if (oddsSum > 0) {
                convertOddsSum(bot, oddsSum);
                game.updateOdds(bot.getOddsHome(), bot.getOddsDraw(), bot.getOddsAway());
            } else {
                oddsHome = convertOdds(bot.getOddsHome(), oddsUpDown, oddsPlus);
                oddsDraw = convertOdds(bot.getOddsDraw(), oddsUpDown, oddsPlus);
                oddsAway = convertOdds(bot.getOddsAway(), oddsUpDown, oddsPlus);
                game.updateOdds(oddsHome, oddsDraw, oddsAway);
            }
        }
    }

    private void convertOddsSum(BotBet365 bot, double oddsSum) {
        double oddsHome = bot.getOddsHome();
        double oddsDraw = bot.getOddsDraw();
        double oddsAway = bot.getOddsAway();
        double odds = 0d;

        if (oddsDraw > 0) { //승무패
            if ((oddsHome + oddsDraw + oddsAway) > oddsSum) {
                odds = (oddsHome + oddsDraw + oddsAway) - oddsSum;
            }
        } else { //승패,핸디,언오버
            if ((oddsHome + oddsAway) > oddsSum) {
                odds = (oddsHome + oddsAway) - oddsSum;
            }
        }

        if (oddsHome >= oddsAway) {
            if (oddsHome - odds > 1) { //배당 결과가 1보다 큰 경우만 적용
                oddsHome = oddsHome - odds;
            }
        } else {
            if (oddsAway - odds > 1) { //배당 결과가 1보다 큰 경우만 적용
                oddsAway = oddsAway - odds;
            }
        }

        bot.setOddsHome(NumberUtils.odds(oddsHome));
        bot.setOddsDraw(NumberUtils.odds(oddsDraw));
        bot.setOddsAway(NumberUtils.odds(oddsAway));
    }

    private double getOddsSum(double oddsDraw, GameCode gameCode, MenuCode menuCode) {
        double odds = 100D;
        switch (gameCode) {
            case MATCH:
                if (oddsDraw > 0) { //승무패
                    odds = Config.getGameConfig().getOddsSumMatch1();
                } else {
                    odds = Config.getGameConfig().getOddsSumMatch2();
                }
                break;
            case HANDICAP:
                odds = menuCode == MenuCode.SPECIAL ? Config.getGameConfig().getOddsSumHandicapSpecial() : Config.getGameConfig().getOddsSumHandicap();
                break;
            case OVER_UNDER:
                odds = menuCode == MenuCode.SPECIAL ? Config.getGameConfig().getOddsSumOverUnderSpecial() : Config.getGameConfig().getOddsSumOverUnder();
                break;
        }
        return odds;
    }

    private double getOddsUpDown(GameCode gameCode, MenuCode menuCode) {
        double odds = 100D;
        switch (gameCode) {
            case MATCH:
                odds = Config.getGameConfig().getOddsUpDownMatch();
                break;
            case HANDICAP:
                odds = menuCode == MenuCode.SPECIAL ? Config.getGameConfig().getOddsUpDownHandicapSpecial() : Config.getGameConfig().getOddsUpDownHandicap();
                break;
            case OVER_UNDER:
                odds = menuCode == MenuCode.SPECIAL ? Config.getGameConfig().getOddsUpDownOverUnderSpecial() : Config.getGameConfig().getOddsUpDownOverUnder();
                break;
        }
        return odds;
    }

    private double getOddsPlus(GameCode gameCode, MenuCode menuCode) {
        double odds = 0D;
        switch (gameCode) {
            case MATCH:
                odds = Config.getGameConfig().getOddsPlusMatch();
                break;
            case HANDICAP:
                odds = menuCode == MenuCode.SPECIAL ? Config.getGameConfig().getOddsPlusHandicapSpecial() : Config.getGameConfig().getOddsPlusHandicap();
                break;
            case OVER_UNDER:
                odds = menuCode == MenuCode.SPECIAL ? Config.getGameConfig().getOddsPlusOverUnderSpecial() : Config.getGameConfig().getOddsPlusOverUnder();
                break;
        }
        return odds;
    }

    private double convertOdds(double odds, double oddsRate, double oddsPlusRate) {
        if (oddsRate != 100D) {
            odds = BigDecimal.valueOf(odds)
                    .multiply(BigDecimal.valueOf(oddsRate))
                    .divide(BigDecimal.valueOf(100D), 2, BigDecimal.ROUND_HALF_UP)
                    .doubleValue();
        }
        if (oddsPlusRate != 0D) {
            odds = BigDecimal.valueOf(odds).add(BigDecimal.valueOf(oddsPlusRate)).doubleValue();
        }
        return odds;
    }

    private void updateTeam(Team team) {
        if (team.getId() == null || team.getId() == 0) {
            teamService.addTeam(team);
        }
    }

    private void updateLeague(League league, String flag) {
        if (league.getId() == null || league.getId() == 0) {
            leagueService.addLeague(league, flag);
        } else if ("league.png".equals(league.getLeagueFlag()) && !"/images/league/league-default.png".equals(flag)) {
            leagueService.updateFlag(league, flag);
        }
    }

    private void updateSports(Sports sports) {
        if (sports.getId() == null || sports.getId() == 0) {
            sportsService.addSports(sports.getSportsName());
        }
    }

    private String getParsingUrl() {
        if (ut == null) ut = gameBotService.getMaxUt("365", false);
        switch (Config.getSysConfig().getSports().getBet365()) {
            case "all":
                return Config.getSysConfig().getSports().getBet365Api() + "/api/marathon/list" + (ut == null ? "" : "?ut=" + WebUtils.encoding(ut));
            /*case "cross":
                return Config.getSysConfig().getSports().getBet365Api() + "/api/marathon/cross/list" + (ut == null ? "" : "?ut=" + WebUtils.encoding(ut));
            case "crossSoccer":
                return Config.getSysConfig().getSports().getBet365Api() + "/api/marathon/crossSoccer/list" + (ut == null ? "" : "?ut=" + WebUtils.encoding(ut));
            case "special":
                return Config.getSysConfig().getSports().getBet365Api() + "/api/marathon/special/list" + (ut == null ? "" : "?ut=" + WebUtils.encoding(ut));*/
            default:
                throw new IllegalArgumentException("SysConfig > Sports > bet365Api 의 정보가 잘못되었습니다. (" + Config.getSysConfig().getSports().getBet365() + ")");
        }
    }

    private boolean autoParsing() {
        return Config.getGameConfig().isAutoParsing();
    }

    private boolean autoUpdate() {
        return Config.getGameConfig().isAutoUpdate();
    }
}
