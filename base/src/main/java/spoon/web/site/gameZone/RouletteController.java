package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.common.utils.JsonUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.roulette.RouletteDto;
import spoon.gameZone.roulette.service.RouletteGameService;
import spoon.gameZone.roulette.service.RouletteService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class RouletteController {

    private RouletteService rouletteService;

    private RouletteGameService rouletteGameService;

    @RequestMapping(value = "zone/roulette", method = RequestMethod.GET)
    public String zone(ModelMap map) {
        map.addAttribute("config", JsonUtils.toString(rouletteService.gameConfig()));
        return "site/zone/roulette";
    }

    @RequestMapping(value = "zone/roulette/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", rouletteService.getClosing(command, pageable));
        return "site/score/roulette";
    }

    @ResponseBody
    @RequestMapping(value = "zone/roulette/config", method = RequestMethod.POST)
    public RouletteDto.Config config() {
        return rouletteService.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/roulette/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return rouletteGameService.betting(bet);
    }
}
