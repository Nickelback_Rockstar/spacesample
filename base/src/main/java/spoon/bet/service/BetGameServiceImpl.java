package spoon.bet.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import spoon.bet.domain.BetDto;
import spoon.bet.entity.Bet;
import spoon.bet.entity.BetItem;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.DateUtils;
import spoon.common.utils.WebUtils;
import spoon.config.domain.Config;
import spoon.config.domain.ConfigDto;
import spoon.config.service.GameConfigService;
import spoon.game.domain.*;
import spoon.game.entity.Game;
import spoon.game.repository.GameRepository;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static spoon.game.domain.MenuCode.isSports;

@Slf4j
@AllArgsConstructor
@Service
public class BetGameServiceImpl implements BetGameService {

    private BetService betService;

    private MemberService memberService;

    private GameConfigService gameConfigService;

    private GameRepository gameRepository;

    private BetItemRepository betItemRepository;

    @Override
    public AjaxResult addGameBetting(BetDto.BetGame betGame) {
        AjaxResult result = new AjaxResult();
        String userid = WebUtils.userid();

        if (userid == null) {
            result.setMessage("베팅에 실패하였습니다.");
            return result;
        }

        Member member = memberService.getMember(userid);
        if (member.isSecession() || !member.isEnabled()) {
            result.setMessage("베팅에 실패하였습니다.");
            return result;
        }

        int gameCnt = betGame.getIds().length;
        ConfigDto.Game gameConfig = gameConfigService.gameConfig(betGame.getMenuCode());

        //인게임은 단폴더만
        if (betGame.getIds().length > 1 && betGame.getMenuCode() == MenuCode.INGAME) {
            result.setMessage("인게임은 단폴더만 배팅 가능합니다..");
            return result;
        }

        //단투폴더 금지회원은 리턴
        if (betGame.getIds().length <= 2 && member.isFolderBlock()) {
            result.setMessage("회원님은 단투폴더는 배팅이 불가능합니다.");
            return result;
        }

        // 보유금액 체크
        if (member.getMoney() < betGame.getBetMoney() && member.getRole() == Role.USER) {
            result.setMessage("보유머니가 부족합니다.");
            return result;
        }

        // 단폴더 베팅 가능 체크
        if (gameCnt == 1 && !gameConfig.isOne() && betGame.getMenuCode() != MenuCode.INGAME) {
            result.setMessage("단폴더 베팅은 불가능 합니다.");
            return result;
        }

        // 최대 폴더 체크
        if (gameCnt > gameConfig.getSportsMaxFolder()) {
            result.setMessage("최대 조합 가능 폴더는 " + gameConfig.getSportsMaxFolder() + " 입니다.");
            return result;
        }

        // 최소 베팅 머니
        int minMoney = gameCnt == 1 ? gameConfig.getOneMin() : gameConfig.getMin();
        if (betGame.getBetMoney() < minMoney) {
            result.setMessage("최소 베팅 금액은 " + NumberFormat.getIntegerInstance().format(minMoney) + "원 입니다.");
            return result;
        }

        // 최대 베팅 머니
        int maxMoney = gameCnt == 1 ? gameConfig.getOneMax() : gameConfig.getMax();
        if (betGame.getBetMoney() > maxMoney) {
            result.setMessage("최대 베팅 금액은 " + NumberFormat.getIntegerInstance().format(maxMoney) + "원 입니다.");
            return result;
        }

        List<BetItem> betItems = new ArrayList<>();
        BigDecimal betOdds = BigDecimal.valueOf(1);
        Date startDate = DateUtils.beforeDays(-1);
        Date endDate = new Date();
        String betInfo = "";

        JSONObject inGame = new JSONObject();
        JSONObject inGameDetail = new JSONObject();
        JSONParser parser = new JSONParser();
        JSONArray jsonArr = new JSONArray();
        String inGameBetScore = "";
        if (betGame.getMenuCode() == MenuCode.INGAME) {
            Long idx = betGame.getIds()[0];
            double odds = betGame.getOdds()[0];
            String oname = betGame.getBets()[0];
            String json = HttpParsing.getJson(Config.getSysConfig().getSports().getInGameApi() + "/api/gameOdds?idx=" + idx);
            //System.out.println(json);
            boolean isInGameApi = false;
            JSONObject inGameJson;
            try {
                inGameJson = (JSONObject) parser.parse(json);
                inGame = (JSONObject) inGameJson.get("game");
                inGameDetail = (JSONObject) inGameJson.get("detail");

                //System.out.println("inGame.toString()=" + inGame.toString());
                JSONObject liveScore = (JSONObject) parser.parse(inGame.get("liveScore").toString());
                JSONObject scoreboard = (JSONObject) liveScore.get("Scoreboard");
                JSONArray scoreArr = (JSONArray) scoreboard.get("Results");
                String home = "";
                String away = "";
                for (int i = 0; i < scoreArr.size(); i++) {
                    JSONObject j = (JSONObject) scoreArr.get(i);
                    if ("1".equals(j.get("Position").toString())) {
                        home = j.get("Value").toString();
                    } else if ("2".equals(j.get("Position").toString())) {
                        away = j.get("Value").toString();
                    }
                }

                if (StringUtils.isEmpty(home) || StringUtils.isEmpty(away)) {
                    result.setMessage("인게임 스코어 정보가 올바르지 않습니다. 잠시 후 다시 베팅하여 주세요.");
                    return result;
                }

                inGameBetScore = home + " : " + away;

            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (inGameDetail.containsKey("idx") && inGameDetail.containsKey("odds") && inGameDetail.containsKey("oname")) {
                //System.out.println("idx=" + idx);
                //System.out.println("odds=" + odds);
                //System.out.println("oname=" + oname);
                if (odds == Double.parseDouble(inGameDetail.get("odds").toString()) && oname.equals(inGameDetail.get("onameKor").toString())) {
                    isInGameApi = true;
                }
            }
            if (!isInGameApi) {
                result.setMessage("인게임 api 정보가 올바르지 않습니다. 잠시 후 다시 베팅하여 주세요.");
                return result;
            }
        }

        boolean eSports = false;

        //인게임 베팅
        String inGameSports = "";
        if (betGame.getMenuCode() == MenuCode.INGAME) {
            /*
            game -----------------------------------------
            gameDate : "2019-08-05 18:00:00"
            aname : "Chiba Lotte Marines"
            league : "NPB"
            hnameKor : "Rakuten Golden Eagles"
            leagueKor : "NPB"
            hms : "18:00:00"
            ymd : "2019-08-05"
            sname : "Baseball"
            anameKor : "Chiba Lotte Marines"
            snameKor : "Baseball"
            location : "Japan"
            idx : 185
            gameTs : 1564995600000
            hname : "Rakuten Golden Eagles"

            detail -----------------------------------------
            result : 0
            onameKor : "12 Including Overtime"
            baseLine : 0
            oname : "12 Including Overtime"
            line : 0
            price : 16
            odds : 16
            lastUpdate : "2019-08-05 20:11:07"
            name : "1"
            idx : 1007503
            lastTs : 1565003467000
            status : 1
            */

            //인게임은 game 테이블에 없으니 임의로 세팅
            Game game = new Game();
            game.setId(Long.parseLong(inGameDetail.get("idx").toString()));
            game.setGameCode(GameCode.INGAME);
            game.setGameDate(new Date(Long.parseLong(inGame.get("gameTs").toString())));
            game.setGroupId(inGame.get("idx").toString());
            game.setHandicap(Double.parseDouble(inGameDetail.get("baseLine").toString()));
            game.setLeague(inGame.get("league").toString());
            game.setMenuCode(MenuCode.INGAME);
            game.setOddsHome(Double.parseDouble(inGameDetail.get("odds").toString()));
            game.setOddsDraw(0);
            game.setOddsAway(0);
            game.setOddsRate(0);
            game.setResult(GameResult.READY);
            game.setSiteCode("inGame");
            game.setSiteId(inGameDetail.get("idx").toString());
            game.setSort(0);
            game.setSports(inGame.get("snameKor").toString());
            inGameSports = game.getSports();
            game.setTeamHome(inGame.get("hnameKor").toString());
            game.setTeamAway(inGame.get("anameKor").toString());
            game.setUpDownHome(UpDown.KEEP);
            game.setUpDownAway(UpDown.KEEP);
            game.setUpDownDraw(UpDown.KEEP);
            game.setUt(inGameDetail.get("lastUpdate").toString());
            String baseLine = "";
            if (Double.parseDouble(inGameDetail.get("baseLine").toString()) != 0) {
                baseLine = "(" + inGameDetail.get("baseLine").toString() + ")";
            }
            game.setSpecial("" + inGameDetail.get("onameKor").toString() + baseLine + " #### " + inGameDetail.get("name").toString());

            betInfo = game.getSports() + "####" + game.getTeamHome() + "####" + game.getTeamAway() + "####" + game.getSpecial();

            //단일 베팅이므로 베팅시 홈으로 지정
            BetItem betItem = new BetItem(game, userid, member.getRole(), "home", betGame.getBetMoney());
            betItems.add(betItem);

        } else {


            for (int i = 0; i < betGame.getIds().length; i++) {
                String pos = betGame.getBets()[i];
                Double odds = betGame.getOdds()[i];
                Game game = gameRepository.findOne(betGame.getIds()[i]);
                if ("e스포츠".equals(game.getSports())) {
                    //이스포츠면 체크
                    eSports = true;
                }

                if (startDate.after(game.getGameDate())) {
                    startDate = game.getGameDate();
                }

                if (endDate.before(game.getGameDate())) {
                    endDate = game.getGameDate();
                }

                // 마감경기 체크 , 인게임은 패스
                if (!game.isBeforeGameDate() && betGame.getMenuCode() != MenuCode.INGAME) {
                    result.setMessage("마감된 경기가 있습니다. 다시 베팅하여 주세요.");
                    return result;
                }

                // 베팅 중지 체크
                switch (pos) {
                    case "home":
                        if (!game.isBetHome()) {
                            result.setMessage("베팅 중지된 경기가 있습니다. 다시 베팅하여 주세요.");
                            return result;
                        }
                        if (game.getOddsHome() != odds) {
                            result.setMessage("배당이 변경된 경기가 있습니다. 다시 베팅하여 주세요.");
                            return result;
                        }
                        betOdds = betOdds.multiply(BigDecimal.valueOf(game.getOddsHome()));
                        break;
                    case "draw":
                        if (!game.isBetDraw()) {
                            result.setMessage("베팅 중지된 경기가 있습니다. 다시 베팅하여 주세요.");
                            return result;
                        }
                        if (game.getOddsDraw() != odds) {
                            result.setMessage("배당이 변경된 경기가 있습니다. 다시 베팅하여 주세요.");
                            return result;
                        }
                        betOdds = betOdds.multiply(BigDecimal.valueOf(game.getOddsDraw()));
                        break;
                    case "away":
                        if (!game.isBetAway()) {
                            result.setMessage("베팅 중지된 경기가 있습니다. 다시 베팅하여 주세요.");
                            return result;
                        }
                        if (game.getOddsAway() != odds) {
                            result.setMessage("배당이 변경된 경기가 있습니다. 다시 베팅하여 주세요.");
                            return result;
                        }
                        betOdds = betOdds.multiply(BigDecimal.valueOf(game.getOddsAway()));
                        break;
                }
                BetItem betItem = new BetItem(game, userid, member.getRole(), pos, betGame.getBetMoney());
                betItems.add(betItem);
            }
        }

        // 최대 적중 금액
        int winMoney = gameCnt == 1 ? gameConfig.getOneWin() : gameConfig.getWin();
        if (betOdds.setScale(2, BigDecimal.ROUND_DOWN).doubleValue() * betGame.getBetMoney() > winMoney) {
            result.setMessage("최대 적중 금액은 " + NumberFormat.getIntegerInstance().format(winMoney) + "원 입니다.");
            return result;
        }



        Bet bet = new Bet(member.getUser());
        bet.setBetMoney(betGame.getBetMoney());
        bet.setBetItems(betItems);
        bet.setMenuCode(betGame.getMenuCode());
        bet.setBetCount(gameCnt);
        bet.setBetDate(new Date());
        bet.setStartDate(startDate);
        bet.setEndDate(endDate);
        bet.setIp(WebUtils.ip());
        bet.setEsports(eSports);
        bet.setBetInfo(betInfo);
        bet.setBetScore(inGameBetScore);
        if (betGame.getMenuCode() == MenuCode.INGAME) {
            if ("농구".equals(inGameSports) || "배구".equals(inGameSports)) {
                bet.setStatus(StatusCode.SUCCESS); //농구 배구는 바로 베팅 성공
            } else {
                bet.setStatus(StatusCode.STAY); //인게임은 대기상태로 저장, 설정시간 후 스코어 비교하여 성공여부
            }
        }

        // 이벤트 배당하락 또는 배당상승 체크 (betOdds, expMoney 채워 줌)
        bet.betExpOdds();
        if (gameCnt == 1 && Config.getGameConfig().getBonusOne() > 0 && betGame.getMenuCode() != MenuCode.LIVE && betGame.getMenuCode() != MenuCode.INGAME) {
            result.setValue("one");
        }

        // 중복베팅 축베팅 확인
        if (isSports(betGame.getMenuCode())) {
            // 중복베팅 확인
            for (Bet oldBet : betService.getCurrentBet(userid)) {
                int count = 0;
                int inGameCount = 0;
                for (BetItem oldItem : oldBet.getBetItems()) {
                    for (BetItem item : bet.getBetItems()) {
                        // 게임코드, 메뉴코드, 그룹아이디로 체크
                        if (item.getMenuCode() != MenuCode.SPECIAL || item.getMenuCode() != MenuCode.INGAME) {
                            if (oldItem.getGroupId().equals(item.getGroupId())
                                    && oldItem.getGameCode() == item.getGameCode()
                                    && oldItem.getMenuCode() == item.getMenuCode()
                                    && !oldItem.isCancel()) {
                                if (item.getGameCode() == GameCode.HANDICAP && item.getHandicap() != oldItem.getHandicap()) {
//                                result.setMessage(oldItem.getTeamHome() + " VS" + oldItem.getTeamAway() + " 경기의 스프레드 핸디는 한번만 선택 가능 합니다.");
//                                return result;
                                } else if (item.getGameCode() == GameCode.OVER_UNDER && item.getHandicap() != oldItem.getHandicap()) {
//                                result.setMessage(oldItem.getTeamHome() + " VS" + oldItem.getTeamAway() + " 경기의 스프레드 오버언더는 한번만 선택 가능 합니다.");
//                                return result;
                                }
                                count++;
                            }
                        }
                        if (oldItem.getGroupId().equals(item.getGroupId())) {
                            inGameCount++;
                        }
                    }
                    /*if(betGame.getMenuCode() == MenuCode.INGAME && inGameCount > 0){
                        result.setMessage("중복된 베팅이 있습니다.\n\n동일 조합베팅은 허용되지 않습니다.");
                        return result;
                    }*/
//                    if (bet.getBetCount() == count && oldBet.getBetCount() == count) {
//                        result.setMessage("중복된 베팅이 있습니다.\n\n동일 조합베팅은 허용되지 않습니다.");
//                        return result;
//                    }
                }
            }

            // 축베팅 확인
            for (BetItem item : bet.getBetItems()) {
                long betSumMoney = 0;
                long betExpMoney = 0;
                int betCnt = 1;

                for (BetItem oldItem : betService.betMoneyByItem(userid, item.getGameId(), item.getBetTeam())) {
                    if (oldItem.getBet().isClosing() || oldItem.getBet().isCancel()) continue;
                    betSumMoney += oldItem.getBet().getBetMoney();
                    betExpMoney += oldItem.getBet().getExpMoney();
                    betCnt++;
                }

                // 축베팅은 2번까지만 허용
                if (betCnt > gameConfig.getSportsBetCnt()) {
                    result.setMessage("규정상 축베팅은 " + gameConfig.getSportsBetCnt() + "번까지만 허용 됩니다.");
                    return result;
                }

                int configMax = gameConfig.getMax();
                if (gameCnt == 1) {
                    configMax = gameConfig.getOneMax();
                }

                if (betSumMoney + bet.getBetMoney() > configMax) {
                    result.setMessage("베팅된 게임중 베팅금액 합이 " + (configMax / 10000) + "만원 넘는 게임이 있습니다.");
                    return result;
                }

                if (betExpMoney + bet.getExpMoney() > gameConfig.getMark()  && item.getMenuCode() != MenuCode.INGAME) {
                    result.setMessage("베팅된 게임중 예상 당첨금 합이 축베팅 한도 " + (gameConfig.getMark() / 10000) + "만원 넘는 게임이 있습니다.");
                    return result;
                }

            }
        }

        // 베팅을 저장한다.
        boolean success = betService.addGameBetting(bet);
        if (success) {
            result.setSuccess(true);
        } else {
            result.setMessage("베팅에 실패하였습니다. 잠시후 다시 시도하세요.");
        }

        return result;
    }


    @Transactional(readOnly = true)
    @Override
    public Iterable<BetItem> getBalanceBet(MenuCode menuCode, String sdate) {
        QBetItem q = QBetItem.betItem;
        return betItemRepository.findAll(q.groupId.eq(sdate).and(q.menuCode.eq(menuCode)).and(q.role.eq(Role.USER)).and(q.bet.balance.isTrue()));
    }

}
