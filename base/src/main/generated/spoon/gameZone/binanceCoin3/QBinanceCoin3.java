package spoon.gameZone.binanceCoin3;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBinanceCoin3 is a Querydsl query type for BinanceCoin3
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBinanceCoin3 extends EntityPathBase<BinanceCoin3> {

    private static final long serialVersionUID = -1541691493L;

    public static final QBinanceCoin3 binanceCoin3 = new QBinanceCoin3("binanceCoin3");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath oddEven = createString("oddEven");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath overUnder = createString("overUnder");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public QBinanceCoin3(String variable) {
        super(BinanceCoin3.class, forVariable(variable));
    }

    public QBinanceCoin3(Path<? extends BinanceCoin3> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBinanceCoin3(PathMetadata metadata) {
        super(BinanceCoin3.class, metadata);
    }

}

