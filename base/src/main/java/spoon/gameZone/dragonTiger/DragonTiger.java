package spoon.gameZone.dragonTiger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import spoon.bot.support.ZoneHelper;
import spoon.common.utils.DateUtils;
import spoon.game.domain.MenuCode;
import spoon.gameZone.GameZoneException;
import spoon.gameZone.Zone;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneScore;
import spoon.support.convert.DoubleArrayConvert;
import spoon.support.convert.LongArrayConvert;

import javax.persistence.*;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "ZONE_DRAGONTIGER", indexes = {
        @Index(name = "IDX_sdate", columnList = "sdate")
})
public class DragonTiger {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date gameDate;

    private int round;

    @Column(length = 16)
    private String dragon;

    @Column(length = 16)
    private String tiger;

    private int sum;

    @Column(length = 16)
    private String result;

    @Column(length = 16)
    private String sdate;

    @Convert(converter = LongArrayConvert.class)
    private long[] amount = new long[5];

    @Convert(converter = DoubleArrayConvert.class)
    private double[] odds = new double[5];

    private boolean closing;

    private boolean cancel;

    //--------------------------------------------------------

    public DragonTiger(int round, Date gameDate) {
        this.round = round;
        this.gameDate = gameDate;
        this.sdate = DateUtils.format(gameDate, "yyyyMMddHHmm");
    }

    public boolean isBeforeGameDate() {
        return this.gameDate.getTime() - System.currentTimeMillis() > 0;
    }

    // 스코어 입력
    public void updateScore(DragonTigerDto.Score score) {
        if (score.isCancel()) {
            this.dragon = "";
            this.tiger = "";
            this.sum = 0;
            this.result = "";
            this.cancel = true;
        } else {
            this.dragon = score.getDragon();
            this.tiger = score.getTiger();
            this.sum = score.getSum();
            this.result = score.getResult();
            this.cancel = false;
        }
        this.closing = true;
    }

    // 경기 결과
    public ZoneScore getGameResult(String gameCode) {
        switch (gameCode) {
            case "dragonTiger":
                return ZoneHelper.zoneResult(result ,cancel);
            case "oddeven":
                return ZoneHelper.zoneDragonTigerOddEven(sum, cancel);
            default:
                throw new GameZoneException("드래곤타이거 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }


    public Zone getZone(String gameCode) {
        switch (gameCode) {
            case "dragonTiger":
                Zone zone1 = getDragonTigerZone("dragonTiger", "드래곤", "타이거", 0, 2);
                zone1.setOddsDraw(ZoneConfig.getDragonTiger().getOdds()[1]);
                return zone1;
            case "oddeven":
                return getDragonTigerZone("oddeven", "홀", "짝", 3, 4);
            default:
                throw new GameZoneException("드래곤타이거 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    private Zone getDragonTigerZone(String gameCode, String teamHome, String teamAway, int idxHome, int idxAway) {
        Zone zone = new Zone();
        zone.setId(this.getId());
        zone.setSdate(this.getSdate());
        zone.setMenuCode(MenuCode.DRAGONTIGER);
        zone.setGameCode(gameCode);
        zone.setLeague(String.format("%03d회차 드래곤타이거", this.round));
        zone.setTeamHome(String.format("%03d회차 [%s]", this.round, teamHome));
        zone.setTeamAway(String.format("%03d회차 [%s]", this.round, teamAway));
        zone.setHandicap(0);
        zone.setOddsHome(ZoneConfig.getDragonTiger().getOdds()[idxHome]);
        zone.setOddsDraw(0);
        zone.setOddsAway(ZoneConfig.getDragonTiger().getOdds()[idxAway]);
        zone.setGameDate(this.gameDate);
        return zone;
    }

    public boolean isChangeResult(DragonTigerDto.Score score) {
        return !this.closing || this.cancel != score.isCancel()
                || !(score.getDragon().equals(this.dragon) && score.getTiger().equals(this.tiger) && score.getResult().equals(this.result));

    }
}
