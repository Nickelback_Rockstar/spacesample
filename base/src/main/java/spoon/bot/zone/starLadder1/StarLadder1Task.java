package spoon.bot.zone.starLadder1;

import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class StarLadder1Task {

    private StarLadder1Parsing starLadder1Parsing;

    @Scheduled(cron = "0 * * * * ?")
    public void gameTask() {
        starLadder1Parsing.parsingGame();
    }

    @Scheduled(cron = "3,8 * * * * ?")
    public void closingTask() {
        starLadder1Parsing.closingGame();
    }

    @Scheduled(cron = "55 0 0 * * ?")
    public void cleanTask() {
        starLadder1Parsing.deleteGame();
    }


}
