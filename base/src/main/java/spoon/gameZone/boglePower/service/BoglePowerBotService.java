package spoon.gameZone.boglePower.service;

import spoon.gameZone.boglePower.BoglePower;

import java.util.Date;

public interface BoglePowerBotService {

    boolean notExist(Date gameDate);

    void addGame(BoglePower boglePower);

    boolean closingGame(BoglePower result);

    void checkResult();

    void deleteGame(int days);

}
