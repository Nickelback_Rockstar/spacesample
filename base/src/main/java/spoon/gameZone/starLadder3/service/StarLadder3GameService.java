package spoon.gameZone.starLadder3.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.starLadder3.StarLadder3;
import spoon.support.web.AjaxResult;

public interface StarLadder3GameService {

    /**
     * 별다리3분 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 별다리3분 게임 베팅을 클로징 한다.
     */
    void closingBetting(StarLadder3 starLadder3);

    /**
     * 별다리3분 게임을 롤백 한다.
     */
    void rollbackPayment(StarLadder3 starLadder3);
}
