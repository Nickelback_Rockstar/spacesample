package spoon.gameZone.eosPower3.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.eosPower3.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class EosPower3ServiceImpl implements EosPower3Service {

    private ConfigService configService;

    private MemberService memberService;

    private EosPower3GameService eosPower3GameService;

    private EosPower3BotService eosPower3BotService;

    private EosPower3Repository eosPower3Repository;

    private BetItemRepository betItemRepository;

    private static QEosPower3 q = QEosPower3.eosPower3;

    @Transactional
    @Override
    public boolean updateConfig(EosPower3Config powerConfig) {
        try {
            configService.updateZoneConfig("eosPower3", JsonUtils.toString(powerConfig));
            ZoneConfig.setEosPower3(powerConfig);
            // 이미 등록된 게임의 배당을 변경한다.
            eosPower3Repository.updateOdds(ZoneConfig.getEosPower3().getOdds());
        } catch (RuntimeException e) {
            log.error("EOS파워볼 3분 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<EosPower3> getComplete() {
        return eosPower3Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<EosPower3> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return eosPower3Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public EosPower3Dto.Score findScore(Long id) {
        EosPower3 power = eosPower3Repository.findOne(id);

        EosPower3Dto.Score score = new EosPower3Dto.Score();
        score.setId(power.getId());
        score.setRound(power.getRound());
        score.setTimes(power.getTimes());
        score.setGameDate(power.getGameDate());
        score.setPb(power.getPb());
        score.setBall(power.getBall());
        score.setCancel(power.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getEosPower3Url() + "?sdate=" + power.getSdate());
        if (json == null) return score;

        power = JsonUtils.toModel(json, EosPower3.class);
        if (power == null) return score;

        // 봇에 결과가 있다면
        if (power.isClosing()) {
            score.setPb(power.getPb());
            score.setBall(power.getBall());

            if (StringUtils.empty(power.getPb())) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(EosPower3Dto.Score score) {
        EosPower3 power = eosPower3Repository.findOne(score.getId());

        try {
            if (power.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                eosPower3GameService.rollbackPayment(power);
            }

            // 스코어 입력
            power.updateScore(score);
            eosPower3Repository.saveAndFlush(power);
            eosPower3GameService.closingBetting(power);
            eosPower3BotService.checkResult();
        } catch (RuntimeException e) {
            log.error("EOS파워볼 3분 {} - {}회차 수동처리를 하지 못하였습니다. - {}", power.getGameDate(), power.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<EosPower3> iterable = eosPower3Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(3))));
        for (EosPower3 power : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.EOSPOWER3).and(qi.groupId.eq(power.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getEosPower3Url() + "?sdate=" + power.getSdate());
            if (json == null) continue;

            EosPower3 result = JsonUtils.toModel(json, EosPower3.class);
            if (result == null) continue;

            if (result.isClosing()) {
                power.setOddeven(result.getOddeven());
                power.setPb_oddeven(result.getPb_oddeven());
                power.setOverunder(result.getOverunder());
                power.setPb_overunder(result.getPb_overunder());
                power.setSize(result.getSize());
                power.setSum(result.getSum());
                power.setPb(result.getPb());
                power.setBall(result.getBall());

                power.setClosing(true);
                eosPower3Repository.saveAndFlush(power);
                closing++;
            }
        }
        eosPower3BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public EosPower3Dto.Config gameConfig() {
        EosPower3Dto.Config gameConfig = new EosPower3Dto.Config();
        EosPower3Config config = ZoneConfig.getEosPower3();

        Date gameDate = config.getZoneMaker().getGameDate();
        EosPower3 power = eosPower3Repository.findOne(q.gameDate.eq(gameDate));

        if (power == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isEosPower3());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(power.getGameDate());
        gameConfig.setSdate(power.getSdate());
        gameConfig.setRound(power.getRound());
        gameConfig.setTimes(power.getTimes());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(power.getOdds());

        int betTime = (int) (power.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setOdd_overunder(config.isOdd_overunder());
        gameConfig.setEven_overunder(config.isEven_overunder());

        gameConfig.setPb_oddeven(config.isPb_oddeven());
        gameConfig.setPb_odd_overunder(config.isPb_odd_overunder());
        gameConfig.setPb_even_overunder(config.isPb_even_overunder());

        gameConfig.setOverunder(config.isOverunder());
        gameConfig.setPb_overunder(config.isPb_overunder());

        gameConfig.setSize(config.isSize());
        gameConfig.setOdd_size(config.isOdd_size());
        gameConfig.setEven_size(config.isEven_size());

        return gameConfig;
    }
}
