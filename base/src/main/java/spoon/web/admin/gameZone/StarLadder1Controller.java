package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.starLadder1.StarLadder1Config;
import spoon.gameZone.starLadder1.StarLadder1Dto;
import spoon.gameZone.starLadder1.service.StarLadder1Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.starLadder1Controller")
@RequestMapping(value = "#{config.pathAdmin}")
public class StarLadder1Controller {

    private StarLadder1Service starLadder1Service;

    /**
     * 별다리1분 설정
     */
    @RequestMapping(value = "zone/starLadder1/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getStarLadder1());
        return "admin/zone/starLadder1/config";
    }

    /**
     * 별다리1분 설정 변경
     */
    @RequestMapping(value = "zone/starLadder1/config", method = RequestMethod.POST)
    public String config(StarLadder1Config starLadder1Config, RedirectAttributes ra) {
        boolean success = starLadder1Service.updateConfig(starLadder1Config);
        if (success) {
            ra.addFlashAttribute("message", "별다리1분게임 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "별다리1분게임 설정변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/starLadder1/config";
    }

    /**
     * 별다리1분 진행
     */
    @RequestMapping(value = "zone/starLadder1/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", starLadder1Service.getComplete());
        map.addAttribute("config", ZoneConfig.getLadder());
        return "admin/zone/starLadder1/complete";
    }

    /**
     * 별다리1분 완료
     */
    @RequestMapping(value = "zone/starLadder1/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", starLadder1Service.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getStarLadder1());
        return "admin/zone/starLadder1/closing";
    }

    /**
     * 별다리1분 스코어 입력 폼
     */
    @RequestMapping(value = "zone/starLadder1/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", starLadder1Service.findScore(id));
        return "admin/zone/starLadder1/score";
    }

    /**
     * 별다리1분 스코어 결과처리
     */
    @RequestMapping(value = "zone/starLadder1/score", method = RequestMethod.POST)
    public String score(StarLadder1Dto.Score score, RedirectAttributes ra) {
        boolean success = starLadder1Service.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "별다리1분 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "별다리1분 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/starLadder1/score?id=" + score.getId();
    }

    /**
     * 별다리1분 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/starLadder1/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return starLadder1Service.closingAllGame();
    }

}
