package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.kenoSpeed.KenoSpeedConfig;
import spoon.gameZone.kenoSpeed.KenoSpeedDto;
import spoon.gameZone.kenoSpeed.service.KenoSpeedService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.kenoSpeedController")
@RequestMapping(value = "#{config.pathAdmin}")
public class KenoSpeedController {

    private KenoSpeedService kenoSpeedService;

    /**
     * 스피드키노 설정
     */
    @RequestMapping(value = "zone/kenoSpeed/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getKenoSpeed());
        return "admin/zone/kenoSpeed/config";
    }

    /**
     * 스피드키노 설정 변경
     */
    @RequestMapping(value = "zone/kenoSpeed/config", method = RequestMethod.POST)
    public String config(KenoSpeedConfig kenoSpeedConfig, RedirectAttributes ra) {
        boolean success = kenoSpeedService.updateConfig(kenoSpeedConfig);
        if (success) {
            ra.addFlashAttribute("message", "스피드키노게임 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "스피드키노게임 설정변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/kenoSpeed/config";
    }

    /**
     * 스피드키노 진행
     */
    @RequestMapping(value = "zone/kenoSpeed/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", kenoSpeedService.getComplete());
        map.addAttribute("config", ZoneConfig.getKenoSpeed());
        return "admin/zone/kenoSpeed/complete";
    }

    /**
     * 스피드키노 완료
     */
    @RequestMapping(value = "zone/kenoSpeed/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", kenoSpeedService.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getKenoSpeed());
        return "admin/zone/kenoSpeed/closing";
    }

    /**
     * 스피드키노 스코어 입력 폼
     */
    @RequestMapping(value = "zone/kenoSpeed/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", kenoSpeedService.findScore(id));
        return "admin/zone/kenoSpeed/score";
    }

    /**
     * 스피드키노 스코어 결과처리
     */
    @RequestMapping(value = "zone/kenoSpeed/score", method = RequestMethod.POST)
    public String score(KenoSpeedDto.Score score, RedirectAttributes ra) {
        boolean success = kenoSpeedService.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "스피드키노 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "스피드키노 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/kenoSpeed/score?id=" + score.getId();
    }

    /**
     * 스피드키노 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/kenoSpeed/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return kenoSpeedService.closingAllGame();
    }

}
