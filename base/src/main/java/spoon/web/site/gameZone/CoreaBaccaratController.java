package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.common.utils.JsonUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.coreaBaccarat.CoreaBaccaratDto;
import spoon.gameZone.coreaBaccarat.service.CoreaBaccaratGameService;
import spoon.gameZone.coreaBaccarat.service.CoreaBaccaratService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class CoreaBaccaratController {

    private CoreaBaccaratService coreaBaccaratService;

    private CoreaBaccaratGameService coreaBaccaratGameService;

    @RequestMapping(value = "zone/coreaBaccarat", method = RequestMethod.GET)
    public String zone(ModelMap map) {
        map.addAttribute("config", JsonUtils.toString(coreaBaccaratService.gameConfig()));
        return "site/zone/coreaBaccarat";
    }

    @RequestMapping(value = "zone/coreaBaccarat/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", coreaBaccaratService.getClosing(command, pageable));
        return "site/score/coreaBaccarat";
    }

    @ResponseBody
    @RequestMapping(value = "zone/coreaBaccarat/config", method = RequestMethod.POST)
    public CoreaBaccaratDto.Config config() {
        return coreaBaccaratService.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/coreaBaccarat/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return coreaBaccaratGameService.betting(bet);
    }
}
