package spoon.gameZone.eosPower3;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import spoon.member.domain.Role;

import javax.persistence.*;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "BET_POWERLOTTO")
public class EosPower3Lotto {

    @Id
    @JsonIgnore
    private Long id;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date startDate;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date betDate;

    private int round;

    private int times;

    private int hitCount;
    private int ball1;
    private int ball2;
    private int ball3;
    private int ball4;
    private int ball5;

    private boolean ballHit1;
    private boolean ballHit2;
    private boolean ballHit3;
    private boolean ballHit4;
    private boolean ballHit5;

    private String result;
    private long betTotMoney;
    private long betMoney;
    private long hitMoney;

    private String nickname;

    private String userid;

    @Column(columnDefinition = "nvarchar(64)")
    private String agency1 = "";

    @Column(columnDefinition = "nvarchar(64)")
    private String agency2 = "";

    @Column(columnDefinition = "nvarchar(64)")
    private String agency3 = "";

    @Column(columnDefinition = "nvarchar(64)")
    private String agency4 = "";

    @Column(columnDefinition = "nvarchar(64)")
    private String agency5 = "";

    @Column(columnDefinition = "nvarchar(64)")
    private String agency6 = "";

    @Column(columnDefinition = "nvarchar(64)")
    private String agency7 = "";

    @Enumerated(EnumType.STRING)
    @Column(length = 16, nullable = false)
    private Role role;
}
