package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.common.utils.JsonUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.kenoSpeed.KenoSpeedDto;
import spoon.gameZone.kenoSpeed.service.KenoSpeedGameService;
import spoon.gameZone.kenoSpeed.service.KenoSpeedService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class KenoSpeedController {

    private KenoSpeedService kenoSpeedService;

    private KenoSpeedGameService kenoSpeedGameService;

    @RequestMapping(value = "zone/kenoSpeed", method = RequestMethod.GET)
    public String zone(ModelMap map) {
        map.addAttribute("config", JsonUtils.toString(kenoSpeedService.gameConfig()));
        //System.out.println(JsonUtils.toString(kenoSpeedService.gameConfig()));
        return "site/zone/kenoSpeed";
    }

    @RequestMapping(value = "zone/kenoSpeed/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", kenoSpeedService.getClosing(command, pageable));
        return "site/score/kenoSpeed";
    }

    @ResponseBody
    @RequestMapping(value = "zone/kenoSpeed/config", method = RequestMethod.POST)
    public KenoSpeedDto.Config config() {
        return kenoSpeedService.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/kenoSpeed/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return kenoSpeedGameService.betting(bet);
    }

}
