package spoon.banking.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;
import spoon.banking.domain.BankingCode;
import spoon.banking.domain.DepositDto;
import spoon.banking.entity.Banking;
import spoon.common.net.HttpParsing;
import spoon.common.utils.CasinoUtils;
import spoon.common.utils.DateUtils;
import spoon.common.utils.StringUtils;
import spoon.common.utils.WebUtils;
import spoon.config.domain.Config;
import spoon.config.domain.SiteConfig;
import spoon.event.entity.Logger;
import spoon.event.service.LoggerService;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.monitor.service.MonitorService;
import spoon.payment.domain.PaymentDto;
import spoon.payment.service.PaymentService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BankingDepositServiceImpl implements BankingDepositService {

    private MemberService memberService;

    private BankingService bankingService;

    private MonitorService monitorService;

    private LoggerService loggerService;

    private PaymentService paymentService;

    @Override
    public AjaxResult deposit(DepositDto.Add add) {
        String userid = WebUtils.userid();
        long max = Config.getSiteConfig().getPoint().getPointMax();

        //System.out.println("충전1");

        if (userid == null) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }

        // 현재 진행중인 충/환전 신청이 있는지 확인
        if("CASINO".equals(add.getCode())) {
            if (bankingService.existCasinoBanking(userid)) {
                return new AjaxResult(false, "현재 처리중인 충/환전 신청이 있습니다.");
            }
        }else{
            if (bankingService.existWorkBanking(userid)) {
                return new AjaxResult(false, "현재 처리중인 충/환전 신청이 있습니다.");
            }
        }

        //System.out.println("충전2");

        Member member = memberService.getMember(userid);

        //System.out.println("충전3");

        Banking banking = new Banking();
        banking.setUserid(member.getUserid());
        banking.setNickname(member.getNickname());
        banking.setAgency1(member.getAgency1());
        banking.setAgency2(member.getAgency2());
        banking.setRole(member.getRole());
        banking.setLevel(member.getLevel());
        banking.setDepositor(member.getDepositor());
        banking.setAccount(member.getAccount());
        banking.setBank(member.getBank());
        if("CASINO".equals(add.getCode())) {
            banking.setBankingCode(BankingCode.CASINO_IN);
        }else{
            banking.setBankingCode(BankingCode.IN);
        }
        banking.setMoney(member.getMoney());
        banking.setPoint(member.getPoint());
        banking.setAmount(add.getAmount());
        banking.setRegDate(new Date());
        banking.setWorker(member.getUserid());
        banking.setIp(WebUtils.ip());
        banking.setRollType(add.getRollType());

        if("CASINO".equals(add.getCode())) {

        }else if("Y".equals(banking.getRollType())){
            int level = member.getLevel();
            long bonusPoint = 0;
            if (member.getDeposit() == 0) { // 가입첫충
                bonusPoint = getBonusPoint(level, "가입첫충");
                banking.setBonus("가입첫충");
            } else if (bankingService.isFirstDeposit(userid)) { // 첫충
                bonusPoint = getBonusPoint(level, "첫충");
                banking.setBonus("첫충");
            } else if (bankingService.isEveryDeposit(userid)) { // 매충
                bonusPoint = getBonusPoint(level, "매충");
                banking.setBonus("매충");
            }
            // 보너스
            long bp = (long) (bonusPoint / 100D * banking.getAmount());
            banking.setBonusPoint(bp > max ? max : bp);
        }

        // 충전 처리를 완료한다.
        boolean success = bankingService.addDeposit(banking);

        //System.out.println("충전3 success="+success);

        if (success) {
            //send(member, add.getAmount());
            monitorService.checkDeposit();
            return new AjaxResult(true);
        }
        return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
    }

    //가상계좌 입금처리 호출
    public void send(Member mem, Long amount) {
        if (!"".equals(Config.getSiteConfig().getVirtualAccountId()) && !"".equals(Config.getSiteConfig().getVirtualAccountPrefix())) {
            //Member mem = memberService.getMember(member.getUserid());
            if (mem != null && mem.getLevel() < 3) {

                String url = CasinoUtils.API_URL+"?";
                String param = "operatorID=" + Config.getSiteConfig().getVirtualAccountId() +
                        "&operatorPW=" + Config.getSiteConfig().getVirtualAccountKey() +
                        "&IC_ID=" + mem.getVirtualId() +
                        "&IC_APIIndex=" + mem.getVirtualId() +
                        "&IC_NAME=" + mem.getDepositor() +
                        "&IC_Amount=" + amount +
                        "&IC_BANKNAME=" + mem.getBank();

                String json = HttpParsing.getJson(url + param);
                JSONParser parser = new JSONParser();
                JSONObject jsonObj = new JSONObject();
                if (!StringUtils.empty(json)) {
                    try {
                        jsonObj = (JSONObject) parser.parse(json);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                Logger logger = new Logger();
                logger.setCode("DEPOSIT");
                logger.setData(url + param + "=" + jsonObj.toString());
                logger.setRegDate(new Date());
                loggerService.addLog(logger);

            }
        }
    }

    @Override
    public AjaxResult delete(long id) {
        String userid = WebUtils.userid();

        if (userid == null) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }

        Banking banking = bankingService.getBanking(id);

        if (!banking.getUserid().equals(userid) || banking.getBankingCode() != BankingCode.IN) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }

        if (!banking.isClosing()) {
            return new AjaxResult(false, "대기중인 충전신청을 삭제할 수 없습니다.");
        }

        boolean success = bankingService.delete(id);

        if (success) {
            return new AjaxResult(true);
        }

        return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
    }

    @Override
    public AjaxResult cancel(long id) {
        Role role = WebUtils.role();

        if (role == null) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }

        Banking banking = bankingService.getBanking(id);

        // 회원일 경우 신청내역의 아이디가 일치하여야 한다.
        if (role == Role.USER && !banking.getUserid().equals(WebUtils.userid())) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }

        if (banking.getBankingCode() != BankingCode.IN && banking.getBankingCode() != BankingCode.CASINO_IN) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }

        if (banking.isCancel()) {
            return new AjaxResult(false, "이미 취소된 충전신청 입니다.");
        }

        if (banking.isClosing()) {
            return new AjaxResult(false, "이미 충전신청이 완료 되었습니다.");
        }

        boolean success = bankingService.cancelDeposit(id);
        if (success) {
            monitorService.checkDeposit();
            return new AjaxResult(true, role == Role.USER ? "" : "충전신청을 취소하였습니다.");
        }

        return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
    }

    @Override
    public AjaxResult submit(long id) {
        Banking banking = bankingService.getBanking(id);

        if (banking.getBankingCode() != BankingCode.IN && banking.getBankingCode() != BankingCode.CASINO_IN) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }

        if (banking.isCancel()) {
            return new AjaxResult(false, "이미 취소된 충전신청 입니다.");
        }

        if (banking.isClosing()) {
            return new AjaxResult(false, "이미 충전신청이 완료 되었습니다.");
        }

        boolean success = bankingService.submitDeposit(id);

        if (success) {
            monitorService.checkDeposit();
            return new AjaxResult(true, "충전신청을 완료 하였습니다.");
        }

        return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
    }


    @Override
    public AjaxResult stop(long id) {
        bankingService.stop(id);
        monitorService.checkDeposit();
        return new AjaxResult(true);
    }

    @Override
    public AjaxResult rollback(long id) {
        Banking banking = bankingService.getBanking(id);

        if (banking.isCancel()) {
            return new AjaxResult(false, "이미 취소된 충전신청 입니다.");
        }

        if (!banking.isClosing()) { // 클로징이 안 되었다는 것은 입금 처리가 되지 않았다는 것이다.
            return new AjaxResult(false, "포인트를 복원 할 수 없습니다.");
        }

        if (banking.isReset()) {
            return new AjaxResult(false, "이미 충전취소 - 포인트 복원된 충전요청 입니다.");
        }

        return bankingService.rollbackDeposit(id);
    }

    @Override
    public AjaxResult account(String pass) {
        String userid = WebUtils.userid();
        if(userid == null || pass == null) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }

        Member member = memberService.getMember(WebUtils.userid());
        if(member == null) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }

        String bank = member.getBankPassword();
        if(bank == null) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }
        //phone = phone.replaceAll("-","");

        AjaxResult result = new AjaxResult(true);
        //휴대폰 번호 가운데 4자리와 입력받은 숫자 비교
        if(StringUtils.notEmpty(pass) && pass.equals(bank)){
            if(StringUtils.empty(member.getVirtualId())){
                int level = member.getLevel(); //회원 등급
                String[] accountNumber = Config.getSiteConfig().getAccountNumber();
                result.setValue(accountNumber[level]); //등급별 계좌번호
            }else{
                result.setValue(member.getVirtualId()); //개인 가상계좌번호
            }
        }else{
            return new AjaxResult(false, "회원가입시 등록하신 환전 비밀번호를 확인 해 주세요.");
        }

        return result;

    }

    private long getBonusPoint(int level, String bonusType) {
        SiteConfig.Point point = Config.getSiteConfig().getPoint();
        int w = DateUtils.week();
        long bonus;
        if ("가입첫충".equals(bonusType)) { // 가입첫충
            if (point.isEventPayment() && point.isEventFirst() && point.getEvent()[w] && point.getEventRate()[w] > 0) {
                bonus = point.getEventRate()[w];
            } else {
                bonus = point.getJoinRate()[level];
            }
        } else if ("첫충".equals(bonusType)) { // 첫충
            if (point.isEventPayment()  && point.getEvent()[w] && point.getEventRate()[w] > 0) {
                bonus = point.getEventRate()[w];
            } else {
                bonus = point.getFirstRate()[level];
            }
        } else { // 매충
            if (point.isEventPayment() && !point.isEventFirst() && point.getEvent()[w] && point.getEventRate()[w] > 0) {
                bonus = point.getEventRate()[w];
            } else {
                bonus = Config.getSiteConfig().getPoint().getEveryRate()[level];
            }
        }
        return bonus;
    }

    @Override
    public AjaxResult deposit2(DepositDto.Add add) {
        String userid = WebUtils.userid();
        if (userid == null) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }

        Member member = memberService.getMember(userid);
        if(member.getMoney() >= add.getAmount()){
            PaymentDto.Add pa = new PaymentDto.Add();
            pa.setUserid(member.getUserid());
            pa.setMemo("파워볼 로또 충전 - 금액 차감");
            pa.setPlus(false);
            pa.setAmount(add.getAmount());
            pa.setType("POWERLOTTO");
            AjaxResult ar = paymentService.addMoney(pa);
            //System.out.println(ar.toString());
            //System.out.println("차감 완료");
            return ar;
        }else{
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }
    }


}
