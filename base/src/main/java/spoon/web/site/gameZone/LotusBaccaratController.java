package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.common.utils.JsonUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.lotusBaccarat.LotusBaccaratDto;
import spoon.gameZone.lotusBaccarat.service.LotusBaccaratGameService;
import spoon.gameZone.lotusBaccarat.service.LotusBaccaratService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class LotusBaccaratController {

    private LotusBaccaratService lotusBaccaratService;

    private LotusBaccaratGameService lotusBaccaratGameService;

    @RequestMapping(value = "zone/lotusBaccarat", method = RequestMethod.GET)
    public String zone(ModelMap map) {
        map.addAttribute("config", JsonUtils.toString(lotusBaccaratService.gameConfig()));
        return "site/zone/lotusBaccarat";
    }

    @RequestMapping(value = "zone/lotusBaccarat/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", lotusBaccaratService.getClosing(command, pageable));
        return "site/score/lotusBaccarat";
    }

    @ResponseBody
    @RequestMapping(value = "zone/lotusBaccarat/config", method = RequestMethod.POST)
    public LotusBaccaratDto.Config config() {
        return lotusBaccaratService.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/lotusBaccarat/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {

        //System.out.println("bet = "+bet.toString());
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return lotusBaccaratGameService.betting(bet);
    }
}
