package spoon.gameZone.coreaBaccarat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import spoon.bot.support.ZoneHelper;
import spoon.common.utils.DateUtils;
import spoon.game.domain.MenuCode;
import spoon.gameZone.GameZoneException;
import spoon.gameZone.Zone;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneScore;
import spoon.support.convert.DoubleArrayConvert;
import spoon.support.convert.LongArrayConvert;

import javax.persistence.*;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "ZONE_COREABACCARAT", indexes = {
        @Index(name = "IDX_sdate", columnList = "sdate")
})
public class CoreaBaccarat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date gameDate;

    private int round;

    @Column(length = 8)
    private String p1;

    @Column(length = 8)
    private String p2;

    @Column(length = 8)
    private String p3 = "";

    @Column(length = 8)
    private String b1;

    @Column(length = 8)
    private String b2;

    @Column(length = 8)
    private String b3 = "";

    @Column(length = 16)
    private String result;

    @Column(name = "pp")
    private boolean pp;

    @Column(name = "bp")
    private boolean bp;

    private String sdate;

    @Convert(converter = LongArrayConvert.class)
    private long[] amount = new long[7];

    @Convert(converter = DoubleArrayConvert.class)
    private double[] odds = new double[7];

    private boolean closing;

    private boolean cancel;

    //-----------------------------------------------------

    public CoreaBaccarat(int round, Date gameDate) {
        this.round = round;
        this.gameDate = gameDate;
        this.sdate = DateUtils.format(this.gameDate, "yyyyMMddHHmmss");
    }

    public boolean isBeforeGameDate() {
        // 60초 보정
        return this.gameDate.getTime() - System.currentTimeMillis() + 60000 > 0;
    }

    // 스코어 입력
    public void updateScore(CoreaBaccaratDto.Score score) {
        if (score.isCancel()) {
            this.p1 = "";
            this.p2 = "";
            this.p3 = "";
            this.b1 = "";
            this.b2 = "";
            this.b3 = "";
            this.result = "";
            this.pp = false;
            this.bp = false;
            this.result = "";
            this.cancel = true;
        } else {
            this.p1 = score.getP1();
            this.p2 = score.getP2();
            this.p3 = score.getP3();
            this.b1 = score.getB1();
            this.b2 = score.getB2();
            this.b3 = score.getB3();
            this.pp = score.isPp();
            this.bp = score.isBp();
            this.result = score.getResult();

            this.cancel = false;
        }
        this.closing = true;
    }

    // 경기 결과
    public ZoneScore getGameResult(String gameCode) {
        switch (gameCode) {
            case "coreaBaccarat":
                return ZoneHelper.zoneResultDraw(result);
            case "coreaBaccaratPP":
                return ZoneHelper.zoneBaccaratPair("PP", pp);
            case "coreaBaccaratBP":
                return ZoneHelper.zoneBaccaratPair("BP", bp);
            default:
                throw new GameZoneException("코리아 바카라 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    public Zone getZone(String gameCode) {
        switch (gameCode) {
            case "coreaBaccarat":
                return getCoreaBaccaratZone("coreaBaccarat", "PLAYER", "BANKER", 0, 1, 2);
            case "coreaBaccaratPP":
                return getCoreaBaccaratZone("coreaBaccaratPP", "PLAYER PAIR", "LOSE", 3, 0, 4);
            case "coreaBaccaratBP":
                return getCoreaBaccaratZone("coreaBaccaratBP", "BANKER PAIR", "LOSE", 5, 0, 6);
            default:
                throw new GameZoneException("코리아 바카라 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    private Zone getCoreaBaccaratZone(String gameCode, String teamHome, String teamAway, int idxHome, int idxDraw, int idxAway) {
        Zone zone = new Zone();
        zone.setId(this.getId());
        zone.setSdate(this.getSdate());
        zone.setMenuCode(MenuCode.CBACCARAT);
        zone.setGameCode(gameCode);
        zone.setLeague(String.format("%04d회차 바카라", this.round));
        zone.setTeamHome(String.format("%04d회차 [%s]", this.round, teamHome));
        zone.setTeamAway(String.format("%04d회차 [%s]", this.round, teamAway));
        zone.setHandicap(0);
        zone.setOddsHome(ZoneConfig.getCoreaBaccarat().getOdds()[idxHome]);
        zone.setOddsDraw(ZoneConfig.getCoreaBaccarat().getOdds()[idxDraw]);
        zone.setOddsAway(ZoneConfig.getCoreaBaccarat().getOdds()[idxAway]);
        zone.setGameDate(this.gameDate);
        return zone;
    }

    public boolean isChangeResult(CoreaBaccaratDto.Score score) {
        return !this.closing || this.cancel != score.isCancel()
                || !(p1.equals(score.getP1()) && p2.equals(score.getP2()) && p3.equals(score.getP3())
                && b1.equals(score.getB1()) && b2.equals(score.getB2()) && b3.equals(score.getB3()));
    }

    public String getWin() {
        if ("P".equals(this.result)) {
            return "PLAYER";
        } else if ("T".equals(this.result)) {
            return "TIE";
        } else if ("B".equals(this.result)) {
            return "BANKER";
        } else if (this.pp) {
            return "PLAYERPAIR";
        } else if (this.bp) {
            return "BANKERPAIR";
        } else {
            return "";
        }
    }
}
