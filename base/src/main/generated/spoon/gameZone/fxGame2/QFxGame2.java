package spoon.gameZone.fxGame2;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QFxGame2 is a Querydsl query type for FxGame2
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFxGame2 extends EntityPathBase<FxGame2> {

    private static final long serialVersionUID = -876077925L;

    public static final QFxGame2 fxGame2 = new QFxGame2("fxGame2");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final StringPath fxResult = createString("fxResult");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath oddeven = createString("oddeven");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath overunder = createString("overunder");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public QFxGame2(String variable) {
        super(FxGame2.class, forVariable(variable));
    }

    public QFxGame2(Path<? extends FxGame2> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFxGame2(PathMetadata metadata) {
        super(FxGame2.class, metadata);
    }

}

