package spoon.bot.zone.roulette;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.DateUtils;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.StringUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.roulette.Roulette;
import spoon.gameZone.roulette.service.RouletteBotService;
import spoon.mapper.GameMapper;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class RouletteParsing implements GameBotParsing {

    private RouletteBotService rouletteBotService;

    private static boolean isClosing = false;

    private static Date sdate = DateUtils.beforeMinutes(1);

    private GameMapper gameMapper;

    @Async
    @Override
    public void parsingGame() {
        isClosing = false;

        int count = 0;
        int round = ZoneConfig.getRoulette().getZoneMaker().getRound();

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(ZoneConfig.getRoulette().getZoneMaker().getGameDate().getTime() - 60 * 1000));

        for (int i = 0; i < 30; i++) {
            if (cal.getTime().before(sdate)) continue;

            if (rouletteBotService.notExist(cal.getTime())) {
                Roulette roulette = new Roulette(round > 1440 ? round % 1440 : round, cal.getTime());
                roulette.setOdds(ZoneConfig.getRoulette().getOdds());
                rouletteBotService.addGame(roulette);
                count++;
            }
            round++;
            cal.add(Calendar.MINUTE, 1);
        }
        sdate = cal.getTime();
        log.debug("코리아 룰렛 경기등록 : {}건", count);
    }

    @Async
    @Override
    public void closingGame() {

        String sdate = gameMapper.closingRouletteSdate();

        String json = null;
        if(StringUtils.notEmpty(sdate)) {
            String url = Config.getSysConfig().getZone().getRouletteUrl() + "/result?sdate=" + sdate;
//            //System.out.println(url);
            json = HttpParsing.getJson(url);
        }
        if (json == null) {
            isClosing = false;
            return;
        }else{
            try {
                JSONParser parser = new JSONParser();
                JSONArray jsonArr = (JSONArray) parser.parse(json);
                if(jsonArr != null && jsonArr.size() > 0){
                    JSONObject obj = (JSONObject) jsonArr.get(0);
                    if(obj.containsKey("result") && obj.get("result") != null && !"".equals(obj.get("result").toString())){
                        json = jsonArr.get(0).toString();
                    }else{
                        return;
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
                return;
            }
        }

//        //System.out.println("json="+json.toString());
        Roulette result = JsonUtils.toModel(json, Roulette.class);

        if (result == null) {
            isClosing = false;
            return;
        }

        rouletteBotService.closingGame(result);

        log.debug("코리아 룰렛 경기 종료 : {}회차", result.getRound());
    }

    @Async
    @Override
    public void checkResult() {
        rouletteBotService.checkResult();
    }

    @Async
    @Override
    public void deleteGame() {
        rouletteBotService.deleteGame(3);
    }
}
