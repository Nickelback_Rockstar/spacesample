package spoon.gameZone.dragonTiger.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.dragonTiger.DragonTiger;
import spoon.gameZone.dragonTiger.DragonTigerRepository;
import spoon.gameZone.dragonTiger.QDragonTiger;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class DragonTigerBotServiceImpl implements DragonTigerBotService {

    private DragonTigerGameService dragonTigerGameService;

    private DragonTigerRepository dragonTigerRepository;

    private static QDragonTiger q = QDragonTiger.dragonTiger;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return dragonTigerRepository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(DragonTiger dragonTiger) {
        dragonTigerRepository.saveAndFlush(dragonTiger);
    }

    @Transactional
    @Override
    public boolean closingGame(DragonTiger result) {
        DragonTiger dragonTiger = dragonTigerRepository.findOne(q.sdate.eq(result.getSdate()));
        if (dragonTiger == null) {
            return true;
        }

        try {
            dragonTiger.setDragon(result.getDragon());
            dragonTiger.setTiger(result.getTiger());
            dragonTiger.setSum(result.getSum());
            dragonTiger.setResult(result.getResult());
            dragonTiger.setClosing(true);

            dragonTigerRepository.saveAndFlush(dragonTiger);
            dragonTigerGameService.closingBetting(dragonTiger);
        } catch (RuntimeException e) {
            log.error("드래곤타이거 {}회차 결과 업데이트에 실패하였습니다. - {}", dragonTiger.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = dragonTigerRepository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getDragonTiger().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        dragonTigerRepository.deleteGame(DateUtils.beforeDays(days));
    }
}
