package spoon.gameZone.lotusBaccarat2.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.lotusBaccarat2.LotusBaccarat2;
import spoon.support.web.AjaxResult;

public interface LotusBaccarat2GameService {

    /**
     * 바카라 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 바카라 게임 베팅을 클로징 한다.
     */
    void closingBetting(LotusBaccarat2 baccarat2);

    /**
     * 바카라 게임을 롤백 한다.
     */
    void rollbackPayment(LotusBaccarat2 baccarat2);
}
