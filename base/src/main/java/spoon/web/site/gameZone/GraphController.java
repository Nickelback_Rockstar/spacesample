package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spoon.common.utils.WebUtils;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class GraphController {

    private MemberService memberService;

    @RequestMapping(value = "zone/graph", method = RequestMethod.GET)
    public String score(ModelMap map, HttpServletRequest request) {

        Member member = memberService.getMember(WebUtils.userid());

        map.addAttribute("userid", WebUtils.userid());
        map.addAttribute("domain", request.getServerName());

        if("abc1".equals(member.getAgency7()) || "ABC1".equals(member.getAgency7()) ||
        "abc2".equals(member.getAgency7()) || "ABC2".equals(member.getAgency7())){
            return "site/zone/noGraph";
        }else{
            return "site/zone/graph";
        }
    }

}
