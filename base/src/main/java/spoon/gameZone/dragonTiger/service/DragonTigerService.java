package spoon.gameZone.dragonTiger.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.dragonTiger.DragonTiger;
import spoon.gameZone.dragonTiger.DragonTigerConfig;
import spoon.gameZone.dragonTiger.DragonTigerDto;
import spoon.support.web.AjaxResult;

public interface DragonTigerService {

    /**
     * 드래곤타이거 설정을 변경한다.
     */
    boolean updateConfig(DragonTigerConfig config);

    /**
     * 드래곤타이거 등록된 게임을 가져온다.
     */
    Iterable<DragonTiger> getComplete();

    /**
     * 드래곤타이거 종료된 게임을 가져온다.
     */
    Page<DragonTiger> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * 드래곤타이거 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    DragonTigerDto.Score findScore(Long id);

    /**
     * 드래곤타이거 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(DragonTigerDto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    DragonTigerDto.Config gameConfig();

}
