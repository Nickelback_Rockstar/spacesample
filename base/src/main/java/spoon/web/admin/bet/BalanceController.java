package spoon.web.admin.bet;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.bet.domain.BetDto;
import spoon.bet.service.BetService;
import spoon.bot.balance.entity.GodBalance;
import spoon.bot.balance.service.BalanceService;
import spoon.config.domain.SysConfig;
import spoon.support.web.AjaxResult;

import java.util.List;

@AllArgsConstructor
@Controller("admin.balanceController")
@RequestMapping("#{config.pathAdmin}")
public class BalanceController {

    private BalanceService balanceService;

    private BetService betService;

    @RequestMapping(value = "betting/balance", method = RequestMethod.GET)
    public String polygon(ModelMap map,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = {"id"}) Pageable pageable) {
        map.addAttribute("page", balanceService.pagePolygon(pageable));

        return "admin/betting/polygon";
    }

    @RequestMapping(value = "betting/balance1", method = RequestMethod.GET)
    public String polygon1(ModelMap map,
                           @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = {"id"}) Pageable pageable) {
        map.addAttribute("page", balanceService.pagePolygon1(pageable));

        return "admin/betting/polygon1";
    }

    @RequestMapping(value = "betting/balance2", method = RequestMethod.GET)
    public String polygon2(ModelMap map,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = {"id"}) Pageable pageable) {
        map.addAttribute("page", balanceService.pagePolygon2(pageable));

        return "admin/betting/polygon2";
    }

    @RequestMapping(value = "betting/godBalance", method = RequestMethod.GET)
    public String godBalance(ModelMap map,
                             @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = {"id"}) Pageable pageable) {

        Page<GodBalance> page = balanceService.pageGodBalance(pageable);
        List<GodBalance> list = page.getContent();
        String lastRound = "0";
        if(list != null && list.size() > 0){
            lastRound = list.get(0).getRound();
        }
        map.addAttribute("page", page);
        map.addAttribute("lastRound", lastRound);

        return "admin/betting/godBalance";
    }

    @RequestMapping(value = "betting/powerResultList", method = RequestMethod.GET)
    public String godPowerResultList(ModelMap map) {

        BetDto.BetBalGame bal = betService.balanceResetDate();
        List<BetDto.BetBalGame> list = betService.balancePowerResult(bal);
        map.addAttribute("list", list);
        map.addAttribute("bal", bal);
        return "admin/betting/powerResult";
    }

    @RequestMapping(value = "betting/powerReResultList", method = RequestMethod.GET)
    public String godPowerReResultList(ModelMap map) {

        BetDto.BetBalGame bal = betService.balanceResetDate();
        List<BetDto.BetBalGame> list = betService.balancePowerReResult(bal);
        map.addAttribute("list", list);
        map.addAttribute("bal", bal);
        return "admin/betting/powerReResult";
    }

    @RequestMapping(value = "betting/powerLadderResultList", method = RequestMethod.GET)
    public String powerLadderResultList(ModelMap map) {

        BetDto.BetBalGame bal = betService.balanceResetDate();
        List<BetDto.BetBalGame> list = betService.balancePowerLadderResult(bal);
        map.addAttribute("list", list);
        map.addAttribute("bal", bal);
        return "admin/betting/powerLadderResult";
    }

    @RequestMapping(value = "betting/powerLadderReResultList", method = RequestMethod.GET)
    public String powerLadderReResultList(ModelMap map) {

        BetDto.BetBalGame bal = betService.balanceResetDate();
        List<BetDto.BetBalGame> list = betService.balancePowerLadderReResult(bal);
        map.addAttribute("list", list);
        map.addAttribute("bal", bal);
        return "admin/betting/powerLadderReResult";
    }

    @ResponseBody
    @RequestMapping(value = "betting/powerBalanceChk", method = RequestMethod.POST)
    public AjaxResult powerBalanceChk(String userid, String menuCode) {
        boolean b = betService.powerBalanceChk(userid, menuCode);
        return new AjaxResult(true, "처리 했습니다.");
    }

    @ResponseBody
    @RequestMapping(value = "betting/powerBalanceChange", method = RequestMethod.POST)
    public AjaxResult powerBalanceChange(String userid, String menuCode) {
        boolean b = betService.powerBalanceChange(userid, menuCode);
        return new AjaxResult(true, "처리 했습니다.");
    }

    @ResponseBody
    @RequestMapping(value = "betting/godPowerOnOff", method = RequestMethod.POST)
    public AjaxResult godPowerOnOff(SysConfig sysConfig, String gb, String menuCode) {
        return betService.godPowerOnOff(sysConfig, gb, menuCode);
    }

    @ResponseBody
    @RequestMapping(value = "betting/godPowerRate", method = RequestMethod.POST)
    public AjaxResult godPowerRate(SysConfig sysConfig, String gb, String rate1, String rate2) {
        return betService.godPowerRate(sysConfig, gb, rate1, rate2);
    }

}
