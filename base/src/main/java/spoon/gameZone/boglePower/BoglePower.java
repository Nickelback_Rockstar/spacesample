package spoon.gameZone.boglePower;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import spoon.bot.support.ZoneHelper;
import spoon.common.utils.DateUtils;
import spoon.game.domain.MenuCode;
import spoon.gameZone.GameZoneException;
import spoon.gameZone.Zone;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneScore;
import spoon.support.convert.DoubleArrayConvert;
import spoon.support.convert.LongArrayConvert;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "ZONE_BOGLEPOWER", indexes = {
        @Index(name = "IDX_sdate", columnList = "sdate")
})
public class BoglePower {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date gameDate;

    private int round;

    // 일반볼 콤마로 연결
    @Column(length = 64)
    private String ball;

    @Column(length = 16)
    private String pb;

    @Column(length = 16)
    private String oddeven;

    @Column(length = 16)
    private String pb_oddeven;

    @Column(length = 16)
    private String overunder;

    @Column(length = 16)
    private String pb_overunder;

    @Column(length = 16)
    private String size;

    private int sum;

    @Column(length = 16)
    private String sdate;

    @Convert(converter = LongArrayConvert.class)
    private long[] amount = new long[11];

    @Convert(converter = DoubleArrayConvert.class)
    private double[] odds = new double[11];

    private boolean closing;

    private boolean cancel;

    //--------------------------------------------------------

    public BoglePower(int round, Date gameDate) {
        this.round = round;
        this.gameDate = gameDate;
        this.sdate = DateUtils.format(gameDate, "yyyyMMddHHmm");
    }

    public boolean isBeforeGameDate() {
        return this.gameDate.getTime() - System.currentTimeMillis() > 0;
    }

    // 스코어 입력
    public void updateScore(BoglePowerDto.Score score) {
        if (score.isCancel()) {
            this.pb = "";
            this.ball = "";
            this.oddeven = "";
            this.pb_oddeven = "";
            this.overunder = "";
            this.pb_overunder = "";
            this.size = "";
            this.cancel = true;
        } else {
            int[] balls = Arrays.stream(score.getBall().trim().split(",")).mapToInt(x -> Integer.parseInt(x, 10)).toArray();
            int pball = Integer.parseInt(score.getPb(), 10);
            int sum = IntStream.of(balls).sum();

            //일반홀짝
            this.oddeven = sum % 2 == 1 ? "ODD" : "EVEN";
            //일반오버언더
            this.overunder = sum < 73 ? "UNDER" : "OVER";
            //일반 대중소
            this.size = sum > 80 ? "대" : (sum < 65 ? "소" : "중");

            //파워볼
            this.pb = score.getPb();
            //파워홀짝
            this.pb_oddeven = pball % 2 == 1 ? "ODD" : "EVEN";
            //파워오버언더
            this.pb_overunder = pball < 5 ? "UNDER" : "OVER";

            this.ball = Arrays.stream(balls).mapToObj(String::valueOf).collect(Collectors.joining(","));
            this.sum = sum;

        }
        this.closing = true;
    }

    // 경기 결과
    public ZoneScore getGameResult(String gameCode) {
        switch (gameCode) {
            case "oddeven":
                return ZoneHelper.zoneResult(oddeven, cancel);
            case "pb_oddeven":
                return ZoneHelper.zoneResult(pb_oddeven, cancel);
            case "overunder":
                return ZoneHelper.zoneResult(overunder, cancel);
            case "pb_overunder":
                return ZoneHelper.zoneResult(pb_overunder, cancel);
            case "size":
                return ZoneHelper.zoneResult(size, cancel);
            default:
                throw new GameZoneException("보글파워볼 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    public Zone getZone(String gameCode) {
        switch (gameCode) {
            case "oddeven":
                return getBoglePowerZone("oddeven", "일반볼", "홀", "짝", 0, 1);
            case "pb_oddeven":
                return getBoglePowerZone("pb_oddeven", "파워볼", "홀", "짝", 2, 3);
            case "overunder":
                return getBoglePowerZone("overunder", "일반볼", "오버", "언더", 4, 5);
            case "pb_overunder":
                return getBoglePowerZone("pb_overunder", "파워볼", "오버", "언더", 6, 7);
            case "size":
                Zone zone = getBoglePowerZone("size", "일반볼", "대", "소", 8, 10);
                zone.setOddsDraw(ZoneConfig.getBoglePower().getOdds()[9]);
                return zone;
            default:
                throw new GameZoneException("보글파워볼 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    private Zone getBoglePowerZone(String gameCode, String type, String teamHome, String teamAway, int idxHome, int idxAway) {
        Zone zone = new Zone();
        zone.setId(this.getId());
        zone.setSdate(this.getSdate());
        zone.setMenuCode(MenuCode.BOGLEPOWER);
        zone.setGameCode(gameCode);
        zone.setLeague(String.format("%03d회차 보글파워볼", this.round));
        zone.setTeamHome(String.format("%03d회차 %s [%s]", this.round, type, teamHome));
        zone.setTeamAway(String.format("%03d회차 %s [%s]", this.round, type, teamAway));
        zone.setHandicap(0);
        zone.setOddsHome(ZoneConfig.getBoglePower().getOdds()[idxHome]);
        zone.setOddsDraw(0);
        zone.setOddsAway(ZoneConfig.getBoglePower().getOdds()[idxAway]);
        zone.setGameDate(this.gameDate);
        return zone;
    }

    public boolean isChangeResult(BoglePowerDto.Score score) {
        return !this.closing || this.cancel != score.isCancel()
                || !(score.getPb().equals(this.pb) && score.getBall().equals(this.ball));

    }
}
