package spoon.mapper;

import org.apache.ibatis.annotations.Param;
import spoon.customer.domain.MemoDto;
import spoon.customer.entity.Memo;

public interface MemoMapper {

    void addMemo(@Param("add") MemoDto.Add add);

    void deleteMemo(@Param("ids") Long[] memoIds);

    void readMemoAll(@Param("userid") String userid);

    void deleteMemoAll(@Param("userid") String userid);

    void addMemoOne(@Param("memo") Memo memo);
}
