package spoon.bot.zone.starLadder3;

import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class StarLadder3Task {

    private StarLadder3Parsing starLadder3Parsing;

    @Scheduled(cron = "0 * * * * ?")
    public void gameTask() {
        starLadder3Parsing.parsingGame();
    }

    @Scheduled(cron = "3,8 * * * * ?")
    public void closingTask() {
        starLadder3Parsing.closingGame();
    }

    @Scheduled(cron = "55 0 0 * * ?")
    public void cleanTask() {
        starLadder3Parsing.deleteGame();
    }


}
