package spoon.gameZone.lotusOddeven.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.lotusOddeven.LotusOddeven;
import spoon.gameZone.lotusOddeven.LotusOddevenConfig;
import spoon.gameZone.lotusOddeven.LotusOddevenDto;
import spoon.support.web.AjaxResult;

public interface LotusOddevenService {

    /**
     * 홀짝 설정을 변경한다.
     */
    boolean updateConfig(LotusOddevenConfig lotusOddevenConfig);

    /**
     * 홀짝 등록된 게임을 가져온다.
     */
    Iterable<LotusOddeven> getComplete();

    /**
     * 홀짝 종료된 게임을 가져온다.
     */
    Page<LotusOddeven> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * 홀짝 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    LotusOddevenDto.Score findScore(Long id);

    /**
     * 홀짝 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(LotusOddevenDto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    LotusOddevenDto.Config gameConfig();

    /**
     * 미처리된 경기 삭제처리
     */
    AjaxResult deleteAllGame();
}
