package spoon.bot.sports.inGame.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.sports.inGame.domain.BotInGame;
import spoon.bot.sports.service.ParsingGame;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.game.domain.GameCode;
import spoon.game.domain.MenuCode;
import spoon.game.entity.Game;
import spoon.game.entity.League;
import spoon.game.entity.Sports;
import spoon.game.entity.Team;
import spoon.game.service.GameBotService;
import spoon.game.service.sports.LeagueService;
import spoon.game.service.sports.SportsService;
import spoon.game.service.sports.TeamService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class InGameParsingGame implements ParsingGame {

    private GameBotService gameBotService;

    private SportsService sportsService;

    private LeagueService leagueService;

    private TeamService teamService;

    private static String ut;

    private static boolean action = false;

    @Override
    @Async
    public void parsingGame() {
        //if (action) return;

        action = true;
        int saved = 0;
        int updated = 0;

        String json = HttpParsing.getJson(getParsingUrl());
        if (json == null) {
            action = false;
            return;
        }

        JSONParser parser = new JSONParser();
        JSONObject jsonObj = null;
        try {
            jsonObj = (JSONObject) parser.parse(json);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        JSONArray jsonArr = (JSONArray) jsonObj.get("soccer");
        JSONArray jsonArr2 = (JSONArray) jsonObj.get("baseball");

        int cnt = 0;
        cnt += setInGame(jsonArr.toString());
        cnt += setInGame(jsonArr2.toString());

        log.debug("인게임 게임 업데이트 - 전체 : {}, 신규: {}", cnt, saved);
        action = false;
    }

    public int setInGame(String jsonArr){
        int result = 0;
        List<BotInGame> list = JsonUtils.toInGameList(jsonArr.toString());

        if (list == null) {
            action = false;
            return result;
        }

        for (BotInGame bot : list) {
            int cnt = 0; //같은 게임 순번
            Date gameDate = new Date();
            addGame(bot, GameCode.MATCH, cnt++, gameDate);
            addGame(bot, GameCode.HANDICAP, cnt++, gameDate);
            addGame(bot, GameCode.OVER_UNDER, cnt++, gameDate);
            result++;
        }

        return result;

    }

    @Override
    public void resetUt(String update) {
        ut = update;
    }

    private boolean addGame(BotInGame bot, GameCode gameCode, int cnt, Date gameDate) {
        Game game = makeGame(bot, gameCode, cnt, gameDate);
        updateSports(game.getSportsBean());
        if(gameCode == GameCode.MATCH) {
            updateLeague(game.getLeagueBean(), "/images/league/league-default.png");
            updateTeam(game.getTeamHomeBean());
            updateTeam(game.getTeamAwayBean());
        }

        return gameBotService.addGame(game);
    }

    private Game makeGame(BotInGame bot, GameCode gameCode, int cnt, Date gameDate) {
        Game game = new Game();
        game.setSiteCode("inGame");
        game.setSiteId(bot.getGameCode()+gameCode.getValue());
        game.setGroupId(bot.getGameCode());
        game.setGameDate(gameDate);
        game.setSports(bot.getSports());
        game.setMenuCode(MenuCode.INGAME);
        game.setGameCode(gameCode);
        game.setSpecial("");
        game.setLeague(bot.getLeague());
        game.setTeamHome(bot.getTeamName1());
        game.setTeamAway(bot.getTeamName2());

        game.setHandicap(0);

        game.setBetHome(true);
        if(gameCode.getValue() == 100) {
            game.setBetDraw(true);
        }else{
            game.setBetDraw(false);
        }
        game.setBetAway(true);

        game.setDeleted(false);
        game.setClosing(false);
        game.setCancel(false);
        game.setSort(cnt);
        game.setUt(String.valueOf(gameDate.getTime()));
        game.setAutoUpdate(false);
        game.setEnabled(true);
        makeOdds(bot, game, gameCode);

        return game;
    }

    private void makeOdds(BotInGame bot, Game game, GameCode gameCode) {
        //어차피 실시간으로 배당 가져올거라 게임 저장시 배당은 의미 없을듯함.
        game.updateOdds(1, 1, 1);
    }

    private double getOddsUpDown(GameCode gameCode, MenuCode menuCode) {
        double odds = 100D;
        switch (gameCode) {
            case MATCH:
                odds = Config.getGameConfig().getOddsUpDownMatch();
                break;
            case HANDICAP:
                odds = menuCode == MenuCode.SPECIAL ? Config.getGameConfig().getOddsUpDownHandicapSpecial() : Config.getGameConfig().getOddsUpDownHandicap();
                break;
            case OVER_UNDER:
                odds = menuCode == MenuCode.SPECIAL ? Config.getGameConfig().getOddsUpDownOverUnderSpecial() : Config.getGameConfig().getOddsUpDownOverUnder();
                break;
        }
        return odds;
    }

    private double getOddsPlus(GameCode gameCode, MenuCode menuCode) {
        double odds = 0D;
        switch (gameCode) {
            case MATCH:
                odds = Config.getGameConfig().getOddsPlusMatch();
                break;
            case HANDICAP:
                odds = menuCode == MenuCode.SPECIAL ? Config.getGameConfig().getOddsPlusHandicapSpecial() : Config.getGameConfig().getOddsPlusHandicap();
                break;
            case OVER_UNDER:
                odds = menuCode == MenuCode.SPECIAL ? Config.getGameConfig().getOddsPlusOverUnderSpecial() : Config.getGameConfig().getOddsPlusOverUnder();
                break;
        }
        return odds;
    }

    private double convertOdds(double odds, double oddsRate, double oddsPlusRate) {
        if (oddsRate != 100D) {
            odds = BigDecimal.valueOf(odds)
                    .multiply(BigDecimal.valueOf(oddsRate))
                    .divide(BigDecimal.valueOf(100D), 2, BigDecimal.ROUND_HALF_UP)
                    .doubleValue();
        }
        if (oddsPlusRate != 0D) {
            odds = BigDecimal.valueOf(odds).add(BigDecimal.valueOf(oddsPlusRate)).doubleValue();
        }
        return odds;
    }

    private void updateTeam(Team team) {
        if (team.getId() == null || team.getId() == 0) {
            teamService.addTeam(team);
        }
    }

    private void updateLeague(League league, String flag) {
        if (league.getId() == null || league.getId() == 0) {
            leagueService.addLeague(league, flag);
        } else if ("league.png".equals(league.getLeagueFlag()) && !"/images/league/league-default.png".equals(flag)) {
            leagueService.updateFlag(league, flag);
        }
    }

    private void updateSports(Sports sports) {
        if (sports.getId() == null || sports.getId() == 0) {
            sportsService.addSports(sports.getSportsName());
        }
    }

    private String getParsingUrl() {
        if (ut == null) ut = gameBotService.getMaxUt("inGame", false);
        switch (Config.getSysConfig().getSports().getInGame()) {
            case "inGame":
                return Config.getSysConfig().getSports().getInGameApi() + "/inGame.json?ut=" + new Date();
            default:
                throw new IllegalArgumentException("SysConfig > Sports > inGameApi 의 정보가 잘못되었습니다. (" + Config.getSysConfig().getSports().getInGameApi() + ")");
        }
    }

    private boolean autoParsing() {
        return Config.getGameConfig().isAutoParsing();
    }

    private boolean autoUpdate() {
        return Config.getGameConfig().isAutoUpdate();
    }
}
