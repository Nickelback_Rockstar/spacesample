package spoon.game.domain;

public enum StatusCode {

    SUCCESS(10, "정상"),
    STAY(20, "대기"),
    CANCEL_IG1(100, "스코어변경 취소"),
    CANCEL_IG2(110, "배당변경 취소"),
    CANCEL_IG3(120, "상태변경 취소"),
    CANCEL(99, "취소");

    private int value;

    private String name;

    StatusCode(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }


}
