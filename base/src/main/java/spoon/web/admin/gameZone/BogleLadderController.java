package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.bogleLadder.BogleLadderConfig;
import spoon.gameZone.bogleLadder.BogleLadderDto;
import spoon.gameZone.bogleLadder.service.BogleLadderService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.bogleLadderController")
@RequestMapping(value = "#{config.pathAdmin}")
public class BogleLadderController {

    private BogleLadderService bogleLadderService;

    /**
     * BogleLadder 설정
     */
    @RequestMapping(value = "zone/bogleLadder/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getBogleLadder());
        return "admin/zone/bogleLadder/config";
    }

    /**
     * BogleLadder 설정 변경
     */
    @RequestMapping(value = "zone/bogleLadder/config", method = RequestMethod.POST)
    public String config(BogleLadderConfig bogleLadderConfig, RedirectAttributes ra) {
        boolean success = bogleLadderService.updateConfig(bogleLadderConfig);
        if (success) {
            ra.addFlashAttribute("message", "BogleLadder게임 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "BogleLadder게임 설정변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/bogleLadder/config";
    }

    /**
     * BogleLadder 진행
     */
    @RequestMapping(value = "zone/bogleLadder/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", bogleLadderService.getComplete());
        map.addAttribute("config", ZoneConfig.getBogleLadder());
        return "admin/zone/bogleLadder/complete";
    }

    /**
     * BogleLadder 완료
     */
    @RequestMapping(value = "zone/bogleLadder/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", bogleLadderService.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getBogleLadder());
        return "admin/zone/bogleLadder/closing";
    }

    /**
     * BogleLadder 스코어 입력 폼
     */
    @RequestMapping(value = "zone/bogleLadder/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", bogleLadderService.findScore(id));
        return "admin/zone/bogleLadder/score";
    }

    /**
     * BogleLadder 스코어 결과처리
     */
    @RequestMapping(value = "zone/bogleLadder/score", method = RequestMethod.POST)
    public String score(BogleLadderDto.Score score, RedirectAttributes ra) {
        boolean success = bogleLadderService.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "BogleLadder 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "BogleLadder 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/bogleLadder/score?id=" + score.getId();
    }

    /**
     * BogleLadder 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/bogleLadder/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return bogleLadderService.closingAllGame();
    }

}
