'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    stylus = require('gulp-stylus'),
    nodemon = require('gulp-nodemon'),
    uglify = require('gulp-uglify'),
    uglifycss = require('gulp-uglifycss'),
    ngmin = require('gulp-ngmin');


var buildPath = '../nano/src/main/resources/static/';

// js
gulp.task('js', function () {
    gulp.src('js/*.js')
        .pipe(gulp.dest('public/js'))
        .pipe(uglify())
        .pipe(gulp.dest(buildPath + 'js'));
});
gulp.task('watch:js', function () {
    gulp.watch('js/**/*.js', ['js']);
});

gulp.task('ng', function () {
    gulp.src('ng/*.js')
        .pipe(gulp.dest('public/js'))
        .pipe(ngmin({dynamic: true}))
        .pipe(uglify())
        .pipe(gulp.dest(buildPath + 'js'));
});
gulp.task('watch:ng', function () {
    gulp.watch('ng/**/*.js', ['ng']);
});

// css
gulp.task('css', function () {
    gulp.src('css/style.styl')
        .pipe(stylus())
        .pipe(gulp.dest('public/css'))
        .pipe(uglifycss())
        .pipe(gulp.dest(buildPath + 'css'));
});
gulp.task('admin.css', function () {
    gulp.src('css/style.admin.styl')
        .pipe(stylus())
        .pipe(gulp.dest('public/css'))
        .pipe(uglifycss())
        .pipe(gulp.dest(buildPath + 'css'));
});
gulp.task('mobile.css', function () {
    gulp.src('css/style.mobile.styl')
        .pipe(stylus())
        .pipe(gulp.dest('public/css'))
        .pipe(uglifycss())
        .pipe(gulp.dest(buildPath + 'css'));
});
gulp.task('watch:css', function () {
    gulp.watch('css/**/*.styl', ['css', 'admin.css', 'mobile.css']);
});

// task
gulp.task('server', function () {
    nodemon({
        script: 'app.js',
        ignore: ['*']
    });
});

gulp.task('dev', ['server', 'watch:css', 'watch:js', 'watch:ng']);