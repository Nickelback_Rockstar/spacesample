package spoon.member.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.member.domain.MemberDto;
import spoon.member.domain.User;
import spoon.member.entity.Member;
import spoon.sale.entity.SaleItem;
import spoon.seller.domain.Seller;

import java.util.List;

public interface MemberListService {

    Page<Member> list(MemberDto.Command command, Pageable pageable);

    Page<Member> sellerList(MemberDto.Seller command, Pageable pageable);

    List<User> getDummyList();

    Iterable<Member> getExcel();

    List<User> userRecommList(String userid);

    List<SaleItem> sellerListNew(MemberDto.Seller command);
    List<Seller> sellerListNew2(MemberDto.Seller command);
}
