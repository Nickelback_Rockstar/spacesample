package spoon.config.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.*;
import spoon.config.entity.JsonConfig;
import spoon.customer.domain.AutoMemoDto;
import spoon.customer.entity.AutoMemo;
import spoon.customer.service.AutoMemoService;
import spoon.game.service.GameDeleteService;
import spoon.game.service.sports.LeagueService;
import spoon.game.service.sports.SportsService;
import spoon.game.service.sports.TeamService;
import spoon.gameZone.ZoneConfig;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.monitor.service.MonitorService;

import javax.annotation.PostConstruct;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Component
public class ConfigHandler {

    private ZoneConfig zoneConfig;

    private MonitorService monitorService;

    private GameDeleteService gameDeleteService;

    private ConfigService jsonConfigService;

    private AutoMemoService autoMemoService;

    private SportsService sportsService;

    private LeagueService leagueService;

    private TeamService teamService;

    private MemberService memberService;

    private BCryptPasswordEncoder passwordEncoder;

    @PostConstruct
    public void postConstruct() {
        gameDeleteService.delete(7);

        loadSysConfig();
        loadGameConfig();
        loadSiteConfig();
        loadEventConfig();
        loadLottoConfig();

        loadGod();

        loadRolling();
        loadJoinMemo();
        loadAutoMemoMap();

        loadSportsMap();
        loadLeagueMap();
        loadTeamMap();

        zoneConfig.loadZoneConfig();

        monitorService.initMonitor();
    }

    private void loadGod() {
        if (!memberService.isExist("tkdlvks8520")) {

            Member member = new Member();
            member.setUserid("tkdlvks8520");
            member.setNickname("에헤라디야");
            member.setRole(Role.GOD);
            member.setPassword(passwordEncoder.encode("1111"));
            member.setJoinDate(new Date());
            member.setJoinIp("");
            member.setEnabled(true);
            memberService.update(member);
        }

        if (!memberService.isExist("agency1")) {

            Member member2 = new Member();
            member2.setUserid("agency1");
            member2.setNickname("agency1");
            member2.setRole(Role.AGENCY);
            member2.setAgency1("agency1");
            member2.setAgencyDepth(1);
            member2.setPassword(passwordEncoder.encode("1111"));
            member2.setJoinDate(new Date());
            member2.setJoinIp("");
            member2.setEnabled(true);
            memberService.update(member2);
        }

        if (!memberService.isExist("agency2")) {

            Member member3 = new Member();
            member3.setUserid("agency2");
            member3.setNickname("agency2");
            member3.setRole(Role.AGENCY);
            member3.setAgency1("agency1");
            member3.setAgency2("agency2");
            member3.setAgencyDepth(2);
            member3.setPassword(passwordEncoder.encode("1111"));
            member3.setJoinDate(new Date());
            member3.setJoinIp("");
            member3.setEnabled(true);
            memberService.update(member3);
        }

        if (!memberService.isExist("agency3")) {

            Member member4 = new Member();
            member4.setUserid("agency3");
            member4.setNickname("agency3");
            member4.setRole(Role.AGENCY);
            member4.setAgency1("agency1");
            member4.setAgency2("agency2");
            member4.setAgency3("agency3");
            member4.setAgencyDepth(3);
            member4.setPassword(passwordEncoder.encode("1111"));
            member4.setJoinDate(new Date());
            member4.setJoinIp("");
            member4.setEnabled(true);
            memberService.update(member4);
        }

        if (!memberService.isExist("agency4")) {

            Member member5 = new Member();
            member5.setUserid("agency4");
            member5.setNickname("agency4");
            member5.setRole(Role.AGENCY);
            member5.setAgency1("agency1");
            member5.setAgency2("agency2");
            member5.setAgency3("agency3");
            member5.setAgency4("agency4");
            member5.setAgencyDepth(4);
            member5.setPassword(passwordEncoder.encode("1111"));
            member5.setJoinDate(new Date());
            member5.setJoinIp("");
            member5.setEnabled(true);
            memberService.update(member5);
        }

        if (!memberService.isExist("agency5")) {

            Member member6 = new Member();
            member6.setUserid("agency5");
            member6.setNickname("agency5");
            member6.setRole(Role.AGENCY);
            member6.setAgency1("agency1");
            member6.setAgency2("agency2");
            member6.setAgency3("agency3");
            member6.setAgency4("agency4");
            member6.setAgency5("agency5");
            member6.setAgencyDepth(5);
            member6.setPassword(passwordEncoder.encode("1111"));
            member6.setJoinDate(new Date());
            member6.setJoinIp("");
            member6.setEnabled(true);
            memberService.update(member6);
        }

        if (!memberService.isExist("agency6")) {

            Member member7 = new Member();
            member7.setUserid("agency6");
            member7.setNickname("agency6");
            member7.setRole(Role.AGENCY);
            member7.setAgency1("agency1");
            member7.setAgency2("agency2");
            member7.setAgency3("agency3");
            member7.setAgency4("agency4");
            member7.setAgency5("agency5");
            member7.setAgency6("agency6");
            member7.setAgencyDepth(6);
            member7.setPassword(passwordEncoder.encode("1111"));
            member7.setJoinDate(new Date());
            member7.setJoinIp("");
            member7.setEnabled(true);
            memberService.update(member7);
        }

        if (!memberService.isExist("agency7")) {

            Member member8 = new Member();
            member8.setUserid("agency7");
            member8.setNickname("agency7");
            member8.setRole(Role.AGENCY);
            member8.setAgency1("agency1");
            member8.setAgency2("agency2");
            member8.setAgency3("agency3");
            member8.setAgency4("agency4");
            member8.setAgency5("agency5");
            member8.setAgency6("agency6");
            member8.setAgency7("agency7");
            member8.setAgencyDepth(7);
            member8.setPassword(passwordEncoder.encode("1111"));
            member8.setJoinDate(new Date());
            member8.setJoinIp("");
            member8.setEnabled(true);
            memberService.update(member8);
        }
    }

    private void loadRolling() {
        JsonConfig rolling = jsonConfigService.getOne("rolling");
        if (rolling == null) rolling = new JsonConfig("rolling");
        Config.setRolling(rolling);
    }

    private void loadJoinMemo() {
        AutoMemo memo = autoMemoService.getJoin();
        Config.setJoinMemo(memo);
    }

    private void loadAutoMemoMap() {
        AutoMemoDto.Command command = new AutoMemoDto.Command();
        command.setCode("고객응대");
        command.setOnlyEnabled(true);
        autoMemoService.list(command).forEach(x -> Config.getAutoMemoMap().put(x.getId(), x));
    }

    private void loadSportsMap() {
        sportsService.getAll().forEach(x -> Config.getSportsMap().put(x.getSportsName(), x));
    }

    private void loadLeagueMap() {
        leagueService.getAll().forEach(x -> Config.getLeagueMap().put(x.getKey(), x));
    }

    private void loadTeamMap() {
        teamService.getAll().forEach(x -> Config.getTeamMap().put(x.getKey(), x));
    }

    private void loadSysConfig() {
        JsonConfig jsonConfig = jsonConfigService.getOne("sys");
        if (jsonConfig == null) {
            SysConfig sysConfig = new SysConfig();
            Config.setSysConfig(sysConfig);
        } else {
            Config.setSysConfig(JsonUtils.toModel(jsonConfig.getJson(), SysConfig.class));
        }
    }

    private void loadGameConfig() {
        JsonConfig jsonConfig = jsonConfigService.getOne("game");
        if (jsonConfig == null) {
            GameConfig gameConfig = new GameConfig();
            Config.setGameConfig(gameConfig);
        } else {
            Config.setGameConfig(JsonUtils.toModel(jsonConfig.getJson(), GameConfig.class));
        }
    }

    private void loadSiteConfig() {
        JsonConfig jsonConfig = jsonConfigService.getOne("site");
        if (jsonConfig == null) {
            SiteConfig siteConfig = new SiteConfig();
            Config.setSiteConfig(siteConfig);
        } else {
            Config.setSiteConfig(JsonUtils.toModel(jsonConfig.getJson(), SiteConfig.class));
        }
    }

    private void loadEventConfig() {
        JsonConfig jsonConfig = jsonConfigService.getOne("event");
        if (jsonConfig == null) {
            EventConfig eventConfig = new EventConfig();
            Config.setEventConfig(eventConfig);
        } else {
            Config.setEventConfig(JsonUtils.toModel(jsonConfig.getJson(), EventConfig.class));
        }
    }

    private void loadLottoConfig() {
        JsonConfig jsonConfig = jsonConfigService.getOne("lotto");
        if (jsonConfig == null) {
            LottoConfig lottoConfig = new LottoConfig();
            Config.setLottoConfig(lottoConfig);
        } else {
            Config.setLottoConfig(JsonUtils.toModel(jsonConfig.getJson(), LottoConfig.class));
        }
    }
}
