package spoon.bot.zone.lotusBaccarat2;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class LotusBaccarat2Task {

    private GameBotParsing lotusBaccarat2Parsing;

    @Scheduled(cron = "0/31 * * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        lotusBaccarat2Parsing.parsingGame();
    }

    @Scheduled(cron = "6/20 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        lotusBaccarat2Parsing.closingGame();
    }

    @Scheduled(cron = "1 * * * * *")
    public void checkResult() {
        if (notParsing()) return;
        lotusBaccarat2Parsing.checkResult();
    }

    @Scheduled(cron = "15 1 4 * * * ")
    public void deleteGame() {
        lotusBaccarat2Parsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isLotusBaccarat2() || !ZoneConfig.getLotusBaccarat2().isEnabled();
    }
}
