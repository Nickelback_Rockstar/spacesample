package spoon.gameZone.eosPower5.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.eosPower5.EosPower5;
import spoon.gameZone.eosPower5.EosPower5Repository;
import spoon.gameZone.eosPower5.QEosPower5;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class EosPower5BotServiceImpl implements EosPower5BotService {

    private EosPower5GameService eosPower5GameService;

    private EosPower5Repository eosPower5Repository;

    private static QEosPower5 q = QEosPower5.eosPower5;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return eosPower5Repository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(EosPower5 power) {
        eosPower5Repository.saveAndFlush(power);
    }

    @Transactional
    @Override
    public boolean closingGame(EosPower5 result) {
        EosPower5 power = eosPower5Repository.findOne(q.sdate.eq(result.getSdate()));
        if (power == null) {
            return true;
        }

        try {
            power.setOddeven(result.getOddeven());
            power.setOdd_overunder(result.getOdd_overunder());
            power.setEven_overunder(result.getEven_overunder());

            power.setPb_oddeven(result.getPb_oddeven());
            power.setPb_odd_overunder(result.getPb_odd_overunder());
            power.setPb_even_overunder(result.getPb_even_overunder());

            power.setOverunder(result.getOverunder());
            power.setPb_overunder(result.getPb_overunder());

            power.setSize(result.getSize());
            power.setOdd_size(result.getOdd_size());
            power.setEven_size(result.getEven_size());

            power.setPb(result.getPb());
            power.setBall(result.getBall());
            power.setSize(result.getSize());
            power.setSum(result.getSum());

            power.setClosing(true);

            eosPower5Repository.saveAndFlush(power);
            eosPower5GameService.closingBetting(power);
        } catch (RuntimeException e) {
            log.error("파워볼 5분 {}회차 결과 업데이트에 실패하였습니다. - {}", power.getTimes(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = eosPower5Repository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getEosPower5().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        eosPower5Repository.deleteGame(DateUtils.beforeDays(days));
    }
}
