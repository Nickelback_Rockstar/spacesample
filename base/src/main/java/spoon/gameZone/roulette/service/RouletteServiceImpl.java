package spoon.gameZone.roulette.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.roulette.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.time.zone.ZoneRulesException;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class RouletteServiceImpl implements RouletteService {

    private ConfigService configService;

    private MemberService memberService;

    private RouletteBotService rouletteBotService;

    private RouletteGameService rouletteGameService;

    private RouletteRepository rouletteRepository;

    private BetItemRepository betItemRepository;

    private static QRoulette q = QRoulette.roulette;

    @Transactional
    @Override
    public boolean updateConfig(RouletteConfig rouletteConfig) {
        try {
            configService.updateZoneConfig("roulette", JsonUtils.toString(rouletteConfig));
            ZoneConfig.setRoulette(rouletteConfig);
            // 이미 등록된 게임의 배당을 변경한다.
            rouletteRepository.updateOdds(ZoneConfig.getRoulette().getOdds());
        } catch (RuntimeException e) {
            log.error("룰렛 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<Roulette> getComplete() {
        return rouletteRepository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Roulette> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return rouletteRepository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public RouletteDto.Score findScore(Long id) {
        Roulette roulette = rouletteRepository.findOne(id);

        RouletteDto.Score score = new RouletteDto.Score();
        score.setId(roulette.getId());
        score.setRound(roulette.getRound());
        score.setGameDate(roulette.getGameDate());
        score.setCancel(roulette.isCancel());
        score.setColor(roulette.getColor());
        score.setResult(roulette.getResult());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getRouletteUrl() + "?sdate=" + roulette.getSdate());
        if (json == null) return score;

        roulette = JsonUtils.toModel(json, Roulette.class);
        if (roulette == null) return score;

        // 봇에 결과가 있다면
        if (roulette.isClosing()) {
            if (StringUtils.empty(roulette.getResult())) {
                score.setCancel(true);
            }
            score.setResult(roulette.getResult());
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(RouletteDto.Score score) {
        Roulette roulette = rouletteRepository.findOne(score.getId());

        //System.out.println("roulette 1 ="+roulette.toString());

        try {
            // 스코어 입력
            if (StringUtils.empty(score.getResult())) {
                throw new ZoneRulesException("결과를 변환하지 못하였습니다.");
            }

            if (roulette.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                rouletteGameService.rollbackPayment(roulette);
            }

            roulette.updateScore(score);

            rouletteRepository.saveAndFlush(roulette);
            rouletteGameService.closingBetting(roulette);
            rouletteBotService.checkResult();

        } catch (RuntimeException e) {
            log.error("룰렛 {} - {}회차 수동처리를 하지 못하였습니다. - {}", roulette.getGameDate(), roulette.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<Roulette> iterable = rouletteRepository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(1))));
        for (Roulette roulette : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.ROULETTE).and(qi.groupId.eq(roulette.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getRouletteUrl() + "?sdate=" + roulette.getSdate());
            if (json == null) continue;

            Roulette result = JsonUtils.toModel(json, Roulette.class);
            if (result == null) continue;

            if (result.isClosing()) {
                roulette.setColor(result.getColor());
                roulette.setResult(result.getResult());

                roulette.setClosing(true);
                rouletteRepository.saveAndFlush(roulette);
                closing++;
            }
        }

        rouletteBotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public RouletteDto.Config gameConfig() {
        return gameConfig("");
    }

    @Override
    public RouletteDto.Config gameConfig(String userid) {
        RouletteDto.Config gameConfig = new RouletteDto.Config();
        RouletteConfig config = ZoneConfig.getRoulette();

        // 1분씩 댕겨.
        Date gameDate = new Date(config.getZoneMaker().getGameDate().getTime() - 1000 * 60);
        Roulette roulette = rouletteRepository.findOne(q.gameDate.eq(gameDate));

        if (roulette == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        userid = StringUtils.notEmpty(userid) ? userid : WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isRoulette());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(roulette.getGameDate());
        gameConfig.setSdate(roulette.getSdate());
        gameConfig.setRound(roulette.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(roulette.getOdds());

        // 60초 보정
        int betTime = (int) (roulette.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000 + 60000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        return gameConfig;
    }
}
