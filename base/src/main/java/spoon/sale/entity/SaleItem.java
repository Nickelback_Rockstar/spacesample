package spoon.sale.entity;

import lombok.Data;
import spoon.common.utils.NumberUtils;
import spoon.member.domain.Role;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "SALE_ITEM", indexes = {
        @Index(name = "IDX_userid", columnList = "userid")
})
public class SaleItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "saleId", insertable = false, updatable = false)
    private Sale sale;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String userid;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String nickname;

    @Column(name="agencyDepth")
    private int agencyDepth = 0;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency1;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency2;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency3;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency4;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency5;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency6;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency7;

    @Enumerated(EnumType.STRING)
    @Column(length = 16, nullable = false)
    private Role role;

    private long inMoney;

    private long outMoney;

    private long betSports;

    @Column(name="betSports2")
    private long betSports2;

    @Column(name="betZoneMoney")
    private String betZoneMoney;

    private long betZone;

    @Column(name="betZone1")
    private long betZone1;

    @Column(name="betZone2")
    private long betZone2;

    private long users;

    @Column(length = 16)
    private String rateCode;

    private double rateShare;

    private double rateSports;

    @Column(name="rateCasino")
    private double rateCasino;

    @Column(name="rateSports2")
    private double rateSports2;

    private double rateZone;

    @Column(name="rateZone1")
    private double rateZone1;

    @Column(name="rateZone2")
    private double rateZone2;

    private long lastMoney;

    private long totalMoney;

    private long tmpMoney1 = 0;
    private long tmpMoney2 = 0;
    private long tmpMoney3 = 0;
    private long tmpMoney4 = 0;
    private long tmpMoney5 = 0;
    private long tmpMoney6 = 0;
    private long tmpMoney7 = 0;

    @Temporal(TemporalType.TIMESTAMP)
    private Date regDate;

    private boolean closing;

    public long getCalcMoney() {
        if ("수익분배".equals(this.rateCode)) {
            //return (long) ((this.inMoney - this.outMoney) * this.rateShare / 100D);
            long money = this.inMoney - this.outMoney;
//            if(money < 0){
//                return money;
//            }else{
                return NumberUtils.lVal(this.inMoney - this.outMoney, this.rateShare);
//            }
        } else if ("롤링분배".equals(this.rateCode)) {
            long sports = NumberUtils.lVal(this.betSports, this.rateSports);
            long sports2 = NumberUtils.lVal(this.betSports2, this.rateSports2);
            long zone = NumberUtils.lVal(this.betZone, this.rateZone);
//            long zone1 = NumberUtils.lVal(this.betZone1, this.rateZone1);
//            long zone2 = NumberUtils.lVal(this.betZone2, this.rateZone2);
//            return sports + zone + zone1 + zone2;
            return sports + sports2 + zone;
        } else {
            return 0;
        }
    }
}

