package spoon.banking.domain;

import lombok.Data;

public class BankingDto {

    @Data
    public static class Command {
        private BankingCode bankingCode;
        private String date;
        private boolean closing;
        private String username;
        private boolean match;
        private String depositor;
    }

    @Data
    public static class Seller {
        private String bankingCode;
        private String agency1;
        private String agency2;
        private String agency3;
        private String agency4;
        private String agency5;
        private String agency6;
        private String agency7;
        private String agency;
        private String username;
        private boolean match;
    }
}
