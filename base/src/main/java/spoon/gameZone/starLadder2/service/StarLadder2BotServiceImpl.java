package spoon.gameZone.starLadder2.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.starLadder2.QStarLadder2;
import spoon.gameZone.starLadder2.StarLadder2;
import spoon.gameZone.starLadder2.StarLadder2Repository;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class StarLadder2BotServiceImpl implements StarLadder2BotService {

    private StarLadder2GameService starLadder2GameService;

    private StarLadder2Repository starLadder2Repository;

    private static QStarLadder2 q = QStarLadder2.starLadder2;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return starLadder2Repository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(StarLadder2 starLadder2) {
        starLadder2Repository.saveAndFlush(starLadder2);
    }

    @Transactional
    @Override
    public boolean closingGame(StarLadder2 result) {
        StarLadder2 starLadder2 = starLadder2Repository.findOne(q.sdate.eq(result.getSdate()));
        if (starLadder2 == null) {
            return true;
        }

        try {
            starLadder2.setOddeven(result.getOddeven());
            starLadder2.setLine(result.getLine());
            starLadder2.setStart(result.getStart());
            starLadder2.setClosing(true);

            starLadder2Repository.saveAndFlush(starLadder2);
            starLadder2GameService.closingBetting(starLadder2);
        } catch (RuntimeException e) {
            log.error("별다리2분 {}회차 결과 업데이트에 실패하였습니다. - {}", starLadder2.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = starLadder2Repository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getStarLadder2().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        starLadder2Repository.deleteGame(DateUtils.beforeDays(days));
    }
}
