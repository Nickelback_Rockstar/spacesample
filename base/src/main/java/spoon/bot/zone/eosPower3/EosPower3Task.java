package spoon.bot.zone.eosPower3;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class EosPower3Task {

    private GameBotParsing eosPower3Parsing;

    @Scheduled(cron = "0 0/3 * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        eosPower3Parsing.parsingGame();
    }

    @Scheduled(cron = "10 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        eosPower3Parsing.closingGame();
    }

    @Scheduled(cron = "5,8 3/3 * * * *")
    public void checkResult() {
        if (notParsing()) return;
        eosPower3Parsing.checkResult();
    }

    @Scheduled(cron = "7 1 4 * * * ")
    public void deleteGame() {
        eosPower3Parsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isEosPower3() || !ZoneConfig.getEosPower3().isEnabled();
    }

}
