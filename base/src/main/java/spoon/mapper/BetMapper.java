package spoon.mapper;

import org.apache.ibatis.annotations.Param;
import spoon.bet.domain.BetDto;
import spoon.bet.domain.BetReport;
import spoon.bet.domain.BetUserRate;
import spoon.casino.domain.BetCasinoDto;
import spoon.game.domain.MenuCode;

import java.util.List;

public interface BetMapper {

    List<BetDto.ZoneAmount> zoneAmount(@Param("menuCode") MenuCode menuCode, @Param("sdate") String sdate);

    long updateGameAmount(@Param("gameIds") long[] gameIds);

    List<BetDto.UserBet> userBetList(String userid);
    List<BetDto.UserBet> userBetList2(String userid);

    List<BetUserRate> userRateList(BetDto.BetRate command);

    void deleteBets(BetDto.Delete command);

    void deleteAllBets(BetDto.Delete command);

    List<BetReport> reportList(BetDto.ReportCommand command);

    long reportListTotal(BetDto.ReportCommand command);

    List<BetCasinoDto.BetList> casinoBetList(BetCasinoDto.Command command);

    long casinoBetListTotal(BetCasinoDto.Command command);

    String casinoMaxDate();

    List<BetDto.BetBalGame> balancePowerBet(@Param("sdate") String sdate);
    List<BetDto.BetBalGame> balancePowerLadderBet(@Param("sdate") String sdate);

    List<BetDto.BetBalGame> balancePowerReBet(@Param("sdate") String sdate);
    List<BetDto.BetBalGame> balancePowerLadderReBet(@Param("sdate") String sdate);

    List<BetDto.BetBalGame> balancePowerResult(BetDto.BetBalGame bal);
    List<BetDto.BetBalGame> balancePowerReResult(BetDto.BetBalGame bal);
    List<BetDto.BetBalGame> balancePowerLadderResult(BetDto.BetBalGame bal);
    List<BetDto.BetBalGame> balancePowerLadderReResult(BetDto.BetBalGame bal);

    List<BetDto.BetBalGame> balanceResetDate();

    boolean powerBalanceChk(@Param("userid") String userid, @Param("menuCode") String menuCode);

    List<BetCasinoDto.BetRoll> casinoBetRollList();

    void closingBetCasino();
}
