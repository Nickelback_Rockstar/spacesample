package spoon.bot.zone.power;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.power.Power;
import spoon.gameZone.power.PowerDto;
import spoon.gameZone.power.service.PowerBotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class PowerParsing implements GameBotParsing {

    private PowerBotService powerBotService;

    private static boolean isClosing = false;

    private static Date sdate = new Date();

    @Async
    @Override
    public void parsingGame() {
        isClosing = false;

        int count = 0;
        int times = ZoneConfig.getPower().getPowerMaker().getTimes();
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getPower().getPowerMaker().getGameDate(times));

        for (int i = 0; i < 6; i++) {
            times++;
            cal.add(Calendar.MINUTE, 5);

            if (cal.getTime().before(sdate)) continue;

            if (powerBotService.notExist(cal.getTime())) {
                Power power = new Power(times, cal.getTime());
                String hh = power.getSdate().substring(8,10);//시간

                if (power.getRound() == 73 || power.getRound() == 288) {
                    continue;
                }

                if(Integer.parseInt(hh) < 6){
                    //경기 생성중 6시 미만 경기는 생성 안한다.
                    continue;
                }
                power.setOdds(ZoneConfig.getPower().getOdds());
                powerBotService.addGame(power);
                count++;
            }
        }
        log.debug("파워볼 경기등록 : {}건", count);
    }

    @Async
    @Override
    public void closingGame() {
        //System.out.println("파워볼 결과 파싱 = " + isClosing);
        //if (isClosing) return;
        isClosing = true;

        int times = ZoneConfig.getPower().getPowerMaker().getTimes();
        Date gameDate = ZoneConfig.getPower().getPowerMaker().getGameDate(times);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getPowerUrl());
        if (json == null) {
            isClosing = false;
            return;
        }

        //System.out.println("파워볼 결과 파싱 = " + json);

        //={"gameDate":1541586180000,
        // "round":836176,
        // "times":233,
        // "ball":"27,13,17,22,11",
        // "pb":"06",
        // "oddeven":"EVEN",
        // "pb_oddeven":"EVEN",
        // "overunder":"OVER",
        // "pb_overunder":"OVER",
        // "size":"대",
        // "sum":90,
        // "sdate":"201811071923",
        // "closing":true}

        try {
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(json);
            Power result = new Power();
            long gDate = Long.parseLong(obj.get("gameDate").toString());
            result.setGameDate(new Date(gDate));
            result.setRound(Integer.parseInt(obj.get("round").toString()));
            result.setTimes(Integer.parseInt(obj.get("times").toString()));
            result.setBall(obj.get("ball").toString());
            result.setPb(obj.get("pb").toString());
            result.setSum(Integer.parseInt(obj.get("sum").toString()));
            result.setSdate(obj.get("sdate").toString());
            result.setClosing(Boolean.parseBoolean(obj.get("closing").toString()));

//            result.setOddeven(obj.get("oddeven").toString());
//            result.setPb_oddeven(obj.get("pb_oddeven").toString());
//            result.setOverunder(obj.get("overunder").toString());
//            result.setPb_overunder(obj.get("pb_overunder").toString());
//            result.setSize(obj.get("size").toString());

            //결과 처리 score
            PowerDto.Score score = new PowerDto.Score();
            score.setBall(result.getBall());
            score.setPb(result.getPb());
            result.updateScore(score);

            if (result == null) {
                isClosing = false;
                return;
            }

            if (!gameDate.equals(result.getGameDate())) {
                isClosing = false;
                return;
            }

            //System.out.println("파워볼 클로징 게임 result =" + result);

            isClosing = powerBotService.closingGame(result);

            log.debug("파워볼 경기 종료 : {}회차", result.getRound());

        } catch (ParseException e) {
            e.printStackTrace();
            isClosing = false;
            return;
        }

    }

    @Async
    @Override
    public void checkResult() {
        powerBotService.checkResult();
    }

    @Async
    @Override
    public void deleteGame() {
        powerBotService.deleteGame(3);
    }
}
