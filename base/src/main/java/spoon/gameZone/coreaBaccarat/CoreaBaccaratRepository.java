package spoon.gameZone.coreaBaccarat;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface CoreaBaccaratRepository extends JpaRepository<CoreaBaccarat, Long>, QueryDslPredicateExecutor<CoreaBaccarat> {

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE CoreaBaccarat o SET o.odds = :odds WHERE o.gameDate > CURRENT_TIMESTAMP")
    void updateOdds(@Param("odds") double[] odds);

    @Modifying(clearAutomatically = true)
    @Query(value = "DELETE FROM CoreaBaccarat o WHERE o.gameDate < :gameDate")
    void deleteGame(@Param("gameDate") Date gameDate);
}
