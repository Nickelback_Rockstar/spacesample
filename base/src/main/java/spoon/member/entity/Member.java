package spoon.member.entity;

import lombok.Data;
import org.hibernate.annotations.Formula;
import spoon.common.utils.WebUtils;
import spoon.member.domain.Role;
import spoon.member.domain.User;
import spoon.payment.domain.MoneyCode;
import spoon.payment.domain.PointCode;
import spoon.support.convert.CryptoConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MEMBER", indexes = {
        @Index(name = "userid", columnList = "userid")
})
public class Member implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "NVARCHAR(64)", unique = true)
    private String userid;

    @Column(columnDefinition = "NVARCHAR(64)", unique = true)
    private String nickname;

    @Column(nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(length = 16, nullable = false)
    private Role role;

    private int level;

    @Column(name="agencyDepth")
    private int agencyDepth = 0;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency1 = "";

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency2 = "";

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency3 = "";

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency4 = "";

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency5 = "";

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency6 = "";

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency7 = "";

    @Column(columnDefinition = "NVARCHAR(64)")
    private String recommender = "";

    @Column(length = 64)
    private String joinCode;

    @Column(length = 64)
    private String bank = "";

    @Convert(converter = CryptoConverter.class)
    private String account = "";

    @Convert(converter = CryptoConverter.class)
    private String depositor = "";

    @Convert(converter = CryptoConverter.class)
    private String bankPassword = "";

    @Convert(converter = CryptoConverter.class)
    private String phone = "";

    @Convert(converter = CryptoConverter.class)
    private String passKey = "";

    private long money;

    private long point;

    private long deposit;

    private long withdraw;

    @Formula("(deposit - withdraw)")
    private long change;

    // 로그인 가능 여부
    private boolean enabled;

    // 탈퇴 여부
    private boolean secession;

    // 블랙유저
    private boolean black;

    // 게시물 블럭
    private boolean block;

    // 보험 유저
    private boolean balanceLadder = false;
    private boolean balanceDari = false;
    private boolean balanceLowhi = false;
    private boolean balanceAladdin = false;
    private boolean balancePower = false;

    @Column(name="balanceLeak", length=1)
    private boolean balanceLeak = false;

    @Column(length=1)
    private boolean balanceDariLeak = false;

    @Column(length=1)
    private boolean balanceLadderLeak = false;

    @Column(length=1)
    private boolean balancePowerLeak = false;

    @Column(length=1)
    private boolean balanceDariReverse = false;

    @Column(length=1)
    private boolean balanceLadderReverse = false;

    @Column(length=1)
    private boolean balancePowerReverse = false;

    @Column(length=1)
    private boolean folderBlock = false;

    @Column(length=1)
    private boolean balancePowerLadder = false;

    @Column(length=1)
    private boolean balancePowerRe = false;

    @Column(length=1)
    private boolean balancePowerLadderRe = false;

    @Column(columnDefinition = "NVARCHAR(MAX)")
    private String memo = "";

    @Column(length = 64)
    private String joinIp;

    @Column(length = 64)
    private String joinDomain;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date joinDate;

    @Column(length = 64)
    private String loginIp;

    @Column(length = 64)
    private String loginDevice;

    @Column(length = 64)
    private String loginDomain;

    private long loginCnt;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date loginDate;

    private long betSportsCnt;

    private long betSports;

    private long betZoneCnt;

    private long betZone;

    @Formula("(betSports + betZone)")
    private long betTotal;

    @Formula("(betSportsCnt + betZoneCnt)")
    private long betCntTotal;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date betDate;

    // 총판 대리점 지급율
    @Column(length = 16)
    private String rateCode = "";

    private double rateShare;

    private double rateSports;

    @Column(name="rateSports2")
    private double rateSports2;

    private double rateZone;

    @Column(name="rateZone1")
    private double rateZone1;

    @Column(name="rateZone2")
    private double rateZone2;

    @Column(name="rateCasino")
    private double rateCasino;

    @Column(name="alarm", length=1)
    private boolean alarm = true;

    // 임의 아이디
    @Column(columnDefinition = "NVARCHAR(64)")
    private String virtualId = "";

    @Column(name="powerLotto", length=1)
    private boolean powerLotto;

    @Column(name="ratePowerLotto")
    private double ratePowerLotto;

    @Column(name="casinoId")
    private String casinoId;

    @Column(name="casinoNick")
    private String casinoNick;

    // ---------------------------------------------

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Member member = (Member) o;

        return userid != null ? userid.equals(member.userid) : member.userid == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (userid != null ? userid.hashCode() : 0);
        return result;
    }

    public User getUser() {
        User user = new User();
        user.setUserid(this.userid);
        user.setNickname(this.nickname);
        user.setAgency1(this.agency1);
        user.setAgency2(this.agency2);
        user.setAgency3(this.agency3);
        user.setAgency4(this.agency4);
        user.setAgency5(this.agency5);
        user.setAgency6(this.agency6);
        user.setAgency7(this.agency7);
        user.setAgencyDepth(this.agencyDepth);
        user.setRole(this.role);
        user.setLevel(this.level);
        user.setLoginIp(this.loginIp);
        user.setBlack(this.black);
        user.setBlock(this.block);
        user.setSecession(this.secession);
        user.setEnabled(this.enabled);

        return user;
    }

    public User addMoney(MoneyCode moneyCode, long amount) {
        this.money += amount;
        switch (moneyCode) {
            case DEPOSIT: // 충전
            case DEPOSIT_ROLLBACK: // 충전 롤백
                this.deposit += amount;
                break;
            case WITHDRAW: // 환전
            case WITHDRAW_ROLLBACK: // 환전 롤백
                this.withdraw -= amount;
                break;
            case BET_SPORTS: // 스포츠 베팅
                this.betSports -= amount;
                this.betSportsCnt++;
                this.betDate = new Date();
                break;
            case BET_ZONE: // 실시간 베팅
                this.betZone -= amount;
                this.betZoneCnt++;
                this.betDate = new Date();
                break;
            case BET_SPORTS_ROLLBACK: // 스포츠 베팅 롤백
                this.betSports -= amount;
                this.betSportsCnt--;
                break;
            case BET_ZONE_ROLLBACK: // 실시간 베팅 롤백
                this.betZone -= amount;
                this.betZoneCnt--;
                break;
        }
        return getUser();
    }

    public User addPoint(PointCode pointCode, long point) {
        this.point += point;
        if (pointCode == PointCode.CHANGE && this.role == Role.USER) {
            this.deposit += point;
        }
        return getUser();
    }

    public User loginHistory() {
        this.loginCnt++;
        this.loginDate = new Date();
        this.loginIp = WebUtils.ip();
        this.loginDevice = WebUtils.device();
        this.loginDomain = WebUtils.domain();
        return getUser();
    }

}
