package spoon.gameZone.binanceCoin1;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import spoon.gameZone.newSnail.NewSnail;

import java.util.Date;

public interface BinanceCoin1Repository extends JpaRepository<BinanceCoin1, Long>, QueryDslPredicateExecutor<BinanceCoin1> {

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE BinanceCoin1 o SET o.odds = :odds WHERE o.gameDate > CURRENT_TIMESTAMP")
    void updateOdds(@Param("odds") double[] odds);

    @Modifying(clearAutomatically = true)
    @Query(value = "DELETE FROM BinanceCoin1 o WHERE o.gameDate < :gameDate")
    void deleteGame(@Param("gameDate") Date gameDate);
}
