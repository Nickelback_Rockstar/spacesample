package spoon.gameZone.roulette.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.roulette.Roulette;
import spoon.gameZone.roulette.RouletteConfig;
import spoon.gameZone.roulette.RouletteDto;
import spoon.support.web.AjaxResult;

public interface RouletteService {

    /**
     * 룰렛 설정을 변경한다.
     */
    boolean updateConfig(RouletteConfig rouletteConfig);

    /**
     * 룰렛 등록된 게임을 가져온다.
     */
    Iterable<Roulette> getComplete();

    /**
     * 룰렛 종료된 게임을 가져온다.
     */
    Page<Roulette> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * 룰렛 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    RouletteDto.Score findScore(Long id);

    /**
     * 룰렛 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(RouletteDto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    RouletteDto.Config gameConfig();
    RouletteDto.Config gameConfig(String userid);

}
