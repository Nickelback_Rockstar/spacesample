package spoon.bot.balance.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.bot.balance.entity.GodBalance;
import spoon.bot.balance.entity.PolygonBalance;
import spoon.bot.balance.entity.PolygonBalance1;
import spoon.bot.balance.entity.PolygonBalance2;

public interface BalanceService {

    Page<PolygonBalance> pagePolygon(Pageable pageable);
    Page<PolygonBalance2> pagePolygon2(Pageable pageable);
    Page<PolygonBalance1> pagePolygon1(Pageable pageable);
    Page<GodBalance> pageGodBalance(Pageable pageable);

    void lockLadderBalance();

    void lockDariBalance();

    void lockPowerBalance();
}
