package spoon.web.seller.accounting;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spoon.casino.domain.BetCasinoDto;
import spoon.casino.service.CasinoListService;
import spoon.common.utils.WebUtils;
import spoon.member.domain.CurrentUser;
import spoon.member.domain.Role;
import spoon.member.service.MemberService;
import spoon.payment.domain.PaymentDto;
import spoon.payment.service.PaymentListService;
import spoon.sale.domain.SaleDto;
import spoon.sale.entity.SaleItem;
import spoon.sale.service.SaleService;

import java.util.List;

@Slf4j
@AllArgsConstructor
@Controller("seller.accountingController")
@RequestMapping("#{config.pathSeller}")
public class AccountingController {

    private SaleService saleService;

    private MemberService memberService;

    private CasinoListService casinoListService;

    private PaymentListService paymentListService;

    /**
     * 현재 정산금 페이지
     */
    @RequestMapping(value = "/accounting/current", method = RequestMethod.GET)
    public String sale(ModelMap map, SaleDto.Command command) {
        CurrentUser user = WebUtils.user();

        command.setAgency1(user.getAgency1());
        command.setAgency2(user.getAgency2());
        command.setAgency3(user.getAgency3());
        command.setAgency4(user.getAgency4());
        command.setAgency5(user.getAgency5());
        command.setAgency6(user.getAgency6());
        command.setAgency7(user.getAgency7());
        command.setAgencyDepth(user.getAgencyDepth());

        List<SaleItem> list = saleService.currentListSale2(command);
        for(SaleItem item : list){
            // '####' 구분자로 합쳐진 문자열 금액들을 배열로 쪼개서 셋해줌
            String[] money = item.getBetZoneMoney().split("####");
            if(money != null && money.length == 3){
                item.setBetSports(Long.parseLong(money[0]));
                item.setBetSports2(Long.parseLong(money[1]));
                item.setBetZone(Long.parseLong(money[2]));
            }
        }

        map.addAttribute("list", list);
        return "/seller/accounting/current";
    }

    /**
     * 총판 정산 리스트
     */
    @RequestMapping(value = "/accounting/list", method = RequestMethod.GET)
    public String list(ModelMap map, SaleDto.Command command, @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = {"regDate"}) Pageable pageable) {
        CurrentUser user = WebUtils.user();
        command.setAgency1(user.getAgency1());
        command.setAgency2(user.getAgency2());
        command.setAgency3(user.getAgency3());
        command.setAgency4(user.getAgency4());
        command.setAgency5(user.getAgency5());
        command.setAgency6(user.getAgency6());
        command.setAgency7(user.getAgency7());
        command.setAgencyDepth(user.getAgencyDepth());

        if (user.getRole() == Role.AGENCY && user.getAgencyDepth() == 1) {
            map.addAttribute("page", saleService.getPage(command, pageable));
            return "/seller/accounting/list";
        }

        map.addAttribute("page", saleService.getPageItem(command, pageable));
        return "/seller/accounting/item";
    }

    /**
     * 카지노 베팅 리스트
     */
    @RequestMapping(value = "/casino/list", method = RequestMethod.GET)
    public String list(ModelMap map, BetCasinoDto.Command command,
                       @PageableDefault(size = 20) Pageable pageable) {
        CurrentUser user = WebUtils.user();
        command.setAgency1(user.getAgency1());
        command.setAgency2(user.getAgency2());
        command.setAgency3(user.getAgency3());
        command.setAgency4(user.getAgency4());
        command.setAgency5(user.getAgency5());
        command.setAgency6(user.getAgency6());
        command.setAgency7(user.getAgency7());
        command.setAgencyDepth(user.getAgencyDepth());
        command.setAgencyid(user.getUserid());

        command.setSize(pageable.getPageSize());

//        System.out.println("command="+command.toString());

        List<BetCasinoDto.BetList> list = casinoListService.casinoBetList(command);

//        System.out.println(list.toString());

        Page<BetCasinoDto.BetList> page = new PageImpl<>(list, pageable, casinoListService.casinoBetListTotal(command));

        map.addAttribute("page", page);
        map.addAttribute("command", command);

        return "/seller/casino/list";
    }

    /**
     * 카지노 정산 포인트 리스트
     */
    @RequestMapping(value = "/casino/point", method = RequestMethod.GET)
    public String pointPage(ModelMap map, PaymentDto.Command command,
                            @PageableDefault(size = 30, direction = Sort.Direction.DESC, sort = "regDate") Pageable pageable) {
        command.setCode("카지노");
        map.addAttribute("page", paymentListService.pointPage(command, pageable));
        return "/seller/casino/point";
    }

}
