package spoon.web.site.payment;

import lombok.AllArgsConstructor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.banking.domain.BankingCode;
import spoon.banking.domain.WithdrawDto;
import spoon.banking.service.BankingListService;
import spoon.banking.service.BankingWithdrawService;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.WebUtils;
import spoon.config.domain.Config;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

@AllArgsConstructor
@Controller
@RequestMapping("#{config.pathSite}")
public class WithdrawController {

    private BankingListService bankingListService;

    private BankingWithdrawService bankWithdrawService;

    private MemberService memberService;

    /**
     * 환전 신청 폼
     */
    @RequestMapping(value = "/payment/withdraw", method = RequestMethod.GET)
    public String withdraw(ModelMap map) {
        if (Config.getSiteConfig().getPoint().isBlock()) {
            return "/site/block";
        }
        map.addAttribute("list", bankingListService.list(WebUtils.userid(), BankingCode.OUT));
        map.addAttribute("withdraw", new WithdrawDto.Add());
        map.addAttribute("zoneAmount", JsonUtils.toString(new WithdrawDto.Amount()));
        map.addAttribute("betting", bankWithdrawService.getRolling(WebUtils.userid()));
        map.addAttribute("member", memberService.getMember(WebUtils.userid()));

        return "/site/payment/withdraw";
    }

    /**
     * 환전 신청
     */
    @ResponseBody
    @RequestMapping(value = "/payment/withdraw", method = RequestMethod.POST)
    public AjaxResult withdraw(WithdrawDto.Add add) {
        return bankWithdrawService.withdraw(add);
    }


    /**
     * 환전 신청 삭제
     */
    @ResponseBody
    @RequestMapping(value = "/payment/withdraw/delete", method = RequestMethod.POST)
    public AjaxResult delete(long id) {
        return bankWithdrawService.delete(id);
    }


    /**
     * 파워로또 환전 신청 폼
     */
    @RequestMapping(value = "/payment/withdraw2", method = RequestMethod.GET)
    public String withdraw2(ModelMap map) {
        map.addAttribute("withdraw", new WithdrawDto.Add());
        map.addAttribute("zoneAmount", JsonUtils.toString(new WithdrawDto.Amount()));
        map.addAttribute("member", memberService.getMember(WebUtils.userid()));
        return "/site/payment/withdraw2";
    }

    /**
     * 환전 신청
     */
    @ResponseBody
    @RequestMapping(value = "/payment/withdraw2", method = RequestMethod.POST)
    public AjaxResult withdraw2(WithdrawDto.Add add) {
        String userid = WebUtils.userid();
        Member member = memberService.getMember(userid);
        if (!add.getBankPassword().equals(member.getBankPassword())) {
            return new AjaxResult(false, "환전 비밀번호를 다시 확인해주세요.");
        }
        String code = Config.getSysConfig().getZone().getPowerLottoCode();
        String param = "?code=" + code + "&userid=" + userid;
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getPowerLottoUrl() + "/ap/userMoney" + param);
        JSONParser parser = new JSONParser();
        try {
            JSONObject jsonObj = (JSONObject) parser.parse(json);
            //System.out.println("jsonObj.toString()=" + jsonObj.toString());
            if ("00".equals(jsonObj.get("resultCode").toString()) && (long) jsonObj.get("money") >= add.getAmount()) {
                return bankWithdrawService.withdraw2(add);
            } else {
                return new AjaxResult(false, "파워볼 로또 보유머니가 부족합니다.");
            }
        } catch (ParseException e) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.[1]");
        }
    }

}
