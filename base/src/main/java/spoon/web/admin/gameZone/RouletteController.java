package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.roulette.RouletteConfig;
import spoon.gameZone.roulette.RouletteDto;
import spoon.gameZone.roulette.service.RouletteService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.rouletteController")
@RequestMapping(value = "#{config.pathAdmin}")
public class RouletteController {

    private RouletteService rouletteService;

    /**
     * 룰렛 설정
     */
    @RequestMapping(value = "zone/roulette/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getRoulette());
        return "admin/zone/roulette/config";
    }

    /**
     * 룰렛 설정 변경
     */
    @RequestMapping(value = "zone/roulette/config", method = RequestMethod.POST)
    public String config(RouletteConfig rouletteConfig, RedirectAttributes ra) {
        boolean success = rouletteService.updateConfig(rouletteConfig);
        if (success) {
            ra.addFlashAttribute("message", "바카라 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "바카라 설정변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/roulette/config";
    }

    /**
     * 룰렛 진행
     */
    @RequestMapping(value = "zone/roulette/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", rouletteService.getComplete());
        map.addAttribute("config", ZoneConfig.getRoulette());
        return "admin/zone/roulette/complete";
    }

    /**
     * 룰렛 완료
     */
    @RequestMapping(value = "zone/roulette/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "gameDate") Pageable pageable) {
        map.addAttribute("page", rouletteService.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getRoulette());
        return "admin/zone/roulette/closing";
    }

    /**
     * 룰렛 스코어 입력 폼
     */
    @RequestMapping(value = "zone/roulette/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", rouletteService.findScore(id));
        return "admin/zone/roulette/score";
    }

    /**
     * 룰렛 결과처리
     */
    @RequestMapping(value = "zone/roulette/score", method = RequestMethod.POST)
    public String score(RouletteDto.Score score, RedirectAttributes ra) {
        boolean success = rouletteService.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "룰렛 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "룰렛 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/roulette/score?id=" + score.getId();
    }

    /**
     * 룰렛 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/roulette/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return rouletteService.closingAllGame();
    }

}
