package spoon.payment.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import org.springframework.util.Assert;
import spoon.banking.domain.BankingCode;
import spoon.banking.entity.Banking;
import spoon.banking.repository.BankingRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.ErrorUtils;
import spoon.common.utils.WebUtils;
import spoon.config.domain.Config;
import spoon.member.domain.Role;
import spoon.member.domain.User;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.monitor.service.MonitorService;
import spoon.payment.domain.MoneyCode;
import spoon.payment.domain.PaymentDto;
import spoon.payment.domain.PointCode;
import spoon.payment.entity.Money;
import spoon.payment.entity.Point;
import spoon.payment.entity.QMoney;
import spoon.payment.entity.QPoint;
import spoon.payment.repository.MoneyRepository;
import spoon.payment.repository.PointRepository;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class PaymentServiceImpl implements PaymentService {

    private MemberService memberService;

    private MoneyRepository moneyRepository;

    private PointRepository pointRepository;

    private BankingRepository bankingRepository;

    private MonitorService monitorService;

    private static QMoney qm = QMoney.money;
    private static QPoint qp = QPoint.point;

    @Override
    public boolean notPaidMoney(MoneyCode moneyCode, long actionId, String userid) {
        return moneyRepository.count(qm.moneyCode.eq(moneyCode).and(qm.actionId.eq(actionId)).and(qm.userid.eq(userid)).and(qm.cancel.isFalse())) == 0;
    }

    @Override
    public boolean notPaidPoint(PointCode pointCode, long actionId, String userid) {
        return pointRepository.count(qp.pointCode.eq(pointCode).and(qp.actionId.eq(actionId)).and(qp.userid.eq(userid)).and(qp.cancel.isFalse())) == 0;
    }

    @Transactional
    @Override
    public AjaxResult addMoney(PaymentDto.Add add) {
        if (add.getAmount() == 0) {
            return new AjaxResult(false, "지급할 머니가 없습니다.");
        }

        MoneyCode moneyCode;
        if("GRAPH".equals(add.getType())){
            moneyCode = add.isPlus() ? MoneyCode.GRAPH_ADD : MoneyCode.GRAPH_REMOVE;
        }else if("POWERLOTTO".equals(add.getType())){
            moneyCode = add.isPlus() ? MoneyCode.POWERlOTTO_ADD : MoneyCode.POWERlOTTO_REMOVE;
        }else if("CASINO".equals(add.getType())){
            moneyCode = add.isPlus() ? MoneyCode.CASINO_ADD : MoneyCode.CASINO_REMOVE;
        }else {
            moneyCode = add.isPlus() ? MoneyCode.ADD : MoneyCode.REMOVE;
        }

        if (!add.isPlus()) add.setAmount(-add.getAmount());

        try {
            addMoney(moneyCode, 0L, add.getUserid(), add.getAmount(), add.getMemo());
            if("POWERLOTTO".equals(add.getType())) {
                String userid = WebUtils.userid();
                String param = ""
                        + "?userid=" + userid
                        + "&money=" + Math.abs(add.getAmount())
                        + "&code=" + Config.getSysConfig().getZone().getPowerLottoCode();
                String json = HttpParsing.getJson(Config.getSysConfig().getZone().getPowerLottoUrl() + "/ap/" +(add.isPlus()? "withdraw":"deposit")+ param);
                JSONParser parser = new JSONParser();
                try {
                    JSONObject jsonObj = (JSONObject) parser.parse(json);
                    if ("00".equals(jsonObj.get("resultCode").toString())) {
                    } else {
                        return new AjaxResult(false, "지급할 머니가 없습니다.[1]");
                    }
                } catch (ParseException e) {
                    return new AjaxResult(false, "지급할 머니가 없습니다.[2]");
                }

                Member member = memberService.getMember(userid);
                Banking banking = new Banking();
                banking.setUserid(member.getUserid());
                banking.setNickname(member.getNickname());
                banking.setAgency1(member.getAgency1());
                banking.setAgency2(member.getAgency2());
                banking.setRole(member.getRole());
                banking.setLevel(member.getLevel());
                banking.setDepositor(member.getDepositor());
                banking.setAccount(member.getAccount());
                banking.setBank(member.getBank());
                banking.setMoney(member.getMoney());
                banking.setPoint(member.getPoint());
                if(add.isPlus()){
                    banking.setBankingCode(BankingCode.IN);
                    banking.setMemo(MoneyCode.POWERlOTTO_ADD.getName());
                }else{
                    banking.setBankingCode(BankingCode.OUT);
                    banking.setMemo(MoneyCode.POWERlOTTO_REMOVE.getName());
                }
                banking.setAmount(Math.abs(add.getAmount()));
                banking.setRegDate(new Date());
                banking.setWorker(member.getUserid());
                banking.setIp(WebUtils.ip());
                banking.setRolling("");
                banking.setClosing(true);
                banking.setClosingDate(new Date());

                bankingRepository.saveAndFlush(banking);

            }
            /*else if("CASINO".equals(add.getType())) {
                String userid = WebUtils.userid();
                Member member = memberService.getMember(userid);
                Banking banking = new Banking();
                banking.setUserid(member.getUserid());
                banking.setNickname(member.getNickname());
                banking.setAgency1(member.getAgency1());
                banking.setAgency2(member.getAgency2());
                banking.setAgency3(member.getAgency3());
                banking.setAgency4(member.getAgency4());
                banking.setAgency5(member.getAgency5());
                banking.setAgency6(member.getAgency6());
                banking.setAgency7(member.getAgency7());
                banking.setRole(member.getRole());
                banking.setLevel(member.getLevel());
                banking.setDepositor(member.getDepositor());
                banking.setAccount(member.getAccount());
                banking.setBank(member.getBank());
                banking.setMoney(member.getMoney());
                banking.setPoint(member.getPoint());
                if(add.isPlus()){
                    banking.setBankingCode(BankingCode.CASINO_IN);
                    banking.setMemo(MoneyCode.CASINO_ADD.getName());
                }else{
                    banking.setBankingCode(BankingCode.CASINO_OUT);
                    banking.setMemo(MoneyCode.CASINO_REMOVE.getName());
                }
                banking.setAmount(Math.abs(add.getAmount()));
                banking.setRegDate(new Date());
                banking.setWorker(member.getUserid());
                banking.setIp(WebUtils.ip());
                banking.setRolling("");
                banking.setClosing(false);
                banking.setClosingDate(new Date());

                bankingRepository.saveAndFlush(banking);

                if(add.isPlus()){
                    monitorService.checkDeposit();
                }else{
                    monitorService.checkWithdraw();
                }

            }*/

        } catch (RuntimeException e) {
            log.error("관리자 머니 지급에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, "관리자 머니 지급에 실패하였습니다.");
        }
        return new AjaxResult(true);
    }

    @Transactional
    @Override
    public AjaxResult addPoint(PaymentDto.Add add) {
        if (add.getAmount() == 0) {
            return new AjaxResult(false, "지급할 포인트가 없습니다.");
        }

        PointCode pointCode = add.isPlus() ? PointCode.ADD : PointCode.REMOVE;
        if (!add.isPlus()) add.setAmount(-add.getAmount());

        try {
            addPoint(pointCode, 0L, add.getUserid(), add.getAmount(), add.getMemo());
        } catch (RuntimeException e) {
            log.error("관리자 포인트 지급에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, "관리자 포인트 지급에 실패하였습니다.");
        }
        return new AjaxResult(true);
    }

    @Transactional
    @Override
    public AjaxResult exchange(PaymentDto.Add add, String bankPassword) {
        if (add.getAmount() == 0) {
            return new AjaxResult(false, "지급할 포인트가 없습니다.");
        }

        Member member = memberService.getMember(WebUtils.userid());
        if (member.getPoint() < add.getAmount()) {
            return new AjaxResult(false, "최대 지급가능 포인트는 " + member.getPoint() + " 입니다.");
        }

        if (!bankPassword.equals(member.getBankPassword())) {
            return new AjaxResult(false, "출금 비밀번호가 일치하지 않습니다.");
        }

        try {
            //총판 작업 하면서 수정해야할 부분
            addPoint(PointCode.CHANGE, 0L, member.getUserid(), -add.getAmount(), "포인트 보냄 - " + add.getUserid() + (member.getRole() == Role.AGENCY ? " 대리점" : " 회원"));
            addPoint(PointCode.CHANGE, 0L, add.getUserid(), add.getAmount(), "포인트 받음 - " + member.getUserid() + (member.getRole() == Role.AGENCY ? " 총판" : " 대리점"));
        } catch (RuntimeException e) {
            log.error("총판 포인트 지급에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, "포인트 지급에 실패하였습니다.");
        }
        return new AjaxResult(true, add.getAmount() + " 포인트 지급을 완료하였습니다.");
    }

    /**
     * 최종 머니를 리턴한다.
     */
    @Transactional
    @Override
    public long addMoney(MoneyCode moneyCode, Long actionId, String userid, long amount, String memo) {
        Assert.notNull(moneyCode, "PaymentService.addMoney - moneyCode must be not null");
        Assert.notNull(userid, "PaymentService.addMoney - userid must be not null");

        Member member = memberService.getMember(userid);
        if (member == null || member.getRole().getValue() > 100) {
            throw new PaymentException(userid + "회원을 찾을 수 없습니다.");
        }
        long orgMoney = member.getMoney();
        User user = member.addMoney(moneyCode, amount);
        memberService.update(member);

        Money money = new Money(user, moneyCode, actionId, orgMoney, amount, memo);
        moneyRepository.saveAndFlush(money);

        return orgMoney + amount;
    }

    /**
     * 최종 포인트를 리턴한다.
     */
    @Transactional
    @Override
    public long addPoint(PointCode pointCode, Long actionId, String userid, long amount, String memo) {
        Assert.notNull(pointCode, "PaymentService.addPoint - pointCode must be not null");
        Assert.notNull(userid, "PaymentService.addPoint - userid must be not null");

        Member member = memberService.getMember(userid);
        if (member == null || member.getRole().getValue() > 100) {
            throw new PaymentException(userid + "회원을 찾을 수 없습니다.");
        }

        long orgPoint = member.getPoint();
        User user = member.addPoint(pointCode, amount);
        memberService.update(member);

        Point point = new Point(user, pointCode, actionId, orgPoint, amount, memo);
        pointRepository.saveAndFlush(point);

        return orgPoint + amount;
    }

    @Override
    public long rollbackMoney(MoneyCode moneyCode, Long actionId, String userid) {
        Assert.notNull(moneyCode, "PaymentService.rollbackMoney - moneyCode must be not null");
        Assert.notNull(actionId, "PaymentService.rollbackMoney - actionId must be not null");
        Assert.notNull(userid, "PaymentService.rollbackMoney - userid must be not null");

        Money money = moneyRepository.findOne(qm.moneyCode.eq(moneyCode).and(qm.actionId.eq(actionId)).and(qm.userid.eq(userid)).and(qm.cancel.isFalse()));
        if (money == null) return 0;

        money.setCancel(true);
        long amount = -money.getAmount();
        MoneyCode rollbackCode = MoneyCode.rollback(moneyCode);
        moneyRepository.saveAndFlush(money);

        Member member = memberService.getMember(userid);
        if (member == null || member.getRole().getValue() > 100) {
            throw new PaymentException(userid + "회원을 찾을 수 없습니다.");
        }

        long orgMoney = member.getMoney();

        User user = member.addMoney(rollbackCode, amount);
        memberService.update(member);

        Money rollback = new Money(user, rollbackCode, actionId, orgMoney, amount, money.getMemo());
        moneyRepository.saveAndFlush(rollback);

        return orgMoney + amount;
    }

    @Override
    public long rollbackPoint(PointCode pointCode, Long actionId, String userid) {
        Assert.notNull(pointCode, "PaymentService.rollbackPoint - pointCode must be not null");
        Assert.notNull(actionId, "PaymentService.rollbackPoint - actionId must be not null");
        Assert.notNull(userid, "PaymentService.rollbackPoint - userid must be not null");

        Point point = pointRepository.findOne(qp.pointCode.eq(pointCode).and(qp.userid.eq(userid)).and(qp.actionId.eq(actionId)).and(qp.cancel.isFalse()));
        if (point == null) return 0;

        point.setCancel(true);
        long amount = -point.getAmount();
        PointCode rollbackCode = PointCode.rollback(pointCode);
        pointRepository.saveAndFlush(point);

        Member member = memberService.getMember(userid);
        if (member == null || member.getRole().getValue() > 100) {
            throw new PaymentException(point.getUserid() + " 회원을 찾을 수 없습니다.");
        }

        long orgPoint = member.getPoint();

        User user = member.addPoint(pointCode, amount);
        memberService.update(member);

        Point rollback = new Point(user, rollbackCode, actionId, orgPoint, amount, point.getMemo());
        pointRepository.saveAndFlush(rollback);

        return orgPoint + amount;
    }

    @Override
    public long rollbackRecomm(PointCode pointCode, Long actionId, String userid) {
        Assert.notNull(pointCode, "PaymentService.rollbackRecomm - pointCode must be not null");
        Assert.notNull(actionId, "PaymentService.rollbackRecomm - actionId must be not null");
        Assert.notNull(userid, "PaymentService.rollbackRecomm - userid must be not null");

        User recomm = memberService.getRecomm(userid);
        if (recomm == null) return 0;

        Point point = pointRepository.findOne(qp.pointCode.eq(pointCode).and(qp.userid.eq(recomm.getUserid())).and(qp.actionId.eq(actionId)).and(qp.cancel.isFalse()));
        if (point == null) return 0;

        point.setCancel(true);
        long amount = -point.getAmount();
        PointCode rollbackCode = PointCode.rollback(pointCode);
        pointRepository.saveAndFlush(point);

        Member member = memberService.getMember(recomm.getUserid());
        if (member == null || member.getRole().getValue() > 100) {
            throw new PaymentException(point.getUserid() + " 회원을 찾을 수 없습니다.");
        }

        long orgPoint = member.getPoint();

        User user = member.addPoint(pointCode, amount);
        memberService.update(member);

        Point rollback = new Point(user, rollbackCode, actionId, orgPoint, amount, point.getMemo());
        pointRepository.saveAndFlush(rollback);

        return orgPoint + amount;
    }

}
