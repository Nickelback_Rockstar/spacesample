package spoon.gameZone.starLadder2.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.starLadder2.StarLadder2;
import spoon.support.web.AjaxResult;

public interface StarLadder2GameService {

    /**
     * 별다리2분 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 별다리2분 게임 베팅을 클로징 한다.
     */
    void closingBetting(StarLadder2 starLadder2);

    /**
     * 별다리2분 게임을 롤백 한다.
     */
    void rollbackPayment(StarLadder2 starLadder2);
}
