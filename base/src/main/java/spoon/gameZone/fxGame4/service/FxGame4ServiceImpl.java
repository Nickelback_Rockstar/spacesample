package spoon.gameZone.fxGame4.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame4.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame4ServiceImpl implements FxGame4Service {

    private ConfigService configService;

    private MemberService memberService;

    private FxGame4GameService fxGame4GameService;

    private FxGame4BotService fxGame4BotService;

    private FxGame4Repository fxGame4Repository;

    private BetItemRepository betItemRepository;

    private static QFxGame4 q = QFxGame4.fxGame4;

    @Transactional
    @Override
    public boolean updateConfig(FxGame4Config fxGame4Config) {
        try {
            configService.updateZoneConfig("fxGame4", JsonUtils.toString(fxGame4Config));
            ZoneConfig.setFxGame4(fxGame4Config);
            // 이미 등록된 게임의 배당을 변경한다.
            fxGame4Repository.updateOdds(ZoneConfig.getFxGame4().getOdds());
        } catch (RuntimeException e) {
            log.error("fxGame4 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<FxGame4> getComplete() {
        return fxGame4Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<FxGame4> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return fxGame4Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public FxGame4Dto.Score findScore(Long id) {
        FxGame4 fxGame4 = fxGame4Repository.findOne(id);

        FxGame4Dto.Score score = new FxGame4Dto.Score();
        score.setId(fxGame4.getId());
        score.setRound(fxGame4.getRound());
        score.setGameDate(fxGame4.getGameDate());
        score.setFxResult(fxGame4.getFxResult());
        score.setOddeven(fxGame4.getOddeven());
        score.setOverunder(fxGame4.getOverunder());
        score.setCancel(fxGame4.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame4Url() + "?sdate=" + fxGame4.getSdate());
        if (json == null) return score;

        fxGame4 = JsonUtils.toModel(json, FxGame4.class);
        if (fxGame4 == null) return score;

        // 봇에 결과가 있다면
        if (fxGame4.isClosing()) {
            score.setFxResult(fxGame4.getFxResult());
            score.setOddeven(fxGame4.getOddeven());
            score.setOverunder(fxGame4.getOverunder());

            if (!"BUY".equals(score.getFxResult()) && !"SELL".equals(score.getFxResult())
                    && !"ODD".equals(score.getOddeven()) && !"EVEN".equals(score.getOddeven())
                    && !"OVER".equals(score.getOverunder()) && !"UNDER".equals(score.getOverunder())
            ) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(FxGame4Dto.Score score) {
        FxGame4 fxGame4 = fxGame4Repository.findOne(score.getId());

        try {
            if (fxGame4.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                fxGame4GameService.rollbackPayment(fxGame4);
            }

            // 스코어 입력
            fxGame4.updateScore(score);
            fxGame4Repository.saveAndFlush(fxGame4);
            fxGame4GameService.closingBetting(fxGame4);
            fxGame4BotService.checkResult();
        } catch (RuntimeException e) {
            log.error("fxGame4 {} - {}회차 수동처리를 하지 못하였습니다. - {}", fxGame4.getGameDate(), fxGame4.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<FxGame4> iterable = fxGame4Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(5))));
        for (FxGame4 fxGame4 : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.FXGAME4).and(qi.groupId.eq(fxGame4.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame4Url() + "?sdate=" + fxGame4.getSdate());
            if (json == null) continue;

            FxGame4 result = JsonUtils.toModel(json, FxGame4.class);
            if (result == null) continue;

            if (result.isClosing()) {
                fxGame4.setFxResult(result.getFxResult());
                fxGame4.setClosing(true);
                fxGame4Repository.saveAndFlush(fxGame4);
                closing++;
            }
        }
        fxGame4BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public FxGame4Dto.Config gameConfig(int r) {
        FxGame4Dto.Config gameConfig = new FxGame4Dto.Config();
        FxGame4Config config = ZoneConfig.getFxGame4();

        Date oriGameDate = config.getZoneMaker().getGameDate();
        Date gameDate = config.getZoneMaker().getGameDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(gameDate);
        cal.add(Calendar.MINUTE, (config.getZoneMaker().getInterval() * r));

        FxGame4 fxGame4 = fxGame4Repository.findOne(q.gameDate.eq(cal.getTime()));

        if (fxGame4 == null) {
            gameConfig.setGameDate2(oriGameDate);
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(0);
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isFxGame4());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setNextRound(r);
        gameConfig.setGameDate2(oriGameDate);
        gameConfig.setGameDate(fxGame4.getGameDate());
        gameConfig.setSdate(fxGame4.getSdate());
        gameConfig.setRound(fxGame4.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(fxGame4.getOdds());

        int betTime = (int) (oriGameDate.getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setFxResult(config.isFxResult());
        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setOverunder(config.isOverunder());

        return gameConfig;
    }
}
