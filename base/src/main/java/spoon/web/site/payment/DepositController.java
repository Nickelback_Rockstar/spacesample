package spoon.web.site.payment;

import lombok.AllArgsConstructor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.banking.domain.BankingCode;
import spoon.banking.domain.DepositDto;
import spoon.banking.service.BankingDepositService;
import spoon.banking.service.BankingListService;
import spoon.common.net.HttpParsing;
import spoon.common.utils.DateUtils;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.WebUtils;
import spoon.config.domain.Config;
import spoon.config.domain.SiteConfig;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

@AllArgsConstructor
@Controller
@RequestMapping("#{config.pathSite}")
public class DepositController {

    private MemberService memberService;

    private BankingListService bankingListService;

    private BankingDepositService bankDepositService;

    /**
     * 충전 신청 폼
     */
    @RequestMapping(value = "/payment/deposit", method = RequestMethod.GET)
    public String deposit(ModelMap map) {
        if (Config.getSiteConfig().getPoint().isBlock()) {
            return "/site/block";
        }
        map.addAttribute("list", bankingListService.list(WebUtils.userid(), BankingCode.IN));
        map.addAttribute("deposit", new DepositDto.Add());
        map.addAttribute("zoneAmount", JsonUtils.toString(new DepositDto.Amount()));
        map.addAttribute("bonus", checkBonus());
        map.addAttribute("member", memberService.getMember(WebUtils.userid()));

        return "/site/payment/deposit";
    }

    /**
     * 충전 신청
     */
    @ResponseBody
    @RequestMapping(value = "/payment/deposit", method = RequestMethod.POST)
    public AjaxResult deposit(DepositDto.Add add) {
        return bankDepositService.deposit(add);
    }

    /**
     * 충전 신청 삭제
     */
    @ResponseBody
    @RequestMapping(value = "/payment/deposit/delete", method = RequestMethod.POST)
    public AjaxResult delete(long id) {
        return bankDepositService.delete(id);
    }

    /**
     * 충전 신청 취소
     */
    @ResponseBody
    @RequestMapping(value = "/payment/deposit/cancel", method = RequestMethod.POST)
    public AjaxResult cancel(long id) {
        return bankDepositService.cancel(id);
    }

    /**
     * 이벤트를 찾는다.
     */
    private String checkBonus() {
        int level = WebUtils.level();
        SiteConfig.Point point = Config.getSiteConfig().getPoint();
        StringBuilder sb = new StringBuilder();

        if (point.isEventPayment() && point.getEvent()[DateUtils.week()] && point.getEventRate()[DateUtils.week()] > 0) {
            if (point.isEventFirst()) {
                sb.append("[이벤트] 첫충전 보너스 ").append(point.getEventRate()[DateUtils.week()]).append("%가 진행중입니다.");
            } else {
                sb.append("[이벤트] 매충전 보너스 ").append(point.getEventRate()[DateUtils.week()]).append("%가 진행중입니다.");
            }
        } else {
            if (point.getFirstRate()[level] > 0) {
                sb.append(" 첫충전 보너스 ").append(point.getFirstRate()[level]).append("%");
            }
            if (point.getEveryRate()[level] > 0) {
                sb.append(sb.length() == 0 ? "" : " / ");
                sb.append(" 매충전 보너스 ").append(point.getEveryRate()[level]).append("%");
            }
        }
        return sb.toString();
    }

    /**
     * 충전 계좌 확인 API
     */
    @ResponseBody
    @RequestMapping(value = "/payment/deposit/account", method = RequestMethod.POST)
    public AjaxResult account(String pass) {
        return bankDepositService.account(pass);
    }

    /**
     * 파워로또 충전 신청 폼
     */
    @RequestMapping(value = "/payment/deposit2", method = RequestMethod.GET)
    public String deposit2(ModelMap map) {

        map.addAttribute("deposit", new DepositDto.Add());
        map.addAttribute("zoneAmount", JsonUtils.toString(new DepositDto.Amount()));
        map.addAttribute("member", memberService.getMember(WebUtils.userid()));
        return "/site/payment/deposit2";
    }

    /**
     * 파워로또 충전 신청
     */
    @ResponseBody
    @RequestMapping(value = "/payment/deposit2", method = RequestMethod.POST)
    public AjaxResult deposit2(DepositDto.Add add) {
        //System.out.println("add.toString="+add.toString());
        String userid = WebUtils.userid();
        Member member = memberService.getMember(userid);
        //System.out.println("add.getBankPassword()="+add.getBankPassword());
        //System.out.println("member.getBankPassword()="+member.getBankPassword());
        if(!add.getBankPassword().equals(member.getBankPassword())){
            return new AjaxResult(false, "환전 비밀번호를 다시 확인해주세요.");
        }

        String code = Config.getSysConfig().getZone().getPowerLottoCode();
        String param = "?code="+code;
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getPowerLottoUrl()+"/ap/sellerMoney" + param);
        JSONParser parser = new JSONParser();
        try {
            JSONObject jsonObj = (JSONObject) parser.parse(json);
            //System.out.println("jsonObj.toString()="+jsonObj.toString());
            if("00".equals(jsonObj.get("resultCode").toString()) && (long)jsonObj.get("money") >= add.getAmount()){
                return bankDepositService.deposit2(add);
            }else{
                return new AjaxResult(false, "업체 보유머니가 부족합니다.");
            }
        } catch (ParseException e) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.[1]");
        }
    }
}
