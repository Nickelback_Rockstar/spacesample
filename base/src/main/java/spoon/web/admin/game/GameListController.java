package spoon.web.admin.game;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.bet.domain.BetDto;
import spoon.bet.service.BetListService;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.config.domain.LogInfo;
import spoon.game.domain.GameDto;
import spoon.game.domain.MenuCode;
import spoon.game.entity.Sports;
import spoon.game.service.GameListService;
import spoon.game.service.GameLoggerService;
import spoon.game.service.GameService;
import spoon.game.service.sports.SportsService;
import spoon.mapper.GameMapper;

import java.util.Date;


@Slf4j
@AllArgsConstructor
@Controller("admin.gameListController")
@RequestMapping(value = "#{config.pathAdmin}")
public class GameListController {

    private GameListService gameListService;

    private GameLoggerService gameLoggerService;

    private GameService gameService;

    private SportsService sportsService;

    private GameMapper gameMapper;

    private BetListService betListService;


    @ModelAttribute("sportsList")
    public Iterable<Sports> sportsList() {
        return sportsService.getAll();
    }

    /**
     * 스포츠 등록대기
     */
    @RequestMapping(value = "game/{menu:match|handicap|special|live|inGame}/ready", method = RequestMethod.GET)
    public String ready(ModelMap map, @PathVariable("menu") String menu, GameDto.Command command,
                        @PageableDefault(size = 50) Pageable pageable) {
        command.setMenu("ready");
        command.setMenuCode(MenuCode.valueOf(menu.toUpperCase()));

        if ("inGame".equals(menu)) {
        } else {
            map.addAttribute("page", gameListService.readyGameList(command, pageable));
        }

        map.addAttribute("leagueList", JsonUtils.toString(gameMapper.readyLeague(menu)));
        map.addAttribute("icon", getCssClass(menu));
        map.addAttribute("config", JsonUtils.toString(new GameDto.GameConfig()));

        if ("inGame".equals(menu)) {
            return "admin/game/inGameReady";
        } else {
            return "admin/game/ready";
        }
    }

    /**
     * 스포츠 등록완료
     */
    @RequestMapping(value = "game/{menu:match|handicap|special|live|inGame}/complete", method = RequestMethod.GET)
    public String complete(ModelMap map, @PathVariable("menu") String menu, GameDto.Command command,
                           @PageableDefault(size = 50) Pageable pageable) {
        command.setMenu("complete");
        command.setMenuCode(MenuCode.valueOf(menu.toUpperCase()));

        map.addAttribute("page", gameListService.completeGameList(command, pageable));
        map.addAttribute("leagueList", JsonUtils.toString(gameMapper.completeLeague(menu)));
        map.addAttribute("icon", getCssClass(menu));
        map.addAttribute("config", JsonUtils.toString(new GameDto.GameConfig()));
        if ("special".equals(menu)) {
            map.addAttribute("parsingDate", LogInfo.getBetParsingDate());
        } else {
            map.addAttribute("parsingDate", LogInfo.getBet365ParsingDate());
        }
        map.addAttribute("bonsaParsingDate", LogInfo.getBonsaParsingDate());

        return "admin/game/complete";
    }

    /**
     * 스포츠 게임종료
     */
    @RequestMapping(value = "game/{menu:match|handicap|special|live|inGame}/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @PathVariable("menu") String menu, GameDto.Command command,
                          @PageableDefault(size = 50) Pageable pageable) {
        command.setMenu("closing");
        command.setMenuCode(MenuCode.valueOf(menu.toUpperCase()));

        if ("inGame".equals(menu)) {
            //인게임은 sport 테이블에 없으니 베팅한 정보만 가져옴
            BetDto.Command betCmd = new BetDto.Command();
            betCmd.setMenuCode(MenuCode.INGAME);
            betCmd.setResult("closing");
            betCmd.setSpecial(command.getSpecial());
            betCmd.setSports(command.getSports());
            betCmd.setTeamName(command.getTeam());
            map.addAttribute("page", betListService.adminPage(betCmd, pageable));
        } else {
            map.addAttribute("page", gameListService.closingGameList(command, pageable));
        }

        map.addAttribute("leagueList", JsonUtils.toString(gameMapper.closingLeague(menu)));
        map.addAttribute("icon", getCssClass(menu));
        map.addAttribute("config", JsonUtils.toString(new GameDto.GameConfig()));

        if ("inGame".equals(menu)) {
            return "admin/game/inGameClosing";
        } else {
            return "admin/game/closing";
        }
    }

    /**
     * 스포츠 결과처리
     */
    @RequestMapping(value = "game/{menu:cross|special|live|inGame}/result", method = RequestMethod.GET)
    public String result(ModelMap map, @PathVariable("menu") String menu, GameDto.Command command,
                         @PageableDefault(size = 50) Pageable pageable) {
        command.setMenu("result");
        command.setMenuCode(MenuCode.valueOf(menu.toUpperCase()));

        if ("inGame".equals(menu)) {
            //인게임은 sport 테이블에 없으니 베팅한 정보만 가져옴
            BetDto.Command betCmd = new BetDto.Command();
            betCmd.setMenuCode(MenuCode.INGAME);
            betCmd.setResult("ing");
            betCmd.setSpecial(command.getSpecial());
            betCmd.setSports(command.getSports());
            betCmd.setTeamName(command.getTeam());
            map.addAttribute("page", betListService.adminPage(betCmd, pageable));
        } else {
            map.addAttribute("page", gameListService.resultGameList(command, pageable));
        }

        map.addAttribute("leagueList", JsonUtils.toString(gameMapper.resultLeague(menu)));
        map.addAttribute("icon", getCssClass(command.getMenu()));
        map.addAttribute("config", JsonUtils.toString(new GameDto.GameConfig()));

        if ("inGame".equals(menu)) {
            return "admin/game/inGameResult";
        } else {
            return "admin/game/result";
        }
    }

    /**
     * 스포츠 삭제된 경기
     */
    @RequestMapping(value = "game/deleted", method = RequestMethod.GET)
    public String deleted(ModelMap map, GameDto.Command command,
                          @PageableDefault(size = 50) Pageable pageable) {
        command.setMenu("");
        command.setMenuCode(MenuCode.NONE);

        map.addAttribute("page", gameListService.deletedGameList(command, pageable));
        map.addAttribute("icon", getCssClass(command.getMenu()));
        map.addAttribute("config", JsonUtils.toString(new GameDto.GameConfig()));

        return "admin/game/deleted";
    }

    /**
     * 스포츠 업데이트 로그
     */
    @RequestMapping(value = "game/logger/{gameId}", method = RequestMethod.GET)
    public String gameLogger(ModelMap map, @PathVariable("gameId") long gameId) {
        map.addAttribute("game", gameService.getGame(gameId));
        map.addAttribute("list", gameLoggerService.getGameLogger(gameId));
        return "admin/game/popup/logger";
    }

    private String getCssClass(String menu) {
        switch (menu) {
            case "match":
                return "bul fa fa-futbol-o";
            case "handicap":
                return "bul fa fa-hand-paper-o";
            case "special":
                return "bul fa fa-hashtag";
            case "live":
                return "bul fa fa-clock-o";
            default:
                return "bul fa fa-minus-circle";
        }
    }

    @ResponseBody
    @RequestMapping(value = "game/inGameNewJson", method = RequestMethod.GET)
    public JSONArray inGameNew() {
        JSONArray jsonArr = new JSONArray();
        String json = HttpParsing.getJson(Config.getSysConfig().getSports().getInGameApi() + "/api/gameNew?sts=2&sdate=" + new Date());
        if (json == null) return jsonArr;
//        //System.out.println("인겜정보 = "+json);
        JSONParser parser = new JSONParser();

        try {
            jsonArr = (JSONArray) parser.parse(json);
            //System.out.println(jsonArr.toJSONString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonArr;
    }

    @ResponseBody
    @RequestMapping(value = "system/godBalanceSetting", method = RequestMethod.POST)
    public String godBalanceSetting() {
        gameService.godBalanceSetting();
        return "1";
    }
}
