package spoon.mapper;

import spoon.member.domain.Agency;
import spoon.member.domain.MemberDto;
import spoon.member.domain.User;
import spoon.member.entity.Member;
import spoon.sale.entity.SaleItem;
import spoon.seller.domain.Seller;

import java.util.List;

public interface MemberMapper {

    List<User> getDummyList();

    List<Agency> getAgencyList();

    List<Agency> getAgencyListNew();

    List<String> getAgency1List(String agency2);

    List<String> getAgency7List(MemberDto.Seller command);

    List<String> getAgency2List();

    List<MemberDto.Exchange> getExchangeList(MemberDto.Seller command);

    List<MemberDto.Exchange> getExchangeListNew(MemberDto.Seller command);

    User getUser(String userid);

    User getUserAndMemo(String userid);

    User getRandomUser();

    User getRecomm(String userid);

    MemberDto.Balance getUserBalance(String userid);

    List<User> userRecommList(String userid);

    List<SaleItem> sellerListNew(MemberDto.Seller command);
    List<Seller> sellerListNew2(MemberDto.Seller command);

    List<String> getAgency7List2(MemberDto.Seller command);

    List<MemberDto.Agency> getAgencyMemberList(MemberDto.Seller seller);

    List<MemberDto.Agency> getAgencyMemberList2(MemberDto.Seller seller);

    void deleteMember(String userid);
}
