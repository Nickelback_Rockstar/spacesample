package spoon.gameZone.eosPower1;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import spoon.bot.support.ZoneMaker;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EosPower1Config {

    @JsonIgnore
    private ZoneMaker zoneMaker;

    private boolean enabled;

    // 결과처리가 잘 되었는지 체크 (관리자에서 사용)
    private long result;

    private int betTime;

    // 회차별 베팅 숫자
    private int betMax = 1;

    private boolean oddeven;

    private boolean odd_overunder;

    private boolean even_overunder;

    private boolean pb_oddeven;

    private boolean pb_odd_overunder;

    private boolean pb_even_overunder;

    private boolean overunder;

    private boolean pb_overunder;

    private boolean size;

    private boolean odd_size;

    private boolean even_size;

    /**
     * 0:홀(oddeven), 1:짝(oddeven), 2:홀(pb_oddeven), 3:짝(pb_oddeven)
     * 4:오버(overunder), 5:언더(overunder), 6:오버(pb_overunder), 7:언더(pb_overunder)
     * 8:대(size), 9:중(size), 10:소(size)
     */
    private double[] odds = new double[25];

    private int[] win = {3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000};

    private int[] max = {1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000};

    private int[] min = {5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000};

    public EosPower1Config() {
        this.zoneMaker = new ZoneMaker(1);
    }

}
