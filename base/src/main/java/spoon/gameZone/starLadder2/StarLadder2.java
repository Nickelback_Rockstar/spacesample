package spoon.gameZone.starLadder2;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import spoon.bot.support.ZoneHelper;
import spoon.common.utils.DateUtils;
import spoon.game.domain.MenuCode;
import spoon.gameZone.GameZoneException;
import spoon.gameZone.Zone;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneScore;
import spoon.support.convert.DoubleArrayConvert;
import spoon.support.convert.LongArrayConvert;

import javax.persistence.*;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "ZONE_BOGLELADDER", indexes = {
        @Index(name = "IDX_sdate", columnList = "sdate")
})
public class StarLadder2 {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date gameDate;

    private int round;

    @JsonProperty("start")
    @Column(length = 16)
    private String start;

    @JsonProperty("line")
    @Column(length = 16)
    private String line;

    @JsonProperty("oddEven")
    @Column(length = 16)
    private String oddeven;

    @Column(length = 16)
    private String sdate;

    @Convert(converter = LongArrayConvert.class)
    private long[] amount = new long[10];

    @Convert(converter = DoubleArrayConvert.class)
    private double[] odds = new double[10];

    private boolean closing;

    private boolean cancel;

    //--------------------------------------------------------

    public StarLadder2(int round, Date gameDate) {
        this.round = round;
        this.gameDate = gameDate;
        this.sdate = DateUtils.format(gameDate, "yyyyMMddHHmm");
    }

    public boolean isBeforeGameDate() {
        return this.gameDate.getTime() - System.currentTimeMillis() > 0;
    }

    // 스코어 입력
    public void updateScore(StarLadder2Dto.Score score) {
        if (score.isCancel()) {
            this.start = "";
            this.line = "";
            this.oddeven = "";
            this.cancel = true;
        } else {
            this.start = score.getStart();
            this.line = score.getLine();
            this.oddeven = score.getOddeven();
            this.cancel = false;
        }
        this.closing = true;
    }

    // 경기 결과
    public ZoneScore getGameResult(String gameCode) {
        switch (gameCode) {
            case "start":
                return ZoneHelper.zoneResult(start, cancel);
            case "line":
                return ZoneHelper.zoneResult(line, cancel);
            case "oddeven":
                return ZoneHelper.zoneResult(oddeven, cancel);
            case "line3Start":
                return ZoneHelper.line3StartResult(line, start, cancel);
            case "line4Start":
                return ZoneHelper.line4StartResult(line, start, cancel);
            default:
                throw new GameZoneException("별다리2분 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    public Zone getZone(String gameCode) {
        switch (gameCode) {
            case "start":
                return getStarLadder2Zone("start", "좌", "우", 0, 1);
            case "line":
                return getStarLadder2Zone("line", "3줄", "4줄", 2, 3);
            case "oddeven":
                return getStarLadder2Zone("oddeven", "홀", "짝", 4, 5);
            case "line3Start":
                return getStarLadder2Zone("line3Start", "3줄/좌", "3줄/우", 6, 7);
            case "line4Start":
                return getStarLadder2Zone("line4Start", "4줄/좌", "4줄/우", 8, 9);
            default:
                throw new GameZoneException("별다리2분 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    private Zone getStarLadder2Zone(String gameCode, String teamHome, String teamAway, int idxHome, int idxAway) {
        Zone zone = new Zone();
        zone.setId(this.getId());
        zone.setSdate(this.getSdate());
        zone.setMenuCode(MenuCode.STARLADDER2);
        zone.setGameCode(gameCode);
        zone.setLeague(String.format("%03d회차 별다리2분", this.round));
        zone.setTeamHome(String.format("%03d회차 별다리2분 [%s]", this.round, teamHome));
        zone.setTeamAway(String.format("%03d회차 별다리2분 [%s]", this.round, teamAway));
        zone.setHandicap(0);
        zone.setOddsHome(ZoneConfig.getBogleLadder().getOdds()[idxHome]);
        zone.setOddsDraw(0);
        zone.setOddsAway(ZoneConfig.getBogleLadder().getOdds()[idxAway]);
        zone.setGameDate(this.gameDate);
        return zone;
    }

    public boolean isChangeResult(StarLadder2Dto.Score score) {
        return !this.closing || this.cancel != score.isCancel()
                || !score.getStart().equals(this.start)
                || !score.getLine().equals(this.line)
                || !score.getOddeven().equals(this.oddeven);

    }
}
