package spoon.bot.balance.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QGodBalance is a Querydsl query type for GodBalance
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QGodBalance extends EntityPathBase<GodBalance> {

    private static final long serialVersionUID = -2101879883L;

    public static final QGodBalance godBalance = new QGodBalance("godBalance");

    public final StringPath betType = createString("betType");

    public final StringPath game = createString("game");

    public final StringPath gameDate = createString("gameDate");

    public final StringPath gameType = createString("gameType");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath message = createString("message");

    public final NumberPath<Long> price = createNumber("price", Long.class);

    public final NumberPath<Long> realPrice = createNumber("realPrice", Long.class);

    public final DateTimePath<java.util.Date> regDate = createDateTime("regDate", java.util.Date.class);

    public final StringPath round = createString("round");

    public QGodBalance(String variable) {
        super(GodBalance.class, forVariable(variable));
    }

    public QGodBalance(Path<? extends GodBalance> path) {
        super(path.getType(), path.getMetadata());
    }

    public QGodBalance(PathMetadata metadata) {
        super(GodBalance.class, metadata);
    }

}

