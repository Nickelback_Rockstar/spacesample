package spoon.bot.zone.dragonTiger;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.dragonTiger.DragonTiger;
import spoon.gameZone.dragonTiger.service.DragonTigerBotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class DragonTigerParsing implements GameBotParsing {

    private DragonTigerBotService dragonTigerBotService;

    private static boolean isClosing = false;

    private static Date sdate = new Date();

    @Async
    @Override
    public void parsingGame() {
        isClosing = false;

        int count = 0;
        int round = ZoneConfig.getDragonTiger().getZoneMaker().getRound();
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getDragonTiger().getZoneMaker().getGameDate());

        for (int i = 0; i < 10; i++) {
            if (cal.getTime().before(sdate)) continue;

            if (dragonTigerBotService.notExist(cal.getTime())) {
                DragonTiger dragonTiger = new DragonTiger(round > 1440 ? round % 1440 : round, cal.getTime());
                dragonTiger.setOdds(ZoneConfig.getDragonTiger().getOdds());
                dragonTigerBotService.addGame(dragonTiger);
                count++;
            }
            round++;
            cal.add(Calendar.MINUTE, 1);
        }
        sdate = cal.getTime();
        log.debug("드래곤타이거 경기등록 : {}건", count);
    }

    @Async
    @Override
    public void closingGame() {
        if (isClosing) return;
        isClosing = true;

        Date gameDate = new Date(ZoneConfig.getDragonTiger().getZoneMaker().getGameDate().getTime() - 1 * 60 * 1000);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getDragonTigerUrl());
        if (json == null) {
            isClosing = false;
            return;
        }

        DragonTiger result = JsonUtils.toModel(json, DragonTiger.class);
        if (result == null) {
            isClosing = false;
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            isClosing = false;
            return;
        }

        isClosing = dragonTigerBotService.closingGame(result);

        log.debug("드래곤타이거 경기 종료 : {}회차", result.getRound());
    }

    @Async
    @Override
    public void checkResult() {
        dragonTigerBotService.checkResult();
    }

    @Async
    @Override
    public void deleteGame() {
        dragonTigerBotService.deleteGame(3);
    }
}
