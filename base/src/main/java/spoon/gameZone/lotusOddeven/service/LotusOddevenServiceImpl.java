package spoon.gameZone.lotusOddeven.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.lotusOddeven.*;

import spoon.gameZone.oddeven.OddevenConfig;
import spoon.mapper.GameMapper;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.time.zone.ZoneRulesException;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class LotusOddevenServiceImpl implements LotusOddevenService {

    private ConfigService configService;

    private MemberService memberService;

    private LotusOddevenGameService lotusOddevenGameService;

    private LotusOddevenBotService lotusOddevenBotService;

    private LotusOddevenRepository lotusOddevenRepository;

    private BetItemRepository betItemRepository;

    private static QLotusOddeven q = QLotusOddeven.lotusOddeven;

    private GameMapper gameMapper;

    @Transactional
    @Override
    public boolean updateConfig(LotusOddevenConfig lotusOddevenConfig) {
        try {
            configService.updateZoneConfig("lotusOddeven", JsonUtils.toString(lotusOddevenConfig));
            ZoneConfig.setLotusOddeven(lotusOddevenConfig);
            // 이미 등록된 게임의 배당을 변경한다.
            lotusOddevenRepository.updateOdds(ZoneConfig.getLotusOddeven().getOdds());
        } catch (RuntimeException e) {
            log.error("홀짝 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<LotusOddeven> getComplete() {
        return lotusOddevenRepository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<LotusOddeven> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return lotusOddevenRepository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public LotusOddevenDto.Score findScore(Long id) {
        LotusOddeven lotusOddeven = lotusOddevenRepository.findOne(id);

        LotusOddevenDto.Score score = new LotusOddevenDto.Score();
        score.setId(lotusOddeven.getId());
        score.setRound(lotusOddeven.getRound());
        score.setGameDate(lotusOddeven.getGameDate());
        score.convertCard(lotusOddeven.getCard1(), lotusOddeven.getCard2());
        score.setCancel(lotusOddeven.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getLotusOddevenUrl() + "?sdate=" + lotusOddeven.getSdate());
        if (json == null) return score;

        lotusOddeven = JsonUtils.toModel(json, LotusOddeven.class);
        if (lotusOddeven == null) return score;

        // 봇에 결과가 있다면
        if (lotusOddeven.isClosing()) {
            score.convertCard(lotusOddeven.getCard1(), lotusOddeven.getCard2());
            if (!"O".equals(lotusOddeven.getOddeven()) && !"E".equals(lotusOddeven.getOddeven())) {
                score.setCancel(true);
            }
        }
        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(LotusOddevenDto.Score score) {
        LotusOddeven lotusOddeven = lotusOddevenRepository.findOne(score.getId());

        try {
            // 스코어 입력
            boolean success = score.convertCard();
            if (!success) {
                throw new ZoneRulesException("결과를 변환하지 못하였습니다.");
            }

            if (lotusOddeven.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                lotusOddevenGameService.rollbackPayment(lotusOddeven);
            }

            lotusOddeven.updateScore(score);

            lotusOddevenRepository.saveAndFlush(lotusOddeven);
            lotusOddevenGameService.closingBetting(lotusOddeven);
            lotusOddevenBotService.checkResult();
        } catch (RuntimeException e) {
            log.error("홀짝 {} - {}회차 수동처리를 하지 못하였습니다. - {}", lotusOddeven.getGameDate(), lotusOddeven.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<LotusOddeven> iterable = lotusOddevenRepository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(1))));
        for (LotusOddeven lotusOddeven : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.LODDEVEN).and(qi.groupId.eq(lotusOddeven.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getLotusOddevenUrl() + "?sdate=" + lotusOddeven.getSdate());
//            //System.out.println("홀짝 베팅없는경기 결과처리 = "+Config.getSysConfig().getZone().getOddevenUrl() + "?sdate=" + oddeven.getSdate());
//            //System.out.println("홀짝 베팅없는경기 결과처리 = "+json);

            if (json == null) continue;

            LotusOddeven result = JsonUtils.toModel(json, LotusOddeven.class);
            if (result == null) continue;

            if (result.isClosing()) {
                lotusOddeven.setCard1(result.getCard1());
                lotusOddeven.setCard2(result.getCard2());
                lotusOddeven.setOddeven(result.getOddeven());
                lotusOddeven.setClosing(true);
                lotusOddevenRepository.saveAndFlush(lotusOddeven);
                closing++;
            }
        }
        lotusOddevenBotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Transactional
    @Override
    public AjaxResult deleteAllGame() {
        gameMapper.deleteLotusOddeven();
        return new AjaxResult(true, "베팅, 결과없는 경기 삭제처리 했습니다.");
    }

    @Override
    public LotusOddevenDto.Config gameConfig() {
        LotusOddevenDto.Config gameConfig = new LotusOddevenDto.Config();
        LotusOddevenConfig config = ZoneConfig.getLotusOddeven();

        // 홀짝 바카라는 1분씩 댕겨 줘야 한다.
        Date gameDate = new Date(config.getZoneMaker().getGameDate().getTime() - 1000 * 60);
        LotusOddeven lotusOddeven = lotusOddevenRepository.findOne(q.gameDate.eq(gameDate));

        if (lotusOddeven == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isLotusOddeven());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(lotusOddeven.getGameDate());
        gameConfig.setSdate(lotusOddeven.getSdate());
        gameConfig.setRound(lotusOddeven.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(lotusOddeven.getOdds());

        // 60초 보정
        int betTime = (int) (lotusOddeven.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000 + 60000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setOverunder(config.isOverunder());
        gameConfig.setPattern(config.isPattern());
        gameConfig.setRedblack(config.isRedblack());

        return gameConfig;
    }
}
