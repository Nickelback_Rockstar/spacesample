package spoon.gameZone.binanceCoin1;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBinanceCoin1 is a Querydsl query type for BinanceCoin1
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBinanceCoin1 extends EntityPathBase<BinanceCoin1> {

    private static final long serialVersionUID = -530574245L;

    public static final QBinanceCoin1 binanceCoin1 = new QBinanceCoin1("binanceCoin1");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath oddEven = createString("oddEven");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath overUnder = createString("overUnder");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public QBinanceCoin1(String variable) {
        super(BinanceCoin1.class, forVariable(variable));
    }

    public QBinanceCoin1(Path<? extends BinanceCoin1> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBinanceCoin1(PathMetadata metadata) {
        super(BinanceCoin1.class, metadata);
    }

}

