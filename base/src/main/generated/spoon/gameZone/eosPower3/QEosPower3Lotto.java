package spoon.gameZone.eosPower3;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QEosPower3Lotto is a Querydsl query type for EosPower3Lotto
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEosPower3Lotto extends EntityPathBase<EosPower3Lotto> {

    private static final long serialVersionUID = 20779999L;

    public static final QEosPower3Lotto eosPower3Lotto = new QEosPower3Lotto("eosPower3Lotto");

    public final StringPath agency1 = createString("agency1");

    public final StringPath agency2 = createString("agency2");

    public final StringPath agency3 = createString("agency3");

    public final StringPath agency4 = createString("agency4");

    public final StringPath agency5 = createString("agency5");

    public final StringPath agency6 = createString("agency6");

    public final StringPath agency7 = createString("agency7");

    public final NumberPath<Integer> ball1 = createNumber("ball1", Integer.class);

    public final NumberPath<Integer> ball2 = createNumber("ball2", Integer.class);

    public final NumberPath<Integer> ball3 = createNumber("ball3", Integer.class);

    public final NumberPath<Integer> ball4 = createNumber("ball4", Integer.class);

    public final NumberPath<Integer> ball5 = createNumber("ball5", Integer.class);

    public final BooleanPath ballHit1 = createBoolean("ballHit1");

    public final BooleanPath ballHit2 = createBoolean("ballHit2");

    public final BooleanPath ballHit3 = createBoolean("ballHit3");

    public final BooleanPath ballHit4 = createBoolean("ballHit4");

    public final BooleanPath ballHit5 = createBoolean("ballHit5");

    public final DateTimePath<java.util.Date> betDate = createDateTime("betDate", java.util.Date.class);

    public final NumberPath<Long> betMoney = createNumber("betMoney", Long.class);

    public final NumberPath<Long> betTotMoney = createNumber("betTotMoney", Long.class);

    public final NumberPath<Integer> hitCount = createNumber("hitCount", Integer.class);

    public final NumberPath<Long> hitMoney = createNumber("hitMoney", Long.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nickname = createString("nickname");

    public final StringPath result = createString("result");

    public final EnumPath<spoon.member.domain.Role> role = createEnum("role", spoon.member.domain.Role.class);

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final DateTimePath<java.util.Date> startDate = createDateTime("startDate", java.util.Date.class);

    public final NumberPath<Integer> times = createNumber("times", Integer.class);

    public final StringPath userid = createString("userid");

    public QEosPower3Lotto(String variable) {
        super(EosPower3Lotto.class, forVariable(variable));
    }

    public QEosPower3Lotto(Path<? extends EosPower3Lotto> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEosPower3Lotto(PathMetadata metadata) {
        super(EosPower3Lotto.class, metadata);
    }

}

