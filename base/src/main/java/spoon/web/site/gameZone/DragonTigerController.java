package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.common.utils.JsonUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.dragonTiger.DragonTigerDto;
import spoon.gameZone.dragonTiger.service.DragonTigerGameService;
import spoon.gameZone.dragonTiger.service.DragonTigerService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class DragonTigerController {

    private DragonTigerService dragonTigerService;

    private DragonTigerGameService dragonTigerGameService;

    @RequestMapping(value = "zone/dragonTiger", method = RequestMethod.GET)
    public String zone(ModelMap map) {
        map.addAttribute("config", JsonUtils.toString(dragonTigerService.gameConfig()));
        return "site/zone/dragonTiger";
    }

    @RequestMapping(value = "zone/dragonTiger/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", dragonTigerService.getClosing(command, pageable));
        return "site/score/dragonTiger";
    }

    @ResponseBody
    @RequestMapping(value = "zone/dragonTiger/config", method = RequestMethod.POST)
    public DragonTigerDto.Config config() {
        return dragonTigerService.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/dragonTiger/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return dragonTigerGameService.betting(bet);
    }
}
