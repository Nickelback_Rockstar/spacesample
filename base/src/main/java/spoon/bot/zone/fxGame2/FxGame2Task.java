package spoon.bot.zone.fxGame2;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class FxGame2Task {

    private GameBotParsing fxGame2Parsing;

    @Scheduled(cron = "0 * * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        fxGame2Parsing.parsingGame();
    }

    @Scheduled(cron = "8 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        fxGame2Parsing.closingGame();
    }

    @Scheduled(cron = "10 * * * * *")
    public void checkResult() {
        if (notParsing()) return;
        fxGame2Parsing.checkResult();
    }

    @Scheduled(cron = "1 1 4 * * * ")
    public void deleteGame() {
        fxGame2Parsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isFxGame2() || !ZoneConfig.getFxGame2().isEnabled();
    }

}
