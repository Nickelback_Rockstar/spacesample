package spoon.casino;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.casino.service.CasinoParsing;
import spoon.config.domain.Config;

@Slf4j
@AllArgsConstructor
@Component
public class CasinoTask {

    private CasinoParsing casinoParsing;

    @Scheduled(cron = "30 0/30 * * * *")
    public void powerBalance1() {
        if (Config.getSysConfig().getCasino().isEnabled()) {
            try {
//                System.out.println("카지노 1 실행");
                casinoParsing.parsing(1);
                Thread.sleep(5000); //5초 대기
//                System.out.println("카지노 2 실행");
                casinoParsing.parsing(2);
                Thread.sleep(5000); //5초 대기
//                System.out.println("카지노 클로징 실행");
                casinoParsing.closing();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
