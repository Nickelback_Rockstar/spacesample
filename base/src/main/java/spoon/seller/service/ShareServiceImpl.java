package spoon.seller.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.ErrorUtils;
import spoon.mapper.ShareMapper;
import spoon.member.domain.MemberDto;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.seller.domain.Seller;
import spoon.seller.domain.SellerDto;
import spoon.support.web.AjaxResult;

import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class ShareServiceImpl implements ShareService {

    private ShareMapper shareMapper;

    private MemberService memberService;

    @Transactional(readOnly = true)
    @Override
    public List<Seller> sellerShare(SellerDto.Command command) {
        return shareMapper.sellerShare(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Seller> sellerShareNew(SellerDto.Command command) {
        return shareMapper.sellerShareNew(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Seller> sellerShareNew2(SellerDto.Command command) {
        return shareMapper.sellerShareNew2(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Seller> sellerList(SellerDto.Command command) {
        return shareMapper.sellerList(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Seller> sellerList2(SellerDto.Command command) {
        return shareMapper.sellerList2(command);
    }

    @Transactional
    @Override
    public AjaxResult updateShare(SellerDto.Update update) {

        //System.out.println("update = "+ update.toString());

        Member agency = memberService.getMember(update.getUserid());

        if(agency.getRateCode().equals(update.getRateCode())){

        }else {
            if ("수익분배".equals(update.getRateCode())) {
                update.setRateSports(0);
                update.setRateSports2(0);
                update.setRateZone(0);
                update.setRateZone1(0);
                update.setRateZone2(0);
            } else {
                update.setRateShare(0);
            }
        }

        try {
            if(agency.getAgencyDepth() == 1) { //상위총판
                ////System.out.println("agency.getAgencyDepth()="+agency.getAgencyDepth());
                // 정산 방식이 바뀌었으면 하위 대리점도 모두 바뀌어야 한다.
                if (!update.getRateCode().equals(agency.getRateCode())) {
                    updateShareAgencyNew(update);
                }

                agency.setRateCode(update.getRateCode());
                agency.setRateShare(update.getRateShare());
                agency.setRateCasino(update.getRateCasino());
                agency.setRateSports(update.getRateSports());
                agency.setRateSports2(update.getRateSports2());
                agency.setRateZone(update.getRateZone());
                agency.setRateZone1(update.getRateZone1());
                agency.setRateZone2(update.getRateZone2());

                //System.out.println("agency1 = "+ agency.toString());


            }else{ //하위총판
                String topDepthAgency = "";
                if(agency.getAgencyDepth() == 2){
                    topDepthAgency = agency.getAgency1();
                }else if(agency.getAgencyDepth() == 3) {
                    topDepthAgency = agency.getAgency2();
                }

                MemberDto.Seller command = new MemberDto.Seller();
                command.setAgency(topDepthAgency);
                command.setAgencyDepth(agency.getAgencyDepth());
                //System.out.println("================================");
                //System.out.println(command.toString());
                //System.out.println("================================");
                List<MemberDto.Agency> topAgencyList = memberService.getAgencyMemberList(command); //상위총판을 가져옴

                //System.out.println("================================");
                //System.out.println(topAgencyList.toString());
                //System.out.println("================================");

                for(MemberDto.Agency m : topAgencyList){
                    if(!m.getRateCode().equals(update.getRateCode())){
                        return new AjaxResult(false, update.getUserid() + " 상위총판과 정산방식이 같아야 합니다.");
                    }
                    if(m.getRateShare() > 0 && update.getRateShare() > m.getRateShare()){
                        return new AjaxResult(false, update.getUserid() + " 상위총판보다 지급율 이하로 수정되어야 합니다. [수익]");
                    }
                    if(m.getRateSports() > 0 && update.getRateSports() > m.getRateSports()){
                        return new AjaxResult(false, update.getUserid() + " 상위총판보다 지급율 이하로 수정되어야 합니다. [스포츠]");
                    }
                    if(m.getRateZone() > 0 && update.getRateZone() > m.getRateZone()){
                        return new AjaxResult(false, update.getUserid() + " 상위총판보다 지급율 이하로 수정되어야 합니다. [실시간]");
                    }
                    if(m.getRateZone1() > 0 && update.getRateZone1() > m.getRateZone1()){
                        return new AjaxResult(false, update.getUserid() + " 상위총판보다 지급율 이하로 수정되어야 합니다. [실시간1]");
                    }
                    if(m.getRateZone2() > 0 && update.getRateZone2() > m.getRateZone2()){
                        return new AjaxResult(false, update.getUserid() + " 상위총판보다 지급율 이하로 수정되어야 합니다. [실시간2]");
                    }
                }

                agency.setRateCode(update.getRateCode());
                agency.setRateShare(update.getRateShare());
                agency.setRateCasino(update.getRateCasino());
                agency.setRateSports(update.getRateSports());
                agency.setRateSports2(update.getRateSports2());
                agency.setRateZone(update.getRateZone());
                agency.setRateZone1(update.getRateZone1());
                agency.setRateZone2(update.getRateZone2());

                //System.out.println("agency2 = "+ agency.toString());

            }

            memberService.update(agency);
        } catch (RuntimeException e) {
            log.error("{} - 총판 수수료율 수정에 실패하였습니다.: {}", e.getMessage(), update.toString());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, update.getUserid() + " 총판의 지급율 수정에 실패 하였습니다.");
        }

        return new AjaxResult(true, update.getUserid() + " 총판의 지급율을 수정하였습니다.");
    }

    @Transactional
    @Override
    public AjaxResult updatePartnerShare(SellerDto.Update update) {
        Member agency = memberService.getMember(update.getUserid());

        if ("수익분배".equals(agency.getRateCode())) {
            update.setRateSports(0);
            update.setRateSports2(0);
            update.setRateZone(0);
        } else {
            update.setRateShare(0);
        }

        try {

            String topDepthAgency = "";
            if(agency.getAgencyDepth() == 2){
                topDepthAgency = agency.getAgency1();
            }else if(agency.getAgencyDepth() == 3){
                topDepthAgency = agency.getAgency2();
            }

            MemberDto.Seller command = new MemberDto.Seller();
            command.setAgency(topDepthAgency);
            command.setAgencyDepth(agency.getAgencyDepth());
            //System.out.println("================================");
            //System.out.println(command.toString());
            //System.out.println("================================");
            List<MemberDto.Agency> topAgencyList = memberService.getAgencyMemberList(command); //상위총판을 가져옴

            //System.out.println("================================");
            //System.out.println(topAgencyList.toString());
            //System.out.println("================================");

            for(MemberDto.Agency m : topAgencyList){
                if(m.getRateShare() > 0 && update.getRateShare() > m.getRateShare()){
                    return new AjaxResult(false, update.getUserid() + " 상위총판보다 지급율 이하로 수정되어야 합니다. [수익]");
                }
                if(m.getRateSports() > 0 && update.getRateSports() > m.getRateSports()){
                    return new AjaxResult(false, update.getUserid() + " 상위총판보다 지급율 이하로 수정되어야 합니다. [스포츠]");
                }
                if(m.getRateZone() > 0 && update.getRateZone() > m.getRateZone()){
                    return new AjaxResult(false, update.getUserid() + " 상위총판보다 지급율 이하로 수정되어야 합니다. [실시간]");
                }
                if(m.getRateZone1() > 0 && update.getRateZone1() > m.getRateZone1()){
                    return new AjaxResult(false, update.getUserid() + " 상위총판보다 지급율 이하로 수정되어야 합니다. [실시간1]");
                }
                if(m.getRateZone2() > 0 && update.getRateZone2() > m.getRateZone2()){
                    return new AjaxResult(false, update.getUserid() + " 상위총판보다 지급율 이하로 수정되어야 합니다. [실시간2]");
                }
            }

            agency.setRateShare(update.getRateShare());
            agency.setRateSports(update.getRateSports());
            agency.setRateSports2(update.getRateSports2());
            agency.setRateZone(update.getRateZone());
            agency.setRateZone1(update.getRateZone1());
            agency.setRateZone2(update.getRateZone2());

            memberService.update(agency);
        } catch (RuntimeException e) {
            log.error("{} - 총판 수수료율 수정에 실패하였습니다.: {}", e.getMessage(), update.toString());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, update.getUserid() + " 총판의 지급율 수정에 실패 하였습니다.");
        }

        return new AjaxResult(true, update.getUserid() + " 총판의 지급율을 수정하였습니다.");
    }

    private void updateShareAgency1(SellerDto.Update update) {
        shareMapper.updateRateCode(update);
    }

    private void updateShareAgencyNew(SellerDto.Update update) {
        shareMapper.updateRateCodeNew(update);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Seller> sellerShareNewPbLotto(SellerDto.Command command) {
        return shareMapper.sellerShareNewPbLotto(command);
    }

    @Override
    public AjaxResult updateSharePbLotto(SellerDto.Update update) {
        Member agency = memberService.getMember(update.getUserid());

        try {
            agency.setRatePowerLotto(update.getRatePowerLotto());
            memberService.update(agency);
        } catch (RuntimeException e) {
            log.error("{} - 총판 수수료율 수정에 실패하였습니다.: {}", e.getMessage(), update.toString());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, update.getUserid() + " 총판의 지급율 수정에 실패 하였습니다.");
        }

        return new AjaxResult(true, update.getUserid() + " 총판의 지급율을 수정하였습니다.");
    }

}
