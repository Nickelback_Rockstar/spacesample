package spoon.gameZone.dragonTiger;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QDragonTiger is a Querydsl query type for DragonTiger
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QDragonTiger extends EntityPathBase<DragonTiger> {

    private static final long serialVersionUID = -1814348329L;

    public static final QDragonTiger dragonTiger = new QDragonTiger("dragonTiger");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final StringPath dragon = createString("dragon");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath result = createString("result");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public final NumberPath<Integer> sum = createNumber("sum", Integer.class);

    public final StringPath tiger = createString("tiger");

    public QDragonTiger(String variable) {
        super(DragonTiger.class, forVariable(variable));
    }

    public QDragonTiger(Path<? extends DragonTiger> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDragonTiger(PathMetadata metadata) {
        super(DragonTiger.class, metadata);
    }

}

