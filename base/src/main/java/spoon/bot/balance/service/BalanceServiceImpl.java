package spoon.bot.balance.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.bot.balance.entity.GodBalance;
import spoon.bot.balance.entity.PolygonBalance;
import spoon.bot.balance.entity.PolygonBalance1;
import spoon.bot.balance.entity.PolygonBalance2;
import spoon.bot.balance.repository.GodBalanceRepository;
import spoon.bot.balance.repository.PolygonBalanceRepository;
import spoon.bot.balance.repository.PolygonBalanceRepository1;
import spoon.bot.balance.repository.PolygonBalanceRepository2;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.dari.DariConfig;
import spoon.gameZone.dari.service.DariService;
import spoon.gameZone.ladder.LadderConfig;
import spoon.gameZone.ladder.service.LadderService;
import spoon.gameZone.power.PowerConfig;
import spoon.gameZone.power.service.PowerService;

@Slf4j
@AllArgsConstructor
@Service
public class BalanceServiceImpl implements BalanceService {

    private GodBalanceRepository godBalanceRepository;
    private PolygonBalanceRepository polygonBalanceRepository;
    private PolygonBalanceRepository1 polygonBalanceRepository1;
    private PolygonBalanceRepository2 polygonBalanceRepository2;

    private LadderService ladderService;

    private DariService dariService;

    private PowerService powerService;

    @Transactional(readOnly = true)
    @Override
    public Page<PolygonBalance> pagePolygon(Pageable pageable) {
        return polygonBalanceRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<PolygonBalance2> pagePolygon2(Pageable pageable) {
        return polygonBalanceRepository2.findAll(pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<PolygonBalance1> pagePolygon1(Pageable pageable) {
        return polygonBalanceRepository1.findAll(pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<GodBalance> pageGodBalance(Pageable pageable) {
        return godBalanceRepository.findAll(pageable);
    }

    @Transactional
    @Override
    public void lockLadderBalance() {
        LadderConfig ladderConfig = ZoneConfig.getLadder();
        ladderConfig.setEnabled(false);
        ladderService.updateConfig(ladderConfig);
    }

    @Transactional
    @Override
    public void lockDariBalance() {
        DariConfig dariConfig = ZoneConfig.getDari();
        dariConfig.setEnabled(false);
        dariService.updateConfig(dariConfig);
    }

    @Transactional
    @Override
    public void lockPowerBalance() {
        PowerConfig powerConfig = ZoneConfig.getPower();
        powerConfig.setEnabled(false);
        powerService.updateConfig(powerConfig);
    }
}
