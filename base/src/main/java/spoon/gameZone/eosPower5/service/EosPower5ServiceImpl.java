package spoon.gameZone.eosPower5.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.eosPower5.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class EosPower5ServiceImpl implements EosPower5Service {

    private ConfigService configService;

    private MemberService memberService;

    private EosPower5GameService eosPower5GameService;

    private EosPower5BotService eosPower5BotService;

    private EosPower5Repository eosPower5Repository;

    private BetItemRepository betItemRepository;

    private static QEosPower5 q = QEosPower5.eosPower5;

    @Transactional
    @Override
    public boolean updateConfig(EosPower5Config powerConfig) {
        try {
            configService.updateZoneConfig("eosPower5", JsonUtils.toString(powerConfig));
            ZoneConfig.setEosPower5(powerConfig);
            // 이미 등록된 게임의 배당을 변경한다.
            eosPower5Repository.updateOdds(ZoneConfig.getEosPower5().getOdds());
        } catch (RuntimeException e) {
            log.error("EOS파워볼 5분 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<EosPower5> getComplete() {
        return eosPower5Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<EosPower5> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return eosPower5Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public EosPower5Dto.Score findScore(Long id) {
        EosPower5 power = eosPower5Repository.findOne(id);

        EosPower5Dto.Score score = new EosPower5Dto.Score();
        score.setId(power.getId());
        score.setRound(power.getRound());
        score.setTimes(power.getTimes());
        score.setGameDate(power.getGameDate());
        score.setPb(power.getPb());
        score.setBall(power.getBall());
        score.setCancel(power.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getEosPower5Url() + "?sdate=" + power.getSdate());
        if (json == null) return score;

        power = JsonUtils.toModel(json, EosPower5.class);
        if (power == null) return score;

        // 봇에 결과가 있다면
        if (power.isClosing()) {
            score.setPb(power.getPb());
            score.setBall(power.getBall());

            if (StringUtils.empty(power.getPb())) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(EosPower5Dto.Score score) {
        EosPower5 power = eosPower5Repository.findOne(score.getId());

        try {
            if (power.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                eosPower5GameService.rollbackPayment(power);
            }

            // 스코어 입력
            power.updateScore(score);
            eosPower5Repository.saveAndFlush(power);
            eosPower5GameService.closingBetting(power);
            eosPower5BotService.checkResult();
        } catch (RuntimeException e) {
            log.error("EOS파워볼 5분 {} - {}회차 수동처리를 하지 못하였습니다. - {}", power.getGameDate(), power.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<EosPower5> iterable = eosPower5Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(5))));
        for (EosPower5 power : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.EOSPOWER5).and(qi.groupId.eq(power.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getPowerUrl() + "?sdate=" + power.getSdate());
            if (json == null) continue;

            EosPower5 result = JsonUtils.toModel(json, EosPower5.class);
            if (result == null) continue;

            if (result.isClosing()) {
                power.setOddeven(result.getOddeven());
                power.setPb_oddeven(result.getPb_oddeven());
                power.setOverunder(result.getOverunder());
                power.setPb_overunder(result.getPb_overunder());
                power.setSize(result.getSize());
                power.setSum(result.getSum());
                power.setPb(result.getPb());
                power.setBall(result.getBall());

                power.setClosing(true);
                eosPower5Repository.saveAndFlush(power);
                closing++;
            }
        }
        eosPower5BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public EosPower5Dto.Config gameConfig() {
        EosPower5Dto.Config gameConfig = new EosPower5Dto.Config();
        EosPower5Config config = ZoneConfig.getEosPower5();

        Date gameDate = config.getZoneMaker().getGameDate();
        EosPower5 power = eosPower5Repository.findOne(q.gameDate.eq(gameDate));

        if (power == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isEosPower5());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(power.getGameDate());
        gameConfig.setSdate(power.getSdate());
        gameConfig.setRound(power.getRound());
        gameConfig.setTimes(power.getTimes());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(power.getOdds());

        int betTime = (int) (power.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setOdd_overunder(config.isOdd_overunder());
        gameConfig.setEven_overunder(config.isEven_overunder());

        gameConfig.setPb_oddeven(config.isPb_oddeven());
        gameConfig.setPb_odd_overunder(config.isPb_odd_overunder());
        gameConfig.setPb_even_overunder(config.isPb_even_overunder());

        gameConfig.setOverunder(config.isOverunder());
        gameConfig.setPb_overunder(config.isPb_overunder());

        gameConfig.setSize(config.isSize());
        gameConfig.setOdd_size(config.isOdd_size());
        gameConfig.setEven_size(config.isEven_size());

        return gameConfig;
    }
}
