package spoon.bot.balance.service.gate;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bet.domain.BetDto;
import spoon.bet.service.BetService;
import spoon.bot.balance.entity.GodBalance;
import spoon.bot.balance.repository.GodBalanceRepository;
import spoon.bot.support.PowerMaker;
import spoon.common.net.HttpParsing;
import spoon.common.utils.DateUtils;
import spoon.common.utils.StringUtils;
import spoon.config.domain.Config;
import spoon.event.entity.Logger;
import spoon.event.service.LoggerService;
import spoon.game.domain.MenuCode;
import spoon.game.service.GameService;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.power.Power;
import spoon.gameZone.power.PowerRepository;
import spoon.gameZone.power.QPower;

import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class GodPowerService {

    private BetService betService;

    private GodBalanceRepository godBalanceRepository;

    private LoggerService loggerService;

    private PowerRepository powerRepository;

    private GameService gameService;

    @Async
    public void balance() {

        double rate = Config.getSysConfig().getZone().getGodPowerBalanceRate();
        double rateHp = Config.getSysConfig().getZone().getGodPowerBalanceHpRate();
        double rate1 = Config.getSysConfig().getZone().getGodPowerBalance1Rate();
        double rate2 = Config.getSysConfig().getZone().getGodPowerBalance2Rate();

        if ( rate == 0 && rateHp == 0 && rate1 == 0 && rate2 == 0 ) return;

        PowerMaker powerMaker = ZoneConfig.getPower().getPowerMaker();
        long times = powerMaker.getTimes() + 1;

        Date gameDate = powerMaker.getGameDate(times);
        String sdate = DateUtils.format(gameDate, "yyyyMMddHHmm");

        long calc;
        long pOdd = 0, pEven = 0, odd = 0, even = 0, over = 0, under = 0, pOver = 0, pUnder = 0;
        long totalPoe = 0, totalOe = 0, totalOu = 0, totalPOu = 0;
        long totalTmpPoe = 0, totalTmpOe = 0, totalTmpOu = 0, totalTmpPOu = 0;
        int posPoe = 0, posOe = 0, posOu = 0, posPOu = 0;
        boolean canPoe = false, canOe = false, canOu = false, canPOu = false;
        String round = String.format("%03d", times);

        List<BetDto.BetBalGame> items = betService.getGodBalanceBet(MenuCode.POWER, sdate);

        long money = 500000;
        long donwMoney = 250000;

        for (BetDto.BetBalGame item : items) {
            if(item.getBetMoney() >= money){
                item.setBetMoney(donwMoney);
            }
            switch (item.getSpecial()) {
                case "pb_oddeven":
                    if ("home".equals(item.getBetTeam())) {
                        pOdd += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        pEven += item.getBetMoney();
                    }
                    break;
                case "oddeven":
                    if ("home".equals(item.getBetTeam())) {
                        odd += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        even += item.getBetMoney();
                    }
                    break;
                case "pb_overunder":
                    if ("home".equals(item.getBetTeam())) {
                        pOver += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        pUnder += item.getBetMoney();
                    }
                    break;
                case "overunder":
                    if ("home".equals(item.getBetTeam())) {
                        over += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        under += item.getBetMoney();
                    }
                    break;
            }
        }

        List<BetDto.BetBalGame> itemsRe = betService.getGodBalanceReBet(MenuCode.POWER, sdate);

        for (BetDto.BetBalGame item : itemsRe) {
            if(item.getBetMoney() >= money){
                item.setBetMoney(donwMoney);
            }
            switch (item.getSpecial()) {
                case "pb_oddeven":
                    if ("home".equals(item.getBetTeam())) {
                        pEven += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        pOdd += item.getBetMoney();
                    }
                    break;
                case "oddeven":
                    if ("home".equals(item.getBetTeam())) {
                        even += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        odd += item.getBetMoney();
                    }
                    break;
                case "pb_overunder":
                    if ("home".equals(item.getBetTeam())) {
                        pUnder += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        pOver += item.getBetMoney();
                    }
                    break;
                case "overunder":
                    if ("home".equals(item.getBetTeam())) {
                        under += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        over += item.getBetMoney();
                    }
                    break;
            }
        }

//        //System.out.println("sdate=" + sdate);
//
//        //System.out.println("파워볼 pOdd=" + pOdd);
//        //System.out.println("파워볼 pEven=" + pEven);
//
//        //System.out.println("파워볼 pOver=" + pOver);
//        //System.out.println("파워볼 pUnder=" + pUnder);
//
//        //System.out.println("일반볼 odd=" + odd);
//        //System.out.println("일반볼 even=" + even);
//
//        //System.out.println("일반볼 over=" + over);
//        //System.out.println("일반볼 under=" + under);



        if(Config.getSysConfig().getZone().isGodPowerBalance()) {

            //System.out.println("스타 파워 시작");

            // 파워볼 홀짝
            calc = (long) ((pOdd - pEven) * rate / 100);
            if (calc >= 10) {
                totalPoe = calc;
                posPoe = 1;
                canPoe = true;
            }
            calc = (long) ((pEven - pOdd) * rate / 100);
            if (calc >= 10) {
                totalPoe = calc;
                posPoe = 2;
                canPoe = true;
            }

            // 일반볼 홀짝
            calc = (long) ((odd - even) * rate / 100);
            if (calc >= 10) {
                totalOe = calc;
                posOe = 1;
                canOe = true;
            }
            calc = (long) ((even - odd) * rate / 100);
            if (calc >= 10) {
                totalOe = calc;
                posOe = 2;
                canOe = true;
            }

            // 일반 오버언더
            calc = (long) ((over - under) * rate / 100);
            if (calc >= 10) {
                totalOu = calc;
                posOu = 1;
                canOu = true;
            }
            calc = (long) ((under - over) * rate / 100);
            if (calc >= 10) {
                totalOu = calc;
                posOu = 2;
                canOu = true;
            }

            // 파워볼 오버언더
            calc = (long) ((pOver - pUnder) * rate / 100);
            if (calc >= 10) {
                totalPOu = calc;
                posPOu = 1;
                canPOu = true;
            }
            calc = (long) ((pUnder - pOver) * rate / 100);
            if (calc >= 10) {
                totalPOu = calc;
                posPOu = 2;
                canPOu = true;
            }

            if (canPoe) {
                String url = Config.getSysConfig().getZone().getGodPowerBalanceUrl()
                        + "?UserId=" + Config.getSysConfig().getZone().getGodPowerBalanceId()
                        + "&AuthCode=" + Config.getSysConfig().getZone().getGodPowerBalanceKey()
                        + "&SiteName=STAR"
                        + "&GameType=C"
                        + "&BetMoney=" + totalPoe
                        + "&PickNum=" + posPoe
                        + "&GameTypeSub=A";
                sendQuery(url, sdate, round, "파워볼 홀짝", posPoe == 1 ? "홀" : "짝", totalPoe, totalTmpPoe);
            }

            if (canPOu) {
                String url = Config.getSysConfig().getZone().getGodPowerBalanceUrl()
                        + "?UserId=" + Config.getSysConfig().getZone().getGodPowerBalanceId()
                        + "&AuthCode=" + Config.getSysConfig().getZone().getGodPowerBalanceKey()
                        + "&SiteName=STAR"
                        + "&GameType=C"
                        + "&BetMoney=" + totalPOu
                        + "&PickNum=" + posPOu
                        + "&GameTypeSub=B";
                sendQuery(url, sdate, round, "파워볼 오버언더", posPOu == 1 ? "오버" : "언더", totalPOu, totalTmpPOu);
            }

            if (canOe) {
                String url = Config.getSysConfig().getZone().getGodPowerBalanceUrl()
                        + "?UserId=" + Config.getSysConfig().getZone().getGodPowerBalanceId()
                        + "&AuthCode=" + Config.getSysConfig().getZone().getGodPowerBalanceKey()
                        + "&SiteName=STAR"
                        + "&GameType=C"
                        + "&BetMoney=" + totalOe
                        + "&PickNum=" + posOe
                        + "&GameTypeSub=C";
                sendQuery(url, sdate, round, "일반볼 홀짝", posOe == 1 ? "홀" : "짝", totalOe, totalTmpOe);
            }

            if (canOu) {
                String url = Config.getSysConfig().getZone().getGodPowerBalanceUrl()
                        + "?UserId=" + Config.getSysConfig().getZone().getGodPowerBalanceId()
                        + "&AuthCode=" + Config.getSysConfig().getZone().getGodPowerBalanceKey()
                        + "&SiteName=STAR"
                        + "&GameType=C"
                        + "&BetMoney=" + totalOu
                        + "&PickNum=" + posOu
                        + "&GameTypeSub=D";
                sendQuery(url, sdate, round, "일반볼 오버언더", posOu == 1 ? "오버" : "언더", totalOu, totalTmpOu);
            }
        }

        QPower q = QPower.power;
        Power power = powerRepository.findOne(q.sdate.eq(sdate));

        if(Config.getSysConfig().getZone().isGodPowerBalanceHp()) {
            sendBalance(power, sdate, round, rateHp, pOdd, pEven, odd, even, over, under, pOver, pUnder, Config.getSysConfig().getZone().getGodPowerBalanceHpUrl(), Config.getSysConfig().getZone().getGodPowerBalanceHpId(), Config.getSysConfig().getZone().getGodPowerBalanceHpKey());
        }
        if(Config.getSysConfig().getZone().isGodPowerBalance1()) {
            sendBalance(power, sdate, round, rate1, pOdd, pEven, odd, even, over, under, pOver, pUnder, Config.getSysConfig().getZone().getGodPowerBalance1Url(), Config.getSysConfig().getZone().getGodPowerBalance1Id(), Config.getSysConfig().getZone().getGodPowerBalance1Key());
        }
        if(Config.getSysConfig().getZone().isGodPowerBalance2()) {
            sendBalance(power, sdate, round, rate2, pOdd, pEven, odd, even, over, under, pOver, pUnder, Config.getSysConfig().getZone().getGodPowerBalance2Url(), Config.getSysConfig().getZone().getGodPowerBalance2Id(), Config.getSysConfig().getZone().getGodPowerBalance2Key());
        }

    }

    public void sendBalance(Power power, String sdate, String round, double rate, long pOdd, long pEven, long odd, long even, long over, long under, long pOver, long pUnder, String url, String id, String key){
        long calc;
        long totalPoe = 0, totalOe = 0, totalOu = 0, totalPOu = 0;
        int posPoe = 0, posOe = 0, posOu = 0, posPOu = 0;
        boolean canPoe = false, canOe = false, canOu = false, canPOu = false;

        //System.out.println("벨런스 호출 요율 = " + rate);

        calc = (long) ((pOdd - pEven) * rate / 100);
        if (calc >= 10) {
            totalPoe = calc;
            posPoe = 1;
            canPoe = true;
        }
        calc = (long) ((pEven - pOdd) * rate / 100);
        if (calc >= 10) {
            totalPoe = calc;
            posPoe = 2;
            canPoe = true;
        }

        // 일반볼 홀짝
        calc = (long) ((odd - even) * rate / 100);
        if (calc >= 10) {
            totalOe = calc;
            posOe = 1;
            canOe = true;
        }
        calc = (long) ((even - odd) * rate / 100);
        if (calc >= 10) {
            totalOe = calc;
            posOe = 2;
            canOe = true;
        }

        // 일반 오버언더
        calc = (long) ((over - under) * rate / 100);
        if (calc >= 10) {
            totalOu = calc;
            posOu = 1;
            canOu = true;
        }
        calc = (long) ((under - over) * rate / 100);
        if (calc >= 10) {
            totalOu = calc;
            posOu = 2;
            canOu = true;
        }

        // 파워볼 오버언더
        calc = (long) ((pOver - pUnder) * rate / 100);
        if (calc >= 10) {
            totalPOu = calc;
            posPOu = 1;
            canPOu = true;
        }
        calc = (long) ((pUnder - pOver) * rate / 100);
        if (calc >= 10) {
            totalPOu = calc;
            posPOu = 2;
            canPOu = true;
        }

        long pp1 = 0;//파워홀
        long pp2 = 0;//파워짝
        long pp3 = 0;//파워언더
        long pp4 = 0;//파워오버

        long pp5 = 0;//일반홀
        long pp6 = 0;//일반짝
        long pp7 = 0;//일반언더
        long pp8 = 0;//일반오버

        if (canPoe) {
            if (posPoe == 1) {
                pp1 = totalPoe;
            } else if (posPoe == 2) {
                pp2 = totalPoe;
            }
        }

        if (canPOu) {
            if (posPOu == 1) {
                pp4 = totalPOu; //오버
            } else if (posPOu == 2) {
                pp3 = totalPOu; //언더
            }
        }

        if (canOe) {
            if (posOe == 1) {
                pp5 = totalOe;
            } else if (posOe == 2) {
                pp6 = totalOe;
            }
        }

        if (canOu) {
            if (posOu == 1) {
                pp8 = totalOu;
            } else if (posOu == 2) {
                pp7 = totalOu;
            }
        }

        url += "?gm=PWB";
        url += "&userid=" + id;
        url += "&key=" + key;
        url += "&tdate=" + power.getSdate().substring(0, 8);
        url += "&rno=" + power.getTimes();
        if (pp1 > 0) url += "&pp1=" + pp1;
        if (pp2 > 0) url += "&pp2=" + pp2;
        if (pp3 > 0) url += "&pp3=" + pp3;
        if (pp4 > 0) url += "&pp4=" + pp4;
        if (pp5 > 0) url += "&pp5=" + pp5;
        if (pp6 > 0) url += "&pp6=" + pp6;
        if (pp7 > 0) url += "&pp7=" + pp7;
        if (pp8 > 0) url += "&pp8=" + pp8;
        url += "&nonce=77777";

        String bpm = "";
        long p = pp1 + pp2 + pp3 + pp4 + pp5 + pp6 + pp7 + pp8;
        if (p > 0) {
            sendQuery(url, sdate, round, pp1, pp2, pp3, pp4, pp5, pp6, pp7, pp8, bpm, id);
        }
    }

    private void sendQuery(String param, String betDate, String round, String gameType, String betType, long price, long viewPrice) {

        Logger logger = new Logger();
        logger.setCode("GOD_POWER_BALANCE");
        logger.setData(param);
        logger.setRegDate(new Date());
        loggerService.addLog(logger);

        //System.out.println("param1 = "+param);

        String json = HttpParsing.getJson(param);
        if (json == null) return;

        JSONParser parser = new JSONParser();
        JSONObject jsonObj = new JSONObject();
        if (!StringUtils.empty(json)) {
            try {
                jsonObj = (JSONObject) parser.parse(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        GodBalance b = new GodBalance();
        b.setGame("갓파워볼");
        b.setGameDate(betDate);
        b.setRound(round);
        b.setGameType(gameType);
        b.setBetType(betType);
        b.setPrice(viewPrice);
        b.setRealPrice(price);
        b.setRegDate(new Date());
        b.setMessage(jsonObj.get("resultcode").toString());
        //b.setMessage("메세지");
        godBalanceRepository.saveAndFlush(b);

    }

    private void sendQuery(String param, String betDate, String round, long pp1, long pp2, long pp3, long pp4, long pp5, long pp6, long pp7, long pp8, String bpm, String id) {

        Logger logger = new Logger();
        logger.setCode("GOD_P_B_"+id);
        logger.setData(param);
        logger.setRegDate(new Date());
        loggerService.addLog(logger);

        //System.out.println("paramH1=" + param);

        String json = HttpParsing.getJson(param);

        if (json == null) return;

        String resultcode = "";
        JSONParser parser = new JSONParser();
        JSONObject jsonObj = new JSONObject();
        if (!StringUtils.empty(json)) {
            try {
                jsonObj = (JSONObject) parser.parse(json);
                resultcode = jsonObj.get("comment").toString()+" / "+jsonObj.get("more_info").toString();
            } catch (Exception e) {
                resultcode = "리턴값 에러남요";
            }
        }

        resultcode += bpm;

        saveBalance("파워볼 홀짝", betDate, round, "홀", pp1, resultcode, id);
        saveBalance("파워볼 홀짝", betDate, round, "짝", pp2, resultcode, id);
        saveBalance("파워볼 오버언더", betDate, round, "언더", pp3, resultcode, id);
        saveBalance("파워볼 오버언더", betDate, round, "오버", pp4, resultcode, id);
        saveBalance("일반볼 홀짝", betDate, round, "홀", pp5, resultcode, id);
        saveBalance("일반볼 홀짝", betDate, round, "짝", pp6, resultcode, id);
        saveBalance("일반볼 오버언더", betDate, round, "언더", pp7, resultcode, id);
        saveBalance("일반볼 오버언더", betDate, round, "오버", pp8, resultcode, id);

    }

    private void saveBalance(String gameType, String betDate, String round, String betType, long price, String resultcode, String id) {

        if (price > 0) {
            GodBalance b = new GodBalance();
            b.setGame("갓파워" + id);
            b.setGameDate(betDate);
            b.setRound(round);
            b.setGameType(gameType);
            b.setBetType(betType);
            b.setPrice(price);
            b.setRealPrice(price);
            b.setRegDate(new Date());
            b.setMessage(resultcode);
            godBalanceRepository.saveAndFlush(b);
        }
    }

    public void setting() {
        gameService.godBalanceSetting();
    }

}
