package spoon.gameZone.roulette;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import spoon.bot.support.ZoneHelper;
import spoon.common.utils.DateUtils;
import spoon.game.domain.MenuCode;
import spoon.gameZone.GameZoneException;
import spoon.gameZone.Zone;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneScore;
import spoon.support.convert.DoubleArrayConvert;
import spoon.support.convert.LongArrayConvert;

import javax.persistence.*;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "ZONE_ROULETTE", indexes = {
        @Index(name = "IDX_sdate", columnList = "sdate")
})
public class Roulette {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date gameDate;

    private int round;

    @Column(length = 8)
    private String color;

    @Column(length = 16)
    private String result;

    private String sdate;

    @Convert(converter = LongArrayConvert.class)
    private long[] amount = new long[19];

    @Convert(converter = DoubleArrayConvert.class)
    private double[] odds = new double[19];

    private boolean closing;

    private boolean cancel;

    //-----------------------------------------------------

    public Roulette(int round, Date gameDate) {
        this.round = round;
        this.gameDate = gameDate;
        this.sdate = DateUtils.format(this.gameDate, "yyyyMMddHHmmss");
    }

    public boolean isBeforeGameDate() {
        // 60초 보정
        return this.gameDate.getTime() - System.currentTimeMillis() + 60000 > 0;
    }

    // 스코어 입력
    public void updateScore(RouletteDto.Score score) {
        if (score.isCancel()) {
            this.color = "";
            this.result = "";
            this.cancel = true;
        } else {
            this.color = score.getColor();
            this.result = score.getResult();

            this.cancel = false;
        }
        this.closing = true;
    }

    // 경기 결과
    public ZoneScore getGameResult(String betNumber) {
        return ZoneHelper.zoneResultRoulette(betNumber, this.result);
    }

    public Zone getZone(String gameCode) {
        switch (gameCode) {
            case "straight":
                return getRouletteZone(gameCode, "스트레이트", "X", 0);
            case "split":
                return getRouletteZone(gameCode, "스플릿", "X", 1);
            case "street":
                return getRouletteZone(gameCode, "스트리트", "X", 2);
            case "triple":
                return getRouletteZone(gameCode, "트리플", "X", 3);
            case "corner":
                return getRouletteZone(gameCode, "코너", "X", 4);
            case "square":
                return getRouletteZone(gameCode, "스퀘어", "X", 5);
            case "line":
                return getRouletteZone(gameCode, "라인", "X", 6);
            case "column1":
                return getRouletteZone(gameCode, "컬럼1", "X", 7);
            case "column2":
                return getRouletteZone(gameCode, "컬럼2", "X", 17);
            case "column3":
                return getRouletteZone(gameCode, "컬럼3", "X", 18);
            case "dozen1":
                return getRouletteZone(gameCode, "다즌[소]", "X", 8);
            case "red":
                return getRouletteZone(gameCode, "레드", "X", 9);
            case "black":
                return getRouletteZone(gameCode, "블랙", "X", 10);
            case "odd":
                return getRouletteZone(gameCode, "홀", "X", 11);
            case "even":
                return getRouletteZone(gameCode, "짝", "X", 12);
            case "low":
                return getRouletteZone(gameCode, "로우", "X", 13);
            case "high":
                return getRouletteZone(gameCode, "하이", "X", 14);
            case "dozen2":
                return getRouletteZone(gameCode, "다즌[중]", "X", 15);
            case "dozen3":
                return getRouletteZone(gameCode, "다즌[대]", "X", 16);
            default:
                throw new GameZoneException("룰렛 코드 " + gameCode + " 를s 확인 할 수 없습니다.");
        }
    }

    private Zone getRouletteZone(String gameCode, String teamHome, String teamAway, int idxHome) {
        Zone zone = new Zone();
        zone.setId(this.getId());
        zone.setSdate(this.getSdate());
        zone.setMenuCode(MenuCode.ROULETTE);
        zone.setGameCode(gameCode);
        zone.setLeague(String.format("%04d회차 룰렛", this.round));
        zone.setTeamHome(String.format("%04d회차 룰렛 [%s]", this.round, teamHome));
        zone.setTeamAway("");
        zone.setHandicap(0);
        zone.setOddsHome(ZoneConfig.getRoulette().getOdds()[idxHome]);
        zone.setOddsDraw(0);
        zone.setOddsAway(0);
        zone.setGameDate(this.gameDate);
        return zone;
    }

    public boolean isChangeResult(RouletteDto.Score score) {
        return !this.closing || this.cancel != score.isCancel();
    }

}
