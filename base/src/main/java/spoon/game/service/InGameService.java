package spoon.game.service;


import spoon.game.entity.InGame;

public interface InGameService {

    InGame getGame(String gameCode);

    void addGame(InGame inGame);

    boolean isExist(String gameCode);
}
