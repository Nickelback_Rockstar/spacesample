package spoon.gameZone.roulette;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QRoulette is a Querydsl query type for Roulette
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRoulette extends EntityPathBase<Roulette> {

    private static final long serialVersionUID = -1606309221L;

    public static final QRoulette roulette = new QRoulette("roulette");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final StringPath color = createString("color");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath result = createString("result");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public QRoulette(String variable) {
        super(Roulette.class, forVariable(variable));
    }

    public QRoulette(Path<? extends Roulette> path) {
        super(path.getType(), path.getMetadata());
    }

    public QRoulette(PathMetadata metadata) {
        super(Roulette.class, metadata);
    }

}

