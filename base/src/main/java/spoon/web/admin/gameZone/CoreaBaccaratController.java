package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.coreaBaccarat.CoreaBaccaratConfig;
import spoon.gameZone.coreaBaccarat.CoreaBaccaratDto;
import spoon.gameZone.coreaBaccarat.service.CoreaBaccaratService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.coreaBaccaratController")
@RequestMapping(value = "#{config.pathAdmin}")
public class CoreaBaccaratController {

    private CoreaBaccaratService coreaBaccaratService;

    /**
     * 바카라 설정
     */
    @RequestMapping(value = "zone/coreaBaccarat/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getCoreaBaccarat());
        return "admin/zone/coreaBaccarat/config";
    }

    /**
     * 바카라 설정 변경
     */
    @RequestMapping(value = "zone/coreaBaccarat/config", method = RequestMethod.POST)
    public String config(CoreaBaccaratConfig coreaBaccaratConfig, RedirectAttributes ra) {
        boolean success = coreaBaccaratService.updateConfig(coreaBaccaratConfig);
        if (success) {
            ra.addFlashAttribute("message", "코리아 바카라 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "코리아 바카라 설정변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/coreaBaccarat/config";
    }

    /**
     * 바카라 진행
     */
    @RequestMapping(value = "zone/coreaBaccarat/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", coreaBaccaratService.getComplete());
        map.addAttribute("config", ZoneConfig.getCoreaBaccarat());
        return "admin/zone/coreaBaccarat/complete";
    }

    /**
     * 바카라 완료
     */
    @RequestMapping(value = "zone/coreaBaccarat/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "gameDate") Pageable pageable) {
        map.addAttribute("page", coreaBaccaratService.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getCoreaBaccarat());
        return "admin/zone/coreaBaccarat/closing";
    }

    /**
     * 바카라 스코어 입력 폼
     */
    @RequestMapping(value = "zone/coreaBaccarat/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", coreaBaccaratService.findScore(id));
        return "admin/zone/coreaBaccarat/score";
    }

    /**
     * 바카라 결과처리
     */
    @RequestMapping(value = "zone/coreaBaccarat/score", method = RequestMethod.POST)
    public String score(CoreaBaccaratDto.Score score, RedirectAttributes ra) {
        boolean success = coreaBaccaratService.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "코리아 바카라 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "코리아 바카라 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/coreaBaccarat/score?id=" + score.getId();
    }

    /**
     * 바카라 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/coreaBaccarat/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return coreaBaccaratService.closingAllGame();
    }

}
