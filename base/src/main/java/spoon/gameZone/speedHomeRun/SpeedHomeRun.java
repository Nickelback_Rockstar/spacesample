package spoon.gameZone.speedHomeRun;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import spoon.bot.support.ZoneHelper;
import spoon.common.utils.DateUtils;
import spoon.config.domain.Config;
import spoon.game.domain.MenuCode;
import spoon.gameZone.GameZoneException;
import spoon.gameZone.Zone;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneScore;
import spoon.support.convert.DoubleArrayConvert;
import spoon.support.convert.LongArrayConvert;

import javax.persistence.*;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "ZONE_SPEEDHOMERUN", indexes = {
        @Index(name = "IDX_sdate", columnList = "sdate")
})
public class SpeedHomeRun {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date gameDate;

    private int round;

    // 플레이어  1 or 2
    @Column(length = 16)
    private String player;

    // 마지막 일반볼 숫자
    @Column(length = 16)
    private String lastBall;

    // 홈런 or 아웃
    @Column(length = 16)
    private String homeRun;

    @Column(length = 10)
    private String direction; //일반볼 마지막 방향

    @Column(length = 16)
    private String sdate;

    @Convert(converter = LongArrayConvert.class)
    private long[] amount = new long[8];

    @Convert(converter = DoubleArrayConvert.class)
    private double[] odds = new double[8];

    private boolean closing;

    private boolean cancel;

    //--------------------------------------------------------

    public SpeedHomeRun(int round, Date gameDate) {
        this.round = (Config.getSysConfig().getZone().getPowerDay() + round) % 288;
        if (this.round == 0) this.round = 288;
        this.gameDate = gameDate;
        this.sdate = DateUtils.format(gameDate, "yyyyMMddHHmmss");
    }

    public boolean isBeforeGameDate() {
        return this.gameDate.getTime() - System.currentTimeMillis() > 0;
    }

    // 스코어 입력
    public void updateScore(SpeedHomeRunDto.Score score) {
        if (score.isCancel()) {
            this.player = "";
            this.homeRun = "";
            this.cancel = true;
        } else {
            int lastBall = Integer.parseInt(score.getLastBall());
            this.lastBall = String.valueOf(lastBall);
            if (1 <= lastBall && lastBall <= 35) {
                this.player = "1";
            } else if (36 <= lastBall && lastBall <= 70) {
                this.player = "2";
            }

            this.direction = lastBall % 2 == 1 ? "LEFT" : "RIGHT";

            if ("1".equals(this.player) && "LEFT".equals(this.direction)) {//1번선수가 좌측 이면 홈런
                this.homeRun = "Y";
            } else if ("2".equals(this.player) && "RIGHT".equals(this.direction)) {//2번선수가 우측 이면 홈런
                this.homeRun = "Y";
            } else {
                this.homeRun = "N";
            }

            this.cancel = false;
        }
        this.closing = true;
    }

    // 경기 결과
    public ZoneScore getGameResult(String gameCode) {
        switch (gameCode) {
            case "player":
                return ZoneHelper.zoneResult(player, cancel);
            case "lastBall":
                return ZoneHelper.zoneResult(lastBall, cancel);
            case "homeRun":
                return ZoneHelper.zoneResult(homeRun, cancel);
            case "direction":
                return ZoneHelper.zoneResult(direction, cancel);
            default:
                throw new GameZoneException("스피드홈런 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    public Zone getZone(String gameCode) {
        switch (gameCode) {
            case "player":
                return getSpeedHomeRunZone("player", "1번", "2번", 0, 1);
            /*case "lastBall":
                return getSpeedHomeRunZone("lastBall", "홀", "짝", 2, 3);*/
            case "homeRun":
                return getSpeedHomeRunZone("homeRun", "홈런", "아웃", 4, 5);
            case "direction":
                return getSpeedHomeRunZone("direction", "좌측", "우측", 6, 7);
            default:
                throw new GameZoneException("스피드홈런 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    private Zone getSpeedHomeRunZone(String gameCode, String teamHome, String teamAway, int idxHome, int idxAway) {
        Zone zone = new Zone();
        zone.setId(this.getId());
        zone.setSdate(this.getSdate());
        zone.setMenuCode(MenuCode.SPEEDHOMERUN);
        zone.setGameCode(gameCode);
        zone.setLeague(String.format("%03d회차 스피드홈런", this.round));
        zone.setTeamHome(String.format("%03d회차 스피드홈런 [%s]", this.round, teamHome));
        zone.setTeamAway(String.format("%03d회차 스피드홈런 [%s]", this.round, teamAway));
        zone.setHandicap(0);
        zone.setOddsHome(ZoneConfig.getSpeedHomeRun().getOdds()[idxHome]);
        zone.setOddsDraw(0);
        zone.setOddsAway(ZoneConfig.getSpeedHomeRun().getOdds()[idxAway]);
        zone.setGameDate(this.gameDate);
        return zone;
    }

    public boolean isChangeResult(SpeedHomeRunDto.Score score) {
        return !this.closing || this.cancel != score.isCancel()
                || !(score.getPlayer().equals(this.player) && score.getLastBall().equals(this.lastBall) && score.getHomeRun().equals(this.homeRun));

    }
}
