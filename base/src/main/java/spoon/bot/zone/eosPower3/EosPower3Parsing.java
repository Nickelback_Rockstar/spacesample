package spoon.bot.zone.eosPower3;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.eosPower3.EosPower3;
import spoon.gameZone.eosPower3.EosPower3Dto;
import spoon.gameZone.eosPower3.service.EosPower3BotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class EosPower3Parsing implements GameBotParsing {

    private EosPower3BotService eosPower3BotService;

    private static boolean isClosing = false;

    private static Date sdate = new Date();

    @Async
    @Override
    public void parsingGame() {
        isClosing = false;

        int count = 0;
        int round = ZoneConfig.getEosPower3().getZoneMaker().getRound();
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getEosPower3().getZoneMaker().getGameDate());

        for (int i = 0; i < 6; i++) {
            if (cal.getTime().before(sdate)) continue;

            if (eosPower3BotService.notExist(cal.getTime())) {
                EosPower3 eosPower3 = new EosPower3(round > 480 ? round % 480 : round, cal.getTime());
                eosPower3.setOdds(ZoneConfig.getEosPower3().getOdds());
                eosPower3BotService.addGame(eosPower3);
                count++;
            }
            round++;
            cal.add(Calendar.MINUTE, 3);
        }
        log.debug("EOS파워볼 3분 경기등록 : {}건", count);
    }

    @Async
    @Override
    public void closingGame() {
        if (isClosing) return;
        isClosing = true;

        Date gameDate = new Date(ZoneConfig.getEosPower3().getZoneMaker().getGameDate().getTime() - 3 * 60 * 1000);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getEosPower3Url());
        if (json == null) {
            isClosing = false;
            return;
        }

        //={"gameDate":1541586180000,
        // "round":836176,
        // "times":233,
        // "ball":"27,13,17,22,11",
        // "pb":"06",
        // "oddeven":"EVEN",
        // "pb_oddeven":"EVEN",
        // "overunder":"OVER",
        // "pb_overunder":"OVER",
        // "size":"대",
        // "sum":90,
        // "sdate":"201811071923",
        // "closing":true}

        try {
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(json);
            EosPower3 result = new EosPower3();
            long gDate = Long.parseLong(obj.get("gameDate").toString());
            result.setGameDate(new Date(gDate));
            result.setRound(Integer.parseInt(obj.get("round").toString()));
            result.setTimes(Integer.parseInt(obj.get("times").toString()));
            result.setBall(obj.get("ball").toString());
            result.setPb(obj.get("pb").toString());
            result.setSum(Integer.parseInt(obj.get("sum").toString()));
            result.setSdate(obj.get("sdate").toString());
            result.setClosing(Boolean.parseBoolean(obj.get("closing").toString()));

//            result.setOddeven(obj.get("oddeven").toString());
//            result.setPb_oddeven(obj.get("pb_oddeven").toString());
//            result.setOverunder(obj.get("overunder").toString());
//            result.setPb_overunder(obj.get("pb_overunder").toString());
//            result.setSize(obj.get("size").toString());

            //결과 처리 score
            EosPower3Dto.Score score = new EosPower3Dto.Score();
            score.setBall(result.getBall());
            score.setPb(result.getPb());
            result.updateScore(score);

            if (result == null) {
                isClosing = false;
                return;
            }

            if (!gameDate.equals(result.getGameDate())) {
                isClosing = false;
                return;
            }

            //System.out.println("EOS파워볼 클로징 게임 result =" + result);

            isClosing = eosPower3BotService.closingGame(result);

            log.debug("EOS파워볼 3분 경기 종료 : {}회차", result.getRound());

        } catch (ParseException e) {
            e.printStackTrace();
            isClosing = false;
            return;
        }

    }

    @Async
    @Override
    public void checkResult() {
        eosPower3BotService.checkResult();
    }

    @Async
    @Override
    public void deleteGame() {
        eosPower3BotService.deleteGame(3);
    }
}
