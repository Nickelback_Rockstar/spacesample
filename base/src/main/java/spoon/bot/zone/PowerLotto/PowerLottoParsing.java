package spoon.bot.zone.PowerLotto;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.config.domain.Config;
import spoon.gameZone.PowerLotto.service.PowerLottoBotService;
import spoon.gameZone.power.PowerLotto;
import spoon.member.domain.Role;
import spoon.member.domain.User;
import spoon.member.service.MemberService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class PowerLottoParsing implements GameBotParsing {

    private PowerLottoBotService powerLottoBotService;

    private MemberService memberService;

    @Async
    @Override
    public void parsingGame() {

        ////System.out.println("여기 들어와?");

        String param = "?code=" + Config.getSysConfig().getZone().getPowerLottoCode()+"&sdate="+new Date();
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getPowerLottoUrl() + "/ap/betList" + param);
        ////System.out.println(Config.getSysConfig().getZone().getPowerLottoUrl() + "/ap/betList" + param);
        ////System.out.println(json);
        JSONParser parser = new JSONParser();
        JSONArray jsonArr = new JSONArray();

        if(json != null) {
            try {
                JSONObject jsonObj = (JSONObject) parser.parse(json);
                if(jsonObj.containsKey("resultCode") && "00".equals(jsonObj.get("resultCode"))){
                    JSONArray arr = (JSONArray) parser.parse(jsonObj.get("data").toString());
                    jsonArr = arr;
                    List<PowerLotto> list = new ArrayList<>();
                    for(int i=0;i<jsonArr.size();i++){
                        PowerLotto pl = new PowerLotto();
                        JSONObject obj = (JSONObject) jsonArr.get(i);
                        String userid = obj.get("orgId").toString();
                        User user = memberService.getUser(userid);
                        pl.setId(Long.parseLong(obj.get("id").toString()));
                        if(user != null) {
                            pl.setNickname(user.getNickname());
                            pl.setAgency1(user.getAgency1());
                            pl.setAgency2(user.getAgency2());
                            pl.setAgency3(user.getAgency3());
                            pl.setAgency4(user.getAgency4());
                            pl.setAgency5(user.getAgency5());
                            pl.setAgency6(user.getAgency6());
                            pl.setAgency7(user.getAgency7());
                        }
                        pl.setUserid(userid);
                        pl.setBall1(Integer.parseInt(obj.get("ball1").toString()));
                        pl.setBall2(Integer.parseInt(obj.get("ball2").toString()));
                        pl.setBall3(Integer.parseInt(obj.get("ball3").toString()));
                        pl.setBall4(Integer.parseInt(obj.get("ball4").toString()));
                        pl.setBall5(Integer.parseInt(obj.get("ball5").toString()));
                        pl.setBallHit1((boolean)(obj.get("ballHit1")));
                        pl.setBallHit2((boolean)(obj.get("ballHit2")));
                        pl.setBallHit3((boolean)(obj.get("ballHit3")));
                        pl.setBallHit4((boolean)(obj.get("ballHit4")));
                        pl.setBallHit5((boolean)(obj.get("ballHit5")));
                        pl.setRole(Role.USER);
                        pl.setHitCount(Integer.parseInt(obj.get("hitCount").toString()));
                        pl.setBetMoney(Long.parseLong(obj.get("betMoney").toString()));
                        pl.setHitMoney(Long.parseLong(obj.get("hitMoney").toString()));
                        pl.setBetTotMoney(Long.parseLong(obj.get("betTotMoney").toString()));
                        pl.setRound(Integer.parseInt(obj.get("round").toString()));
                        pl.setTimes(Integer.parseInt(obj.get("times").toString()));
                        pl.setResult(obj.get("result").toString());
                        pl.setBetDate(new Date(Long.parseLong(obj.get("betDate").toString())));
                        pl.setStartDate(new Date(Long.parseLong(obj.get("startDate").toString())));
                        list.add(pl);
                    }
                    powerLottoBotService.savePowerLotto(list);

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void closingGame() {

    }

    @Override
    public void checkResult() {

    }

    @Override
    public void deleteGame() {

    }

}
