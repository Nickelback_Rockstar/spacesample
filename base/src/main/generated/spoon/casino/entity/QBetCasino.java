package spoon.casino.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QBetCasino is a Querydsl query type for BetCasino
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QBetCasino extends EntityPathBase<BetCasino> {

    private static final long serialVersionUID = 1111075959L;

    public static final QBetCasino betCasino = new QBetCasino("betCasino");

    public final NumberPath<Long> amount = createNumber("amount", Long.class);

    public final StringPath category = createString("category");

    public final BooleanPath closing = createBoolean("closing");

    public final DateTimePath<java.util.Date> closingDate = createDateTime("closingDate", java.util.Date.class);

    public final DateTimePath<java.util.Date> endDate = createDateTime("endDate", java.util.Date.class);

    public final StringPath gameID = createString("gameID");

    public final StringPath history = createString("history");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final DateTimePath<java.util.Date> regDate = createDateTime("regDate", java.util.Date.class);

    public final StringPath roundID = createString("roundID");

    public final DateTimePath<java.util.Date> startDate = createDateTime("startDate", java.util.Date.class);

    public final NumberPath<Integer> thirdParty = createNumber("thirdParty", Integer.class);

    public final StringPath transID = createString("transID");

    public final StringPath transID2 = createString("transID2");

    public final DateTimePath<java.util.Date> transTime = createDateTime("transTime", java.util.Date.class);

    public final StringPath transType = createString("transType");

    public final StringPath userID = createString("userID");

    public QBetCasino(String variable) {
        super(BetCasino.class, forVariable(variable));
    }

    public QBetCasino(Path<? extends BetCasino> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBetCasino(PathMetadata metadata) {
        super(BetCasino.class, metadata);
    }

}

