package spoon.gameZone.starLadder2.service;

import spoon.gameZone.starLadder2.StarLadder2;

import java.util.Date;

public interface StarLadder2BotService {

    boolean notExist(Date gameDate);

    void addGame(StarLadder2 starLadder2);

    boolean closingGame(StarLadder2 result);

    void checkResult();

    void deleteGame(int days);

}
