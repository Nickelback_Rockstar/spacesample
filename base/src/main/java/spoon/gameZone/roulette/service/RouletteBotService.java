package spoon.gameZone.roulette.service;

import spoon.gameZone.roulette.Roulette;

import java.util.Date;

public interface RouletteBotService {

    boolean notExist(Date gameDate);

    void addGame(Roulette roulette);

    boolean closingGame(Roulette result);

    void checkResult();

    void deleteGame(int days);

}
