package spoon.gameZone.kenoSpeed.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.kenoSpeed.*;
import spoon.gameZone.speedHomeRun.SpeedHomeRun;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class KenoSpeedServiceImpl implements KenoSpeedService {

    private ConfigService configService;

    private MemberService memberService;

    private KenoSpeedGameService kenoSpeedGameService;

    private KenoSpeedBotService kenoSpeedBotService;

    private KenoSpeedRepository kenoSpeedRepository;

    private BetItemRepository betItemRepository;

    private static QKenoSpeed q = QKenoSpeed.kenoSpeed;

    @Transactional
    @Override
    public boolean updateConfig(KenoSpeedConfig kenoSpeedConfig) {
        try {

            kenoSpeedConfig.setNum1(kenoSpeedConfig.isNum0());
            kenoSpeedConfig.setNum2(kenoSpeedConfig.isNum0());
            kenoSpeedConfig.setNum3(kenoSpeedConfig.isNum0());
            kenoSpeedConfig.setNum4(kenoSpeedConfig.isNum0());
            kenoSpeedConfig.setNum5(kenoSpeedConfig.isNum0());
            kenoSpeedConfig.setNum6(kenoSpeedConfig.isNum0());
            kenoSpeedConfig.setNum7(kenoSpeedConfig.isNum0());
            kenoSpeedConfig.setNum8(kenoSpeedConfig.isNum0());
            kenoSpeedConfig.setNum9(kenoSpeedConfig.isNum0());
            //숫자 0~9 배당 동일하게 세팅해줌 odds[4] 0번배당
            double odds[] = kenoSpeedConfig.getOdds();
            odds[5] = odds[4];
            odds[6] = odds[4];
            odds[7] = odds[4];
            odds[8] = odds[4];
            odds[9] = odds[4];
            odds[10] = odds[4];
            odds[11] = odds[4];
            odds[12] = odds[4];
            odds[13] = odds[4];
            kenoSpeedConfig.setOdds(odds);

            configService.updateZoneConfig("keno_speed", JsonUtils.toString(kenoSpeedConfig));
            ZoneConfig.setKenoSpeed(kenoSpeedConfig);

            // 이미 등록된 게임의 배당을 변경한다.
            kenoSpeedRepository.updateOdds(ZoneConfig.getKenoSpeed().getOdds());
        } catch (RuntimeException e) {
            log.error("키노 스피드 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<KenoSpeed> getComplete() {
        return kenoSpeedRepository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<KenoSpeed> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return kenoSpeedRepository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public KenoSpeedDto.Score findScore(Long id) {
        KenoSpeed kenoSpeed = kenoSpeedRepository.findOne(id);

        KenoSpeedDto.Score score = new KenoSpeedDto.Score();
        score.setId(kenoSpeed.getId());
        score.setRound(kenoSpeed.getRound());
        score.setGameDate(kenoSpeed.getGameDate());
        score.setOverUnder(kenoSpeed.getOverUnder());
        score.setNum0(kenoSpeed.getNum0());
        score.setNum1(kenoSpeed.getNum1());
        score.setNum2(kenoSpeed.getNum2());
        score.setNum3(kenoSpeed.getNum3());
        score.setNum4(kenoSpeed.getNum4());
        score.setNum5(kenoSpeed.getNum5());
        score.setNum6(kenoSpeed.getNum6());
        score.setNum7(kenoSpeed.getNum7());
        score.setNum8(kenoSpeed.getNum8());
        score.setNum9(kenoSpeed.getNum9());
        score.setOddeven(kenoSpeed.getOddeven());
        score.setCancel(kenoSpeed.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getKenoSpeedUrl() + "?sdate=" + kenoSpeed.getSdate());
        if (json == null) return score;

//        //System.out.println(json);

        JSONParser parser = new JSONParser();
        JSONObject jsonObj = null;

        // 봇에 결과가 있다면
        if (kenoSpeed.isClosing()) {
            score.setOddeven(kenoSpeed.getOddeven());
            score.setOverUnder(kenoSpeed.getOverUnder());
            score.setNum0(kenoSpeed.getNum0());
            score.setNum1(kenoSpeed.getNum1());
            score.setNum2(kenoSpeed.getNum2());
            score.setNum3(kenoSpeed.getNum3());
            score.setNum4(kenoSpeed.getNum4());
            score.setNum5(kenoSpeed.getNum5());
            score.setNum6(kenoSpeed.getNum6());
            score.setNum7(kenoSpeed.getNum7());
            score.setNum8(kenoSpeed.getNum8());
            score.setNum9(kenoSpeed.getNum9());

            if (!"ODD".equals(score.getOddeven()) && !"EVEN".equals(score.getOddeven())) {
                score.setCancel(true);
            }

            return score;
        }

        /*try {
            jsonObj = (JSONObject) parser.parse(json);
            kenoSpeed.setOverUnder(jsonObj.get("sumOverUnder").toString());
            kenoSpeed.setOddeven(jsonObj.get("sumOddEven").toString());

            String lastNum = jsonObj.get("sum").toString();
            lastNum = lastNum.substring(lastNum.length()-1,lastNum.length());

            if("0".equals(lastNum)) kenoSpeed.setNum0(lastNum);
            else if("1".equals(lastNum)) kenoSpeed.setNum1(lastNum);
            else if("2".equals(lastNum)) kenoSpeed.setNum2(lastNum);
            else if("3".equals(lastNum)) kenoSpeed.setNum3(lastNum);
            else if("4".equals(lastNum)) kenoSpeed.setNum4(lastNum);
            else if("5".equals(lastNum)) kenoSpeed.setNum5(lastNum);
            else if("6".equals(lastNum)) kenoSpeed.setNum6(lastNum);
            else if("7".equals(lastNum)) kenoSpeed.setNum7(lastNum);
            else if("8".equals(lastNum)) kenoSpeed.setNum8(lastNum);
            else if("9".equals(lastNum)) kenoSpeed.setNum9(lastNum);

            kenoSpeed.setClosing(true);

        } catch (ParseException e) {
            e.printStackTrace();
        }*/

//        if (kenoSpeed == null) return score;



        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(KenoSpeedDto.Score score) {
        KenoSpeed speed = kenoSpeedRepository.findOne(score.getId());

        try {
            if (speed.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                kenoSpeedGameService.rollbackPayment(speed);
            }

            // 스코어 입력
            speed.updateScore(score);
            kenoSpeedRepository.saveAndFlush(speed);
            kenoSpeedGameService.closingBetting(speed);
            kenoSpeedBotService.checkResult();
        } catch (RuntimeException e) {
            log.error("키노 스피드 {} - {}회차 수동처리를 하지 못하였습니다. - {}", speed.getGameDate(), speed.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<KenoSpeed> iterable = kenoSpeedRepository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(5))));
        for (KenoSpeed speed : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.KENOSPEED).and(qi.groupId.eq(speed.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getKenoSpeedUrl() + "?sdate=" + speed.getSdate());
            if (json == null) continue;

//            //System.out.println(json);

            KenoSpeed result = new KenoSpeed();
            JSONParser parser = new JSONParser();
            JSONObject jsonObj = null;
            try {
                jsonObj = (JSONObject) parser.parse(json);
                result.setOverUnder(jsonObj.get("sumOverUnder").toString());
                result.setOddeven(jsonObj.get("sumOddEven").toString());

                String lastNum = jsonObj.get("sum").toString();
                lastNum = lastNum.substring(lastNum.length()-1,lastNum.length());

                if("0".equals(lastNum)) result.setNum0(lastNum);
                else if("1".equals(lastNum)) result.setNum1(lastNum);
                else if("2".equals(lastNum)) result.setNum2(lastNum);
                else if("3".equals(lastNum)) result.setNum3(lastNum);
                else if("4".equals(lastNum)) result.setNum4(lastNum);
                else if("5".equals(lastNum)) result.setNum5(lastNum);
                else if("6".equals(lastNum)) result.setNum6(lastNum);
                else if("7".equals(lastNum)) result.setNum7(lastNum);
                else if("8".equals(lastNum)) result.setNum8(lastNum);
                else if("9".equals(lastNum)) result.setNum9(lastNum);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (result == null) continue;

            if (result.isClosing()) {
                speed.setOverUnder(result.getOverUnder());
                speed.setNum0(result.getNum0());
                speed.setNum0(result.getNum0());
                speed.setNum1(result.getNum1());
                speed.setNum2(result.getNum2());
                speed.setNum3(result.getNum3());
                speed.setNum4(result.getNum4());
                speed.setNum5(result.getNum5());
                speed.setNum6(result.getNum6());
                speed.setNum7(result.getNum7());
                speed.setNum8(result.getNum8());
                speed.setNum9(result.getNum9());
                speed.setOddeven(result.getOddeven());
                speed.setClosing(true);
                kenoSpeedRepository.saveAndFlush(speed);
                closing++;
            }
        }
        kenoSpeedBotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public KenoSpeedDto.Config gameConfig() {
        KenoSpeedDto.Config gameConfig = new KenoSpeedDto.Config();
        KenoSpeedConfig config = ZoneConfig.getKenoSpeed();

        // 동행 현재 회차에서 1을 더해준다.
        int times = ZoneConfig.getKenoSpeed().getPowerMaker().getTimes() + 1;
        Date gameDate = ZoneConfig.getKenoSpeed().getPowerMaker().getGameDate(times);
        KenoSpeed speed = kenoSpeedRepository.findOne(q.gameDate.eq(gameDate));

        if (speed == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getPowerMaker().getRound(times));
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isKenoSpeed());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(speed.getGameDate());
        gameConfig.setSdate(speed.getSdate());
        gameConfig.setRound(speed.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(speed.getOdds());

        int betTime = (int) (speed.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setOverUnder(config.isOverUnder());
        gameConfig.setNum0(config.isNum0());
        gameConfig.setNum1(config.isNum0());
        gameConfig.setNum2(config.isNum0());
        gameConfig.setNum3(config.isNum0());
        gameConfig.setNum4(config.isNum0());
        gameConfig.setNum5(config.isNum0());
        gameConfig.setNum6(config.isNum0());
        gameConfig.setNum7(config.isNum0());
        gameConfig.setNum8(config.isNum0());
        gameConfig.setNum9(config.isNum0());

        return gameConfig;
    }
}
