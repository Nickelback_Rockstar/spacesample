package spoon.casino.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(
        name = "BET_CASINO",
        indexes = {
                @Index(name = "IDX_01", columnList = "transType, transTime"),
                @Index(name = "IDX_02", columnList = "transTime")
        },
        uniqueConstraints = @UniqueConstraint(columnNames = {"transID", "gameID"})
)
public class BetCasino {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String transID;

    private String gameID;

    private int thirdParty;

    private long amount;

    private Date transTime;

    private String transType;

    @Column(columnDefinition = "NVARCHAR(max)")
    private String history;

    private String transID2;

    private String category;

    private String userID;

    private String roundID;

    private Date regDate;

    private Date startDate;
    private Date endDate;

    private boolean closing;
    private Date closingDate;

}
