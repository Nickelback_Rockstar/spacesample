package spoon.gameZone.eosPower2;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QEosPower2 is a Querydsl query type for EosPower2
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEosPower2 extends EntityPathBase<EosPower2> {

    private static final long serialVersionUID = -284922133L;

    public static final QEosPower2 eosPower2 = new QEosPower2("eosPower2");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final StringPath ball = createString("ball");

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final StringPath even_overunder = createString("even_overunder");

    public final StringPath even_size = createString("even_size");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath odd_overunder = createString("odd_overunder");

    public final StringPath odd_size = createString("odd_size");

    public final StringPath oddeven = createString("oddeven");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath overunder = createString("overunder");

    public final StringPath pb = createString("pb");

    public final StringPath pb_even_overunder = createString("pb_even_overunder");

    public final StringPath pb_odd_overunder = createString("pb_odd_overunder");

    public final StringPath pb_oddeven = createString("pb_oddeven");

    public final StringPath pb_overunder = createString("pb_overunder");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public final StringPath size = createString("size");

    public final NumberPath<Integer> sum = createNumber("sum", Integer.class);

    public final NumberPath<Integer> times = createNumber("times", Integer.class);

    public QEosPower2(String variable) {
        super(EosPower2.class, forVariable(variable));
    }

    public QEosPower2(Path<? extends EosPower2> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEosPower2(PathMetadata metadata) {
        super(EosPower2.class, metadata);
    }

}

