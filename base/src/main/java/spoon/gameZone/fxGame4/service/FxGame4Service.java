package spoon.gameZone.fxGame4.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame4.FxGame4;
import spoon.gameZone.fxGame4.FxGame4Config;
import spoon.gameZone.fxGame4.FxGame4Dto;
import spoon.support.web.AjaxResult;

public interface FxGame4Service {

    /**
     * FxGame4 설정을 변경한다.
     */
    boolean updateConfig(FxGame4Config fxGame4Config);

    /**
     * FxGame4 등록된 게임을 가져온다.
     */
    Iterable<FxGame4> getComplete();

    /**
     * FxGame4 종료된 게임을 가져온다.
     */
    Page<FxGame4> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * FxGame4 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    FxGame4Dto.Score findScore(Long id);

    /**
     * FxGame4 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(FxGame4Dto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    FxGame4Dto.Config gameConfig(int r);

}
