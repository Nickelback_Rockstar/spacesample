package spoon.member.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.*;
import spoon.member.domain.MemberDto;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.entity.QMember;
import spoon.member.repository.MemberRepository;
import spoon.payment.domain.MoneyCode;
import spoon.payment.domain.PointCode;
import spoon.payment.service.PaymentService;
import spoon.seller.domain.JoinCodeDto;
import spoon.seller.service.JoinCodeService;
import spoon.support.web.AjaxResult;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@AllArgsConstructor
@Service
public class MemberAddServiceImpl implements MemberAddService {

    private MemberRepository memberRepository;

    private PaymentService paymentService;

    private BCryptPasswordEncoder passwordEncoder;

    private JoinCodeService joinCodeService;

    private static QMember q = QMember.member;

    @Transactional
    @Override
    public AjaxResult adminAdd(MemberDto.Add add) {

        if (WebUtils.role().getValue() < Role.ADMIN.getValue()) {
            return new AjaxResult(false, "권한이 부족합니다.");
        }

        try {
            Member member = new Member();
            BeanUtils.copyProperties(add, member);

            member.setEnabled(true);
            member.setJoinDate(new Date());
            member.setJoinIp(WebUtils.ip());
            member.setPassword(passwordEncoder.encode(add.getPassword()));

            int agencyDepth = 0;

            // 대총판일 경우
            if (add.getRole() == Role.AGENCY) {
                boolean bDepth = false;

                if (!"".equals(add.getAgency1())) {
                    member.setAgency1(add.getAgency1());
                    bDepth = true;
                    agencyDepth++;
                } else {
                    member.setAgency1(add.getUserid());
                    bDepth = false;
                    agencyDepth++;
                }

                if (!"".equals(add.getAgency2())) {
                    member.setAgency2(add.getAgency2());
                    bDepth = true;
                    agencyDepth++;
                } else if (bDepth) {
                    member.setAgency2(add.getUserid());
                    bDepth = false;
                    agencyDepth++;
                }

                member.setRateCode("수익분배");
            }

            //총판뎁스
            member.setAgencyDepth(agencyDepth);

            if (StringUtils.notEmpty(add.getPhone())) {
                member.setPhone(add.getPhone().replaceAll("[^0-9]", ""));
                member.setPassKey(member.getPhone());
            }
            if (StringUtils.notEmpty(add.getAccount())) {
                member.setAccount(add.getAccount().replaceAll("[^0-9]", ""));
            }
            memberRepository.saveAndFlush(member);

            // 관리자 머니 추가
            if (add.getMoney() > 0) {
                paymentService.addMoney(MoneyCode.ADD, 0L, add.getUserid(), add.getMoney(), "관리자 회원 생성 머니 지급");
            }

            // 관리자 포인트 추가
            if (add.getPoint() > 0) {
                paymentService.addPoint(PointCode.ADD, 0L, add.getUserid(), add.getPoint(), "관리자 회원 생성 포인트 지급");
            }
        } catch (RuntimeException e) {
            log.error("{} - 관리자 회원생성에 실패하였습니다.: {}", e.getMessage(), add.toString());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, add.getUserid() + "님 등록에 실패 하였습니다.");
        }

        return new AjaxResult(true, add.getUserid() + "님을 등록하였습니다.");
    }

    @Transactional
    @Override
    public AjaxResult sellerAdd(MemberDto.Agency add) {

        String userid = WebUtils.userid();

        try {

            Member aMember = memberRepository.findByUserid(userid);

            Member member = new Member();
            BeanUtils.copyProperties(add, member);

            member.setEnabled(true);
            member.setJoinDate(new Date());
            member.setJoinIp(WebUtils.ip());
            member.setPassword(passwordEncoder.encode(add.getPassword()));
            member.setRole(Role.AGENCY);
            member.setAgency1(aMember.getAgency1());
            member.setAgency2(aMember.getAgency2());

            if (aMember.getAgencyDepth() == 1) {
                member.setAgency2(add.getUserid());
            }

            member.setAgencyDepth(aMember.getAgencyDepth() + 1);

            member.setRateCode(memberRepository.getRateCode(userid));


            if ("둘다분배".equals(member.getRateCode())) {
                member.setRateShare(add.getRateShare());
                member.setRateSports(add.getRateSports());
                member.setRateSports2(add.getRateSports2());
                member.setRateZone(add.getRateZone());
                member.setRateZone1(add.getRateZone1());
                member.setRateZone2(add.getRateZone2());
            } else if ("롤링분배".equals(member.getRateCode())) {
                if (add.getRateSports() > aMember.getRateSports() || add.getRateSports2() > aMember.getRateSports2() || add.getRateZone() > aMember.getRateZone()) {
                    return new AjaxResult(false, "지급가능한 수수료율을 초과합니다.");
                }

                member.setRateShare(0);
                member.setRateSports(add.getRateSports());
                member.setRateSports2(add.getRateSports2());
                member.setRateZone(add.getRateZone());
                member.setRateZone1(add.getRateZone1());
                member.setRateZone2(add.getRateZone2());
            } else if ("수익분배".equals(member.getRateCode())) {
                if (add.getRateShare() > aMember.getRateShare()) {
                    return new AjaxResult(false, "지급가능한 수수료율을 초과합니다.");
                }

                member.setRateShare(add.getRateShare());
                member.setRateSports(0);
                member.setRateSports2(0);
                member.setRateZone(0);
                member.setRateZone1(0);
                member.setRateZone2(0);
            }

            if (StringUtils.notEmpty(add.getPhone())) {
                member.setPhone(add.getPhone().replaceAll("[^0-9]", ""));
                member.setPassKey(member.getPhone());
            }
            if (StringUtils.notEmpty(add.getAccount())) {
                member.setAccount(add.getAccount().replaceAll("[^0-9]", ""));
            }
            memberRepository.saveAndFlush(member);

            if (aMember.getAgencyDepth() == 6) {
                JoinCodeDto.Add joinAdd = new JoinCodeDto.Add();
                joinAdd.setCode(add.getUserid());
                joinAdd.setEnabled(true);
                joinAdd.setAgency1(member.getAgency1());
                joinAdd.setAgency2(member.getAgency2());
                joinAdd.setAgency7(add.getUserid());
                joinCodeService.add(joinAdd);
            }

        } catch (RuntimeException e) {
            log.error("{} - 대리점 회원생성에 실패하였습니다.: {}", e.getMessage(), add.toString());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, add.getUserid() + "님 등록에 실패 하였습니다.");
        }

        return new AjaxResult(true, add.getUserid() + "님을 등록하였습니다.");
    }

    @Transactional
    @Override
    public AjaxResult adminAddDummy(MemberDto.Dummy dummy) {
        String maxUserid = memberRepository.maxDummyId();
        if (maxUserid == null) maxUserid = "0";
        int maxId = Integer.parseInt(maxUserid.replaceAll("[^0-9]", ""));
        int update = 0;
        Set<String> dummyLine = new HashSet<>(Arrays.asList(dummy.getText().split(System.lineSeparator())));
        for (String nick : dummyLine) {
            if (StringUtils.empty(nick) || nick.length() < 2) continue;

            long count = memberRepository.count(q.nickname.eq(nick));
            if (count == 0) {
                update++;
                Member member = new Member();
                member.setUserid("dm_" + String.format("%04d", maxId + update));
                member.setNickname(nick);
                member.setRole(Role.DUMMY);
                member.setLevel(NumberUtils.random(dummy.getStart(), dummy.getEnd()));
                member.setJoinDate(new Date());
                member.setJoinIp(WebUtils.ip());
                member.setPassword(passwordEncoder.encode(""));
                memberRepository.saveAndFlush(member);
            }
        }
        return new AjaxResult(true, "전체 중복제외 " + dummyLine.size() + "명 중 " + update + "명의 게시판회원을 등록하였습니다.");
    }

    @Override
    public AjaxResult sellerMemberAdd(MemberDto.Add add) {
        String userid = WebUtils.userid();

        if (WebUtils.role() != Role.AGENCY) {
            return new AjaxResult(false, "권한이 부족합니다.");
        }

        try {

            Member aMember = memberRepository.findByUserid(userid);

            Member member = new Member();
            BeanUtils.copyProperties(add, member);

            member.setEnabled(true);
            member.setJoinDate(new Date());
            member.setJoinIp(WebUtils.ip());
            member.setPassword(passwordEncoder.encode(add.getPassword()));
            member.setRole(Role.USER);
            member.setAgency1(aMember.getAgency1());
            member.setAgency2(aMember.getAgency2());
            member.setAgencyDepth(0);

            if (StringUtils.notEmpty(add.getPhone())) {
                member.setPhone(add.getPhone().replaceAll("[^0-9]", ""));
                member.setPassKey(member.getPhone());
            }
            if (StringUtils.notEmpty(add.getAccount())) {
                member.setAccount(add.getAccount().replaceAll("[^0-9]", ""));
            }
            memberRepository.saveAndFlush(member);

        } catch (RuntimeException e) {
            log.error("{} - 회원생성에 실패하였습니다.: {}", e.getMessage(), add.toString());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, add.getUserid() + "님 등록에 실패 하였습니다.");
        }

        return new AjaxResult(true, add.getUserid() + "님을 등록하였습니다.");
    }

    @Override
    public long addText(String memberText) {
        String[] lines = memberText.split(System.getProperty("line.separator"));
        long cnt = 0;

        //System.out.println("lines="+lines.length);

        for (int i = 0; i < lines.length; i++) {
            log.info(lines[i]);
            if (StringUtils.empty(lines[i])) continue;

            String[] fields = lines[i].split("\\t");

            if (fields.length != 20) continue;

            try {
                Member member = new Member();
                member.setUserid(fields[0].trim());
                member.setNickname(fields[1].trim());
                member.setLevel(Integer.parseInt(fields[2].trim()));
                member.setPhone(fields[3].trim());
                member.setBank(fields[4].trim());
                member.setDepositor(fields[5].trim());
                member.setAccount(fields[6].trim());
                member.setDeposit(Long.parseLong(fields[7].trim()));
                member.setWithdraw(Long.parseLong(fields[8].trim()));
//                member.setJoinDate(DateUtils.parse(fields[9].trim(), "yyyy-MM-dd HH:mm"));

                /*String role = fields[9].trim();
                if(role.equals("USER")){
                    member.setRole(Role.USER);
                }else if(role.equals("AGENCY")){
                    member.setRole(Role.AGENCY);
                }*/

                member.setRole(Role.USER);

                member.setAgency1(fields[9].trim());
                member.setAgency2(fields[10].trim());
//                member.setAgencyDepth(Integer.parseInt(fields[17].trim()));
                member.setAgencyDepth(0);

                member.setBankPassword(fields[16].trim());
                member.setMoney(Integer.parseInt(fields[17].trim()));
                member.setPoint(Integer.parseInt(fields[18].trim()));
                member.setJoinDomain("mobi.com");

                member.setPassword(passwordEncoder.encode("1234"));
//                member.setMemo(fields[22].trim());

                member.setSecession(false);
                member.setEnabled(true);

                /*String secession = fields[23].trim();
                if(secession.equals("정상")){
                    member.setSecession(false);
                    member.setEnabled(true);
                }else if(secession.equals("탈퇴")){
                    member.setSecession(true);
                    member.setEnabled(false);
                }*/

                member.setJoinCode(fields[19].trim());

                memberRepository.saveAndFlush(member);
                cnt++;
            } catch (RuntimeException e) {
                log.error("회원 등록시 에러가 발생하였습니다. : {}", e.getMessage());
            }
        }

        return cnt;
    }

}
