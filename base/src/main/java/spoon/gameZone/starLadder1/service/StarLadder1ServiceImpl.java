package spoon.gameZone.starLadder1.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.ladder.*;
import spoon.gameZone.starLadder1.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class StarLadder1ServiceImpl implements StarLadder1Service {

    private ConfigService configService;

    private MemberService memberService;

    private StarLadder1GameService starLadder1GameService;

    private StarLadder1BotService starLadder1BotService;

    private StarLadder1Repository starLadder1Repository;

    private BetItemRepository betItemRepository;

    private static QStarLadder1 q = QStarLadder1.starLadder1;

    @Transactional
    @Override
    public boolean updateConfig(StarLadder1Config starLadder1Config) {
        try {
            configService.updateZoneConfig("starladder1", JsonUtils.toString(starLadder1Config));
            ZoneConfig.setStarLadder1(starLadder1Config);
            // 이미 등록된 게임의 배당을 변경한다.
            starLadder1Repository.updateOdds(ZoneConfig.getStarLadder1().getOdds());
        } catch (RuntimeException e) {
            log.error("별다리1분 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<StarLadder1> getComplete() {
        return starLadder1Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<StarLadder1> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return starLadder1Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public StarLadder1Dto.Score findScore(Long id) {
        StarLadder1 starLadder1 = starLadder1Repository.findOne(id);

        StarLadder1Dto.Score score = new StarLadder1Dto.Score();
        score.setId(starLadder1.getId());
        score.setRound(starLadder1.getRound());
        score.setGameDate(starLadder1.getGameDate());
        score.setStart(starLadder1.getStart());
        score.setLine(starLadder1.getLine());
        score.setOddeven(starLadder1.getOddeven());
        score.setCancel(starLadder1.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getLadderUrl() + "?sdate=" + starLadder1.getSdate());
        if (json == null) return score;

        starLadder1 = JsonUtils.toModel(json, StarLadder1.class);
        if (starLadder1 == null) return score;

        // 봇에 결과가 있다면
        if (starLadder1.isClosing()) {
            score.setOddeven(starLadder1.getOddeven());
            score.setLine(starLadder1.getLine());
            score.setStart(starLadder1.getStart());

            if (!"ODD".equals(score.getOddeven()) && !"EVEN".equals(score.getOddeven())) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(StarLadder1Dto.Score score) {
        StarLadder1 starLadder1 = starLadder1Repository.findOne(score.getId());

        try {
            if (starLadder1.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                starLadder1GameService.rollbackPayment(starLadder1);
            }

            // 스코어 입력
            starLadder1.updateScore(score);
            starLadder1Repository.saveAndFlush(starLadder1);
            starLadder1GameService.closingBetting(starLadder1);
            starLadder1BotService.checkResult();
        } catch (RuntimeException e) {
            log.error("별다리1분 {} - {}회차 수동처리를 하지 못하였습니다. - {}", starLadder1.getGameDate(), starLadder1.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<StarLadder1> iterable = starLadder1Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(5))));
        for (StarLadder1 starLadder1 : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.STARLADDER1).and(qi.groupId.eq(starLadder1.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getStarLadder1Url() + "?sdate=" + starLadder1.getSdate());
            if (json == null) continue;

            StarLadder1 result = JsonUtils.toModel(json, StarLadder1.class);
            if (result == null) continue;

            if (result.isClosing()) {
                starLadder1.setStart(result.getStart());
                starLadder1.setLine(result.getLine());
                starLadder1.setOddeven(result.getOddeven());
                starLadder1.setClosing(true);
                starLadder1Repository.saveAndFlush(starLadder1);
                closing++;
            }
        }
        starLadder1BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public StarLadder1Dto.Config gameConfig() {
        StarLadder1Dto.Config gameConfig = new StarLadder1Dto.Config();
        StarLadder1Config config = ZoneConfig.getStarLadder1();

        Date gameDate = config.getZoneMaker().getGameDate();
        StarLadder1 starLadder1 = starLadder1Repository.findOne(q.gameDate.eq(gameDate));

        if (starLadder1 == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isStarLadder1());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(starLadder1.getGameDate());
        gameConfig.setSdate(starLadder1.getSdate());
        gameConfig.setRound(starLadder1.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(starLadder1.getOdds());

        int betTime = (int) (starLadder1.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setStart(config.isStart());
        gameConfig.setLine(config.isLine());
        gameConfig.setLineStart(config.isLineStart());

        return gameConfig;
    }
}
