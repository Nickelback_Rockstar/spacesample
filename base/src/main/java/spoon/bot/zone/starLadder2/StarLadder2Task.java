package spoon.bot.zone.starLadder2;

import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class StarLadder2Task {

    private StarLadder2Parsing starLadder2Parsing;

    @Scheduled(cron = "0 * * * * ?")
    public void gameTask() {
        starLadder2Parsing.parsingGame();
    }

    @Scheduled(cron = "3,8 * * * * ?")
    public void closingTask() {
        starLadder2Parsing.closingGame();
    }

    @Scheduled(cron = "55 0 0 * * ?")
    public void cleanTask() {
        starLadder2Parsing.deleteGame();
    }


}
