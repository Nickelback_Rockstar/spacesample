package spoon.gameZone.coreaBaccarat.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.baccarat.Baccarat;
import spoon.gameZone.baccarat.BaccaratConfig;
import spoon.gameZone.baccarat.BaccaratDto;
import spoon.gameZone.baccarat.BaccaratRepository;
import spoon.gameZone.baccarat.service.BaccaratBotService;
import spoon.gameZone.baccarat.service.BaccaratGameService;
import spoon.gameZone.baccarat.service.BaccaratService;
import spoon.gameZone.coreaBaccarat.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.time.zone.ZoneRulesException;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class CoreaBaccaratServiceImpl implements CoreaBaccaratService {

    private ConfigService configService;

    private MemberService memberService;

    private CoreaBaccaratBotService coreaBaccaratBotService;

    private CoreaBaccaratGameService coreaBaccaratGameService;

    private CoreaBaccaratRepository coreaBaccaratRepository;

    private BetItemRepository betItemRepository;

    private static QCoreaBaccarat q = QCoreaBaccarat.coreaBaccarat;

    @Transactional
    @Override
    public boolean updateConfig(CoreaBaccaratConfig coreaBaccaratConfig) {
        try {
            configService.updateZoneConfig("coreaBaccarat", JsonUtils.toString(coreaBaccaratConfig));
            ZoneConfig.setCoreaBaccarat(coreaBaccaratConfig);
            // 이미 등록된 게임의 배당을 변경한다.
            coreaBaccaratRepository.updateOdds(ZoneConfig.getCoreaBaccarat().getOdds());
        } catch (RuntimeException e) {
            log.error("코리아 바카라 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<CoreaBaccarat> getComplete() {
        return coreaBaccaratRepository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<CoreaBaccarat> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return coreaBaccaratRepository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public CoreaBaccaratDto.Score findScore(Long id) {
        CoreaBaccarat coreaBaccarat = coreaBaccaratRepository.findOne(id);


        CoreaBaccaratDto.Score score = new CoreaBaccaratDto.Score();
        score.setId(coreaBaccarat.getId());
        score.setRound(coreaBaccarat.getRound());
        score.setGameDate(coreaBaccarat.getGameDate());
        score.convertCard(coreaBaccarat.getP1(), coreaBaccarat.getP2(), coreaBaccarat.getP3(), coreaBaccarat.getB1(), coreaBaccarat.getB2(), coreaBaccarat.getB3());
        score.setCancel(coreaBaccarat.isCancel());
        score.setP1(coreaBaccarat.getP1());
        score.setP2(coreaBaccarat.getP2());
        score.setP3(coreaBaccarat.getP3());
        score.setB1(coreaBaccarat.getB1());
        score.setB2(coreaBaccarat.getB2());
        score.setB3(coreaBaccarat.getB3());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getCoreaBaccaratUrl() + "?sdate=" + coreaBaccarat.getSdate());
        if (json == null) return score;


        coreaBaccarat = JsonUtils.toModel(json, CoreaBaccarat.class);
        if (coreaBaccarat == null) return score;


        // 봇에 결과가 있다면
        if (coreaBaccarat.isClosing()) {
            score.convertCard(coreaBaccarat.getP1(), coreaBaccarat.getP2(), coreaBaccarat.getP3(), coreaBaccarat.getB1(), coreaBaccarat.getB2(), coreaBaccarat.getB3());
            if (StringUtils.empty(coreaBaccarat.getP1())) {
                score.setCancel(true);
            }
        }
        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(CoreaBaccaratDto.Score score) {
        CoreaBaccarat coreaBaccarat = coreaBaccaratRepository.findOne(score.getId());

        try {
            // 스코어 입력
            boolean success = score.convertCardNew();
            if (!success) {
                throw new ZoneRulesException("결과를 변환하지 못하였습니다.");
            }

            if (coreaBaccarat.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                coreaBaccaratGameService.rollbackPayment(coreaBaccarat);
            }

            coreaBaccarat.updateScore(score);

            coreaBaccaratRepository.saveAndFlush(coreaBaccarat);
            coreaBaccaratGameService.closingBetting(coreaBaccarat);
            coreaBaccaratBotService.checkResult();

        } catch (RuntimeException e) {
            log.error("코리아 바카라 {} - {}회차 수동처리를 하지 못하였습니다. - {}", coreaBaccarat.getGameDate(), coreaBaccarat.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<CoreaBaccarat> iterable = coreaBaccaratRepository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(1))));
        for (CoreaBaccarat coreaBaccarat : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.CBACCARAT).and(qi.groupId.eq(coreaBaccarat.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getCoreaBaccaratUrl() + "?sdate=" + coreaBaccarat.getSdate());
            if (json == null) continue;

            CoreaBaccarat result = JsonUtils.toModel(json, CoreaBaccarat.class);
            if (result == null) continue;

            if (result.isClosing()) {
                coreaBaccarat.setP1(result.getP1());
                coreaBaccarat.setP2(result.getP2());
                coreaBaccarat.setP3(result.getP3());
                coreaBaccarat.setB1(result.getB1());
                coreaBaccarat.setB2(result.getB2());
                coreaBaccarat.setB3(result.getB3());
                coreaBaccarat.setResult(result.getResult());

                coreaBaccarat.setClosing(true);
                coreaBaccaratRepository.saveAndFlush(coreaBaccarat);
                closing++;
            }
        }

        coreaBaccaratBotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public CoreaBaccaratDto.Config gameConfig() {
        return gameConfig("");
    }

    @Override
    public CoreaBaccaratDto.Config gameConfig(String userid) {
        CoreaBaccaratDto.Config gameConfig = new CoreaBaccaratDto.Config();
        CoreaBaccaratConfig config = ZoneConfig.getCoreaBaccarat();

        // 홀짝 바카라는 1분씩 댕겨 줘야 한다.
        Date gameDate = new Date(config.getZoneMaker().getGameDate().getTime() - 1000 * 60);
        CoreaBaccarat coreaBaccarat = coreaBaccaratRepository.findOne(q.gameDate.eq(gameDate));

        if (coreaBaccarat == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        userid = StringUtils.notEmpty(userid) ? userid : WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isCoreaBaccarat());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(coreaBaccarat.getGameDate());
        gameConfig.setSdate(coreaBaccarat.getSdate());
        gameConfig.setRound(coreaBaccarat.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(coreaBaccarat.getOdds());

        // 60초 보정
        int betTime = (int) (coreaBaccarat.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000 + 60000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        return gameConfig;
    }
}
