package spoon.gameZone.coreaBaccarat.service;

import spoon.gameZone.coreaBaccarat.CoreaBaccarat;

import java.util.Date;

public interface CoreaBaccaratBotService {

    boolean notExist(Date gameDate);

    void addGame(CoreaBaccarat coreaBaccarat);

    boolean closingGame(CoreaBaccarat result);

    void checkResult();

    void deleteGame(int days);

}
