package spoon.gameZone.eosPower4.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.eosPower4.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class EosPower4ServiceImpl implements EosPower4Service {

    private ConfigService configService;

    private MemberService memberService;

    private EosPower4GameService eosPower4GameService;

    private EosPower4BotService eosPower4BotService;

    private EosPower4Repository eosPower4Repository;

    private BetItemRepository betItemRepository;

    private static QEosPower4 q = QEosPower4.eosPower4;

    @Transactional
    @Override
    public boolean updateConfig(EosPower4Config powerConfig) {
        try {
            configService.updateZoneConfig("eosPower4", JsonUtils.toString(powerConfig));
            ZoneConfig.setEosPower4(powerConfig);
            // 이미 등록된 게임의 배당을 변경한다.
            eosPower4Repository.updateOdds(ZoneConfig.getEosPower4().getOdds());
        } catch (RuntimeException e) {
            log.error("EOS파워볼 4분 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<EosPower4> getComplete() {
        return eosPower4Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<EosPower4> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return eosPower4Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public EosPower4Dto.Score findScore(Long id) {
        EosPower4 power = eosPower4Repository.findOne(id);

        EosPower4Dto.Score score = new EosPower4Dto.Score();
        score.setId(power.getId());
        score.setRound(power.getRound());
        score.setTimes(power.getTimes());
        score.setGameDate(power.getGameDate());
        score.setPb(power.getPb());
        score.setBall(power.getBall());
        score.setCancel(power.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getEosPower5Url() + "?sdate=" + power.getSdate());
        if (json == null) return score;

        power = JsonUtils.toModel(json, EosPower4.class);
        if (power == null) return score;

        // 봇에 결과가 있다면
        if (power.isClosing()) {
            score.setPb(power.getPb());
            score.setBall(power.getBall());

            if (StringUtils.empty(power.getPb())) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(EosPower4Dto.Score score) {
        EosPower4 power = eosPower4Repository.findOne(score.getId());

        try {
            if (power.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                eosPower4GameService.rollbackPayment(power);
            }

            // 스코어 입력
            power.updateScore(score);
            eosPower4Repository.saveAndFlush(power);
            eosPower4GameService.closingBetting(power);
            eosPower4BotService.checkResult();
        } catch (RuntimeException e) {
            log.error("EOS파워볼 4분 {} - {}회차 수동처리를 하지 못하였습니다. - {}", power.getGameDate(), power.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<EosPower4> iterable = eosPower4Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(4))));
        for (EosPower4 power : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.EOSPOWER4).and(qi.groupId.eq(power.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getEosPower4Url() + "?sdate=" + power.getSdate());
            if (json == null) continue;

            EosPower4 result = JsonUtils.toModel(json, EosPower4.class);
            if (result == null) continue;

            if (result.isClosing()) {
                power.setOddeven(result.getOddeven());
                power.setPb_oddeven(result.getPb_oddeven());
                power.setOverunder(result.getOverunder());
                power.setPb_overunder(result.getPb_overunder());
                power.setSize(result.getSize());
                power.setSum(result.getSum());
                power.setPb(result.getPb());
                power.setBall(result.getBall());

                power.setClosing(true);
                eosPower4Repository.saveAndFlush(power);
                closing++;
            }
        }
        eosPower4BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public EosPower4Dto.Config gameConfig() {
        EosPower4Dto.Config gameConfig = new EosPower4Dto.Config();
        EosPower4Config config = ZoneConfig.getEosPower4();

        Date gameDate = config.getZoneMaker().getGameDate();
        EosPower4 power = eosPower4Repository.findOne(q.gameDate.eq(gameDate));

        if (power == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isEosPower4());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(power.getGameDate());
        gameConfig.setSdate(power.getSdate());
        gameConfig.setRound(power.getRound());
        gameConfig.setTimes(power.getTimes());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(power.getOdds());

        int betTime = (int) (power.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setOdd_overunder(config.isOdd_overunder());
        gameConfig.setEven_overunder(config.isEven_overunder());

        gameConfig.setPb_oddeven(config.isPb_oddeven());
        gameConfig.setPb_odd_overunder(config.isPb_odd_overunder());
        gameConfig.setPb_even_overunder(config.isPb_even_overunder());

        gameConfig.setOverunder(config.isOverunder());
        gameConfig.setPb_overunder(config.isPb_overunder());

        gameConfig.setSize(config.isSize());
        gameConfig.setOdd_size(config.isOdd_size());
        gameConfig.setEven_size(config.isEven_size());

        return gameConfig;
    }
}
