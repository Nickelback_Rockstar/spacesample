package spoon.accounting.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.accounting.domain.AccountingDto;
import spoon.game.domain.MenuCode;
import spoon.mapper.AccountingMapper;
import spoon.mapper.MonitorMapper;
import spoon.monitor.domain.MonitorDto;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class AccountingServiceImpl implements AccountingService {

    private AccountingMapper accountingMapper;

    private MonitorMapper monitorMapper;

    @Transactional(readOnly = true)
    @Override
    public List<AccountingDto.Daily> daily(AccountingDto.Command command) {
        return accountingMapper.daily(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<AccountingDto.Game> gameAccount(AccountingDto.Command command) {
        List<AccountingDto.Game> list = new ArrayList<>();

        // 스포츠
        command.setMenuCode("SPORTS");
        list.add(accountingMapper.gameAccount(command));

        // 인게임
        command.setMenuCode("INGAME");
        list.add(accountingMapper.gameAccountInGame(command));

        // 스포츠
        command.setMenuCode("ESPORTS");
        AccountingDto.Game eg = accountingMapper.gameAccountEsports(command);
        //System.out.println("eg===="+eg);
        list.add(eg);

        // 실시간
        for (MenuCode menuCode : MenuCode.getZoneList()) {
            command.setMenuCode(menuCode.toString());
            list.add(accountingMapper.gameAccount(command));
        }

        command.setMenuCode("POWER_LOTTO");
        list.add(accountingMapper.gameAccountPbLotto(command));

        return list;
    }

    @Transactional(readOnly = true)
    @Override
    public AccountingDto.Game gameAccountCasino(AccountingDto.Command command) {
        return accountingMapper.gameAccountCasino(command);
    }

    @Override
    public List<AccountingDto.DailyCasino> dailyCasino(AccountingDto.Command command) {
        return accountingMapper.dailyCasino(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<AccountingDto.Amount> money(AccountingDto.Command command) {
        return accountingMapper.money(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<AccountingDto.Amount> point(AccountingDto.Command command) {
        return accountingMapper.point(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<AccountingDto.Amount> board(AccountingDto.Command command) {
        return accountingMapper.board(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<AccountingDto.Amount> comment(AccountingDto.Command command) {
        return accountingMapper.comment(command);
    }

    @Transactional(readOnly = true)
    @Override
    public MonitorDto.Amount amount() {
        return monitorMapper.getAmount();
    }

    @Override
    public MonitorDto.Amount amount(AccountingDto.Command command) {
        return accountingMapper.amount(command);
    }

    @Transactional(readOnly = true)
    @Override
    public Long fees(AccountingDto.Command command) {
        return accountingMapper.fees(command);
    }

    @Transactional(readOnly = true)
    @Override
    public List<AccountingDto.Daily> dailyPbLotto(AccountingDto.Command command) {
        return accountingMapper.dailyPbLotto(command);
    }
}
