package spoon.report.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.bet.domain.BetDto;
import spoon.bet.domain.BetReport;
import spoon.mapper.BetMapper;

import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class ReportListServiceImpl implements ReportListService {

    private BetMapper betMapper;

    @Transactional(readOnly = true)
    @Override
    public List<BetReport> reportList(BetDto.ReportCommand command) {
        return betMapper.reportList(command);
    }


    @Transactional(readOnly = true)
    @Override
    public long reportListTotal(BetDto.ReportCommand command) {
        return betMapper.reportListTotal(command);
    }
}
