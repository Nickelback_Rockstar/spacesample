package spoon.gameZone.kenoLadder.service;

import spoon.gameZone.kenoLadder.KenoLadder;

import java.util.Date;

public interface KenoLadderBotService {

    boolean notExist(Date gameDate);

    void addGame(KenoLadder kenoLadder);

    boolean closingGame(KenoLadder result);

    void checkResult();

    void deleteGame(int days);

}
