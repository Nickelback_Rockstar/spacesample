package spoon.bot.zone.kenoSpeed;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.kenoLadder.KenoLadder;
import spoon.gameZone.kenoSpeed.KenoSpeed;
import spoon.gameZone.kenoSpeed.service.KenoSpeedBotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class KenoSpeedParsing implements GameBotParsing {

    private KenoSpeedBotService kenoSpeedBotService;

    private static boolean isClosing = false;

    private static Date sdate = new Date();

    @Async
    @Override
    public void parsingGame() {
        isClosing = false;

        int count = 0;
        int times = ZoneConfig.getKenoSpeed().getPowerMaker().getTimes();
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getKenoSpeed().getPowerMaker().getGameDate(times));

        for (int i = 0; i < 6; i++) {
            times++;
            cal.add(Calendar.MINUTE, 5);

            if (cal.getTime().before(sdate)) continue;

            if (kenoSpeedBotService.notExist(cal.getTime())) {
                KenoSpeed kenoSpeed = new KenoSpeed(times, cal.getTime());
                String hh = kenoSpeed.getSdate().substring(8,10);//시간
                if(Integer.parseInt(hh) < 6){
                    //경기 생성중 6시 미만 경기는 생성 안한다.
                    continue;
                }
                kenoSpeed.setOdds(ZoneConfig.getKenoSpeed().getOdds());
                kenoSpeedBotService.addGame(kenoSpeed);
                count++;
            }
        }

        log.debug("키노 스피드 경기등록 : {}건", count);
    }

    @Async
    @Override
    public void closingGame() {
        //if (isClosing) return;
        isClosing = true;

        int times = ZoneConfig.getKenoSpeed().getPowerMaker().getTimes();
        Date gameDate = ZoneConfig.getKenoSpeed().getPowerMaker().getGameDate(times);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getKenoSpeedUrl());
        if (json == null) {
            isClosing = false;
            return;
        }

        JSONParser parser = new JSONParser();
        JSONObject jsonObj = null;
        KenoSpeed result = null;
        try {
            jsonObj = (JSONObject) parser.parse(json);
            result = new KenoSpeed();

            long gDate = Long.parseLong(jsonObj.get("gameDate").toString());
            result.setGameDate(new Date(gDate));

            result.setRound(Integer.parseInt(jsonObj.get("round").toString()));
            result.setOverUnder(jsonObj.get("sumOverUnder").toString());
            result.setOddeven(jsonObj.get("sumOddEven").toString());
            result.setSdate(jsonObj.get("sdate").toString());
            result.setClosing(false);
            result.setCancel(false);

            String lastNum = jsonObj.get("sum").toString();
            lastNum = lastNum.substring(lastNum.length() - 1, lastNum.length());

            if ("0".equals(lastNum)) result.setNum0(lastNum);
            else if ("1".equals(lastNum)) result.setNum1(lastNum);
            else if ("2".equals(lastNum)) result.setNum2(lastNum);
            else if ("3".equals(lastNum)) result.setNum3(lastNum);
            else if ("4".equals(lastNum)) result.setNum4(lastNum);
            else if ("5".equals(lastNum)) result.setNum5(lastNum);
            else if ("6".equals(lastNum)) result.setNum6(lastNum);
            else if ("7".equals(lastNum)) result.setNum7(lastNum);
            else if ("8".equals(lastNum)) result.setNum8(lastNum);
            else if ("9".equals(lastNum)) result.setNum9(lastNum);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (result == null) {
            isClosing = false;
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            isClosing = false;
            return;
        }

        isClosing = kenoSpeedBotService.closingGame(result);

        log.debug("키노 스피드 경기 종료 : {}회차", result.getRound());
    }

    @Async
    @Override
    public void checkResult() {
        kenoSpeedBotService.checkResult();
    }

    @Async
    @Override
    public void deleteGame() {
        kenoSpeedBotService.deleteGame(3);
    }
}
