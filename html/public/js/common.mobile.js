var path = '/';
try {
    path = location.pathname.match(/^(\/.*?)\//)[1];
} catch (e) {
}

var showMessage = function (message) {
    alert(message);
};

var errorMessage = function (message) {
    alert(message);
};

$(window).on('scroll load', function(){
    if ($(window).scrollTop() > 50) {
        $('#btn-top').addClass('show');
    } else {
        $('#btn-top').removeClass('show');
    }
});

$('#btn-top').on('click', function(e) {
    e.preventDefault();
    e.stopPropagation();

    $('html, body').stop().animate({
        scrollTop: 0
    }, 300);
});

$(function() {
    if ($('.tab-menu-wrap').length > 0) {
        var width = ($('.tab-menu-wrap').find('a').length * 25);
        $('.tab-menu-wrap').css({
            width: width + '%'
        }).find('a').css({
            width: (100 / $('.tab-menu-wrap').find('a').length) + '%'
        });
        $('.tab-menu').scrollLeft($('.tab-menu-wrap').find('a.on').offset().left);
    }
});

$('#btn-menu').on('click', function () {
    $('#gnb').addClass('on');
});

$('#btn-close').on('click', function () {
    $('#gnb').removeClass('on');
});

$('#btn-cart').on('click', function () {
    $('#cart-wrap').toggleClass('on');
    if($('#cart-wrap').hasClass('on')){
        $('#btn-cart').text("카트닫기");
    }else{
        $('#btn-cart').text("카트보기");
    }
});

var goPage = function (page) {
    $('#page').val(page);
    $('#frm-search').submit();
};

var goSearch = function () {
    $('#page').val(1);
    $('#frm-search').submit();
};

var iframeScale = function(x, y) {

    var scale = ($('main').width() / x);
    var height = y * scale;
    $('#zoneBox > div').css({
        transform: 'scale(' + scale + ')',
        height: height
    });
};


var point = {
    exchange: function () {
        if (!confirm('포인트를 머니로 전환하겠습니까?')) {
            return false;
        }

        $.post(path + '/payment/exchange').done(function (data) {
            if (data.success) {
                var money = parseInt(data.value);
                $('#user-money').text((parseInt($('#user-money').text().num()) + money).toString().money());
                $('#user-point').text(0);
            }
        });
    }
};

var payment = {
    account: function () {
        if (!confirm('계좌문의를 하시겠습니까?')) {
            return false;
        }
        $.post(path + '/customer/account').done(function (data) {
            if (data.success) {
                location.href = path + '/customer/qna';
            }
        });
    }
};