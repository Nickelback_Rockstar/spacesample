package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.common.utils.JsonUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.binanceCoin3.BinanceCoin3Dto;
import spoon.gameZone.binanceCoin3.service.BinanceCoin3GameService;
import spoon.gameZone.binanceCoin3.service.BinanceCoin3Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class BinanceCoin3Controller {

    private BinanceCoin3Service binanceCoin3Service;

    private BinanceCoin3GameService binanceCoin3GameService;

    @RequestMapping(value = "zone/binanceCoin3", method = RequestMethod.GET)
    public String zone(ModelMap map) {
        map.addAttribute("config", JsonUtils.toString(binanceCoin3Service.gameConfig()));
        return "site/zone/binanceCoin3";
    }

    @RequestMapping(value = "zone/binanceCoin3/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", binanceCoin3Service.getClosing(command, pageable));
        return "site/score/binanceCoin3";
    }

    @ResponseBody
    @RequestMapping(value = "zone/binanceCoin3/config", method = RequestMethod.POST)
    public BinanceCoin3Dto.Config config() {
        return binanceCoin3Service.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/binanceCoin3/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return binanceCoin3GameService.betting(bet);
    }
}
