package spoon.bot.zone.starLadder1;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.starLadder1.StarLadder1;
import spoon.gameZone.starLadder1.service.StarLadder1BotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class StarLadder1Parsing implements GameBotParsing {

    private StarLadder1BotService starLadder1BotService;

    private static boolean isClosing = false;

    private static Date sdate = new Date();

    @Override
    public void parsingGame() {
        isClosing = false;

        int count = 0;
        int round = ZoneConfig.getBogleLadder().getZoneMaker().getRound();
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getBogleLadder().getZoneMaker().getGameDate());

        for (int i = 0; i < 10; i++) {
            if (cal.getTime().before(sdate)) continue;
            if (starLadder1BotService.notExist(cal.getTime())) {
                StarLadder1 starLadder1 = new StarLadder1(round > 1440 ? round % 1440 : round, cal.getTime());
                starLadder1.setOdds(ZoneConfig.getStarLadder1().getOdds());
                starLadder1BotService.addGame(starLadder1);
                count++;
            }
            round++;
            cal.add(Calendar.MINUTE, 1);
        }
        sdate = cal.getTime();
        log.debug("별다리1분 경기등록 : {}건", count);
    }

    @Override
    public void closingGame() {
        if (isClosing) return;
        isClosing = true;

        Date gameDate = new Date(ZoneConfig.getBogleLadder().getZoneMaker().getGameDate().getTime() - 3 * 60 * 1000);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getBogleLadderUrl());
        if (json == null) {
            isClosing = false;
            return;
        }

        StarLadder1 result = JsonUtils.toModel(json, StarLadder1.class);
        if (result == null) {
            isClosing = false;
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            isClosing = false;
            return;
        }

        starLadder1BotService.closingGame(result);

        log.debug("별다리1분 경기 종료 : {}회차", result.getRound());
    }

    @Override
    public void checkResult() {
        starLadder1BotService.checkResult();
    }

    @Override
    public void deleteGame() {
        starLadder1BotService.deleteGame(3);
    }


}
