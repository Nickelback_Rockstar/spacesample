package spoon.gameZone.starLadder1;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import spoon.gameZone.bogleLadder.BogleLadder;

import java.util.Date;

public interface StarLadder1Repository extends JpaRepository<StarLadder1, Long>, QueryDslPredicateExecutor<StarLadder1> {

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE StarLadder1 o SET o.odds = :odds WHERE o.gameDate > CURRENT_TIMESTAMP")
    void updateOdds(@Param("odds") double[] odds);

    @Modifying(clearAutomatically = true)
    @Query(value = "DELETE FROM StarLadder1 o WHERE o.gameDate < :gameDate")
    void deleteGame(@Param("gameDate") Date gameDate);

}
