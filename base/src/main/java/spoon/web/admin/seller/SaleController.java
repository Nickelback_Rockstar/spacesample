package spoon.web.admin.seller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.common.utils.StringUtils;
import spoon.common.utils.WebUtils;
import spoon.member.domain.CurrentUser;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.sale.domain.SaleDto;
import spoon.sale.entity.PowerLottoSaleItem;
import spoon.sale.entity.SaleItem;
import spoon.sale.service.SaleService;
import spoon.support.web.AjaxResult;

import java.util.List;

@Slf4j
@AllArgsConstructor
@Controller("admin.saleController")
@RequestMapping("#{config.pathAdmin}")
public class SaleController {

    private SaleService saleService;

    private MemberService memberService;

    /**
     * 총판 현재 정산금 페이지
     */
    @RequestMapping(value = "/seller/sale", method = RequestMethod.GET)
    public String sale(ModelMap map) {

        //List<SaleItem> list = saleService.currentSaleNew(new SaleDto.Command());
        List<SaleItem> list = saleService.currentSaleNew2(new SaleDto.Command());
        for(SaleItem item : list){
            // '####' 구분자로 합쳐진 문자열 금액들을 배열로 쪼개서 셋해줌
            String[] money = item.getBetZoneMoney().split("####");
            if(money != null && money.length == 3){
                item.setBetSports(Long.parseLong(money[0]));
                item.setBetSports2(Long.parseLong(money[1]));
                item.setBetZone(Long.parseLong(money[2]));
            }
        }
        for(SaleItem si1 : list){
            long tmpMoney1 = 0;
            long tmpMoney2 = 0;
            long tmpMoney3 = 0;
            long tmpMoney4 = 0;
            long tmpMoney5 = 0;
            long tmpMoney6 = 0;
            long tmpMoney7 = 0;
            for(SaleItem si2 : list){
                if(si1.getAgencyDepth() == 1 && si2.getAgencyDepth() == 2 && si1.getAgency1().equals(si2.getAgency1())){
                    tmpMoney1 += si2.getCalcMoney();
                }else if(si1.getAgencyDepth() == 2 && si2.getAgencyDepth() == 3 && si1.getAgency2().equals(si2.getAgency2())){
                    tmpMoney2 += si2.getCalcMoney();
                }else if(si1.getAgencyDepth() == 3 && si2.getAgencyDepth() == 4 && si1.getAgency3().equals(si2.getAgency3())){
                    tmpMoney3 += si2.getCalcMoney();
                }else if(si1.getAgencyDepth() == 4 && si2.getAgencyDepth() == 5 && si1.getAgency4().equals(si2.getAgency4())){
                    tmpMoney4 += si2.getCalcMoney();
                }else if(si1.getAgencyDepth() == 5 && si2.getAgencyDepth() == 6 && si1.getAgency5().equals(si2.getAgency5())){
                    tmpMoney5 += si2.getCalcMoney();
                }else if(si1.getAgencyDepth() == 6 && si2.getAgencyDepth() == 7 && si1.getAgency6().equals(si2.getAgency6())){
                    tmpMoney6 += si2.getCalcMoney();
                }
            }
            si1.setTmpMoney1(tmpMoney1);
            si1.setTmpMoney2(tmpMoney2);
            si1.setTmpMoney3(tmpMoney3);
            si1.setTmpMoney4(tmpMoney4);
            si1.setTmpMoney5(tmpMoney5);
            si1.setTmpMoney6(tmpMoney6);
            si1.setTmpMoney7(tmpMoney7);
        }

        System.out.println(list);

        map.addAttribute("list", list);
        return "/admin/seller/sale/current";
    }

    /**
     * 총판 현재 정산금 페이지
     */
    @RequestMapping(value = "/seller/saleNew", method = RequestMethod.GET)
    public String saleNew(ModelMap map) {
        List<SaleItem> list = saleService.currentSaleReNew(new SaleDto.Command());
        map.addAttribute("list", list);
        return "/admin/seller/sale/currentNew";
    }

    /**
     * 총판 현재 정산 팝업 페이지
     */
    @RequestMapping(value = "/seller/saleNew/popup", method = RequestMethod.GET)
    public String salePopup(ModelMap map, SaleDto.Command cmd) {
        List<SaleItem> list = currentSale(cmd);
        map.addAttribute("list", list);
        return "/admin/seller/sale/popup/salePopup";
    }

    public List<SaleItem> currentSale(SaleDto.Command cmd){
        List<SaleItem> list = saleService.currentSaleNew2(cmd);
        for(SaleItem item : list){
            // '####' 구분자로 합쳐진 문자열 금액들을 배열로 쪼개서 셋해줌
            String[] money = item.getBetZoneMoney().split("####");
            if(money != null && money.length == 3){
                item.setBetSports(Long.parseLong(money[0]));
                item.setBetSports2(Long.parseLong(money[1]));
                item.setBetZone(Long.parseLong(money[2]));
            }
        }
        for(SaleItem si1 : list){
            long tmpMoney1 = 0;
            long tmpMoney2 = 0;
            long tmpMoney3 = 0;
            long tmpMoney4 = 0;
            long tmpMoney5 = 0;
            long tmpMoney6 = 0;
            long tmpMoney7 = 0;
            for(SaleItem si2 : list){
                if(si1.getAgencyDepth() == 1 && si2.getAgencyDepth() == 2 && si1.getAgency1().equals(si2.getAgency1())){
                    tmpMoney1 += si2.getCalcMoney();
                }else if(si1.getAgencyDepth() == 2 && si2.getAgencyDepth() == 3 && si1.getAgency2().equals(si2.getAgency2())){
                    tmpMoney2 += si2.getCalcMoney();
                }else if(si1.getAgencyDepth() == 3 && si2.getAgencyDepth() == 4 && si1.getAgency3().equals(si2.getAgency3())){
                    tmpMoney3 += si2.getCalcMoney();
                }else if(si1.getAgencyDepth() == 4 && si2.getAgencyDepth() == 5 && si1.getAgency4().equals(si2.getAgency4())){
                    tmpMoney4 += si2.getCalcMoney();
                }else if(si1.getAgencyDepth() == 5 && si2.getAgencyDepth() == 6 && si1.getAgency5().equals(si2.getAgency5())){
                    tmpMoney5 += si2.getCalcMoney();
                }else if(si1.getAgencyDepth() == 6 && si2.getAgencyDepth() == 7 && si1.getAgency6().equals(si2.getAgency6())){
                    tmpMoney6 += si2.getCalcMoney();
                }
            }
            si1.setTmpMoney1(tmpMoney1);
            si1.setTmpMoney2(tmpMoney2);
            si1.setTmpMoney3(tmpMoney3);
            si1.setTmpMoney4(tmpMoney4);
            si1.setTmpMoney5(tmpMoney5);
            si1.setTmpMoney6(tmpMoney6);
            si1.setTmpMoney7(tmpMoney7);
        }

        return list;
    }

    /**
     * 총판 정산 작업하기
     */
    @ResponseBody
    @RequestMapping(value = "/seller/sale", method = RequestMethod.POST)
    public AjaxResult sale(String userid) {
        return saleService.balanceSaleNew(userid);
    }

    /**
     * 총판 정산 리스트
     */
    @RequestMapping(value = "/seller/sale/list", method = RequestMethod.GET)
    public String list(ModelMap map, SaleDto.Command command, @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = {"id"}) Pageable pageable) {
        if(StringUtils.notEmpty(command.getAgency())) {
            Member member = memberService.getMember(command.getAgency());
            command.setAgency1(member.getAgency1());
            command.setAgency2(member.getAgency2());
            command.setAgencyDepth(member.getAgencyDepth());
        }
        map.addAttribute("page", saleService.getPage(command, pageable));
        map.addAttribute("command", command);
        map.addAttribute("seller", memberService.getAgency2List());

        return "/admin/seller/sale/list";
    }

    /**
     * 총판 정산금 지급하기
     */
    @ResponseBody
    @RequestMapping(value = "/seller/sale/payment", method = RequestMethod.POST)
    public AjaxResult payment(SaleDto.Payment payment) {
        return saleService.payment(payment);
    }

    /**
     * 총판 정산금 지급하기
     */
    @ResponseBody
    @RequestMapping(value = "/seller/sale/paymentPbLotto", method = RequestMethod.POST)
    public AjaxResult paymentPbLotto(SaleDto.Payment payment) {
        return saleService.paymentPbLotto(payment);
    }

}
