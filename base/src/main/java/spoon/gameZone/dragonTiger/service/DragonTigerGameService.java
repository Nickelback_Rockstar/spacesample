package spoon.gameZone.dragonTiger.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.dragonTiger.DragonTiger;
import spoon.support.web.AjaxResult;

public interface DragonTigerGameService {

    /**
     * 다리다리 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 다리다리 게임 베팅을 클로징 한다.
     */
    void closingBetting(DragonTiger dt);

    /**
     * 다리다리 게임을 롤백 한다.
     */
    void rollbackPayment(DragonTiger dt);
}
