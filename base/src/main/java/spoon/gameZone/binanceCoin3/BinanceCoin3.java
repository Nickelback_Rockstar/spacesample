package spoon.gameZone.binanceCoin3;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import spoon.bot.support.ZoneHelper;
import spoon.common.utils.DateUtils;
import spoon.game.domain.GameResult;
import spoon.game.domain.MenuCode;
import spoon.gameZone.GameZoneException;
import spoon.gameZone.Zone;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneScore;
import spoon.support.convert.DoubleArrayConvert;
import spoon.support.convert.LongArrayConvert;

import javax.persistence.*;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "ZONE_BINANCECOIN3", indexes = {
        @Index(name = "IDX_sdate", columnList = "sdate")
})
public class BinanceCoin3 {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date gameDate;

    private int round;

    @Column(length = 16)
    private String sdate;

    /*@Column(length = 16)
    private String lowHigh;*/

    @Column(length = 16)
    private String overUnder;

    @Column(length = 16)
    private String oddEven;

    @Convert(converter = LongArrayConvert.class)
    private long[] amount = new long[6];

    @Convert(converter = DoubleArrayConvert.class)
    private double[] odds = new double[6];

    private boolean closing;

    private boolean cancel;

    //---------------------------------------------------------

    public BinanceCoin3(int round, Date gameDate) {
        this.round = round;
        this.gameDate = gameDate;
        this.sdate = DateUtils.format(gameDate, "yyyyMMddHHmm");
    }

    public boolean isBeforeGameDate() {
        return this.gameDate.getTime() - System.currentTimeMillis() > 0;
    }

    public Zone getZone(String gameCode) {
        switch (gameCode) {
            /*case "lowHigh":
                return getZone("lowHigh", "로우/하이", "로우", "하이", 0, 1);*/
            case "overUnder":
                return getZone("overUnder", "오버/언더", "오버", "언더", 2, 3);
            case "oddEven":
                return getZone("oddEven", "홀/짝", "홀", "짝", 4, 5);
            default:
                throw new GameZoneException("바이낸스코인3분 코드 " + gameCode + " 를 확인 할 수 없습니다.");

        }
    }

    private Zone getZone(String gameCode, String type, String teamHome, String teamAway, int idxHome, int idxAway) {
        Zone zone = new Zone();
        zone.setId(this.getId());
        zone.setSdate(this.getSdate());
        zone.setMenuCode(MenuCode.BINANCECOIN3);
        zone.setGameCode(gameCode);
        zone.setLeague(String.format("%03d회차 바이낸스코인3분", this.round));
        zone.setTeamHome(String.format("%03d회차 %s [%s]", this.round, type, teamHome));
        zone.setTeamAway(String.format("%03d회차 %s [%s]", this.round, type, teamAway));
        zone.setHandicap(0);
        zone.setOddsHome(ZoneConfig.getBinanceCoin3().getOdds()[idxHome]);
        zone.setOddsDraw(0);
        zone.setOddsAway(ZoneConfig.getBinanceCoin3().getOdds()[idxAway]);
        zone.setGameDate(this.gameDate);
        return zone;
    }

    public ZoneScore getGameResult(String gameCode) {
        if(this.cancel){
            return new ZoneScore(0, 0, GameResult.CANCEL);
        /*}else if(gameCode.startsWith("lowHigh")) {
            return ZoneHelper.zoneResult(this.lowHigh);*/
        }else if(gameCode.startsWith("overUnder")){
            return ZoneHelper.zoneResult(this.overUnder ,this.cancel);
        }else if(gameCode.startsWith("oddEven")){
            return ZoneHelper.zoneResult(this.oddEven ,this.cancel);
        }else{
            throw new GameZoneException("바이낸스코인3분 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }


    public boolean isChangeResult(BinanceCoin3Dto.Score score) {
        return !this.closing || this.cancel != score.isCancel()
//                || !score.getLowHigh().equals(this.lowHigh)
                || !score.getOverUnder().equals(this.overUnder)
                || !score.getOddEven().equals(this.oddEven);
    }

    public void updateScore(BinanceCoin3Dto.Score score) {
        if (score.isCancel()) {
//            this.lowHigh = "";
            this.overUnder = "";
            this.oddEven = "";
            this.cancel = true;
        } else {
//            this.lowHigh = score.getLowHigh();
            this.overUnder = score.getOverUnder();
            this.oddEven = score.getOddEven();

            this.cancel = false;
        }
        this.closing = true;
    }
}
