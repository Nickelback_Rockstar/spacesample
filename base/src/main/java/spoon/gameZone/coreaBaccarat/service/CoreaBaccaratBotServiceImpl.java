package spoon.gameZone.coreaBaccarat.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.common.utils.StringUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.coreaBaccarat.CoreaBaccarat;
import spoon.gameZone.coreaBaccarat.CoreaBaccaratRepository;
import spoon.gameZone.coreaBaccarat.QCoreaBaccarat;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class CoreaBaccaratBotServiceImpl implements CoreaBaccaratBotService {

    private CoreaBaccaratGameService coreaBaccaratGameService;

    private CoreaBaccaratRepository coreaBaccaratRepository;

    private static QCoreaBaccarat q = QCoreaBaccarat.coreaBaccarat;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return coreaBaccaratRepository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmmss"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(CoreaBaccarat coreaBaccarat) {
        coreaBaccaratRepository.saveAndFlush(coreaBaccarat);
    }

    @Transactional
    @Override
    public boolean closingGame(CoreaBaccarat result) {
        CoreaBaccarat coreaBaccarat = coreaBaccaratRepository.findOne(q.sdate.eq(result.getSdate()));
        if (coreaBaccarat == null) {
            return true;
        }

        try {
            coreaBaccarat.setP1(result.getP1());
            coreaBaccarat.setP2(result.getP2());
            coreaBaccarat.setP3(result.getP3());
            coreaBaccarat.setB1(result.getB1());
            coreaBaccarat.setB2(result.getB2());
            coreaBaccarat.setB3(result.getB3());
            coreaBaccarat.setResult(result.getResult());

            if (StringUtils.empty(result.getP1())) {
                coreaBaccarat.setCancel(true);
            } else {
                coreaBaccarat.setCancel(false);
            }
            coreaBaccarat.setClosing(true);

            coreaBaccaratRepository.saveAndFlush(coreaBaccarat);
            coreaBaccaratGameService.closingBetting(coreaBaccarat);
        } catch (RuntimeException e) {
            log.error("코리아 바카라 {}회차 결과 업데이트에 실패하였습니다. - {}", coreaBaccarat.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = coreaBaccaratRepository.count(q.gameDate.before(DateUtils.beforeSeconds(100)).and(q.closing.isFalse()));
        ZoneConfig.getCoreaBaccarat().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        coreaBaccaratRepository.deleteGame(DateUtils.beforeDays(days));
    }

}
