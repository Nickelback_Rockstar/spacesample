package spoon.web.seller.sale;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spoon.accounting.domain.AccountingDto;
import spoon.accounting.service.AccountingService;
import spoon.common.utils.StringUtils;
import spoon.common.utils.WebUtils;
import spoon.member.domain.CurrentUser;
import spoon.member.domain.MemberDto;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;

import java.util.List;

@Slf4j
@AllArgsConstructor
@Controller("seller.saleController")
@RequestMapping("#{config.pathSeller}")
public class SaleController {

    private AccountingService accountingService;

    private MemberService memberService;

    @RequestMapping(value = "sale/daily", method = RequestMethod.GET)
    public String daily(ModelMap map, @ModelAttribute("command") AccountingDto.Command command) {
        CurrentUser user = WebUtils.user();
        setCommand(user, command);

        MemberDto.Seller seller = new MemberDto.Seller();
        seller.setAgency(user.getUserid());
        seller.setAgencyDepth(user.getAgencyDepth());
        List<MemberDto.Agency> agencyList = memberService.getAgencyMemberList2(seller); //상위총판을 가져옴
        map.addAttribute("agencyList", agencyList);

        if (StringUtils.notEmpty(command.getAgency())) {
            Member member = memberService.getMember(command.getAgency());
            if (member.getAgencyDepth() >= user.getAgencyDepth() &&
                    (
                            member.getAgency1().equals(user.getUserid()) ||
                            member.getAgency2().equals(user.getUserid())
                    )) {
                command.setAgency1("");
                command.setAgency2("");
                command.setAgency3("");
                command.setAgency4("");
                command.setAgency5("");
                command.setAgency6("");
                command.setAgency7("");

                if (StringUtils.notEmpty(member.getAgency2())) {
                    command.setAgency1(member.getAgency2());
                } else if (StringUtils.notEmpty(member.getAgency1())) {
                    command.setAgency1(member.getAgency1());
                }
            }
        }

        //System.out.println("command 2=" + command.toString());

        map.addAttribute("list", accountingService.daily(command));
        return "seller/sale/daily";
    }

    @RequestMapping(value = "sale/detail", method = RequestMethod.GET)
    public String detail(ModelMap map, @ModelAttribute("command") AccountingDto.Command command) {
        CurrentUser user = WebUtils.user();
        setCommand(user, command);

        MemberDto.Seller seller = new MemberDto.Seller();
        seller.setAgency(user.getUserid());
        seller.setAgencyDepth(user.getAgencyDepth());
        List<MemberDto.Agency> agencyList = memberService.getAgencyMemberList2(seller); //상위총판을 가져옴
        map.addAttribute("agencyList", agencyList);

        if (StringUtils.notEmpty(command.getAgency())) {
            Member member = memberService.getMember(command.getAgency());
            if (member.getAgencyDepth() >= user.getAgencyDepth() &&
                    (
                            member.getAgency1().equals(user.getUserid()) ||
                                    member.getAgency2().equals(user.getUserid())
                    )) {

                command.setAgency1("");
                command.setAgency2("");
                command.setAgency3("");
                command.setAgency4("");
                command.setAgency5("");
                command.setAgency6("");
                command.setAgency7("");

                if (StringUtils.notEmpty(member.getAgency2())) {
                    command.setAgency1(member.getAgency2());
                } else if (StringUtils.notEmpty(member.getAgency1())) {
                    command.setAgency1(member.getAgency1());
                }

            }
        }

        //System.out.println("command 2=" + command.toString());

        map.addAttribute("list", accountingService.gameAccount(command));
        map.addAttribute("amount", accountingService.amount(command));
        map.addAttribute("money", accountingService.money(command));
        map.addAttribute("point", accountingService.point(command));
        map.addAttribute("member", memberService.getMember(WebUtils.userid()));

        return "seller/sale/detail";
    }

    public static void setCommand(CurrentUser user, AccountingDto.Command command) {
        if (user.getAgencyDepth() == 1) {
            command.setAgency1(user.getAgency1());
        } else if (user.getAgencyDepth() == 2) {
            command.setAgency2(user.getAgency2());
        } else if (user.getAgencyDepth() == 3) {
            command.setAgency3(user.getAgency3());
        } else if (user.getAgencyDepth() == 4) {
            command.setAgency4(user.getAgency4());
        } else if (user.getAgencyDepth() == 5) {
            command.setAgency5(user.getAgency5());
        } else if (user.getAgencyDepth() == 6) {
            command.setAgency6(user.getAgency6());
        } else if (user.getAgencyDepth() == 7) {
            command.setAgency7(user.getAgency7());
        }
    }

    @RequestMapping(value = "sale/dailyPbLotto", method = RequestMethod.GET)
    public String dailyPbLotto(ModelMap map, @ModelAttribute("command") AccountingDto.Command command) {
        CurrentUser user = WebUtils.user();
        setCommand(user, command);
        map.addAttribute("list", accountingService.dailyPbLotto(command));
        return "seller/sale/dailyPbLotto";
    }

    @RequestMapping(value = "sale/dailyCasino", method = RequestMethod.GET)
    public String dailyCasino(ModelMap map, @ModelAttribute("command") AccountingDto.Command command) {
        CurrentUser user = WebUtils.user();
        setCommand(user, command);

        MemberDto.Seller seller = new MemberDto.Seller();
        seller.setAgency(user.getUserid());
        seller.setAgencyDepth(user.getAgencyDepth());
        List<MemberDto.Agency> agencyList = memberService.getAgencyMemberList2(seller); //상위총판을 가져옴
        map.addAttribute("agencyList", agencyList);
        command.setUserid(user.getUserid());
        if (StringUtils.notEmpty(command.getAgency())) {
            Member member = memberService.getMember(command.getAgency());
            if (member.getAgencyDepth() >= user.getAgencyDepth() &&
                    (
                            member.getAgency1().equals(user.getUserid()) ||
                            member.getAgency2().equals(user.getUserid())

                    )) {

                command.setAgency1("");
                command.setAgency2("");


                if (StringUtils.notEmpty(member.getAgency2())) {
                    command.setAgency2(member.getAgency2());
                    command.setUserid(member.getUserid());
                } else if (StringUtils.notEmpty(member.getAgency1())) {
                    command.setAgency1(member.getAgency1());
                    command.setUserid(member.getUserid());
                }
            }
        }

//        System.out.println("command 222222222222=" + command.toString());

        map.addAttribute("list", accountingService.dailyCasino(command));
        return "seller/sale/dailyCasino";
    }
}
