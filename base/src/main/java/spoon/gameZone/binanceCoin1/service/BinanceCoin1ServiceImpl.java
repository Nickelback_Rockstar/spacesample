package spoon.gameZone.binanceCoin1.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.binanceCoin1.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BinanceCoin1ServiceImpl implements BinanceCoin1Service {

    private ConfigService configService;

    private MemberService memberService;

    private BinanceCoin1GameService binanceCoin1GameService;

    private BinanceCoin1BotService binanceCoin1BotService;

    private BinanceCoin1Repository binanceCoin1Repository;

    private BetItemRepository betItemRepository;

    private static QBinanceCoin1 q = QBinanceCoin1.binanceCoin1;

    @Transactional
    @Override
    public boolean updateConfig(BinanceCoin1Config binanceCoin1Config) {
        try {
            configService.updateZoneConfig("binanceCoin1", JsonUtils.toString(binanceCoin1Config));
            ZoneConfig.setBinanceCoin1(binanceCoin1Config);
            // 이미 등록된 게임의 배당을 변경한다.
            binanceCoin1Repository.updateOdds(ZoneConfig.getBinanceCoin1().getOdds());
        } catch (RuntimeException e) {
            log.error("바이낸스코인1 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<BinanceCoin1> getComplete() {
        return binanceCoin1Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<BinanceCoin1> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return binanceCoin1Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public BinanceCoin1Dto.Score findScore(Long id) {
        BinanceCoin1 binanceCoin1 = binanceCoin1Repository.findOne(id);

        BinanceCoin1Dto.Score score = new BinanceCoin1Dto.Score();
        score.setId(binanceCoin1.getId());
        score.setRound(binanceCoin1.getRound());
        score.setGameDate(binanceCoin1.getGameDate());
//        score.setLowHigh(binanceCoin1.getLowHigh());
        score.setOverUnder(binanceCoin1.getOverUnder());
        score.setOddEven(binanceCoin1.getOddEven());
        score.setCancel(binanceCoin1.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getBinanceCoin1Url() + "?sdate=" + binanceCoin1.getSdate());
        if (json == null) return score;

        binanceCoin1 = JsonUtils.toModel(json, BinanceCoin1.class);
        if (binanceCoin1 == null) return score;

        // 봇에 결과가 있다면
        if (binanceCoin1.isClosing()) {
            if (StringUtils.empty(score.getOverUnder())) {
//                score.setLowHigh(binanceCoin1.getLowHigh());
                score.setOverUnder(binanceCoin1.getOverUnder());
                score.setOddEven(binanceCoin1.getOddEven());
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(BinanceCoin1Dto.Score score) {
        BinanceCoin1 binanceCoin1 = binanceCoin1Repository.findOne(score.getId());

        try {
            if (binanceCoin1.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                binanceCoin1GameService.rollbackPayment(binanceCoin1);
            }

            // 스코어 입력
            binanceCoin1.updateScore(score);
            binanceCoin1Repository.saveAndFlush(binanceCoin1);
            binanceCoin1GameService.closingBetting(binanceCoin1);
            binanceCoin1BotService.checkResult();
        } catch (RuntimeException e) {
            log.error("바이낸스코인1 {} - {}회차 수동처리를 하지 못하였습니다. - {}", binanceCoin1.getGameDate(), binanceCoin1.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<BinanceCoin1> iterable = binanceCoin1Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(3))));
        for (BinanceCoin1 binanceCoin1 : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.BINANCECOIN1).and(qi.groupId.eq(binanceCoin1.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getBinanceCoin1Url() + "?sdate=" + binanceCoin1.getSdate());
            if (json == null) continue;

            BinanceCoin1 result = JsonUtils.toModel(json, BinanceCoin1.class);
            if (result == null) continue;

            if (result.isClosing()) {
//                binanceCoin1.setLowHigh(result.getLowHigh());
                binanceCoin1.setOverUnder(result.getOverUnder());
                binanceCoin1.setOddEven(result.getOddEven());
                binanceCoin1.setClosing(true);
                binanceCoin1Repository.saveAndFlush(binanceCoin1);
                closing++;
            }
        }
        binanceCoin1BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public BinanceCoin1Dto.Config gameConfig() {
        BinanceCoin1Dto.Config gameConfig = new BinanceCoin1Dto.Config();
        BinanceCoin1Config config = ZoneConfig.getBinanceCoin1();

        Date gameDate = config.getZoneMaker().getGameDate();
        BinanceCoin1 binanceCoin1 = binanceCoin1Repository.findOne(q.gameDate.eq(gameDate));

        if (binanceCoin1 == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isBinanceCoin1());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(binanceCoin1.getGameDate());
        gameConfig.setSdate(binanceCoin1.getSdate());
        gameConfig.setRound(binanceCoin1.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(binanceCoin1.getOdds());

        int betTime = (int) (binanceCoin1.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;

        gameConfig.setBetTime(betTime);

        return gameConfig;
    }
}
