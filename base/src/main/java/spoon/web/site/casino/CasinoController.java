package spoon.web.site.casino;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.banking.domain.DepositDto;
import spoon.banking.domain.WithdrawDto;
import spoon.banking.service.BankingDepositService;
import spoon.banking.service.BankingWithdrawService;
import spoon.common.utils.CasinoUtils;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.WebUtils;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class CasinoController {

    private MemberService memberService;
    private BankingDepositService bankDepositService;
    private BankingWithdrawService bankWithdrawService;


    @RequestMapping(value = "casino/liveCasino", method = RequestMethod.GET)
    public String live(ModelMap map) {
        String userid = WebUtils.userid();
        map.addAttribute("member", memberService.getMember(userid));
        return "site/casino/live";
    }

    @ResponseBody
    @RequestMapping(value = "casino/status", method = RequestMethod.GET)
    public JSONArray status(ModelMap map, @RequestParam(value = "code") String code) {
        String url = CasinoUtils.API_URL + "?mode=getLobbyList&operatorID=" + CasinoUtils.API_ID + "&thirdPartyCode=" + code + "&vendorID=0";
        //System.out.println("url = " + url);
        JSONObject result = JsonUtils.getJson(url);
        return (JSONArray) result.get("games");
    }

    @ResponseBody
    @RequestMapping(value = "casino/lobby", method = RequestMethod.GET)
    public JSONObject lobby(ModelMap map, @RequestParam(value = "code") String code, @RequestParam(value = "platform") String platform) {
        String userid = WebUtils.userid();
        String url = CasinoUtils.API_URL + "?mode=getLobbyUrl&lang=kr&operatorID=" + CasinoUtils.API_ID + "&platform=" + platform + "&thirdPartyCode=" + code + "&userID=" + CasinoUtils.getConvertUserid(userid) + "&vendorID=0";
        //System.out.println("url = " + url);
        JSONObject result = JsonUtils.getJson(url);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "casino/gameList", method = RequestMethod.GET)
    public JSONArray gameList(ModelMap map, @RequestParam(value = "code") String code) {
        String url = CasinoUtils.API_URL + "?mode=getGameList&operatorID=" + CasinoUtils.API_ID + "&thirdPartyCode=" + code + "&vendorID=0";
        //System.out.println("url = " + url);
        JSONObject result = JsonUtils.getJson(url);
        return (JSONArray) result.get("games");
    }

    @ResponseBody
    @RequestMapping(value = "casino/game", method = RequestMethod.GET)
    public JSONObject game(ModelMap map, @RequestParam(value = "code") String code, @RequestParam(value = "gameId") String gameId) {
        String userid = WebUtils.userid();
        JSONObject result = new JSONObject();
        String url1 = CasinoUtils.API_URL + "?mode=generateSession&operatorID=" + CasinoUtils.API_ID + "&userID=" + CasinoUtils.getConvertUserid(userid) + "&thirdPartyCode=" + code + "&vendorID=0";
        //System.out.println("url = " + url1);
        JSONObject result1 = JsonUtils.getJson(url1);
        //System.out.println("result=" + result1);
        /*{
            "returnCode": 0,
            "session": "F287CDAB598F8D0533979BCDB80605C7",
            "description": "Success"
        }*/
        if ("0".equals(result1.get("returnCode").toString())) {
            String url2 = CasinoUtils.API_URL + "?mode=getGameUrl&gameID=" + gameId + "&lang=kr&operatorID=" + CasinoUtils.API_ID + "&session=" + result1.get("session").toString() + "&vendorID=0";
            //System.out.println("url = " + url2);
            JSONObject result2 = JsonUtils.getJson(url2);
            result = result2;
            //System.out.println("result=" + result2);
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "casino/createAccount", method = RequestMethod.GET)
    public JSONObject createAccount(ModelMap map) {
        String userid = WebUtils.userid();
        Member m = memberService.getMember(userid);
        String zeroId = "000000" + m.getId();
        String walletId = CasinoUtils.PREFIX + zeroId.substring(zeroId.length() - 6, zeroId.length());
        String password = ""; //패스워드 공백
        String url = CasinoUtils.API_URL + "?mode=createAccount&operatorID=" + CasinoUtils.API_ID + "&userID=" + CasinoUtils.getConvertUserid(userid) + "&userPassword=" + password + "&vendorID=0&walletID=" + walletId;

        //System.out.println(" ==== text join url = " + url);
        JSONObject result = JsonUtils.getJson(url);
        if ("Success".equals(result.get("description").toString())) {
            m.setCasinoNick(result.get("givenUserID").toString());
            m.setCasinoId(CasinoUtils.getConvertUserid(userid));
            memberService.update(m);
        }
        /*{
            "returnCode": 0,
            "givenUserID": "caetest22",
            "description": "Success"
        }*/
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "casino/addPoint", method = RequestMethod.GET)
    public synchronized AjaxResult addPoint(ModelMap map, @RequestParam(value = "amount") long amount) {
        String userid = WebUtils.userid();
        Member m = memberService.getMember(userid);
        //System.out.println("casino/addPoint");
        AjaxResult ajax = new AjaxResult();
        if (m != null && m.getMoney() >= amount) {
            DepositDto.Add add = new DepositDto.Add();
            add.setAmount(amount);
            add.setCode("CASINO");
            ajax = bankDepositService.deposit(add);
            //System.out.println("ajax=" + ajax.toString());
            /*if(ajax.isSuccess()) {
                String url = API_URL + "?mode=addMemberPoint&amount="+amount+"&operatorID=" + API_ID + "&transactionID=" + ("AP" + new Date().getTime()) + "&userID=" + getConvertUserid(userid) + "&vendorID=0";
                //System.out.println("url = " + url);
                result = getJson(url);
            }*/
        }
        return ajax;
    }

    @ResponseBody
    @RequestMapping(value = "casino/subtractPoint", method = RequestMethod.GET)
    public AjaxResult subtractPoint(ModelMap map, @RequestParam(value = "amount") long amount) {
        String userid = WebUtils.userid();
        //System.out.println("casino/subtractPoint");
        JSONObject result = new JSONObject();

        WithdrawDto.Add add = new WithdrawDto.Add();
        add.setAmount(amount);
        add.setCode("CASINO");
        AjaxResult ajax = bankWithdrawService.withdraw(add);

        //게임사 -> 유저에게 포인트 이동
        /*String url1 = API_URL + "?mode=collectAccountGameBalanceAll&operatorID=" + API_ID + "&userID=" + getConvertUserid(userid) + "&vendorID=0";
        //System.out.println("url1 = " + url1);
        JSONObject result1 = getJson(url1);
        //System.out.println(result1.toString());

        if("0".equals(result1.get("returnCode").toString()) || "1".equals(result1.get("returnCode").toString())){
            //유저 -> 관리자에게 포인트 이동
            String url2 = API_URL + "?mode=subtractMemberPoint&amount="+amount+"&operatorID=" + API_ID + "&transactionID=" + ("SP" + new Date().getTime()) + "&userID=" + getConvertUserid(userid) + "&vendorID=0";
            //System.out.println("url2 = " + url2);
            JSONObject result2 = getJson(url2);
            //System.out.println(result1.toString());
            if("0".equals(result2.get("returnCode").toString())){
                WithdrawDto.Add add = new WithdrawDto.Add();
                add.setAmount(amount);
                AjaxResult ajax = bankWithdrawService.depositCasino(add);
                result.put("returnCode",0);
            }else{
                result.put("returnCode",99);
            }
        }else{
            result.put("returnCode",98);
        }*/

        return ajax;
    }


}
