package spoon.bot.sports.bet365;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.sports.service.ParsingGame;
import spoon.bot.sports.service.ParsingResult;
import spoon.config.domain.Config;

@Slf4j
@AllArgsConstructor
@Component
public class Bet365Task {

    private ParsingGame bet365ParsingGame;
    private ParsingGame bonsaParsingGame;

    private ParsingResult bet365ParsingResult;
    private ParsingResult bonsaParsingResult;

    @Scheduled(cron = "0 0/2 * * * *")
    public void parsingGame() {
        if ("none".equals(Config.getSysConfig().getSports().getBet365())) return;
        bet365ParsingGame.parsingGame();
    }

    @Scheduled(cron = "25/30 * * * * *")
    public void parsingResult() {
        if ("none".equals(Config.getSysConfig().getSports().getBet365())) return;
        bet365ParsingResult.closingGame();
    }

//    @Scheduled(cron = "0/10 * * * * *")
    public void parsingGame2() {
        if ("none".equals(Config.getSysConfig().getSports().getBet365())) return;
        bonsaParsingGame.parsingGame();
    }

//    @Scheduled(cron = "25/30 * * * * *")
    public void parsingResult2() {
        if ("none".equals(Config.getSysConfig().getSports().getBet365())) return;
        bonsaParsingResult.closingGame();
    }

}
