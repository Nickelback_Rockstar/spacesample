jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();

function IsInstalled(axProgId) {
	try {        
		var objTest = new ActiveXObject(axProgId);
		return true;
	} catch (ex) {
	}
	return false;
}

var audio = function () {
	
	this.id = "player";
	this.ie = $.browser.msie;
	this.html5 = true;
	this.flag = false; // 6.4 초기화
	
	this._rate = 1;
	this._playcount = 1; // audio 태그에서는 playcount 가 없이 때문에 별도의 변수를 이용한다.
	this._loopcount = 1;

	if (this.ie) {
		/* // 9를 왜 html5 로 안했냐면... rate 가 지원되지 않아서 이다.
		if ($.browser.version < 9) {
			this.html5 = false;
		} else {
			this.html5 = true;
		}
		*/
		this.html5 = false;
	} else {
		this.html5 = true;
	}
	if (!this.html5) {
		if (!IsInstalled("WMPlayer.OCX")) {
			alert("윈도우 미디어 플레이어가 설치되지 않아서 학습(숙제)을 진행할 수 없습니다.\n\n'확인' 버튼을 클릭하면 설치파일을 자동으로 다운로드 합니다.\n\n다운로드가 완료되면 해당파일을 설치해 주세요.");
			location.href = "http://www.microsoft.com/downloads/ko-kr/details.aspx?FamilyID=1d224714-e238-4e45-8668-5166114010ca&DisplayLang=ko";
			//location.href = "/mp9setupXP_kor.asp";
			return;
		}
		
		this.classid = "CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95";
		//this.classid = "CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6";
		this.type = "application/x-oleobject";
		this.allowScriptAccess = "always";
		this.codebase = "http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,7,1113";
		//this.codebase = "http://www.microsoft.com/Windows/MediaPlayer/";
	}
}
audio.prototype.setup = function(callback) {
	
	if (this.classid === "CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6") {
		this.flag = true;
	}

	if (this.html5) {
		this.player = new Audio();
		this.player.id = this.id;
	} else {
		$("body").append("<object"
			+ " classid=\"" + this.classid + "\"" 
			+ " type=\"" + this.type + "\""
			+ " name=\"" + this.id + "\""
			+ " id=\"" + this.id + "\""
			+ " codebase=\"" + this.codebase + "\""
			+ " allowScriptAccess=\"" + this.allowScriptAccess + "\""
			+ " style=\"position:absolute;top:-1000px;left:-1000px;width:0px;height:0px;display:none\">"
		+ "</object>");

		this.player = document.getElementById(this.id);
		
		if (this.flag) {
			this.player.settings.AutoStart = 1;
			this.player.settings.PlayCount = 1;
		} else {
			this.player.AutoStart = 1;
			this.player.PlayCount = 1;
		}
	}

	this.play = function() {
		if (this.html5) {
			this.player.playbackRate = this._rate; // 크롬만 된다. -_-;;
			this.player.play();
		} else {
			if (this.flag) {
				this.player.settings.rate = this._rate;
				this.player.controls.play();
			}
			else {
				this.player.rate = this._rate;
				this.player.play();
			}
		}
	};

	this.pause = function() {
		
		if (this.html5) {
			this.player.pause();
		} else {
			if (this.flag) {
				this.player.controls.pause();
			} else {
				this.player.pause();
			}
		}
	};

	this.stop = function() {
		if (this.html5) {
			this.player.pause();
			if (this.player.src != "") { 
				// currentTime 는 파일이 들어왔을때에만 실행된다.
				// 조낸 그냥 오류가 나길래 ... 조낸 해맸음... -_-;;
				this.player.currentTime = 0;
			}
		} else {
			if (this.flag) {
				this.player.controls.stop();
			} else {
				this.player.stop();
			}
		}
	};

	this.filename = function(value) {
		if (this.html5) {
			if (!value)
				return this.player.src;
			else {
				this.player.src = value; 
				// 파일이 바꼈다면 루프카운트를 초기화 한다.
				this._loopcount = 1;
			}
		} else {
			if (this.flag) {
				if (!value)
					return this.player.url;
				else 
					this.player.url = value;
			} else {
				if (!value)
					return this.player.filename;
				else 
					this.player.filename = value; 
			}
		}
	};
	this.rate = function(value) {
		this._rate = value;
		if (this.html5) { 
			this.player.playbackRate = value;
		} else {
			if (this.flag)
				this.player.settings.rate = value;
			else 
				this.player.rate = value;
		}
	};
	this.volume = function(value) {
		if (this.html5) {
			this.player.volume = (value/100);
		} else {
			if (this.flag)
				this.player.settings.volume = value;
			else 
				this.player.volume = -3000 + (value * 30);
		}
	};
	this.playcount = function(value) {
		if (this.html5) {
			if (!value) {
				return this._playcount;
			} else {
				//this.player.playcount = value;
				this._loopcount = 1;
				this._playcount = value;
				//if (this._playcount > 1) {
				//	this.player.loop = true;
				//}
			}
		}
		else {
			if (this.flag)
				this.player.settings.playcount = value;
			else 
				this.player.playcount = value;
		}
	};
	this.autostart = function(value) {
		if (this.html5)
			this.player.AutoStart = value;
		else {
			if (this.flag)
				this.player.settings.AutoStart = value;
			else 
				this.player.AutoStart = value;
		}
	};

	if (typeof callback === "function") {
		callback();
	};
}
audio.prototype.EndOfStream = function(callback) { // 엔드이벤트
	if (this.html5) {
		this.player.addEventListener("ended", callback, false);
		//this.player.addEventListener('seeked', this.seeked, false);
	} 
	else {
		if (this.flag) {
			this.player.attachEvent("playStateChange", function(newState) {
				if (newState == 8)	{
					callback();
				}
			});
		}
		else {
			this.player.attachEvent("EndOfStream", callback);
		}
	}
}