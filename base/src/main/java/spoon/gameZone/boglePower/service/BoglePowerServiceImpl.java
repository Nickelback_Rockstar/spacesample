package spoon.gameZone.boglePower.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.boglePower.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BoglePowerServiceImpl implements BoglePowerService {

    private ConfigService configService;

    private MemberService memberService;

    private BoglePowerGameService boglePowerGameService;

    private BoglePowerBotService boglePowerBotService;

    private BoglePowerRepository boglePowerRepository;

    private BetItemRepository betItemRepository;

    private static QBoglePower q = QBoglePower.boglePower;

    @Transactional
    @Override
    public boolean updateConfig(BoglePowerConfig boglePowerConfig) {
        try {
            configService.updateZoneConfig("boglePower", JsonUtils.toString(boglePowerConfig));
            ZoneConfig.setBoglePower(boglePowerConfig);
            // 이미 등록된 게임의 배당을 변경한다.
            boglePowerRepository.updateOdds(ZoneConfig.getBoglePower().getOdds());
        } catch (RuntimeException e) {
            log.error("보글파워볼 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<BoglePower> getComplete() {
        return boglePowerRepository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<BoglePower> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return boglePowerRepository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public BoglePowerDto.Score findScore(Long id) {
        BoglePower boglePower = boglePowerRepository.findOne(id);

        BoglePowerDto.Score score = new BoglePowerDto.Score();
        score.setId(boglePower.getId());
        score.setRound(boglePower.getRound());
        score.setGameDate(boglePower.getGameDate());
        score.setPb(boglePower.getPb());
        score.setBall(boglePower.getBall());
        score.setCancel(boglePower.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getBoglePowerUrl() + "?sdate=" + boglePower.getSdate());
        if (json == null) return score;

        boglePower = JsonUtils.toModel(json, BoglePower.class);
        if (boglePower == null) return score;

        // 봇에 결과가 있다면
        if (boglePower.isClosing()) {
            score.setPb(boglePower.getPb());
            score.setBall(boglePower.getBall());

            if (StringUtils.empty(boglePower.getPb())) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(BoglePowerDto.Score score) {
        BoglePower boglePower = boglePowerRepository.findOne(score.getId());

        try {
            if (boglePower.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                boglePowerGameService.rollbackPayment(boglePower);
            }

            // 스코어 입력
            boglePower.updateScore(score);
            boglePowerRepository.saveAndFlush(boglePower);
            boglePowerGameService.closingBetting(boglePower);
            boglePowerBotService.checkResult();
        } catch (RuntimeException e) {
            log.error("보글파워볼 {} - {}회차 수동처리를 하지 못하였습니다. - {}", boglePower.getGameDate(), boglePower.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<BoglePower> iterable = boglePowerRepository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(3))));
        for (BoglePower boglePower : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.BOGLEPOWER).and(qi.groupId.eq(boglePower.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getBoglePowerUrl() + "?sdate=" + boglePower.getSdate());
            if (json == null) continue;

            BoglePower result = JsonUtils.toModel(json, BoglePower.class);
            if (result == null) continue;

            if (result.isClosing()) {
                boglePower.setOddeven(result.getOddeven());
                boglePower.setOverunder(result.getOverunder());
                boglePower.setPb_oddeven(result.getPb_oddeven());
                boglePower.setPb_overunder(result.getPb_overunder());
                boglePower.setPb(result.getPb());
                boglePower.setBall(result.getBall());
                boglePower.setSum(result.getSum());
                boglePower.setSize(result.getSize());
                boglePower.setClosing(true);
                boglePowerRepository.saveAndFlush(boglePower);
                closing++;
            }
        }
        boglePowerBotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public BoglePowerDto.Config gameConfig() {
        BoglePowerDto.Config gameConfig = new BoglePowerDto.Config();
        BoglePowerConfig config = ZoneConfig.getBoglePower();

        Date gameDate = config.getZoneMaker().getGameDate();
        BoglePower boglePower = boglePowerRepository.findOne(q.gameDate.eq(gameDate));

        if (boglePower == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isBoglePower());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(boglePower.getGameDate());
        gameConfig.setSdate(boglePower.getSdate());
        gameConfig.setRound(boglePower.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(boglePower.getOdds());

        int betTime = (int) (boglePower.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setBetTime(betTime);
        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setOverunder(config.isOverunder());
        gameConfig.setPb_oddeven(config.isPb_oddeven());
        gameConfig.setPb_overunder(config.isPb_overunder());
        gameConfig.setSize(config.isSize());

        return gameConfig;
    }
}
