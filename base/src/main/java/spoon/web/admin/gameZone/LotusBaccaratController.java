package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.lotusBaccarat.LotusBaccaratConfig;
import spoon.gameZone.lotusBaccarat.LotusBaccaratDto;
import spoon.gameZone.lotusBaccarat.service.LotusBaccaratService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.lotusBaccaratController")
@RequestMapping(value = "#{config.pathAdmin}")
public class LotusBaccaratController {

    private LotusBaccaratService lotusBaccaratService;

    /**
     * 바카라 설정
     */
    @RequestMapping(value = "zone/lotusBaccarat/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getLotusBaccarat());
        return "admin/zone/lotusBaccarat/config";
    }

    /**
     * 바카라 설정 변경
     */
    @RequestMapping(value = "zone/lotusBaccarat/config", method = RequestMethod.POST)
    public String config(LotusBaccaratConfig lotusBaccaratConfig, RedirectAttributes ra) {

        //pair 게임 lose 배당은 무조건 0으로 셋팅해준다.
        double[] odds = lotusBaccaratConfig.getOdds();
        odds[4] = 0;
        odds[6] = 0;
        lotusBaccaratConfig.setOdds(odds);

        boolean success = lotusBaccaratService.updateConfig(lotusBaccaratConfig);
        if (success) {
            ra.addFlashAttribute("message", "바카라 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "바카라 설정변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/lotusBaccarat/config";
    }

    /**
     * 바카라 진행
     */
    @RequestMapping(value = "zone/lotusBaccarat/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", lotusBaccaratService.getComplete());
        map.addAttribute("config", ZoneConfig.getLotusBaccarat());
        return "admin/zone/lotusBaccarat/complete";
    }

    /**
     * 바카라 완료
     */
    @RequestMapping(value = "zone/lotusBaccarat/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "gameDate") Pageable pageable) {
        map.addAttribute("page", lotusBaccaratService.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getLotusBaccarat());
        return "admin/zone/lotusBaccarat/closing";
    }

    /**
     * 바카라 스코어 입력 폼
     */
    @RequestMapping(value = "zone/lotusBaccarat/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", lotusBaccaratService.findScore(id));
        return "admin/zone/lotusBaccarat/score";
    }

    /**
     * 바카라 결과처리
     */
    @RequestMapping(value = "zone/lotusBaccarat/score", method = RequestMethod.POST)
    public String score(LotusBaccaratDto.Score score, RedirectAttributes ra) {
        boolean success = lotusBaccaratService.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "바카라 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "바카라 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/lotusBaccarat/score?id=" + score.getId();
    }

    /**
     * 바카라 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/lotusBaccarat/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return lotusBaccaratService.closingAllGame();
    }

    /**
     * 바카라 베팅 없는경기 모두 삭제처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/lotusBaccarat/delete", method = RequestMethod.POST)
    public AjaxResult delete() {
        return lotusBaccaratService.deleteAllGame();
    }
}
