package spoon.gameZone.binanceCoin3;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface BinanceCoin3Repository extends JpaRepository<BinanceCoin3, Long>, QueryDslPredicateExecutor<BinanceCoin3> {

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE BinanceCoin3 o SET o.odds = :odds WHERE o.gameDate > CURRENT_TIMESTAMP")
    void updateOdds(@Param("odds") double[] odds);

    @Modifying(clearAutomatically = true)
    @Query(value = "DELETE FROM BinanceCoin3 o WHERE o.gameDate < :gameDate")
    void deleteGame(@Param("gameDate") Date gameDate);
}
